#include <jni.h>
#include <string>
#include <iostream>
#include "def.h"
#include "client-id.h"

//class EgatConfig {
//protected:
//    const std::string clientId = "egatId";
//    const std::string clientSecret = "egatSecret" ;
//};

class MEAConfig {
protected:
    const std::string clientId = "meaAndroid";
    const std::string clientSecret = "meaSecret";
};

class TOPConfig {
protected:
    const std::string clientId = "443bfdc910b5442ea3f25cadfe75b0f9";
    const std::string clientSecret = "xdwNWbRxvLvTAj9sSyFUe6uOb9rJaWMIkx3YNR56iUE=";
};
class NHKConfig {
protected:
    ////// prod /////
    const std::string clientId = "ecc5b9e82b7e4e6bb6b0c41fdf3dddea";
    const std::string clientSecret = "tYQtolHklDzGHYtZrrWQ9iC5LxHGOwnSbXcbw97TZTw=";
    ////// UAT /////
//    const std::string clientId = "2cb7dce0670e4de89219437894a618c8";
//    const std::string clientSecret = "66LvDkLy4H/MTI2eLdSYtWMYlq7SyCZ+dzzL7lS5H5E=";

};
class AppConfig : NHKConfig {
private:
    bool validSignAPK = true;
    const std::string apiCipherAlgorithm = "RSA/None/OAEPWITHSHA-256ANDMGF1PADDING";
    const std::string apiEncryptSecret = "IN4mYmTnwH3q";
    const std::string localCipherAlgorithm = "AES/GCM/NoPadding";
    const std::string localEncryptAlgorithm = "AES";
    const std::string hmacAlgorithm = "HmacSHA256";
    const int encryptKeySize = 32;
    const int encryptSecretSize = 16;
    // Public KEY for encrypt PIN No.
    const std::string apiPublicKEY = "-----BEGIN PUBLIC KEY-----\n"
                                     "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwIGUN/ODgDuiPZ4Hypy4\n"
                                     "Y5dYuiErqDsx8TKTic/sdlJd9RuIFfXwGMVyY9irL4sJxbz/yAMR6hzRbHOYMh5/\n"
                                     "ZpBTUMvufG3DSxgzFelaPevFPwUFyCAnX58x/tcpvfZeFfHTYK6w7lPmvjEj3KeI\n"
                                     "/yf9t5xZ2kN6krmWe/owY7cbKp8P7w1E2Ji9OHfbS5utNJkOTjlUolcvK0W84fgl\n"
                                     "DlCOlrj7vygEZgsz7F8dF0erlUbWkdQ9pjCL+b2tabD3ggbzVMaER3inS343xdcL\n"
                                     "gQ27XOAy6dbNCAmd+M26l/y2wM+XTSWXJB97D4eH5ZiWwQhgkrZrz1D1o43Hzv5e\n"
                                     "/QIDAQAB\n"
                                     "-----END PUBLIC KEY-----";

public:
    void init(JNIEnv *env, jobject context) {
        // context
        jclass native_context = env->GetObjectClass(context);

        // context.getPackageManager()
        jmethodID methodID_func = env->GetMethodID(native_context, "getPackageManager",
                                                   "()Landroid/content/pm/PackageManager;");
        jobject package_manager = env->CallObjectMethod(context, methodID_func);
        jclass pm_clazz = env->GetObjectClass(package_manager);

        //packageManager.getPackageInfo()
        jmethodID methodId_pm = env->GetMethodID(pm_clazz, "getPackageInfo",
                                                 "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");

        //context.getPackageName()
        jmethodID methodID_packagename = env->GetMethodID(native_context, "getPackageName",
                                                          "()Ljava/lang/String;");
        auto name_str = static_cast<jstring>(env->CallObjectMethod(context, methodID_packagename));
        jobject package_info = env->CallObjectMethod(package_manager, methodId_pm, name_str, 64);
        jclass pi_clazz = env->GetObjectClass(package_info);

        //packageInfo.signatures
        jfieldID fieldID_signatures = env->GetFieldID(pi_clazz, "signatures",
                                                      "[Landroid/content/pm/Signature;");
        jobject signatur = env->GetObjectField(package_info, fieldID_signatures);
        auto signatures = reinterpret_cast<jobjectArray>(signatur);

        //signatures[0]
        jobject signature = env->GetObjectArrayElement(signatures, 0);
        jclass s_clazz = env->GetObjectClass(signature);

        //signatures[0].toCharString()
        jmethodID methodId_ts = env->GetMethodID(s_clazz, "toCharsString", "()Ljava/lang/String;");
        jobject ts = env->CallObjectMethod(signature, methodId_ts);

        std::string key = "308201dd30820146020101300d06092a864886f70d010105050030373116301406035504030c0d416e64726f69642044656275673110300e060355040a0c07416e64726f6964310b3009060355040613025553301e170d3138303531353039353831395a170d3438303530373039353831395a30373116301406035504030c0d416e64726f69642044656275673110300e060355040a0c07416e64726f6964310b300906035504061302555330819f300d06092a864886f70d010101050003818d0030818902818100928532db4a408bc022f37fbac9efcb9758515bbb205d473dd79bd10750aa847b129c8bfa5db73a4142da996c34d81552b59d859912330f52b766a95c549172cdbb4ef52bab08b1aab7a056d4d556bbdcc14c7bd37364f7db9125247116e66a94e8ec57c399657d7c9afed39a599efc104bb54187c32d85af362c127edfe3058b0203010001300d06092a864886f70d0101050500038181004dd88520639c677af24e5236cae5609f4256950c3fc6f0b4d49874fab001c2f7c81fc0bbb97e3d046f462b9fc730369dd3e091d07c1c333d4c82358f3f239aa1860cbff86ee21a8063c2a4023391bcb2bce235c42f334a8b45c3f1c53529d967d2af5294c43b6e3fbc68ec536a3871170b5bc11a597bcd0de24f2baaa4036a0c";

        jstring signKEY = env->NewStringUTF(key.c_str());
        auto signAPK = reinterpret_cast<jstring>(ts);

        jclass cls = env->GetObjectClass(signAPK);
        jmethodID mID = env->GetMethodID(cls, "equals", "(Ljava/lang/Object;)Z");
//        validSignAPK = env->CallBooleanMethod(signAPK, mID, signKEY);

        jclass buildConfigClazz = env->FindClass("com/krungsri/coop/BuildConfig");
        jfieldID isDebugId = env->GetStaticFieldID(buildConfigClazz, "DEBUG", "Z");
        jboolean isDebug = env->GetStaticBooleanField(buildConfigClazz, isDebugId);
//        if (!isDebug && validSignAPK) {
        if (!isDebug) {
            jclass commonUtils = env->FindClass(
                    "com/google/firebase/crashlytics/internal/common/CommonUtils");

            jmethodID checkRoot = env->GetStaticMethodID(commonUtils, "isRooted",
                                                         "(Landroid/content/Context;)Z");
            jboolean isRooted = env->CallStaticBooleanMethod(commonUtils, checkRoot, context);

            if (!isRooted) {
                jclass rootBeerClazz = env->FindClass("com/scottyab/rootbeer/RootBeer");
                jmethodID rootBeerCon = env->GetMethodID(rootBeerClazz, "<init>",
                                                         "(Landroid/content/Context;)V");
                checkRoot = env->GetMethodID(rootBeerClazz, "isRooted", "()Z");
                jobject rootBeer = env->NewObject(rootBeerClazz, rootBeerCon, context);
                isRooted = env->CallBooleanMethod(rootBeer, checkRoot);
            }
            validSignAPK = !isRooted;
        }
    }

    jstring getStaticStringValue(JNIEnv *env, const char *clazz, const char *field) const {
        jclass buildClazz = env->FindClass(clazz);
        jfieldID jfieldId = env->GetStaticFieldID(buildClazz, field,
                                                  "Ljava/lang/String;");
        return (jstring) env->GetStaticObjectField(buildClazz, jfieldId);
    }

    bool isValidSignAPK() const {
        return validSignAPK;
    }

    std::string getClientId() const {
        return !validSignAPK ? "" : clientId;
    }

    std::string getClientSecret() const {
        return !validSignAPK ? "" : clientSecret;
    }

    std::string getApiCipherAlgorithm() const {
        return !validSignAPK ? "" : apiCipherAlgorithm;
    }

    std::string getApiPublicKEY() const {
        return !validSignAPK ? "" : apiPublicKEY;
    }

    std::string getHmacAlgorithm() {
        return !validSignAPK ? "" : hmacAlgorithm;
    }

    std::string getLocalCipherAlgorithm() {
        return !validSignAPK ? "" : localCipherAlgorithm;
    }

    std::string getLocalEncryptAlgorithm() {
        return !validSignAPK ? "" : localEncryptAlgorithm;
    }

    jbyteArray getLocalEncryptKey(JNIEnv *env) const {

        if (!validSignAPK) {
            return env->NewByteArray(encryptKeySize);
        }
        //265 Bits = 32 Chars - dateTimeKeySize
        //yyyyMMddhhmm = size 12
//        jint dateTimeKeySize = 12;
//        jbyteArray dateTimeKey = env->NewByteArray(dateTimeKeySize);
//        jclass dateHelper = env->FindClass("com/krungsri/coop/helper/DateHelper");
//        jmethodID getCurrentDateTime = env->GetStaticMethodID(dateHelper, "getModDateTime",
//                                                              "()Ljava/lang/String;");
//        auto dateTime = static_cast<jstring>(env->CallStaticObjectMethod(dateHelper,
//                                                                         getCurrentDateTime));
//        const char *nativeString = env->GetStringUTFChars(dateTime, 0);
//        env->SetByteArrayRegion(dateTimeKey, 0, dateTimeKeySize, (jbyte *) nativeString);


        jclass securityHelper = env->FindClass("com/krungsri/coop/helper/SecurityHelper");
        jmethodID randomByteID = env->GetStaticMethodID(securityHelper, "random",
                                                        "(I)[B");
        jbyteArray randomByte = static_cast<jbyteArray>(env->CallStaticObjectMethod(securityHelper,
                                                                                    randomByteID,
                                                                                    encryptKeySize));
        return randomByte;
//        jclass byteBuffer = env->FindClass("java/nio/ByteBuffer");
//        jmethodID allocateId = env->GetStaticMethodID(byteBuffer, "allocate","(I)Ljava/nio/ByteBuffer;");
//        jobject allocateKeySize = env->CallStaticObjectMethod(byteBuffer, allocateId,encryptKeySize);

//        jmethodID putId = env->GetMethodID(byteBuffer, "put", "([B)Ljava/nio/ByteBuffer;");


//        jobject encryptKeyBuffer = env->CallObjectMethod(allocateKeySize, putId, dateTimeKey);
//
//        encryptKeyBuffer = env->CallObjectMethod(encryptKeyBuffer, putId, randomByte);

//        jmethodID arrayId = env->GetMethodID(byteBuffer, "array", "()[B");

//        jbyteArray encryptKey = static_cast<jbyteArray>(env->CallObjectMethod(encryptKeyBuffer,
//                                                                              arrayId));
//        return encryptKey;
    }

    jbyteArray getLocalEncryptSecret(JNIEnv *env) const {
        jclass securityHelper = env->FindClass("com/krungsri/coop/helper/SecurityHelper");
        jmethodID randomByteID = env->GetStaticMethodID(securityHelper, "random",
                                                        "(I)[B");
        return static_cast<jbyteArray>(env->CallStaticObjectMethod(securityHelper,
                                                                   randomByteID,
                                                                   encryptSecretSize));
    }

    jbyteArray getApiEncryptSecret(JNIEnv *env) const {
        jbyteArray arr = env->NewByteArray(apiEncryptSecret.length());
        env->SetByteArrayRegion(arr, 0, apiEncryptSecret.length(),
                                (jbyte *) apiEncryptSecret.c_str());
        return arr;
    }

    jint getLocalEncryptKeySize() {
        return encryptKeySize;
    }

    jint getLocalEncryptSecretSize() {
        return encryptSecretSize;
    }
};

static AppConfig appConfig = AppConfig();

extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_CoopApp_initCoopLib(
        JNIEnv *env,
        jobject context) {

    appConfig.init(env, context);

    std::string result =
            appConfig.isValidSignAPK() ? "Welcome to CWL Smart Platform!!" : "I see the cat.";
    return env->NewStringUTF(result.c_str());
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_isRooted(
        JNIEnv *env,
        jobject) {
    return !appConfig.isValidSignAPK();
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getApiClientId(
        JNIEnv *env,
        jobject) {
    std::string key = appConfig.getClientId();
    return env->NewStringUTF(key.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getApiClientSecret(
        JNIEnv *env,
        jobject) {
    std::string key = appConfig.getClientSecret();
    return env->NewStringUTF(key.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getApiCipherAlgorithm(
        JNIEnv *env,
        jobject) {
    std::string key = appConfig.getApiCipherAlgorithm();
    return env->NewStringUTF(key.c_str());
}



extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getApiPublicKey(
        JNIEnv *env,
        jobject) {
    std::string key = appConfig.getApiPublicKEY();
    return env->NewStringUTF(key.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getHmacAlgorithm(
        JNIEnv *env,
        jobject) {
    std::string key = appConfig.getHmacAlgorithm();
    return env->NewStringUTF(key.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getLocalCipherAlgorithm(
        JNIEnv *env,
        jobject) {
    std::string key = appConfig.getLocalCipherAlgorithm();
    return env->NewStringUTF(key.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getLocalEncryptAlgorithm(
        JNIEnv *env,
        jobject) {
    std::string key = appConfig.getLocalEncryptAlgorithm();
    return env->NewStringUTF(key.c_str());
}

extern "C" JNIEXPORT jbyteArray JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getLocalEncryptKey(
        JNIEnv *env, jobject) {
    return appConfig.getLocalEncryptKey(env);
}

extern "C" JNIEXPORT jbyteArray JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getLocalEncryptSecret(
        JNIEnv *env, jobject) {
    return appConfig.getLocalEncryptSecret(env);
}

extern "C" JNIEXPORT jbyteArray JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getApiEncryptSecret(
        JNIEnv *env, jobject) {
    return appConfig.getApiEncryptSecret(env);
}

extern "C" JNIEXPORT jint JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getLocalEncryptKeySize(
        JNIEnv *env, jobject) {
    return appConfig.getLocalEncryptKeySize();
}

extern "C" JNIEXPORT jint JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getLocalEncryptSecretSize(
        JNIEnv *env, jobject) {
    return appConfig.getLocalEncryptSecretSize();
}


extern "C" JNIEXPORT jstring JNICALL
Java_com_krungsri_coop_helper_SecurityHelper_getSSHCert(
        JNIEnv *env, jobject) {
    //////////////PROD////////////////
    std::string key =                     "-----BEGIN CERTIFICATE-----\n"
                                          "MIIHSDCCBjCgAwIBAgIQBHhS83U/v7nViJwE781xYTANBgkqhkiG9w0BAQsFADBPMQswCQYDVQQG\n"
                                          "EwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMSkwJwYDVQQDEyBEaWdpQ2VydCBUTFMgUlNBIFNI\n"
                                          "QTI1NiAyMDIwIENBMTAeFw0yMTAxMTMwMDAwMDBaFw0yMjAxMTkyMzU5NTlaMGwxCzAJBgNVBAYT\n"
                                          "AlRIMRAwDgYDVQQIEwdCYW5na29rMS8wLQYDVQQKEyZCQU5LIE9GIEFZVURIWUEgUFVCTElDIENP\n"
                                          "TVBBTlkgTElNSVRFRDEaMBgGA1UEAxMRY29vcC5rcnVuZ3NyaS5jb20wggIiMA0GCSqGSIb3DQEB\n"
                                          "AQUAA4ICDwAwggIKAoICAQC+lzbsi5AGnVojqmkNvtXtSfYaC4jfObBj1ySbLCEYCUrCa5C4K3HW\n"
                                          "RUb/zIngURQAo6Sljwl7nQfFXlKzQEPs0SfOtwuPsaryeUpguIf3AehDBUw/A9Qq3JeVmW18IDn7\n"
                                          "Pr9NcPG0TDm1HFw4NZwB/CG0R9tYB1Osr1omGkESE2q4IlGoB0iynjdNjO3F5ayr1xVH9FtZmAID\n"
                                          "HkkmJKj3fwZUpFGzm1ujHdJPPcRJVC2CHzD/+9wWzPbR+MhCvin93w8j8wX7XC26M5lOV0oVLsMu\n"
                                          "cJ33bfBHGyN2amX64ecET9Vs0D+am3b6h8Sgr32S97UKCeOFPsViDpRD00pE4PFVfK/nMYHyYV3s\n"
                                          "eNmhy3Mhnf+hFXsFj3wuoft0MIwcczeFPvuivlD2zkRZ87nXiNV5xTwbTFaB5HJSI1LddIWfu2nV\n"
                                          "yxqzDJuQObDkZtxQ9EZln9u6SDs3FPLtpADN3Xhtdb87bEKzHskIMLCrgVUWimgtikK2oeRyjyqM\n"
                                          "NVjSl8b+ZbHNi+4fkUlAHNtyzCnbT/B6LC+F55KUD4YT6hWBfCLy+k9+Gg7rwO0e+1CSyoXtE5Tp\n"
                                          "IiJg/UoU7lwxz1NPR2E7qmcLOk8IskKybQfmK93M6yXQIizTqqADJrZPUthaXZSfo4gtz0K/SMVI\n"
                                          "8WqXJ/Gtnr9Azoqf9El5jQIDAQABo4IDATCCAv0wHwYDVR0jBBgwFoAUt2ui6qiqhIx56rTaD5iy\n"
                                          "xZV2ufQwHQYDVR0OBBYEFEPVb7X/PEXAjVVt1oRCAiSQlAXSMBwGA1UdEQQVMBOCEWNvb3Aua3J1\n"
                                          "bmdzcmkuY29tMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIw\n"
                                          "gYsGA1UdHwSBgzCBgDA+oDygOoY4aHR0cDovL2NybDMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0VExT\n"
                                          "UlNBU0hBMjU2MjAyMENBMS5jcmwwPqA8oDqGOGh0dHA6Ly9jcmw0LmRpZ2ljZXJ0LmNvbS9EaWdp\n"
                                          "Q2VydFRMU1JTQVNIQTI1NjIwMjBDQTEuY3JsMEsGA1UdIAREMEIwNgYJYIZIAYb9bAEBMCkwJwYI\n"
                                          "KwYBBQUHAgEWG2h0dHA6Ly93d3cuZGlnaWNlcnQuY29tL0NQUzAIBgZngQwBAgIwfQYIKwYBBQUH\n"
                                          "AQEEcTBvMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdpY2VydC5jb20wRwYIKwYBBQUHMAKG\n"
                                          "O2h0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydFRMU1JTQVNIQTI1NjIwMjBDQTEu\n"
                                          "Y3J0MAwGA1UdEwEB/wQCMAAwggEEBgorBgEEAdZ5AgQCBIH1BIHyAPAAdgApeb7wnjk5IfBWc59j\n"
                                          "pXflvld9nGAK+PlNXSZcJV3HhAAAAXb5qi/qAAAEAwBHMEUCIGRtw7qA5u2aL9FYUj7bkpt+/VKa\n"
                                          "lVqvZxo2lFZBtU0VAiEAwz2rc+LYzAHbjdnkigNH8RB8wxbxKuuq+3u1k+Vi+XAAdgAiRUUHWVUk\n"
                                          "VpY/oS/x922G4CMmY63AS39dxoNcbuIPAgAAAXb5qjBAAAAEAwBHMEUCIQCwIojOK7xoKl9t66k+\n"
                                          "I1l7YDgdhybHYCXGP56Px10m6gIgC4xsP2B669PpdNoeyDeSEnSNC/a2Kz8wZqmlvNhoNacwDQYJ\n"
                                          "KoZIhvcNAQELBQADggEBAGConVvfzuHoOsYOyvXrZnoWOAp97dWtc1WUNBXyTcSTrQO9oDLboFrX\n"
                                          "q33JqJnwWWpM9COaU4saHMkqScM/eWWS5xw1n811saXiY+0f+5bHHDE6yCcFfKOmOOvBo6Kz61Et\n"
                                          "kSAVC1DLyX0fYih1O+9E76IslJ0Z2jnFNnfNXkWAz+TT40lCiIjk0M42ODWC/cVohyBRs6V/iFOj\n"
                                          "657KPbXuf9Vuc9fS4CgckFp0TIhj4m5iJZRgSjROT4CxFIHbgvB8MXVI4/8+pcDABH+Ozr+HAgsf\n"
                                          "X8eu8oJgXGGVuBBKj7eKR7lruWI5xhLA5IpL4MuHq5TADvAusfzSlJoXnOE=\n"
                                          "-----END CERTIFICATE-----\n";

    //////////UAT/////////
//    std::string key =                    "-----BEGIN CERTIFICATE-----\n"
//                                         "MIIHLTCCBhWgAwIBAgIQCMs87DAtXxlOtwN1IIqQfjANBgkqhkiG9w0BAQsFADBN\n"
//                                         "MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMScwJQYDVQQDEx5E\n"
//                                         "aWdpQ2VydCBTSEEyIFNlY3VyZSBTZXJ2ZXIgQ0EwHhcNMjAxMDAxMDAwMDAwWhcN\n"
//                                         "MjExMDA2MTIwMDAwWjBwMQswCQYDVQQGEwJUSDEQMA4GA1UECBMHQmFuZ2tvazEv\n"
//                                         "MC0GA1UEChMmQkFOSyBPRiBBWVVESFlBIFBVQkxJQyBDT01QQU5ZIExJTUlURUQx\n"
//                                         "HjAcBgNVBAMTFWNvb3AtdWF0LmtydW5nc3JpLmNvbTCCAiIwDQYJKoZIhvcNAQEB\n"
//                                         "BQADggIPADCCAgoCggIBAK2fbi0nTLQ4chQJVl5eQZu1ufyu0SMkW3YF8jOupiqa\n"
//                                         "LLda/P+TuL5A3PcgbgUege7UCwlVUDrB7yfiFkquMPLcSqdR+dLCvVZM4U4crWwo\n"
//                                         "fZjoSp7om5oesKUJIh55rvag0lVVtON00OLENT+WG4h+DzdEYxVxoMyqnSJb6j4k\n"
//                                         "LHQxIyygp/7HMSWgpbEdUwP+O6GaCzZKkhXqOFlOYL+Gl/UgnTXVW4f6KJwxzuPz\n"
//                                         "sTcMpoWmKq1xgCOartwWIZywiYEh1B7IpPV9PgjwZpFvbLiE9Aj0HuGWU2RcelqJ\n"
//                                         "tOL3Q7HXM3p0PKQJRXUEt/bOWf+LZX7BgydTAIQKxbn7Gz0zW07vPz/ir+KeYUcH\n"
//                                         "0wX0JwWKjsILqg5dpasu+fBIRyCyyOusOVHC2kW/Qc+h7ufEKvJ28cjoU9R98Yde\n"
//                                         "P/inQx4nJw6xwY323CDyZFz0qQlZ9VlXhZWdh2K2beWo/mivSnIF/BF4HImaQMv9\n"
//                                         "wDhN2OwixIVIhAf/vvuSaBfKTrpx4f3nwVJcaFOCmU9NQTsLZR0TyMLDZdZbSuzO\n"
//                                         "hesc+b406pIbmsPau3OgDpsfgHKol4XkTnFbQmmQZwpvLGbjzzWzxZFDV5MPpwax\n"
//                                         "Cnp2kOhp3l2wDt4BNoDVRtdya7XWc9SC6TtX9JCxEn6zc8u72vbm3S6JNtPul0+t\n"
//                                         "AgMBAAGjggLkMIIC4DAfBgNVHSMEGDAWgBQPgGEcgjFh1S8o541GOLQs4cbZ4jAd\n"
//                                         "BgNVHQ4EFgQUg1sg6ZcMKlr2B+UjAE5Kla74CbYwIAYDVR0RBBkwF4IVY29vcC11\n"
//                                         "YXQua3J1bmdzcmkuY29tMA4GA1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEF\n"
//                                         "BQcDAQYIKwYBBQUHAwIwawYDVR0fBGQwYjAvoC2gK4YpaHR0cDovL2NybDMuZGln\n"
//                                         "aWNlcnQuY29tL3NzY2Etc2hhMi1nNy5jcmwwL6AtoCuGKWh0dHA6Ly9jcmw0LmRp\n"
//                                         "Z2ljZXJ0LmNvbS9zc2NhLXNoYTItZzcuY3JsMEwGA1UdIARFMEMwNwYJYIZIAYb9\n"
//                                         "bAEBMCowKAYIKwYBBQUHAgEWHGh0dHBzOi8vd3d3LmRpZ2ljZXJ0LmNvbS9DUFMw\n"
//                                         "CAYGZ4EMAQICMHwGCCsGAQUFBwEBBHAwbjAkBggrBgEFBQcwAYYYaHR0cDovL29j\n"
//                                         "c3AuZGlnaWNlcnQuY29tMEYGCCsGAQUFBzAChjpodHRwOi8vY2FjZXJ0cy5kaWdp\n"
//                                         "Y2VydC5jb20vRGlnaUNlcnRTSEEyU2VjdXJlU2VydmVyQ0EuY3J0MAwGA1UdEwEB\n"
//                                         "/wQCMAAwggEEBgorBgEEAdZ5AgQCBIH1BIHyAPAAdgD2XJQv0XcwIhRUGAgwlFaO\n"
//                                         "400TGTO/3wwvIAvMTvFk4wAAAXTjXCD5AAAEAwBHMEUCIQCCcALm5bI+dm+XQff6\n"
//                                         "h5QLFe2o6/pj5tJRjR6DzwsovgIgbkw0/h/YMrcHMsVp0fYTwEDYglsXcGlWth5O\n"
//                                         "TWkXw5UAdgBc3EOS/uarRUSxXprUVuYQN/vV+kfcoXOUsl7m9scOygAAAXTjXCFH\n"
//                                         "AAAEAwBHMEUCIQD/hXNMnNtEp8V1w6F97jIFi8i7miIrMLA7ro/Z0d/P5gIgGSNQ\n"
//                                         "rTyCu81a/jGfnS4bzu+AmzKXDMKm6L4O6m8fUjEwDQYJKoZIhvcNAQELBQADggEB\n"
//                                         "AIHhm8Rn9J5p1UQ9nnyAAVFSE0GgyxdosyNnyDib17jMmT4J4Chd/IhI9J9J2B3j\n"
//                                         "sv/pqYBAtidthUaDthqvFki7RiZhNAGNKawavIrjaIPs4onTbZE0N6mvRNUui5lx\n"
//                                         "EIA8Ervnup+7gBSPCr4qus3cXYUVipgt68ANEMWiKqZKLr3Dg6MWFQTs9egIMgSd\n"
//                                         "c40tIdP855J0WW2h//htM2rxPzOqEms5zB2S5eWKWyE/1RcsajinOvxCTmhbQPNB\n"
//                                         "EyZt6QMNd8sISC2roEgiF9DD7wLDrUfrhefQpN+1m95Lf47aukkOS+Z5GVOnuAZ0\n"
//                                         "drf6IMJFr9ttmHfSI0rNze4=\n"
//                                         "-----END CERTIFICATE-----";

    if (!appConfig.isValidSignAPK()) {
        key = "I see the cat.";
    }
    return env->NewStringUTF(key.c_str());

}