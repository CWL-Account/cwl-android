<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="com.krungsri.coop.BuildConfig" />

        <import type="com.krungsri.coop.model.policy.PolicyType" />

        <variable
            name="fragment"
            type="com.krungsri.coop.ui.component.setting.SettingFragment" />

        <variable
            name="vm"
            type="com.krungsri.coop.ui.component.setting.SettingViewModel" />
    </data>


    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <include
            android:id="@+id/header"
            layout="@layout/layout_toolbar"
            app:navTitle="@{@string/label_setting_title}"
            app:showNavBack="@{true}" />

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:fillViewport="true"
            app:layout_behavior="@string/appbar_scrolling_view_behavior">

            <androidx.appcompat.widget.LinearLayoutCompat
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:orientation="vertical"
                android:paddingBottom="@dimen/margin32">

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/textSetting"
                    style="@style/settingViewTitle"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:text="@string/label_setting_secure_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/textChangePin"
                    style="@style/settingViewButton"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:onClick="@{()->fragment.onChangePinClick()}"
                    android:text="@string/label_setting_change_password"
                    app:drawableRightCompat="@drawable/ic_next" />

                <RelativeLayout
                    android:id="@+id/layoutBiometric"
                    style="@style/settingViewButton.Lite"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <androidx.appcompat.widget.AppCompatTextView
                        android:id="@+id/textBiometric"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_centerVertical="true"
                        android:layout_toStartOf="@id/buttonBiometric"
                        android:text="@string/label_setting_scan_biometric" />

                    <androidx.appcompat.widget.SwitchCompat
                        android:id="@+id/buttonBiometric"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentEnd="true"
                        android:layout_centerInParent="true"
                        android:checked="@={vm.biometricSettingValue}"
                        android:onClick="@{()->fragment.onBiometricSettingClick()}"
                        android:thumb="@drawable/switch_thumb"
                        app:track="@drawable/switch_track"
                        tools:checked="false" />
                </RelativeLayout>

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/textAccountTitle"
                    style="@style/settingViewTitle"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/margin32"
                    android:text="@string/label_setting_account_title" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/textAccountManagement"
                    style="@style/settingViewButton"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:text="@string/label_setting_account_manage_title"
                    android:textColor="@color/gray"
                    app:drawableRightCompat="@drawable/ic_next" />

                <FrameLayout
                    android:id="@+id/layoutNoAccountSetting"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <androidx.appcompat.widget.AppCompatTextView
                        android:id="@+id/textAccountNoSetting"
                        style="@style/settingViewButton.Lite"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@string/label_setting_account_no" />

                    <androidx.appcompat.widget.SwitchCompat
                        android:id="@+id/buttonAccountNoSetting"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_gravity="center_vertical|end"
                        android:layout_marginEnd="@dimen/margin16"
                        android:onClick="@{()->fragment.onAccountMaskingClick()}"
                        android:thumb="@drawable/switch_thumb"
                        app:track="@drawable/switch_track"
                        tools:checked="false" />
                </FrameLayout>

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/textOther"
                    style="@style/settingViewTitle"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="@dimen/margin32"
                    android:text="@string/label_setting_other" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/textHelp"
                    style="@style/settingViewButton"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:onClick="@{()->fragment.onHelpClick()}"
                    android:text="@string/label_setting_help"
                    app:drawableRightCompat="@drawable/ic_next" />

                <FrameLayout
                    android:id="@+id/layoutVersion"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <androidx.appcompat.widget.AppCompatTextView
                        android:id="@+id/textVersionTitle"
                        style="@style/settingViewButton.Lite"
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:gravity="center_vertical"
                        android:text="@string/label_setting_version" />

                    <androidx.appcompat.widget.AppCompatTextView
                        android:id="@+id/textVersionName"
                        style="@style/TextMedium"
                        android:layout_width="wrap_content"
                        android:layout_height="match_parent"
                        android:layout_gravity="center_vertical|end"
                        android:layout_marginEnd="@dimen/margin16"
                        android:gravity="center_vertical"
                        android:text="@{BuildConfig.VERSION_NAME}"
                        tools:text="1.0" />
                </FrameLayout>

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/textTc"
                    style="@style/settingViewButton"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:onClick="@{()->vm.getMemberPolicy(PolicyType.COOP_TC)}"
                    android:text="@string/label_setting_tc"
                    app:drawableRightCompat="@drawable/ic_next" />

                <androidx.appcompat.widget.AppCompatTextView
                    android:id="@+id/textConsent"
                    style="@style/settingViewButton"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:onClick="@{()->vm.getMemberPolicy(PolicyType.COOP_CONSENT)}"
                    android:text="@string/label_setting_consent"
                    app:drawableRightCompat="@drawable/ic_next" />

            </androidx.appcompat.widget.LinearLayoutCompat>
        </androidx.core.widget.NestedScrollView>
    </androidx.coordinatorlayout.widget.CoordinatorLayout>
</layout>