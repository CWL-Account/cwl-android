package com.krungsri.coop.exception

class ApiException(
    val code: String,
    override val message: String
) : RuntimeException()