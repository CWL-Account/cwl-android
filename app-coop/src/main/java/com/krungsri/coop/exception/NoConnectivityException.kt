package com.krungsri.coop.exception

import java.io.IOException

class NoConnectivityException : IOException()