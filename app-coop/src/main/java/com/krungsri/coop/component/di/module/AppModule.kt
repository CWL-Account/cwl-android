package com.krungsri.coop.component.di.module

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.krungsri.coop.BuildConfig
import com.krungsri.coop.CoopApp
import com.krungsri.coop.data.api.*
import com.krungsri.coop.data.session.AnalyticService
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.extension.addSslPinner
import com.krungsri.coop.extension.addSslSocketFactory
import com.krungsri.coop.extension.initConnectionSpec
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import org.threeten.bp.Clock
import org.threeten.bp.ZoneId
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideContext(mainApplication: CoopApp): Context = mainApplication.applicationContext

    @Singleton
    @Provides
    fun provideClock(): Clock = Clock.system(ZoneId.systemDefault())

    @Singleton
    @Provides
    fun provideLocalSession(clock: Clock): LocalSession = LocalSession(clock)

    @Singleton
    @Provides
    fun provideRetrofit(
        context: Context,
        gson: Gson,
        tokenService: TokenService
    ): Retrofit.Builder {

        val builder = OkHttpClient().newBuilder()
            .initConnectionSpec()
            .addInterceptor(ApiInterceptor(context, tokenService))
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addSslPinner(
                BuildConfig.API_ENDPOINT,
                BuildConfig.SSL_PIN_ROOT,
                BuildConfig.SSL_PIN_IMMEDIATE,
                BuildConfig.SSL_PIN_LEAF
            ).addSslSocketFactory(context)

        return Retrofit.Builder()
            .client(builder.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }


    @Singleton
    @Provides
    fun provideTokenAPI(
        context: Context,
        retrofit: Retrofit.Builder,
        tokenService: TokenService
    ): TokenAPI {
        return retrofit
            .baseUrl(BuildConfig.API_ENDPOINT)
            .build().create(TokenAPI::class.java).also {
                tokenService.init(context, it)
            }
    }

    @Singleton
    @Provides
    fun providePolicyAPI(retrofit: Retrofit.Builder): PolicyAPI {
        return retrofit
            .baseUrl(BuildConfig.API_ENDPOINT)
            .build()
            .create(PolicyAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideMemberAPI(retrofit: Retrofit.Builder): MemberAPI {
        return retrofit
            .baseUrl(BuildConfig.API_ENDPOINT)
            .build().create(MemberAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideTransactionAPI(retrofit: Retrofit.Builder): TransactionAPI {
        return retrofit
            .baseUrl(BuildConfig.API_ENDPOINT)
            .build().create(TransactionAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideAccountAPI(retrofit: Retrofit.Builder): AccountAPI {
        return retrofit
            .baseUrl(BuildConfig.API_ENDPOINT)
            .build().create(AccountAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideRestAPI(retrofit: Retrofit.Builder): RestAPI {
        return retrofit
            .baseUrl(BuildConfig.API_ENDPOINT)
            .build().create(RestAPI::class.java)
    }

    @Singleton
    @Provides
    fun provideAnalyticAPI(retrofit: Retrofit.Builder): AnalyticAPI {
        return retrofit
            .baseUrl(BuildConfig.API_ENDPOINT)
            .build().create(AnalyticAPI::class.java)
    }


    @Singleton
    @Provides
    fun provideAnalyticService(api: AnalyticAPI): AnalyticService = AnalyticService(api)


    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            .create()
    }

    @Singleton
    @Provides
    fun provideTokenService(): TokenService = TokenService()


}
