package com.krungsri.coop.component.di

import com.krungsri.coop.CoopApp
import com.krungsri.coop.component.di.module.AppModule
import com.krungsri.coop.component.di.module.MainActivityModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        MainActivityModule::class]
)

interface AppComponent : AndroidInjector<CoopApp> {
    @Component.Factory
    interface Factory : AndroidInjector.Factory<CoopApp>
}
