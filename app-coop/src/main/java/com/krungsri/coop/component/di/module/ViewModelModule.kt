package com.krungsri.coop.component.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krungsri.coop.component.di.ViewModelKey
import com.krungsri.coop.component.viewmodel.ViewModelFactory
import com.krungsri.coop.ui.component.home.HomeViewModel
import com.krungsri.coop.ui.component.login.LoginViewModel
import com.krungsri.coop.ui.component.member.change.MemberChangePinViewModel
import com.krungsri.coop.ui.component.member.confirm.MemberConfirmViewModel
import com.krungsri.coop.ui.component.member.data.MemberDataViewModel
import com.krungsri.coop.ui.component.member.data.info.MemberDataInfoViewModel
import com.krungsri.coop.ui.component.member.data.slip.MemberDataSlipViewModel
import com.krungsri.coop.ui.component.member.landing.MemberLandingViewModel
import com.krungsri.coop.ui.component.member.pin.MemberPinViewModel
import com.krungsri.coop.ui.component.member.validation.MemberVerificationViewModel
import com.krungsri.coop.ui.component.news.NewsViewModel
import com.krungsri.coop.ui.component.otp.OTPVerificationViewModel
import com.krungsri.coop.ui.component.policy.PolicyViewModel
import com.krungsri.coop.ui.component.setting.SettingViewModel
import com.krungsri.coop.ui.component.setting.account.AccountAddViewModel
import com.krungsri.coop.ui.component.setting.account.AccountManageViewModel
import com.krungsri.coop.ui.component.setting.help.HelpViewModel
import com.krungsri.coop.ui.component.setting.policy.MemberPolicyViewModel
import com.krungsri.coop.ui.component.splash.SplashScreenViewModel
import com.krungsri.coop.ui.component.transaction.confirm.TrxConfirmViewModel
import com.krungsri.coop.ui.component.transaction.history.TrxHistoryViewModel
import com.krungsri.coop.ui.component.transaction.inquiry.TrxInquiryViewModel
import com.krungsri.coop.ui.component.transaction.payment.BillPaymentViewModel
import com.krungsri.coop.ui.component.transaction.pin.TrxPinViewModel
import com.krungsri.coop.ui.component.transaction.slip.TrxSlipViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SplashScreenViewModel::class)
    abstract fun bindSplashScreenViewModel(screenViewModel: SplashScreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PolicyViewModel::class)
    abstract fun bindPolicyViewModel(viewModel: PolicyViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(MemberLandingViewModel::class)
    abstract fun bindMemberRegisterViewModel(viewModel: MemberLandingViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(MemberVerificationViewModel::class)
    abstract fun bindMemberVerificationViewModel(viewModel: MemberVerificationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MemberConfirmViewModel::class)
    abstract fun bindMemberConfirmViewModel(viewModel: MemberConfirmViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MemberChangePinViewModel::class)
    abstract fun bindMemberChangePinViewModel(viewModel: MemberChangePinViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(OTPVerificationViewModel::class)
    abstract fun bindOTPVerificationViewModel(viewModel: OTPVerificationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MemberPinViewModel::class)
    abstract fun bindMemberPinViewModel(viewModel: MemberPinViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MemberDataViewModel::class)
    abstract fun bindMemberDataViewModel(viewModel: MemberDataViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MemberDataInfoViewModel::class)
    abstract fun bindMemberInfoViewModel(viewModelData: MemberDataInfoViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(MemberDataSlipViewModel::class)
    abstract fun bindMemberSlipViewModel(viewModelData: MemberDataSlipViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewsViewModel::class)
    abstract fun bindNewsViewModel(viewModel: NewsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BillPaymentViewModel::class)
    abstract fun bindBillPaymentViewModel(viewModel: BillPaymentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingViewModel::class)
    abstract fun bindSettingViewModel(viewModel: SettingViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(AccountManageViewModel::class)
    abstract fun bindAccountManageViewModel(viewModel: AccountManageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountAddViewModel::class)
    abstract fun bindAccountAddViewModel(viewModel: AccountAddViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MemberPolicyViewModel::class)
    abstract fun bindMemberPolicyViewModel(viewModel: MemberPolicyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HelpViewModel::class)
    abstract fun bindHelpViewModel(viewModel: HelpViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TrxInquiryViewModel::class)
    abstract fun bindTrxInquiryViewModel(viewModel: TrxInquiryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrxPinViewModel::class)
    abstract fun bindTrxPinViewModel(viewModel: TrxPinViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(TrxConfirmViewModel::class)
    abstract fun bindTrxConfirmViewModel(viewModel: TrxConfirmViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrxSlipViewModel::class)
    abstract fun bindTrxSlipViewModel(viewModel: TrxSlipViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrxHistoryViewModel::class)
    abstract fun bindTrxHistoryViewModel(viewModel: TrxHistoryViewModel): ViewModel

//    @Binds
//    @IntoMap
//    @ViewModelKey(BillPaymentViewModel::class)
//    abstract fun bindBillPaymentViewModel(viewModel: BillPaymentViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
