package com.krungsri.coop.component.di.module

import com.krungsri.coop.ui.component.home.HomeFragment
import com.krungsri.coop.ui.component.login.LoginFragment
import com.krungsri.coop.ui.component.member.change.MemberChangePinFragment
import com.krungsri.coop.ui.component.member.confirm.MemberConfirmFragment
import com.krungsri.coop.ui.component.member.data.MemberDataFragment
import com.krungsri.coop.ui.component.member.data.info.MemberDataInfoFragment
import com.krungsri.coop.ui.component.member.data.slip.MemberDataSlipFragment
import com.krungsri.coop.ui.component.member.landing.MemberLandingFragment
import com.krungsri.coop.ui.component.member.pin.MemberPinConfirmFragment
import com.krungsri.coop.ui.component.member.pin.MemberPinNewFragment
import com.krungsri.coop.ui.component.member.validation.MemberVerificationFragment
import com.krungsri.coop.ui.component.news.NewsFragment
import com.krungsri.coop.ui.component.otp.OTPVerificationFragment
import com.krungsri.coop.ui.component.policy.PolicyFragment
import com.krungsri.coop.ui.component.setting.SettingFragment
import com.krungsri.coop.ui.component.setting.account.AccountAddFragment
import com.krungsri.coop.ui.component.setting.account.AccountManageFragment
import com.krungsri.coop.ui.component.setting.help.HelpFragment
import com.krungsri.coop.ui.component.setting.policy.MemberPolicyFragment
import com.krungsri.coop.ui.component.splash.SplashScreenFragment
import com.krungsri.coop.ui.component.transaction.confirm.TrxConfirmFragment
import com.krungsri.coop.ui.component.transaction.history.TrxHistoryFragment
import com.krungsri.coop.ui.component.transaction.inquiry.TrxInquiryFragment
import com.krungsri.coop.ui.component.transaction.payment.BillPaymentFragment
import com.krungsri.coop.ui.component.transaction.pin.TrxPinFragment
import com.krungsri.coop.ui.component.transaction.slip.TrxSlipFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {


    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashScreenFragment

    @ContributesAndroidInjector
    abstract fun contributePolicyFragment(): PolicyFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberRegisterFragment(): MemberLandingFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberVerificationFragment(): MemberVerificationFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberConfirmFragment(): MemberConfirmFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberChangePinFragment(): MemberChangePinFragment

    @ContributesAndroidInjector
    abstract fun contributeOTPVerificationFragment(): OTPVerificationFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberPinNewFragment(): MemberPinNewFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberDataFragment(): MemberDataFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberInfoFragment(): MemberDataInfoFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberSlipFragment(): MemberDataSlipFragment

    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun contributeNewsFragment(): NewsFragment

    @ContributesAndroidInjector
    abstract fun contributeBillPaymentFragment(): BillPaymentFragment

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberPinConfirmFragment(): MemberPinConfirmFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingFragment(): SettingFragment

    @ContributesAndroidInjector
    abstract fun contributeAccountManageFragment(): AccountManageFragment

    @ContributesAndroidInjector
    abstract fun contributeAccountAddFragment(): AccountAddFragment

    @ContributesAndroidInjector
    abstract fun contributeMemberPolicyFragment(): MemberPolicyFragment

    @ContributesAndroidInjector
    abstract fun contributeHelpFragment(): HelpFragment

    @ContributesAndroidInjector
    abstract fun contributeTransactionInquiryFragment(): TrxInquiryFragment

    @ContributesAndroidInjector
    abstract fun contributeTransactionPinFragment(): TrxPinFragment

    @ContributesAndroidInjector
    abstract fun contributeTransactionSlipFragment(): TrxSlipFragment

    @ContributesAndroidInjector
    abstract fun contributeTransactionHistoryFragment(): TrxHistoryFragment

    @ContributesAndroidInjector
    abstract fun contributeTransactionConfirmFragment(): TrxConfirmFragment

//    @ContributesAndroidInjector
//    abstract fun contributeBillPaymentFragment(): BillPaymentFragment

}
