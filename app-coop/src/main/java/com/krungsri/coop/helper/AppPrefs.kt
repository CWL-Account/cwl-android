package com.krungsri.coop.helper

import android.content.Context
import android.content.SharedPreferences
import com.krungsri.coop.BuildConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object AppPrefs {
    lateinit var encryptedPrefs: SharedPreferences
    lateinit var encryptedStaticPrefs: SharedPreferences
    private const val CHECK_PREFS = "PREFS"

    fun initEncryptedPrefs(context: Context) {
        encryptedPrefs = context.getSharedPreferences(
            "${BuildConfig.APPLICATION_ID}.local_data",
            Context.MODE_PRIVATE
        )
        encryptedStaticPrefs = context.getSharedPreferences(
            "${BuildConfig.APPLICATION_ID}.local_data_s",
            Context.MODE_PRIVATE
        )
        val checkPrefs = getStaticString(CHECK_PREFS);
        if (checkPrefs == null || checkPrefs != BuildConfig.APPLICATION_ID) {
            clearAll()
        }
        setStaticString(CHECK_PREFS, BuildConfig.APPLICATION_ID)
    }

    fun getString(key: String, defaultValue: String? = null): String? {
        val value = encryptedPrefs.getString(key, null)
//        Timber.d("getString = %s : %s", key, value)
        return value?.let {
            SecurityHelper.decryptLocalText(value)
        } ?: defaultValue
    }

    fun hasString(key: String): Boolean {
        return encryptedPrefs.contains(key)
    }

    fun setString(key: String, value: String) {
        GlobalScope.launch(Dispatchers.IO) {
            val saveValue = SecurityHelper.encryptLocalText(value)
//        Timber.d("getString = %s : %s", key, saveValue)
            encryptedPrefs.edit().putString(key, saveValue).apply()
        }
    }

    fun remove(key: String) {
        encryptedPrefs.edit().remove(key).apply()
    }

    fun clearAll() {
        encryptedPrefs.edit().clear().apply()
    }

    fun clearAllData() {
        encryptedStaticPrefs.edit().clear().apply()
        encryptedPrefs.edit().clear().apply()
    }

    fun getStaticString(key: String, defaultValue: String? = null): String? {
        val value = encryptedStaticPrefs.getString(key, defaultValue)
        return value?.let {
            SecurityHelper.decryptLocalText(value)
        } ?: defaultValue
    }

    fun setStaticString(key: String, value: String) {
        GlobalScope.launch(Dispatchers.IO) {
            val saveValue = SecurityHelper.encryptLocalText(value)
            encryptedStaticPrefs.edit().putString(key, saveValue).apply()
        }
    }


}