package com.krungsri.coop.helper

import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.regex.Pattern

object FormatHelper {

    fun formatPhoneNo(phoneNo: String, masked: Boolean = false): String {
        //089-000-0000
        //02-000-0000
        val phone = phoneNo.replace("-", "")
        if (phone.length == 10) {
            return formatMobileNNo(phoneNo, masked)
        } else if (phone.length == 9) {
            val number = if (masked) MaskHelper.maskMobileNo(phoneNo) else phoneNo
            val pattern = Pattern.compile("(.{2})(.{3})(.{4})")
            val matcher = pattern.matcher(number)
            return if (matcher.matches()) {
                "${matcher.group(1)}-${matcher.group(2)}-${matcher.group(3)}"
            } else {
                number
            }
        } else {
            return phoneNo
        }
    }

    fun formatMobileNNo(mobileNo: String, masked: Boolean = false): String {
        val number = if (masked) MaskHelper.maskMobileNo(mobileNo) else mobileNo
        val pattern = Pattern.compile("(.{3})(.{3})(.{4})")
        val matcher = pattern.matcher(number)
        return if (matcher.matches()) {
            "${matcher.group(1)}-${matcher.group(2)}-${matcher.group(3)}"
        } else {
            number
        }
    }

    fun formatAccountNo(accNumber: String, masked: Boolean = false): String {
        val maskedNumber = if (masked) MaskHelper.maskAccountNo(accNumber) else accNumber
        val pattern = Pattern.compile("([0-9,X]{3})([0-9,X]{1})([0-9,X]{5})([0-9,X]+)")
        val matcher = pattern.matcher(maskedNumber)
        return if (matcher.matches()) {
            "${matcher.group(1)}-${matcher.group(2)}-${matcher.group(3)}-${matcher.group(4)}"
        } else {
            maskedNumber
        }
    }

    fun formatCitizenNo(citizenNo: String, masked: Boolean = false): String {
        val id = if (masked) MaskHelper.maskCitizenNo(citizenNo) else citizenNo
        val pattern = Pattern.compile("(.{1})(.{4})(.{5})(.{2})(.{1})")
        val matcher = pattern.matcher(id)
        return if (matcher.matches()) {
            "${matcher.group(1)}-${matcher.group(2)}-${matcher.group(3)}-${matcher.group(4)}-${
                matcher.group(
                    5
                )
            }"
        } else {
            id
        }
    }

    fun formatAmount(amount: BigDecimal?, defaultValue: String = "0.00"): String {
        return amount?.let {
            if (it.toDouble() == 0.toDouble()) {
                "0.00"
            } else DecimalFormat("#,###.00").format(it)
        } ?: defaultValue
    }

}