package com.krungsri.coop.helper

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.res.XmlResourceParser
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import androidx.core.content.FileProvider
import com.krungsri.coop.BuildConfig
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import org.xmlpull.v1.XmlPullParser
import timber.log.Timber
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs


fun Date.diffSec(dateEnd: Date): Long {
    return TimeUnit.SECONDS.convert(abs(dateEnd.time - this.time), TimeUnit.MILLISECONDS)
}

object AppHelper {

    @SuppressLint("HardwareIds")
    fun getDeviceId(context: Context): String {
//        val telMgr = ContextCompat.getSystemService(context, TelephonyManager::class.java)
//        var deviceId = ""
//        if (Build.VERSION.SDK_INT >= 26) {
//            if (telMgr?.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
//                deviceId = telMgr.getMeid();
//            } else if (telMgr?.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
//                deviceId = telMgr.getImei();
//            } else {
//                deviceId = ""; // default!!!
//            }
//        } else {
//            deviceId = telMgr.getDeviceId();
//        }
        val androidID = Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        ).toByteArray()
        return UUID.nameUUIDFromBytes(androidID).toString()
//            val telephonyManager =
//                context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
//            return if (Build.VERSION.SDK_INT >= 26) {
//                when (telephonyManager.phoneType) {
//                    TelephonyManager.PHONE_TYPE_CDMA -> {
//                        telephonyManager.meid
//                    }
//                    TelephonyManager.PHONE_TYPE_GSM -> {
//                        telephonyManager.imei
//                    }
//                    else -> {
//                        Settings.Secure.getString(
//                            context.contentResolver,
//                            Settings.Secure.ANDROID_ID
//                        )
//                    }
//                }
//            } else {
//                telephonyManager.deviceId
//            }
    }

    fun isThaiCitizenNo(citizenNo: String?): Boolean {
        if (citizenNo?.length != 13) return false
        return try {
            val c = citizenNo.toCharArray()
            var sum = 0
            for (i in 0..11) {
                sum += c[i].toString().toInt() * (13 - i)
            }
            (11 - sum % 11) % 10 == c[12].toString().toInt()
        } catch (e: Exception) {
            Timber.e(e)
            false
        }
    }

    fun isMobileNo(number: String): Boolean {

        if (number.length != 10) return false
        val prefix: String = number.substring(0, 2)
        return (prefix == "08" || prefix == "09" || prefix == "01" || prefix == "06")
    }


    fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCapabilities = connectivityManager.activeNetwork ?: return false
        val actNw =
            connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
        return when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }

    fun getPicturesPath(context: Context, fileName: String): Uri? {
        try {
            val xpp: XmlResourceParser = context.resources.getXml(R.xml.provider_paths)
            xpp.next()
            var eventType = xpp.eventType
            var picturesPath = "Pictures/${context.getString(R.string.app_name)}"
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG && "external-path" == xpp.name) {
                    val name = xpp.getAttributeValue(null, "name")
                    val path = xpp.getAttributeValue(null, "path")
                    if (name == AppConstant.PICTURES_PATH && !path.isNullOrEmpty()) {
                        picturesPath = path
                        Timber.d("getPicturesPath 0 = %s", picturesPath)
                        break
                    }
                }
                eventType = xpp.next();
            }
            Timber.d("getPicturesPath 1 = %s", picturesPath)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val resolver = context.contentResolver
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                    put(MediaStore.MediaColumns.DATE_ADDED, System.currentTimeMillis())
                    put(MediaStore.MediaColumns.DATE_TAKEN, System.currentTimeMillis())
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, picturesPath)
                }

                return resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
            } else {

                val savePath = File(Environment.getExternalStorageDirectory(), picturesPath)
                if (!savePath.exists()) {
                    Timber.d("savePath = %s, %s", savePath.mkdir(), savePath)
                }
                val filePath = File(savePath, fileName)
                Timber.d("getPicturesPath 3 = %s", filePath)
                return FileProvider.getUriForFile(
                    context,
                    BuildConfig.APPLICATION_ID + ".fileprovider",
                    filePath
                )
            }
        } catch (e: IllegalArgumentException) {
            Timber.e(e)
        }
        return null
    }

}