package com.krungsri.coop.helper

import android.Manifest
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.media.MediaScannerConnection
import android.net.Uri
import android.view.View
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.krungsri.coop.R
import com.krungsri.coop.lib.eventbut.EventBus
import com.krungsri.coop.model.transaction.BillPaymentResponse
import com.krungsri.coop.ui.base.Alert
import org.bouncycastle.util.Times
import timber.log.Timber


object BarcodeHelper {
    fun generateBarcode(data: String, width: Int, height: Int): Bitmap? {
        return try {
            Timber.d("generateBarcode = %s, %sx%s", data, width, height)
            BarcodeEncoder().encodeBitmap(data, BarcodeFormat.CODE_128, width, height)

        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }

    fun generateQRCode(data: String, size: Int): Bitmap? {
        return try {
            Timber.d("generateQRCode = %s, %s", data, size)
            val hintMap = HashMap<EncodeHintType, Any>().apply {
                put(EncodeHintType.MARGIN, 0)
            }
            BarcodeEncoder().encodeBitmap(data, BarcodeFormat.QR_CODE, size, size, hintMap)
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }

    fun saveBillPayment(
        context: Context,
        billPayment: BillPaymentResponse,
        view: View,
        onSaveCompleted: ((pathUri: Uri) -> Unit)? = null
    ) {
        Timber.d("saveBillPayment")
        Dexter.withContext(context)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        AppHelper.getPicturesPath(context, "BillPayment-${billPayment.refNo2}-${Times.nanoTime()}.png")?.let { filePath ->
                            context.contentResolver.openOutputStream(filePath).use { output ->
                                val bitmap = Bitmap.createBitmap(
                                    view.width,
                                    view.height,
                                    Bitmap.Config.ARGB_8888
                                )
                                val canvas = Canvas(bitmap)
                                canvas.drawColor(Color.WHITE)
                                view.draw(canvas)
                                bitmap.compress(
                                    Bitmap.CompressFormat.PNG,
                                    100,
                                    output
                                )

                                MediaScannerConnection.scanFile(
                                    context, arrayOf(filePath.toString()), null
                                ) { path, uri ->
                                    Timber.i("MediaScannerConnection1.Scanned $path:")
                                    Timber.i("MediaScannerConnection1.uri=$uri")
                                }

                                onSaveCompleted?.invoke(filePath)
                                EventBus.publish(
                                    Alert.Toast(
                                        message = context.getString(R.string.alert_bill_payment_save)
                                    )
                                )
                            }
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()
    }


}