package com.krungsri.coop.helper

import java.text.SimpleDateFormat
import java.util.*

object DateHelper {
    private val sdf = SimpleDateFormat("yyyyMMddhhmm", Locale("EN", "th"))

    init {
        sdf.timeZone = TimeZone.getTimeZone("asia/bangkok")
    }

    @JvmStatic
    fun getModDateTime(): String {
        val dateTime = sdf.format(Date())
        val date = dateTime.substring(0, dateTime.length - 2)
        var mm = dateTime.substring(dateTime.length - 2).toInt()
        if (mm % 2 != 0) {
            mm -= 1
        }
        val mmStr = (if (mm < 10) "0" else "") + mm
        return "$date$mmStr"
    }
}