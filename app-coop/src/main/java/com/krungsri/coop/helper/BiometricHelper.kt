package com.krungsri.coop.helper

import android.content.Context
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.krungsri.coop.R
import java.util.concurrent.Executor


class BiometricHelper(
    context: Context, fragment: Fragment,
    onAuthenticationSucceeded: (() -> Unit),
    onAuthenticationFailed: (() -> Unit),
    onAuthenticationError: (() -> Unit)
) {
    private var executor: Executor = ContextCompat.getMainExecutor(context)
    private var biometricPrompt: BiometricPrompt
    private var promptInfo: BiometricPrompt.PromptInfo


    init {
        biometricPrompt = BiometricPrompt(fragment, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    onAuthenticationError.invoke()
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    onAuthenticationSucceeded.invoke()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    onAuthenticationFailed.invoke()
                }
            })
        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(context.getString(R.string.coop_name_home))
//            .setSubtitle(context.getString(R.string.label_biometric_subtitle))
            .setDescription(context.getString(R.string.label_biometric_desc))
            .setNegativeButtonText(context.getString(R.string.label_biometric_with_pin))
            .build()
    }

    fun biometricPrompt() {
        biometricPrompt.authenticate(promptInfo)
    }

    companion object {
        fun canAuthenticate(context: Context): Boolean {
            return BiometricManager.from(context)
                .canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK) == BiometricManager.BIOMETRIC_SUCCESS
        }
    }

}