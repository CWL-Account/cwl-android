package com.krungsri.coop.helper

import android.os.Build
import android.util.Base64
import com.krungsri.coop.BuildConfig
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.openssl.PEMKeyPair
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import timber.log.Timber
import java.io.StringReader
import java.nio.ByteBuffer
import java.security.KeyPair
import java.security.MessageDigest
import java.security.PublicKey
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.Mac
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec


object SecurityHelper {

    external fun isRooted(): Boolean
    external fun getApiClientId(): String
    external fun getApiClientSecret(): String

    private external fun getApiPublicKey(): String
    private external fun getApiCipherAlgorithm(): String
    external fun getApiEncryptSecret(): ByteArray

    private external fun getHmacAlgorithm(): String
    private external fun getLocalEncryptAlgorithm(): String
    private external fun getLocalCipherAlgorithm(): String

    private external fun getLocalEncryptKeySize(): Int
    private external fun getLocalEncryptSecretSize(): Int

    private external fun getLocalEncryptKey(): ByteArray
    private external fun getLocalEncryptSecret(): ByteArray


    external fun getSSHCert(): String


    private val secureRandom = SecureRandom()

    @JvmStatic
    fun random(length: Int): ByteArray {
        val bytes = ByteArray(length)
        this.secureRandom.nextBytes(bytes)
        return bytes
    }

    fun getClientCredentials(): String {
        return "${getApiClientId()}:${getApiClientSecret()}"
    }

    fun toHmac(message: String): String {
        val mac = Mac.getInstance(getHmacAlgorithm())
        val secretKeySpec =
            SecretKeySpec(getClientCredentials().toByteArray(Charsets.UTF_8), getHmacAlgorithm())
        mac.init(secretKeySpec)
        val hmacSha256 = mac.doFinal(message.toByteArray(Charsets.UTF_8))
        return Base64.encodeToString(hmacSha256, Base64.NO_WRAP)
    }

    fun toMD5(message: String): String {
        val md5 = MessageDigest.getInstance("MD5").digest(message.toByteArray(Charsets.UTF_8))
        return Base64.encodeToString(md5, Base64.NO_WRAP)
    }

    @JvmStatic
    fun isEmulator(): Boolean {
        if (BuildConfig.DEBUG) return false;
        return (Build.FINGERPRINT.startsWith("google/sdk_gphone_")
                && Build.FINGERPRINT.endsWith(":user/release-keys")
                && Build.MANUFACTURER == "Google" && Build.PRODUCT.startsWith("sdk_gphone_") && Build.BRAND == "google"
                && Build.MODEL.contains("sdk_gphone_"))
                //
                || Build.FINGERPRINT.contains("generic")
                || Build.FINGERPRINT.contains("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                //bluestacks
                || "QC_Reference_Phone" == Build.BOARD && !"Xiaomi".equals(
            Build.MANUFACTURER,
            ignoreCase = true
        ) //bluestacks
                || Build.MANUFACTURER.contains("Genymotion")
                || Build.HOST.startsWith("Build") //MSI App Player
                || Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic")
                || Build.PRODUCT == "google_sdk"


    }

    private fun publicKey(pemKey: String): PublicKey {
        val pem = PEMParser(StringReader(pemKey))
        val jcaPEMKeyConverter = JcaPEMKeyConverter()
        return when (val pemContent: Any = pem.readObject()) {
            is PEMKeyPair -> {
                val pemKeyPair: PEMKeyPair = pemContent
                val keyPair: KeyPair = jcaPEMKeyConverter.getKeyPair(pemKeyPair)
                keyPair.public
            }
            is SubjectPublicKeyInfo -> {
                val keyInfo: SubjectPublicKeyInfo = pemContent
                jcaPEMKeyConverter.getPublicKey(keyInfo)
            }
            is X509CertificateHolder -> {
                val cert: X509CertificateHolder = pemContent
                jcaPEMKeyConverter.getPublicKey(cert.subjectPublicKeyInfo)
            }
            else -> {
                throw IllegalArgumentException(
                    "Unsupported public key format '" +
                            pemContent.javaClass.simpleName + '"'
                )
            }
        }
    }

    fun encryptPinNo(pinNo: String): String {
        return encryptWithPubicKey(pinNo.toByteArray(Charsets.UTF_8)) ?: pinNo
    }

    fun encryptWithPubicKey(byteArray: ByteArray): String? {
        try {
            val cipher: Cipher = Cipher.getInstance(getApiCipherAlgorithm())
            cipher.init(Cipher.ENCRYPT_MODE, publicKey(getApiPublicKey()))
            return Base64.encodeToString(
                cipher.doFinal(byteArray),
                Base64.NO_WRAP
            )
        } catch (e: Exception) {
            Timber.e(e, "encryptWithPubicKey")
        }
        return null
    }


    fun generateAesKey(): ByteArray {
        return getLocalEncryptKey()
    }

    fun generateIV(): ByteArray {
        return getLocalEncryptSecret()
    }

    fun encryptLocalText(text: String): String {
        return encryptLocalText(generateAesKey(), generateIV(), text)
    }

    fun encryptLocalText(aesKey: ByteArray, iv: ByteArray, text: String): String {
        try {
            val cipher = Cipher.getInstance(getLocalCipherAlgorithm())
            val aesKeySpec = SecretKeySpec(aesKey, getLocalEncryptAlgorithm())

            cipher.init(Cipher.ENCRYPT_MODE, aesKeySpec, GCMParameterSpec(getLocalEncryptSecretSize() * 8, iv))

            val encryptText = cipher.doFinal(text.toByteArray(Charsets.UTF_8))

            val cipherText = ByteBuffer.allocate(aesKey.size + iv.size + encryptText.size).apply {
                put(aesKey)
                put(iv)
                put(encryptText)
            }.array()

            return Base64.encodeToString(cipherText, Base64.NO_WRAP)
        } catch (e: Exception) {
            Timber.e(e, "encryptText")
        }

        return text
    }

    fun encryptApiText(aesKey: ByteArray, iv: ByteArray, text: String): String {
        try {
            val cipher = Cipher.getInstance(getLocalCipherAlgorithm())
            val aesKeySpec = SecretKeySpec(aesKey, getLocalEncryptAlgorithm())

            cipher.init(Cipher.ENCRYPT_MODE, aesKeySpec, GCMParameterSpec(getLocalEncryptSecretSize() * 8, iv))

            val encryptText = cipher.doFinal(text.toByteArray(Charsets.UTF_8))

            return Base64.encodeToString(aesKey.plus(encryptText), Base64.NO_WRAP)
        } catch (e: Exception) {
            Timber.e(e, "encryptText")
        }

        return text
    }

    fun decryptLocalText(encryptText: String): String {
        return try {
            val aesKey = ByteArray(getLocalEncryptKeySize())
            val iv = ByteArray(getLocalEncryptSecretSize())
            val cipherText = ByteBuffer.wrap(Base64.decode(encryptText, Base64.NO_WRAP)).let {
                it.get(aesKey)
                it.get(iv)
                ByteArray(it.remaining()).apply {
                    it.get(this)
                }
            }
            decryptLocalByte(aesKey, iv, cipherText)
        } catch (e: java.lang.Exception) {
            Timber.e(e)
            encryptText
        }
    }

    fun decryptLocalText(aesKey: ByteArray, iv: ByteArray, encryptText: String): String {
        val aesKeyOriginal = ByteArray(getLocalEncryptKeySize())
        val ivOriginal = ByteArray(getLocalEncryptSecretSize())
        val cipherText = ByteBuffer.wrap(Base64.decode(encryptText, Base64.NO_WRAP)).let {
            it.get(aesKeyOriginal)
            it.get(ivOriginal)
            ByteArray(it.remaining()).apply {
                it.get(this)
            }
        }
        return decryptLocalByte(aesKey, iv, cipherText)
    }

    fun decryptLocalBase64(aesKey: ByteArray, iv: ByteArray, cipherBase64: String): String {
        return decryptLocalByte(aesKey, iv, Base64.decode(cipherBase64, Base64.NO_WRAP))
    }

    fun decryptLocalByte(aesKey: ByteArray, iv: ByteArray, cipherByte: ByteArray): String {
        try {
            val cipher = Cipher.getInstance(getLocalCipherAlgorithm())
            val aesKeySpec = SecretKeySpec(aesKey, getLocalEncryptAlgorithm())
            cipher.init(Cipher.DECRYPT_MODE, aesKeySpec, GCMParameterSpec(getLocalEncryptSecretSize() * 8, iv))
            return String(
                cipher.doFinal(cipherByte),
                Charsets.UTF_8
            )
        } catch (e: Exception) {
            Timber.e(e, "decryptText")
        }

        return ""
    }

    fun checkPinDuplicateNumber(pinNo: String, duplicateCount: Int): Boolean {
        var latest = ""
        var count = 0
        for (c in pinNo.toCharArray()) {
            val c2 = c.toString()
            if (latest == c2) {
                count++
            } else {
                count = 1
            }
            latest = c2
            if (count >= duplicateCount) {
                return true
            }
        }
        return false
    }
}