package com.krungsri.coop.helper

object MaskHelper {
    fun maskMobileNo(mobileNo: String, maskCharacter: String = "X"): String {
        return if (mobileNo.length > 4) {
            "${maskCharacter.repeat(mobileNo.length - 4)}${mobileNo.takeLast(4)}"
        } else {
            mobileNo
        }
    }

    fun maskAccountNo(accountNumber: String): String {
        return if (accountNumber.length > 5) {
            "${"X".repeat(3)}${accountNumber.substring(3, accountNumber.length-1)}X"
        } else {
            accountNumber
        }
    }

    fun maskCitizenNo(citizenID: String, maskCharacter: String = "X"): String {
        return if (citizenID.length > 4) {
            "${maskCharacter.repeat(citizenID.length - 4)}${citizenID.takeLast(4)}"
        } else {
            citizenID
        }
    }
}