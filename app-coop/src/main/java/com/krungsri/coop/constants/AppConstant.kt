package com.krungsri.coop.constants

class AppConstant {
    companion object {
        const val BASIC_TOKEN = "BASIC_TOKEN"
        const val ACCESS_TOKEN = "ACCESS_TOKEN"
        const val REFRESH_TOKEN = "REFRESH_TOKEN"
        const val BIOMETRIC_TOKEN = "BIOMETRIC_TOKEN"
        const val TOKEN_TYPE = "TOKEN_TYPE"
        const val MEMBER_CODE = "MEMBER_CODE"
        const val FIRST_NAME = "FIRST_NAME"
        const val LAST_NAME = "LAST_NAME"

        const val PICTURES_PATH = "pictures_path"
        const val IMAGE_CAPTURE_CODE = 20214
        const val IMAGE_PICK_CODE = 20215

        const val CODE_INTERNET_CONNECT = "INTERNET_CONNECT"
        const val CODE_MS599ERROR = "MS599ERROR"
        const val CODE_SUCCESS200 = "MS200SUCCESS"
        const val CODE_SUCCESS201 = "MS201SUCCESS"

        const val CODE_DUPLICATE_DEVICE = "MER006ERR"
        const val CODE_DUPLICATE_MEMBER = "MER011ERR"
        const val CODE_DEVICE_NOTFOUND = "MER004ERR"
        const val CODE_DEVICE_INVALID = "MER005ERR"
        const val CODE_MEMBER_NOTFOUND = "MER003ERR"

        const val CODE_PIN_LOCK = "MER010ERR"
        const val CODE_PRODUCT_SERVICE_NOTFOUND = "TRA003ERR"
        const val CODE_OTP_INVALID = "OTP001ERR"
        const val CODE_OTP_EXPIRED = "OTP006ERR"

        const val PIN_SEQ = "0123456789012345678909876543210987654321"

        const val CODE_TOKEN_TOKEN_ACCESS = "MS401ERROR"

        const val CODE_REQUEST_NEW_PIN = "TKN007ERR"
        const val CODE_TOKEN_ERROR = "TKN001ERR"
        const val CODE_TOKEN_VERIFY_EXP = "TKN008ERR"
        const val CODE_TOKEN_ACCESS_EXP = "TKN009ERR"
        const val CODE_TOKEN_REFRESH_EXP = "TKN010ERR"
        const val CODE_TOKEN_BIO_EXP = "TKN011ERR"
        val CODE_TOKEN_EXR =
            arrayOf(CODE_REQUEST_NEW_PIN, CODE_TOKEN_ERROR, CODE_TOKEN_VERIFY_EXP, CODE_TOKEN_ACCESS_EXP, CODE_TOKEN_REFRESH_EXP, CODE_TOKEN_BIO_EXP, CODE_TOKEN_TOKEN_ACCESS)

        const val SET_MARK_ACCOUNT = "SET_MARK_ACCOUNT"
        var IS_MARK_ACCOUNT = false

    }
}