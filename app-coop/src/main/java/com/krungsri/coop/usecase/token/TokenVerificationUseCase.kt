package com.krungsri.coop.usecase.token

import com.krungsri.coop.data.api.TokenAPI
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.model.token.TokenValidationRequest
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class TokenVerificationUseCase @Inject constructor(
    private val api: TokenAPI,
    private val tokenService: TokenService
) : BaseUseCase.WithParams<TokenValidationRequest, ApiResponse<Any>>() {
    override fun onExecute(params: TokenValidationRequest): Observable<ApiResponse<Any>> {
        tokenService.logout()
        return api.tokenVerification(params)
    }

}