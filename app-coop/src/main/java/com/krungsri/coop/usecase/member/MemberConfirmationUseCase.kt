package com.krungsri.coop.usecase.member

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.member.MemberConfirmationRequest
import com.krungsri.coop.model.member.MemberConfirmationResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberConfirmationUseCase @Inject constructor(
    private val api: MemberAPI
) : BaseUseCase.WithParams<MemberConfirmationRequest, MemberConfirmationResponse>() {
    override fun onExecute(params: MemberConfirmationRequest): Observable<MemberConfirmationResponse> {
        return if (params.verificationType == VerificationType.REGISTER) {
            api.memberConfirmationWithBasicToken(params).map {
                AppPrefs.setStaticString(AppConstant.MEMBER_CODE, params.memberCode)
                it.result
            }
        } else {
            api.memberConfirmation(params).map {
                AppPrefs.setStaticString(AppConstant.MEMBER_CODE, params.memberCode)
                it.result
            }
        }
    }


}