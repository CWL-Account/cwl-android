package com.krungsri.coop.usecase.policy

import com.krungsri.coop.data.api.PolicyAPI
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetMemberTCUseCase @Inject constructor(
    private val api: PolicyAPI
) : BaseUseCase.WithoutParams<MutableList<PolicyResponse>>() {
    override fun onExecute(): Observable<MutableList<PolicyResponse>> {
        return api.getTCMember().map {
            it.result
        }
    }

}