package com.krungsri.coop.usecase.transaction

import com.krungsri.coop.data.api.TransactionAPI
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.transaction.TransactionConfirmRequest
import com.krungsri.coop.model.transaction.TransactionResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class ConfirmTransactionUseCase @Inject constructor(
    private val api: TransactionAPI
) : BaseUseCase.WithParams<TransactionConfirmRequest, TransactionResponse>() {
    override fun onExecute(params: TransactionConfirmRequest): Observable<TransactionResponse> {
       return when(params.serviceCode){
            ServiceCode.TRANSFERRING -> api.confirmTransactionTransfer(params)
            ServiceCode.WITHDRAW ->api.confirmTransactionWithdraw(params)
            ServiceCode.DEPOSIT -> api.confirmTransactionDeposit(params)
            ServiceCode.LOAN_WITHDRAW -> api.confirmTransactionWithdrawLoan(params)
            ServiceCode.LOAN_PAYMENT -> api.confirmTransactionDepositLoan(params)
            else -> Observable.just(null)
        }.map {
           it.result
       }
    }
}