package com.krungsri.coop.usecase


import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class BaseUseCase {

    abstract class WithParams<Params, Result> : BaseUseCase() {

        protected abstract fun onExecute(params: Params): Observable<Result>

        fun build(
            param: Params
        ): Observable<Result> {
            return Observable.defer {
                return@defer this.onExecute(param)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

            }
        }

        fun buildWithoutSchedulers(param: Params): Observable<Result> {
            return Observable.defer { this.onExecute(param) }
        }
    }

    abstract class WithoutParams<Result> : BaseUseCase() {

        protected abstract fun onExecute(): Observable<Result>

        fun build(): Observable<Result> {
            return Observable.defer {
                return@defer this.onExecute()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
        }

        fun buildWithoutSchedulers(): Observable<Result> {
            return Observable.defer { this.onExecute() }
        }

    }

}