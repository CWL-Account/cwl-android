package com.krungsri.coop.usecase.home

import com.krungsri.coop.data.api.TransactionAPI
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.extension.withCache
import com.krungsri.coop.model.home.InquiryHomeResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import timber.log.Timber
import javax.inject.Inject

class InquiryHomeUseCase @Inject constructor(
    private val api: TransactionAPI,
    private val localSession: LocalSession
) : BaseUseCase.WithoutParams<MutableList<InquiryHomeResponse>>() {
    override fun onExecute(): Observable<MutableList<InquiryHomeResponse>> {
        Timber.d("InquiryHomeUseCase")
        return api.homeAccount().withCache(localSession.inquiryHome)
            .map {
                it.result
            }
    }
}