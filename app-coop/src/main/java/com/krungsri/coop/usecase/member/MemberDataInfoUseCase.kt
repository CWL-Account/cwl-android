package com.krungsri.coop.usecase.member

import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.model.member.MemberDataInfoResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberDataInfoUseCase @Inject constructor(
    private val api: MemberAPI
) : BaseUseCase.WithoutParams<MemberDataInfoResponse>() {
    override fun onExecute(): Observable<MemberDataInfoResponse> {
        return api.memberInfo().map {
            it.result
        }
    }


}