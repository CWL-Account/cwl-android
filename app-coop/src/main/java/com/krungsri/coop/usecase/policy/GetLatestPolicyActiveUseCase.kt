package com.krungsri.coop.usecase.policy

import com.krungsri.coop.data.api.PolicyAPI
import com.krungsri.coop.model.policy.PolicyRequest
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetLatestPolicyActiveUseCase @Inject constructor(
    private val api: PolicyAPI
) : BaseUseCase.WithParams<PolicyRequest, PolicyResponse>() {
    override fun onExecute(params: PolicyRequest): Observable<PolicyResponse> {
        return api.getPolicyActive(params).map {
            it.result
        }
    }

}