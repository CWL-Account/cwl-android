package com.krungsri.coop.usecase.member

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.model.member.MemberBiometricLoginRequest
import com.krungsri.coop.model.member.MemberLoginResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberBiometricLoginCase @Inject constructor(
    private val api: MemberAPI,
    private val tokenService: TokenService
) : BaseUseCase.WithParams<MemberBiometricLoginRequest, MemberLoginResponse>() {
    override fun onExecute(params: MemberBiometricLoginRequest): Observable<MemberLoginResponse> {
        return api.memberBiometricLogin(params)
            .doOnNext {
                if (it.code == AppConstant.CODE_SUCCESS200) {
                    tokenService.setLoginToken(it.result)
                }
            }.map {
                it.result
            }
    }


}