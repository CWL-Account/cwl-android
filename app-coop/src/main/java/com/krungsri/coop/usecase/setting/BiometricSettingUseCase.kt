package com.krungsri.coop.usecase.setting

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.setting.BiometricSettingRequest
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class BiometricSettingUseCase @Inject constructor(
    private val api: MemberAPI
) : BaseUseCase.WithParams<BiometricSettingRequest, Boolean>() {
    override fun onExecute(params: BiometricSettingRequest): Observable<Boolean> {
        if (params.isEnable == Flag.N) {
            AppPrefs.remove(AppConstant.BIOMETRIC_TOKEN)
        }
        return api.biometricSetting(params).map {
            it.result?.biometricToken?.run {
                AppPrefs.setString(AppConstant.BIOMETRIC_TOKEN, this)
            }
            it.code == AppConstant.CODE_SUCCESS200
        }
    }

}