package com.krungsri.coop.usecase.transaction

import com.krungsri.coop.data.api.TransactionAPI
import com.krungsri.coop.model.transaction.InitialFormRequest
import com.krungsri.coop.model.transaction.InitialFormResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class InitialFormUseCase @Inject constructor(
    private val api: TransactionAPI
) : BaseUseCase.WithParams<InitialFormRequest, InitialFormResponse>() {
    override fun onExecute(params: InitialFormRequest): Observable<InitialFormResponse> {
        return api.inquiryInitialForm(params).map {
            it.result
        }
    }

}