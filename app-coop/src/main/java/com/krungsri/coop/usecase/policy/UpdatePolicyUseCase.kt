package com.krungsri.coop.usecase.policy

import com.krungsri.coop.data.api.PolicyAPI
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.policy.PolicyUpdateRequest
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class UpdatePolicyUseCase @Inject constructor(
    private val api: PolicyAPI
) : BaseUseCase.WithParams<PolicyUpdateRequest, ApiResponse<Any>>() {
    override fun onExecute(params: PolicyUpdateRequest): Observable<ApiResponse<Any>> {
        return api.updatePolicy(params)
    }

}