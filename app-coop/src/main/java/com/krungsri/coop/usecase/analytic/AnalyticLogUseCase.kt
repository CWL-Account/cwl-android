package com.krungsri.coop.usecase.analytic

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.AnalyticAPI
import com.krungsri.coop.model.analytic.AnalyticRequest
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class AnalyticLogUseCase @Inject constructor(
    private val api: AnalyticAPI
) : BaseUseCase.WithParams<AnalyticRequest, Boolean>() {
    override fun onExecute(params: AnalyticRequest): Observable<Boolean> {
        return api.log(params)
            .map {
                it.code == AppConstant.CODE_SUCCESS200
            }
    }
}
