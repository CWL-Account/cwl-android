package com.krungsri.coop.usecase.coop

import com.krungsri.coop.data.api.RestAPI
import com.krungsri.coop.model.coop.CoopInfoResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetCoopInfoUseCase @Inject constructor(
    private val api: RestAPI
) : BaseUseCase.WithoutParams<CoopInfoResponse>() {
    override fun onExecute(): Observable<CoopInfoResponse> {
        return api.getCoopInfo()
            .map {
                it.result
            }
    }
}