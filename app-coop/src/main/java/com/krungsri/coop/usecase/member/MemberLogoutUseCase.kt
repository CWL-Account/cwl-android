package com.krungsri.coop.usecase.member

import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberLogoutUseCase @Inject constructor(
    private val api: MemberAPI,
    private val tokenService: TokenService
) : BaseUseCase.WithoutParams<Boolean>() {
    override fun onExecute(): Observable<Boolean> {
        return if (tokenService.isLogIn()) {
            api.memberLogout().map {
                tokenService.removeAccessToken()
                tokenService.logout()
                it.result
            }
        } else {
            Observable.just(true)
        }
    }


}