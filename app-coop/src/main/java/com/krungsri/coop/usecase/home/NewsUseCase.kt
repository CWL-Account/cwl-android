package com.krungsri.coop.usecase.home

import com.krungsri.coop.data.api.RestAPI
import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.extension.withCache
import com.krungsri.coop.model.news.NewsRequest
import com.krungsri.coop.model.news.NewsResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class NewsUseCase @Inject constructor(
    private val api: RestAPI,
    private val localSession: LocalSession
) : BaseUseCase.WithParams<NewsRequest, ApiPageResponse<MutableList<NewsResponse>>>() {
    override fun onExecute(params: NewsRequest): Observable<ApiPageResponse<MutableList<NewsResponse>>> {
        return api.listNews(params.pageNo, params.size).withCache(
            if (params.pageNo == 0 && params.size == 1) {
                localSession.newsListHome
            } else {
                localSession.newsListAll
            }
        )
    }
}