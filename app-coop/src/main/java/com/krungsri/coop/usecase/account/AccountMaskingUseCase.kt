package com.krungsri.coop.usecase.account

import com.krungsri.coop.data.api.AccountAPI
import com.krungsri.coop.model.account.AccountMaskingRequest
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class AccountMaskingUseCase @Inject constructor(
    private val api: AccountAPI
) : BaseUseCase.WithParams<Flag, Boolean>() {
    override fun onExecute(params: Flag): Observable<Boolean> {
        return api.accountMasking(AccountMaskingRequest(params))
            .map {
//                it.code == AppConstant.CODE_SUCCESS200
                true
            }
    }
}
