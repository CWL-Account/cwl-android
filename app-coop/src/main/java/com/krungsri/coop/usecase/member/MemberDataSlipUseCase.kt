package com.krungsri.coop.usecase.member

import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.model.member.MemberSlipRequest
import com.krungsri.coop.model.member.MemberSlipResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberDataSlipUseCase @Inject constructor(
    private val api: MemberAPI
) : BaseUseCase.WithParams<MemberSlipRequest, MemberSlipResponse>() {
    override fun onExecute(params: MemberSlipRequest): Observable<MemberSlipResponse> {
        return api.memberSlip(params).map {
            it.result
        }
    }


}