package com.krungsri.coop.usecase.otp

import com.krungsri.coop.data.api.RestAPI
import com.krungsri.coop.model.otp.OTPRequest
import com.krungsri.coop.model.otp.OTPResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class RequestNewOTPUseCase @Inject constructor(
    private val api: RestAPI
) : BaseUseCase.WithParams<OTPRequest, OTPResponse>() {
    override fun onExecute(params: OTPRequest): Observable<OTPResponse> {
        return api.otpRequest(params).map {
            it.result
        }
    }
}