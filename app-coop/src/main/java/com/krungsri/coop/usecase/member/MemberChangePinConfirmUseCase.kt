package com.krungsri.coop.usecase.member

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.model.member.MemberChangePinConfirmRequest
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberChangePinConfirmUseCase @Inject constructor(
    private val api: MemberAPI,
    private var tokenService: TokenService
) : BaseUseCase.WithParams<MemberChangePinConfirmRequest, Boolean>() {
    override fun onExecute(params: MemberChangePinConfirmRequest): Observable<Boolean> {
        return api.changePinConfirm(params).map {
            if (it.code == AppConstant.CODE_SUCCESS200) {
                tokenService.removeAccessToken()
            }
            it.code == AppConstant.CODE_SUCCESS200
        }
    }


}