package com.krungsri.coop.usecase.member

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.model.member.MemberLoginResponse
import com.krungsri.coop.model.member.MemberUnlockRequest
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberUnlockUseCase @Inject constructor(
    private val api: MemberAPI,
    private val tokenService: TokenService
) : BaseUseCase.WithParams<MemberUnlockRequest, MemberLoginResponse>() {
    override fun onExecute(params: MemberUnlockRequest): Observable<MemberLoginResponse> {
        return api.memberUnlock(params).map {
            if (it.code == AppConstant.CODE_SUCCESS200) {
                tokenService.setLoginToken(it.result)
            }
            it.result
        }
    }


}