package com.krungsri.coop.usecase.account

import com.krungsri.coop.data.api.AccountAPI
import com.krungsri.coop.model.account.AccountInActiveRequest
import com.krungsri.coop.model.account.AccountInActiveResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetAccountInActiveListUseCase @Inject constructor(
    private val api: AccountAPI
) : BaseUseCase.WithParams<AccountInActiveRequest, AccountInActiveResponse>() {
    override fun onExecute(params: AccountInActiveRequest): Observable<AccountInActiveResponse> {
        return api.getCoopAccountInactiveList(params)
            .map {
                it.result
            }
    }
}