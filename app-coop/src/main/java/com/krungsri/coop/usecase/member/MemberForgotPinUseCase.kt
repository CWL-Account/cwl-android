package com.krungsri.coop.usecase.member

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.model.member.MemberForgotPinRequest
import com.krungsri.coop.model.member.MemberForgotPinResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberForgotPinUseCase @Inject constructor(
    private val api: MemberAPI,
    private val tokenService: TokenService
) : BaseUseCase.WithParams<MemberForgotPinRequest, MemberForgotPinResponse>() {
    override fun onExecute(params: MemberForgotPinRequest): Observable<MemberForgotPinResponse> {
        return api.memberForgotPin(params).map {
            if (it.code == AppConstant.CODE_SUCCESS200) {
                tokenService.newAccessToken(it.result)
            }
            it.result
        }
    }


}