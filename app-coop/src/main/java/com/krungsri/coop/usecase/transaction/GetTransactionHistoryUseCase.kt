package com.krungsri.coop.usecase.transaction

import com.krungsri.coop.data.api.TransactionAPI
import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.model.transaction.TransactionHistoryRequest
import com.krungsri.coop.model.transaction.TransactionResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetTransactionHistoryUseCase @Inject constructor(
    private val api: TransactionAPI
) : BaseUseCase.WithParams<TransactionHistoryRequest, ApiPageResponse<MutableList<TransactionResponse>>>() {
    override fun onExecute(params: TransactionHistoryRequest): Observable<ApiPageResponse<MutableList<TransactionResponse>>> {
        return api.transactionHistory(params)
    }
}