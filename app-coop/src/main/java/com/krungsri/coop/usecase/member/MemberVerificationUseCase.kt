package com.krungsri.coop.usecase.member

import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.member.MemberVerificationRequest
import com.krungsri.coop.model.member.MemberVerificationResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberVerificationUseCase @Inject constructor(
    private val api: MemberAPI
) : BaseUseCase.WithParams<MemberVerificationRequest, MemberVerificationResponse>() {
    override fun onExecute(params: MemberVerificationRequest): Observable<MemberVerificationResponse> {
        return if (params.verificationType == VerificationType.REGISTER) {
            api.memberVerificationWithBasicToken(params).map {
                it.result
            }
        } else {
            api.memberVerificationWith(params).map {
                it.result
            }
        }

    }


}