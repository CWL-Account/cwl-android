package com.krungsri.coop.usecase.account

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.AccountAPI
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.model.account.AccountRemoveRequest
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class RemoveAccountListUseCase @Inject constructor(
    private val api: AccountAPI,
    private val localSession: LocalSession
) : BaseUseCase.WithParams<AccountRemoveRequest, Boolean>() {
    override fun onExecute(params: AccountRemoveRequest): Observable<Boolean> {
        return api.removeCoopAccountList(params)
            .map {
                localSession.inquiryHome.clear()
                it.code == AppConstant.CODE_SUCCESS200
            }
    }
}
