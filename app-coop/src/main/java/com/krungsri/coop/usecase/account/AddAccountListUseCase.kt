package com.krungsri.coop.usecase.account

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.AccountAPI
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.model.account.AccountAddRequest
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class AddAccountListUseCase @Inject constructor(
    private val api: AccountAPI,
    private val localSession: LocalSession
) : BaseUseCase.WithParams<AccountAddRequest, Boolean>() {
    override fun onExecute(params: AccountAddRequest): Observable<Boolean> {
        return api.addCoopAccountList(params)
            .map {
                localSession.inquiryHome.clear()
                it.code == AppConstant.CODE_SUCCESS200
            }
    }
}
