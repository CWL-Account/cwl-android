package com.krungsri.coop.usecase.otp

import com.krungsri.coop.data.api.RestAPI
import com.krungsri.coop.model.otp.OTPVerificationRequest
import com.krungsri.coop.model.otp.OTPVerificationResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class OTPVerificationUseCase @Inject constructor(
    private val api: RestAPI
) : BaseUseCase.WithParams<OTPVerificationRequest, OTPVerificationResponse>() {
    override fun onExecute(params: OTPVerificationRequest): Observable<OTPVerificationResponse> {
        return api.otpVerification(params).map {
            it.result
        }
    }
}