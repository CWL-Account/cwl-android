package com.krungsri.coop.usecase.member

import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.model.member.MemberChangePinVerifyRequest
import com.krungsri.coop.model.member.MemberChangePinVerifyResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberChangePinVerificationUseCase @Inject constructor(
    private val api: MemberAPI
) : BaseUseCase.WithParams<MemberChangePinVerifyRequest, MemberChangePinVerifyResponse>() {
    override fun onExecute(params: MemberChangePinVerifyRequest): Observable<MemberChangePinVerifyResponse> {
        return api.changePinVerification(params).map {
            it.result
        }
    }


}