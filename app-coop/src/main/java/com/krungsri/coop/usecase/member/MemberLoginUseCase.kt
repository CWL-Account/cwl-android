package com.krungsri.coop.usecase.member

import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.MemberAPI
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.model.member.MemberLoginRequest
import com.krungsri.coop.model.member.MemberLoginResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class MemberLoginUseCase @Inject constructor(
    private val api: MemberAPI,
    private val tokenService: TokenService,
    private val localSession: LocalSession
) : BaseUseCase.WithParams<MemberLoginRequest, MemberLoginResponse>() {
    override fun onExecute(params: MemberLoginRequest): Observable<MemberLoginResponse> {
        return api.memberLogin(params).map {
            if (it.code == AppConstant.CODE_SUCCESS200) {
                localSession.clearAll()
                tokenService.setLoginToken(it.result)
            }
            it.result
        }
    }


}