package com.krungsri.coop.usecase.transaction

import com.krungsri.coop.data.api.TransactionAPI
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.transaction.TransactionInquiryRequest
import com.krungsri.coop.model.transaction.TransactionInquiryResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import timber.log.Timber
import javax.inject.Inject

class InquiryTransactionUseCase @Inject constructor(
    private val api: TransactionAPI
) : BaseUseCase.WithParams<TransactionInquiryRequest, ApiResponse<TransactionInquiryResponse>>() {
    override fun onExecute(params: TransactionInquiryRequest): Observable<ApiResponse<TransactionInquiryResponse>> {
        Timber.d("inquiryTransaction.serviceCode %s", params.serviceCode)
        return when (params.serviceCode) {
            ServiceCode.TRANSFERRING -> api.inquiryTransactionTransfer(params)
            ServiceCode.WITHDRAW -> api.inquiryTransactionWithdraw(params)
            ServiceCode.DEPOSIT -> api.inquiryTransactionDeposit(params)
            ServiceCode.LOAN_WITHDRAW -> api.inquiryTransactionWithdrawLoan(params)
            ServiceCode.LOAN_PAYMENT -> api.inquiryTransactionDepositLoan(params)
            else -> Observable.just(null)
        }
    }
}