package com.krungsri.coop.usecase.transaction

import com.krungsri.coop.data.api.TransactionAPI
import com.krungsri.coop.model.transaction.BillPaymentRequest
import com.krungsri.coop.model.transaction.BillPaymentResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class InquiryBillPaymentUseCase @Inject constructor(
    private val api: TransactionAPI
) : BaseUseCase.WithParams<BillPaymentRequest, BillPaymentResponse>() {
    override fun onExecute(params: BillPaymentRequest): Observable<BillPaymentResponse> {
        return api.inquiryBillPayment(request = params).map {
            it.result
        }
    }
}