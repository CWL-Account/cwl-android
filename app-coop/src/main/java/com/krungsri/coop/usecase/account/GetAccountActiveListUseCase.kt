package com.krungsri.coop.usecase.account

import com.krungsri.coop.data.api.AccountAPI
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.account.AccountInfo
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetAccountActiveListUseCase @Inject constructor(
    private val api: AccountAPI
) : BaseUseCase.WithoutParams<ApiResponse<MutableList<AccountInfo>>>() {
    override fun onExecute(): Observable<ApiResponse<MutableList<AccountInfo>>> {
        return api.getCoopAccountActiveList()
    }
}