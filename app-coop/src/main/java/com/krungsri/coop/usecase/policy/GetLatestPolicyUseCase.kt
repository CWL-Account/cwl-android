package com.krungsri.coop.usecase.policy

import com.krungsri.coop.data.api.PolicyAPI
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.usecase.BaseUseCase
import io.reactivex.Observable
import javax.inject.Inject

class GetLatestPolicyUseCase @Inject constructor(
    private val api: PolicyAPI
) : BaseUseCase.WithoutParams<PolicyResponse>() {
    override fun onExecute(): Observable<PolicyResponse> {
        return api.getPolicyLatest().map {
            it.result
        }
    }

}