package com.krungsri.coop.lib.lifecycle


import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import timber.log.Timber

open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set

    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    fun peekContent(): T = content
}

inline fun <T> LiveData<Event<T>>.observeEvent(
    owner: LifecycleOwner,
    crossinline onEventUnhandledContent: (T) -> Unit
) {
    observe(owner, Observer { it?.getContentIfNotHandled()?.let(onEventUnhandledContent) })
}

abstract class LiveEvent<T> : LiveData<Event<T>> {
    constructor() : super()
    constructor(value: T) : super(Event(value))

    val eventValue: T?
        get() = value?.peekContent()
}

class MutableLiveEvent<T> : LiveEvent<T> {
    constructor() : super()
    constructor(value: T) : super(value)

    fun setEventValue(value: T) {
        setValue(Event(value))
    }

    fun postEventValue(value: T) {
        Timber.d("Event.postEventValue %s", value)
        postValue(Event(value))
    }
}