package com.krungsri.coop.data.session


import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.home.InquiryHomeResponse
import com.krungsri.coop.model.news.NewsResponse
import org.threeten.bp.Clock
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalSession @Inject constructor(private val clock: Clock) {

    val inquiryHome: CacheData<ApiResponse<MutableList<InquiryHomeResponse>>> by createCacheCache(name = "inquiryHome")
    val newsListHome: CacheData<ApiPageResponse<MutableList<NewsResponse>>> by createCacheCache(name = "newsListHome", maxAge = (60 * 1).toLong())
    val newsListAll: CacheData<ApiPageResponse<MutableList<NewsResponse>>> by createCacheCache(name = "newsListHome", maxAge = (60 * 1).toLong())

    private fun <T> createCacheCache(
        name: String,
        maxAge: Long = (60 * 5).toLong() //5 minutes
    ) = lazy { CacheData<T>(name, clock, maxAge) }

    fun clearAll(){
        inquiryHome.clear()
        newsListHome.clear()
        newsListAll.clear()
    }
}