package com.krungsri.coop.data.api

import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.home.InquiryHomeResponse
import com.krungsri.coop.model.transaction.*
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface TransactionAPI {

    @Headers(value = ["x-api-version:v1"])
    @GET("/home/account-inquiry")
    fun homeAccount(): Observable<ApiResponse<MutableList<InquiryHomeResponse>>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/initial-form")
    fun inquiryInitialForm(@Body request: InitialFormRequest): Observable<ApiResponse<InitialFormResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/inquiry/transfer-coop")
    fun inquiryTransactionTransfer(@Body request: TransactionInquiryRequest): Observable<ApiResponse<TransactionInquiryResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/confirmation/transfer-coop")
    fun confirmTransactionTransfer(@Body request: TransactionConfirmRequest): Observable<ApiResponse<TransactionResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/inquiry/withdraw-coop-bay")
    fun inquiryTransactionWithdraw(@Body request: TransactionInquiryRequest): Observable<ApiResponse<TransactionInquiryResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/confirmation/withdraw-coop-bay")
    fun confirmTransactionWithdraw(@Body request: TransactionConfirmRequest): Observable<ApiResponse<TransactionResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/inquiry/withdraw-loan-bay")
    fun inquiryTransactionWithdrawLoan(@Body request: TransactionInquiryRequest): Observable<ApiResponse<TransactionInquiryResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/confirmation/withdraw-loan-bay")
    fun confirmTransactionWithdrawLoan(@Body request: TransactionConfirmRequest): Observable<ApiResponse<TransactionResponse>>


    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/inquiry/deposit-bay-coop")
    fun inquiryTransactionDeposit(@Body request: TransactionInquiryRequest): Observable<ApiResponse<TransactionInquiryResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/confirmation/deposit-bay-coop")
    fun confirmTransactionDeposit(@Body request: TransactionConfirmRequest): Observable<ApiResponse<TransactionResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/inquiry/deposit-bay-loan")
    fun inquiryTransactionDepositLoan(@Body request: TransactionInquiryRequest): Observable<ApiResponse<TransactionInquiryResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/confirmation/deposit-bay-loan")
    fun confirmTransactionDepositLoan(@Body request: TransactionConfirmRequest): Observable<ApiResponse<TransactionResponse>>


    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/history")
    fun transactionHistory(@Body request: TransactionHistoryRequest): Observable<ApiPageResponse<MutableList<TransactionResponse>>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/transaction/inquiry/bill-payment")
    fun inquiryBillPayment(@Body request: BillPaymentRequest): Observable<ApiResponse<BillPaymentResponse>>


}