package com.krungsri.coop.data.api


import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.member.MemberLoginResponse
import com.krungsri.coop.model.token.RefreshTokenRequest
import com.krungsri.coop.model.token.TokenValidationRequest
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*

interface TokenAPI {
    @Headers(value = ["x-api-version:v1"])
    @POST("/token/verification")
    fun tokenVerification(@Body request: TokenValidationRequest): Observable<ApiResponse<Any>>

    @Headers(value = ["x-api-version:v1", "x-cwl-refresh:refresh "])
    @POST("/token/refreshToken")
    fun tokenRefreshToken(@Body request: RefreshTokenRequest): Observable<ApiResponse<MemberLoginResponse>>

    @Headers(value = ["x-api-version:v1"])
    @GET("/member/logout")
    fun callMemberLogout(
        @Header("x-cwl-token") token: String
    ): Call<ApiResponse<Boolean>>

}
