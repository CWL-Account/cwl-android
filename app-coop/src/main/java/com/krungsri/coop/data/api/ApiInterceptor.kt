package com.krungsri.coop.data.api

import android.content.Context
import com.krungsri.coop.BuildConfig
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.helper.SecurityHelper
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import okio.Buffer
import org.json.JSONObject
import timber.log.Timber
import java.util.*


class ApiInterceptor(val context: Context, val tokenService: TokenService) : Interceptor {

    companion object {
        private val MEDIA_TYPE = "application/json".toMediaTypeOrNull()
        private var REQUEST_COUNT = 0
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val generateAesKey = SecurityHelper.generateAesKey()
        val generateIV = SecurityHelper.getApiEncryptSecret()

        val requestOriginal = chain.request()
        val newRequestBuilder = tokenService.addToken(requestOriginal)
        tokenService.setSignature(requestOriginal, newRequestBuilder, generateAesKey)
        val request = newRequestBuilder.build()

        REQUEST_COUNT++
        if (BuildConfig.DEBUG) {
            Timber.d("C%s============== API Security ===============", REQUEST_COUNT)
            Timber.d("C%s======= Security Key %s, IV %s", REQUEST_COUNT, generateAesKey.size, generateIV.size)
            Timber.d("C%s======= Security Key %s", REQUEST_COUNT, String(generateAesKey))
            Timber.d("C%s======= Security IV %s", REQUEST_COUNT, String(generateIV))
            Timber.d("C%s============== API Request ================", REQUEST_COUNT)
            Timber.d("C%sRequest URL -> %s:%s", REQUEST_COUNT, requestOriginal.method, requestOriginal.url)
            Timber.d("C%s===========Request Original================", REQUEST_COUNT)
            requestOriginal.headers.names().forEach {
                Timber.d("C%sRequest Original Header -> %s : %s", REQUEST_COUNT, it, requestOriginal.headers[it])
            }
            Timber.d("C%s==========================================", REQUEST_COUNT)
            Timber.d("C%s============= Request New ================", REQUEST_COUNT)
            request.headers.names().forEach {
                Timber.d("C%sRequest New Header -> %s : %s", REQUEST_COUNT, it, request.headers[it])
            }
            Timber.d("C%s==========================================", REQUEST_COUNT)
        }
        val requestBodyString = if (requestOriginal.method.toUpperCase(Locale.ROOT) != "GET") {
            val buffer = Buffer()
            requestOriginal.body?.writeTo(buffer)
            buffer.readUtf8()
        } else {
            SecurityHelper.getClientCredentials()
        }

        val response = if (request.method.toUpperCase(Locale.ROOT) == "POST") {
            val encryptedBody =
                SecurityHelper.encryptApiText(
                    aesKey = generateAesKey,
                    iv = generateIV,
                    text = requestBodyString
                )
            chain.proceed(request.newBody(encryptedBody))
        } else {
            chain.proceed(request)
        }

        if (BuildConfig.DEBUG) {
            Timber.d("C%sRequest Body -> %s", REQUEST_COUNT, requestBodyString)
            Timber.d("C%s==========================================", REQUEST_COUNT)
            Timber.d("C%sResponse Code :: %d, msg:%s", REQUEST_COUNT, response.code, response.message)
            Timber.d("C%s===========================================", REQUEST_COUNT)
        }
//        if (generateAesKey != null && generateIV != null) {
        response.body?.run {
            try {
                val responseEncrypted = String(bytes())
//                    Timber.d("C%sResponse Encrypted :: %s ", REQUEST_COUNT, responseEncrypted)
                var responseString = ""
                if (responseEncrypted.startsWith("{")) {
                    val json = JSONObject(responseEncrypted)
                    if (json.has("data")) {
                        responseString = SecurityHelper.decryptLocalBase64(
                            aesKey = generateAesKey,
                            iv = generateIV,
                            cipherBase64 = json.getString("data")
                        )
                    }
                }
                Timber.d("C%sResponse Body :: %s ", REQUEST_COUNT, responseString)
                Timber.d("C%s===========================================", REQUEST_COUNT)
                return createResponse(newRequestBuilder.build(), response, responseString)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
//        } else if (BuildConfig.DEBUG) {
//            val responseString = response.body?.string() ?: ""
//            Timber.d("C%sResponse Body :: %s ", REQUEST_COUNT, responseString)
//            Timber.d("C%s===========================================", REQUEST_COUNT)
//            return createResponse(newRequestBuilder.build(), response, responseString)
//        }
        return response
    }

    private fun Request.newBody(encryptedBody: String): Request {
        val newRequest = Request.Builder()
        newRequest.url(url)
        newRequest.headers(headers)
        if (method.toUpperCase(Locale.ROOT) == "POST") {
            val json = JSONObject()
            json.put("data", encryptedBody)
            newRequest.post(json.toString().toRequestBody())
        }
        return newRequest.build()
    }

    private fun createResponse(
        request: Request,
        response: Response,
        responseBodyString: String
    ): Response {
        return Response.Builder()
            .code(response.code)
            .message(response.message)
            .body(responseBodyString.toResponseBody(MEDIA_TYPE))
            .request(request)
            .protocol(Protocol.HTTP_1_1)
            .build()
    }


}