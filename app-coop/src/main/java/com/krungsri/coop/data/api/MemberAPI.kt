package com.krungsri.coop.data.api

import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.member.*
import com.krungsri.coop.model.setting.BiometricSettingRequest
import com.krungsri.coop.model.setting.BiometricSettingResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface MemberAPI {

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/verification")
    fun memberVerificationWith(@Body request: MemberVerificationRequest): Observable<ApiResponse<MemberVerificationResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/confirmation")
    fun memberConfirmation(@Body request: MemberConfirmationRequest): Observable<ApiResponse<MemberConfirmationResponse>>

    @Headers(value = ["x-api-version:v1", "Authorization:Basic "])
    @POST("/member/verification")
    fun memberVerificationWithBasicToken(@Body request: MemberVerificationRequest): Observable<ApiResponse<MemberVerificationResponse>>

    @Headers(value = ["x-api-version:v1", "Authorization:Basic "])
    @POST("/member/confirmation")
    fun memberConfirmationWithBasicToken(@Body request: MemberConfirmationRequest): Observable<ApiResponse<MemberConfirmationResponse>>

    @Headers(value = ["x-api-version:v1", "Authorization:Basic "])
    @POST("/member/registration")
    fun memberRegistrationWithBasicToken(@Body request: MemberRegistrationRequest): Observable<ApiResponse<MemberLoginResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/new-pin")
    fun memberNewPin(@Body request: MemberNewPinRequest): Observable<ApiResponse<MemberLoginResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/forgot-pin")
    fun memberForgotPin(@Body request: MemberForgotPinRequest): Observable<ApiResponse<MemberForgotPinResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/unlock")
    fun memberUnlock(@Body request: MemberUnlockRequest): Observable<ApiResponse<MemberLoginResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/login")
    fun memberLogin(@Body request: MemberLoginRequest): Observable<ApiResponse<MemberLoginResponse>>

    @Headers(value = ["x-api-version:v1"])
    @GET("/member/logout")
    fun memberLogout(): Observable<ApiResponse<Boolean>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/biometric-login")
    fun memberBiometricLogin(@Body request: MemberBiometricLoginRequest): Observable<ApiResponse<MemberLoginResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/change-pin-verification")
    fun changePinVerification(@Body request: MemberChangePinVerifyRequest): Observable<ApiResponse<MemberChangePinVerifyResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/change-pin")
    fun changePinConfirm(@Body request: MemberChangePinConfirmRequest): Observable<ApiResponse<Any>>


    @Headers(value = ["x-api-version:v1"])
    @POST("/member/biometric-setting")
    fun biometricSetting(@Body request: BiometricSettingRequest): Observable<ApiResponse<BiometricSettingResponse>>

    @Headers(value = ["x-api-version:v1"])
    @GET("/member/info")
    fun memberInfo(): Observable<ApiResponse<MemberDataInfoResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/member/slip")
    fun memberSlip(@Body request: MemberSlipRequest): Observable<ApiResponse<MemberSlipResponse>>

}