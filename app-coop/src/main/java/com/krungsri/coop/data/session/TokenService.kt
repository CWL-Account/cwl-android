package com.krungsri.coop.data.session

import android.content.Context
import android.util.Base64
import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.TokenAPI
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.lib.eventbut.AppEvent
import com.krungsri.coop.lib.eventbut.EventBus
import com.krungsri.coop.model.member.MemberForgotPinResponse
import com.krungsri.coop.model.member.MemberLoginResponse
import com.krungsri.coop.model.member.MemberNewPinResponse
import com.krungsri.coop.model.token.RefreshTokenRequest
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.Credentials
import okhttp3.Request
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.nio.charset.StandardCharsets
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.HashMap

@Singleton
class TokenService @Inject constructor() {
    companion object {
        private const val HEADER_CWL_ACCESS = "x-cwl-token"
        private const val HEADER_CWL_REFRESH = "x-cwl-refresh"
        private const val HEADER_AUTHORIZATION = "Authorization"
        private const val HEADER_REQUEST_ID = "x-request-id"
        private const val HEADER_SIGNATURE = "x-signature"
        private const val HEADER_GET_TOKEN = "x-get-token"
        private const val HEADER_REQUEST_TIME = "x-request-time"
        private const val TIME_INTERVAL = 5000L
        private const val ACCESS_TOKEN_TIMEOUT = (60000L * 7) - TIME_INTERVAL //7m
        private const val USER_INTERACTION_TIMEOUT = (60000L * 5) - TIME_INTERVAL //5m

        private var REQUEST_ID = UUID.randomUUID().toString()
    }

    private val latestAccessToken = MutableLiveData<Long>()
    private val userInteraction = MutableLiveData<Long>()
    private var isAutoLogout = false
    private var tokens: MutableMap<String, AuthorizationToken> = createBasicTokens()
    private lateinit var charSet: HashSet<Char>
    private lateinit var context: Context
    private lateinit var tokenAPI: TokenAPI
    private var disposeBag: CompositeDisposable = CompositeDisposable()

    fun init(context: Context, tokenAPI: TokenAPI) {
        Timber.d("init ")
        this.context = context
        this.tokenAPI = tokenAPI
        charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray().toHashSet()
    }

    private fun getBasicToken(): AuthorizationToken? {
        return tokens[AppConstant.BASIC_TOKEN]
    }


    private fun getAccessToken(): AuthorizationToken? {
        return if (tokens.containsKey(AppConstant.ACCESS_TOKEN)) {
            tokens[AppConstant.ACCESS_TOKEN]
        } else
            null
    }


    fun newAccessToken(result: Any?) {
        when (result) {
            is MemberNewPinResponse -> {
                setAccessToken(result.tokenType, result.accessToken)
            }
            is MemberForgotPinResponse -> {
                setAccessToken(result.tokenType, result.accessToken)
            }
        }
    }

    fun getTokenMap(): Map<String, String> {
        val map = HashMap<String, String>()
        map[HEADER_REQUEST_ID] = REQUEST_ID
        getAccessToken()?.run {
            map[name] = "$type $token"
        } ?: getRefreshToken()?.run {
            map[name] = "$type $token"
        } ?: getBasicToken()?.run {
            map[name] = token
        }

        Timber.d("getTokenMap %s", map);
        return map;
    }

    fun addToken(requestOriginal: Request): Request.Builder {
        val newRequest = requestOriginal.newBuilder()
        newRequest.addHeader(HEADER_REQUEST_ID, REQUEST_ID)
        when {
            requestOriginal.headers[HEADER_AUTHORIZATION] != null -> {
                newRequest.removeHeader(HEADER_AUTHORIZATION)
                tokens[AppConstant.BASIC_TOKEN]?.run {
                    newRequest.addHeader(name, token)
                }
            }
            requestOriginal.headers[HEADER_CWL_REFRESH] != null -> {
                Timber.d("addToken:HEADER_CWL_REFRESH")
                newRequest.removeHeader(HEADER_CWL_REFRESH)
                getRefreshToken()?.run {
                    Timber.d("addToken:%s:%s", name, token)
                    newRequest.addHeader(name, "$type $token")
                } ?: getBasicToken()?.run {
                    newRequest.addHeader(name, token)
                }
            }
            else -> {
                getAccessToken()?.run {
                    newRequest.addHeader(name, "$type $token")
                } ?: getRefreshToken()?.run {
                    newRequest.addHeader(name, "$type $token")
                } ?: getBasicToken()?.run {
                    newRequest.addHeader(name, token)
                }
            }
        }
        return newRequest
    }

    fun setSignature(requestOriginal: Request, requestBuilder: Request.Builder, aesKey: ByteArray): Request.Builder {
        val cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Bangkok"))
        val signature = StringBuilder()
        signature.append(REQUEST_ID)

        var token = getAccessToken()?.token ?: getRefreshToken()?.token ?: ""

        var jti = "";
        if (token == "") {
            token = getBasicToken()?.token?.substring("Basic ".length) ?: ""
            Timber.d("rowSignature 0.1 %s", token)
        } else {
            val tokenList = token.split(".")
            if(tokenList.size == 3){
                val payloadString = String(Base64.decode(tokenList[1], Base64.NO_WRAP))
                val payloadJson = JSONObject(payloadString)
                if(payloadJson.has("jti")) {
                    jti = payloadJson.getString("jti")
                }
            }
            Timber.d("jwt jti = %s", jti)
        }
        signature.append(token)
        signature.append(jti)
        if (requestOriginal.method == "POST") {
            signature.append(Base64.encodeToString(aesKey, Base64.NO_WRAP))
        } else {
            SecurityHelper.encryptWithPubicKey(aesKey)?.run {
                requestBuilder.addHeader(HEADER_GET_TOKEN, this)
            }
        }


        val signatureMD5 = SecurityHelper.toMD5(signature.toString())
        Timber.d("rowSignature 1 %s %s", requestOriginal.url, signature)
        Timber.d("rowSignature 2 %s %s", requestOriginal.url, signatureMD5)
        requestBuilder.addHeader(HEADER_SIGNATURE, signatureMD5)
        requestBuilder.addHeader(HEADER_REQUEST_TIME, cal.time.time.toString())
        return requestBuilder
    }

    private fun getRefreshToken(): AuthorizationToken? {
        return when {
            tokens.containsKey(AppConstant.REFRESH_TOKEN) -> {
                tokens[AppConstant.REFRESH_TOKEN]
            }
            AppPrefs.hasString(AppConstant.REFRESH_TOKEN) -> {
                AuthorizationToken(
                    name = HEADER_CWL_REFRESH,
                    type = AppPrefs.getString(AppConstant.TOKEN_TYPE) ?: "",
                    token = AppPrefs.getString(AppConstant.REFRESH_TOKEN) ?: ""
                )
            }
            else -> null
        }
    }

    fun setLoginToken(memberLogin: MemberLoginResponse?) {
        memberLogin?.run {
            setAccessToken(tokenType, accessToken)
            refreshToken?.run {
                tokens[AppConstant.REFRESH_TOKEN] = AuthorizationToken(
                    name = HEADER_CWL_REFRESH,
                    type = tokenType,
                    token = this
                )
                AppPrefs.setString(AppConstant.REFRESH_TOKEN, refreshToken)
            }
            AppPrefs.setString(AppConstant.TOKEN_TYPE, tokenType)
            firstName?.run {
                AppPrefs.setString(AppConstant.FIRST_NAME, firstName)
            }
            lastName?.run {
                AppPrefs.setString(AppConstant.LAST_NAME, lastName)
            }
        }
    }

    private fun setAccessToken(tokenType: String, accessToken: String) {
        Timber.d("setAccessToken")
        tokens[AppConstant.ACCESS_TOKEN] = AuthorizationToken(
            name = HEADER_CWL_ACCESS,
            type = tokenType,
            token = accessToken
        )
        latestAccessToken.postValue(System.currentTimeMillis())
        userInteraction.postValue(System.currentTimeMillis())
        startTimer()
    }

    private fun listenEvent() {
        EventBus.listen(AppEvent::class.java)
            .subscribeOn(Schedulers.io())
            .subscribe {
                if (it == AppEvent.UserActive) {
                    Timber.d("listenEvent %s", it)
                    userInteraction.postValue(System.currentTimeMillis())
                }
            }.also {
                disposeBag.add(it)
            }
    }

    fun removeAccessToken() {
        tokens.remove(AppConstant.ACCESS_TOKEN)
    }

    fun logout(): Boolean {
        val accessToken = tokens[AppConstant.ACCESS_TOKEN]
        Timber.d("Auto Logout = %s", accessToken)
        if (accessToken != null) {
            GlobalScope.launch(Dispatchers.IO) {
                val token = "${accessToken.type} ${accessToken.token}"
                val call = tokenAPI.callMemberLogout(token)
                call.enqueue(object : Callback<ApiResponse<Boolean>> {
                    override fun onResponse(
                        call: Call<ApiResponse<Boolean>>,
                        response: Response<ApiResponse<Boolean>>
                    ) {
                        tokens.clear()
                        tokens = createBasicTokens()
                    }

                    override fun onFailure(call: Call<ApiResponse<Boolean>>, t: Throwable) {
                        tokens.clear()
                        tokens = createBasicTokens()
                    }
                })

            }
        } else {
            tokens.clear()
            tokens = createBasicTokens()
        }
        stopTimer()

        return true
    }

    fun isLogIn(): Boolean {
        return getAccessToken() != null
    }

    private fun startTimer() {
        stopTimer()
        GlobalScope.launch(Dispatchers.IO) {
            isAutoLogout = false
            do {
                delay(TIME_INTERVAL)
                val lastUserInteraction = userInteraction.value?.let {
                    (System.currentTimeMillis() - it)
                } ?: 0

//                Timber.d("TOKEN ScreenTimeOut = %s >= %s", lastUserInteraction, USER_INTERACTION_TIMEOUT)
                if (lastUserInteraction >= USER_INTERACTION_TIMEOUT) {
                    isAutoLogout = logout()
                    launch(Dispatchers.Main) {
                        EventBus.publish(AppEvent.AutoLogout)
                    }
                }
            } while (!isAutoLogout)
        }

        GlobalScope.launch(Dispatchers.IO) {
            var loop = true
            do {
                delay(TIME_INTERVAL)
                val expIn = latestAccessToken.value?.let {
                    (System.currentTimeMillis() - it)
                } ?: 0
//                Timber.d("TOKEN expIn = %s, %s", expIn, ACCESS_TOKEN_TIMEOUT)
                if (expIn >= ACCESS_TOKEN_TIMEOUT) {
                    loop = false
                    stopTimer()
                    autoRefreshToken()
                }
            } while (loop && !isAutoLogout)
        }

        listenEvent()
    }

    private fun stopTimer() {
        disposeBag.clear()
    }

    private fun createBasicTokens() = mutableMapOf(
        AppConstant.BASIC_TOKEN to AuthorizationToken(
            name = HEADER_AUTHORIZATION,
            type = "Basic",
            token = Credentials.basic(
                SecurityHelper.getApiClientId(),
                SecurityHelper.getApiClientSecret()
            )
        )
    )

    private fun autoRefreshToken() {
        Timber.d(" autoRefreshToken.start")
        val refreshTokenRequest = tokenAPI.tokenRefreshToken(
            RefreshTokenRequest(
                accessToken = tokens[AppConstant.ACCESS_TOKEN]?.token ?: "",
                uuid = AppHelper.getDeviceId(context)
            )
        )

        Observable.defer {
            return@defer refreshTokenRequest
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
        }
            .subscribe({
                Timber.d("autoRefreshToken.subscribe :: %s", it.code)
                if (it.code == AppConstant.CODE_SUCCESS200) {
                    setLoginToken(it.result)
                }
            }, {
                if (it is retrofit2.HttpException) {
                    val apiError = it.response()?.let { response ->
                        response.errorBody()?.byteString()?.string(StandardCharsets.UTF_8)
                    }?.let { errorJson ->
                        Timber.d("autoRefreshToken.errorJson = %s", errorJson)
                    }
                } else {
                    Timber.e(it, "autoRefreshToken.error")
                }
            }).also {
                disposeBag.add(it)
            }
    }
}

data class AuthorizationToken(
    val name: String,
    val type: String,
    val token: String,
    val createTime: Long = 0
)