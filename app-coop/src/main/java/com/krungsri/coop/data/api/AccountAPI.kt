package com.krungsri.coop.data.api

import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.account.*
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface AccountAPI {

    @Headers(value = ["x-api-version:v1"])
    @GET("/account/setting/account-management/active")
    fun getCoopAccountActiveList(): Observable<ApiResponse<MutableList<AccountInfo>>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/account/setting/account-management/inactive")
    fun getCoopAccountInactiveList(@Body request: AccountInActiveRequest): Observable<ApiResponse<AccountInActiveResponse>>

    @Headers(value = ["x-api-version:v1"])
    @GET("/account/setting/masking")
    fun accountMasking(request: AccountMaskingRequest): Observable<ApiResponse<Any>>
    
    @Headers(value = ["x-api-version:v1"])
    @POST("/account/setting/account-management/remove")
    fun removeCoopAccountList(@Body request: AccountRemoveRequest): Observable<ApiResponse<Any>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/account/setting/account-management/add")
    fun addCoopAccountList(@Body request: AccountAddRequest): Observable<ApiResponse<Any>>

}