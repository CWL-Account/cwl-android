package com.krungsri.coop.data.api

import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.policy.PolicyRequest
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.model.policy.PolicyUpdateRequest
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface PolicyAPI {
    @Headers(value = ["x-api-version:v1"])
    @POST("/policy/latest")
    fun getPolicyActive(@Body request: PolicyRequest): Observable<ApiResponse<PolicyResponse>>

    @Headers(value = ["x-api-version:v1"])
    @GET("/policy/latest")
    fun getPolicyLatest(): Observable<ApiResponse<PolicyResponse>>

    @Headers(value = ["x-api-version:v1"])
    @GET("/policy/member/tc")
    fun getTCMember(): Observable<ApiResponse<MutableList<PolicyResponse>>>

    @Headers(value = ["x-api-version:v1"])
    @GET("/policy/member/consent")
    fun getConsentMember(): Observable<ApiResponse<MutableList<PolicyResponse>>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/policy/update")
    fun updatePolicy(@Body request: PolicyUpdateRequest): Observable<ApiResponse<Any>>

}