package com.krungsri.coop.data.api


import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.coop.CoopInfoResponse
import com.krungsri.coop.model.news.NewsResponse
import com.krungsri.coop.model.otp.OTPRequest
import com.krungsri.coop.model.otp.OTPResponse
import com.krungsri.coop.model.otp.OTPVerificationRequest
import com.krungsri.coop.model.otp.OTPVerificationResponse
import io.reactivex.Observable
import retrofit2.http.*

interface RestAPI {
    @Headers(value = ["x-api-version:v1"])
    @GET("/coop/info")
    fun getCoopInfo(): Observable<ApiResponse<CoopInfoResponse>>


    @Headers(value = ["x-api-version:v1"])
    @POST("/otp/verification")
    fun otpVerification(@Body request: OTPVerificationRequest): Observable<ApiResponse<OTPVerificationResponse>>

    @Headers(value = ["x-api-version:v1"])
    @POST("/otp/request")
    fun otpRequest(@Body request: OTPRequest): Observable<ApiResponse<OTPResponse>>

    @Headers(value = ["x-api-version:v1"])
    @GET("/news/list")
    fun listNews(
        @Query("page_no") pageNo: Int,
        @Query("size") size: Int
    ): Observable<ApiPageResponse<MutableList<NewsResponse>>>
}
