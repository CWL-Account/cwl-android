package com.krungsri.coop.data.api

import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.model.analytic.AnalyticRequest
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface AnalyticAPI {
    @Headers(value = ["x-api-version:v1"])
    @POST("/analytic/log")
    fun log(@Body request: AnalyticRequest): Observable<ApiResponse<Any>>
}