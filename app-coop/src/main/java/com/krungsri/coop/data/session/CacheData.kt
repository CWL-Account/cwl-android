package com.krungsri.coop.data.session

import org.threeten.bp.Clock


class CacheData<T>(
    val name: String,
    private val clock: Clock,
    private val maxAge: Long
) {
    private var data: T? = null
    private var cachedAt: Long? = null

    fun set(value: T) {
        this.data = value
        this.cachedAt = clock.instant().epochSecond
    }

    fun get(): T? {
        return if (isCacheDataExpired()) {
            return null
        } else data

    }

    fun clear() {
        this.data = null
        this.cachedAt = null
    }


    private fun isCacheDataExpired(): Boolean {
        return cachedAt?.let {
            val age = clock.instant().epochSecond - it
            val isExpired = age > maxAge
            if (isExpired) {
                clear()
            }
            isExpired
        } ?: false
    }

    fun exists(): Boolean {
        if (isCacheDataExpired()) {
            return false
        }
        return data != null
    }

}