package com.krungsri.coop.data.api.http

import com.google.gson.annotations.SerializedName


data class ApiError(
    val code: String,
    val message: String,
    val httpCode: Int,
    val throwable: Throwable? = null
)

data class ApiResponse<T>(
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("result")
    val result: T?
)

data class ApiPageResponse<T>(
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("page_no")
    val pageNo: Int,
    @SerializedName("total_page")
    val totalPage: Int,
    @SerializedName("last")
    val last: Boolean,
    @SerializedName("result")
    val result: T?
)

data class ApiSignature(
    @SerializedName("date")
    val date: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("body")
    val body: String
)