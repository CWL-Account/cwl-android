package com.krungsri.coop.ui.component.transaction.slip

import android.os.Parcelable
import com.krungsri.coop.model.transaction.DestinationAccount
import com.krungsri.coop.model.transaction.SourceAccount
import com.krungsri.coop.model.transaction.TransactionResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class TrxSlipContext(
    val transaction: TransactionResponse,
    val sourceAccount: SourceAccount?,
    val destinationAccount: DestinationAccount?
) : Parcelable