package com.krungsri.coop.ui.component.member.pin

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.model.member.MemberLoginResponse
import com.krungsri.coop.model.member.MemberNewPinRequest
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.member.landing.LandingDestination
import com.krungsri.coop.ui.component.member.landing.MemberLandingContext
import com.krungsri.coop.usecase.member.MemberNewPinUseCase
import javax.inject.Inject

class MemberPinViewModel @Inject
constructor(
    private val memberNewPinUseCase: MemberNewPinUseCase
) : BaseViewModel() {

    private val memberPinContext = MutableLiveData<MemberPinContext>()

    fun initData(_memberPinContext: MemberPinContext) {
        memberPinContext.value = _memberPinContext
    }

    fun setupNewPin() {
        memberPinContext.value?.run {
            memberNewPinUseCase.build(
                MemberNewPinRequest(
                    pinNo = SecurityHelper.encryptPinNo(this.newPin!!)
                )
            ).doToggleLoading()
                .subscribeWithViewModel(
                    this@MemberPinViewModel,
                    this@MemberPinViewModel::onNewPinSuccess,
                    this@MemberPinViewModel::onApiError
                )
                .disposedBy(this@MemberPinViewModel)
        }
    }

    private fun onNewPinSuccess(response: MemberLoginResponse) {
        gotoPage(
            MemberPinConfirmFragmentDirections.gotoWelcomePage(
                MemberLandingContext(
                    destination = memberPinContext.value?.destination
                        ?: LandingDestination.HOME_FROM_AFTER_NEW_PIN
                )
            )
        )
    }

}