package com.krungsri.coop.ui.component.transaction.payment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.krungsri.coop.R
import com.krungsri.coop.databinding.ItemBillPaymentBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.setAccountNo
import com.krungsri.coop.extension.setColorTint
import com.krungsri.coop.extension.visible
import com.krungsri.coop.helper.BarcodeHelper
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.transaction.BillPaymentData
import com.krungsri.coop.model.transaction.BillPaymentResponse
import com.krungsri.coop.model.transaction.BillerDataType
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter

class BillPaymentAdapter(
    private val onSaveSlip: ((layoutBill: View, billPaymentData: BillPaymentResponse) -> Unit),
    private val onShareSlip: ((layoutBill: View, billPaymentData: BillPaymentResponse) -> Unit),
    private val onBackToHomeClick: (() -> Unit ),
    private val accountType: AccountType
) : BaseRecyclerViewAdapter<BillPaymentData, ItemBillPaymentBinding>() {

    override fun createBinding(parent: ViewGroup): ItemBillPaymentBinding {
        val binding : ItemBillPaymentBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_bill_payment,
            parent,
            false
        )

        if(accountType == AccountType.LOAN){
            binding.buttonBackToHome.setBackgroundResource(R.drawable.button_main_loan)
            binding.buttonShare.setBackgroundResource(R.drawable.button_empty_loan)
            binding.buttonSave.setBackgroundResource(R.drawable.button_empty_loan)
            binding.imgSave.setColorTint(R.color.colorSecondary)
            binding.imgShare.setColorTint(R.color.colorSecondary)
        }
        return binding
    }

    override fun bind(binding: ItemBillPaymentBinding, position: Int, item: BillPaymentData) {
        binding.adapter = this
        binding.layoutBill = binding.layoutBillSave
        binding.billPaymentData = item.response
        binding.textAccountNo.setAccountNo(item.response.coopAccountNo, null)
        binding.imgBarcode.visible()
        val qrCodeSize = binding.imgBarcode.context.resources.getDimension(R.dimen.bill_qrcode_size).toInt()
        if (item.dataType == BillerDataType.BARCODE) {
            binding.layoutBarcode.visible()
            binding.layoutQr.gone()
//            binding.textBarcode.visible()
//            binding.textBarcode.text = item.data
            binding.imgBarcode.setImageBitmap(
                BarcodeHelper.generateBarcode(
                    item.data,
                    0, qrCodeSize
                )
            )
        } else {

            binding.layoutBarcode.gone()
            binding.layoutQr.visible()
            binding.textBarcode.gone()
            binding.imgQRCode.setImageBitmap(
                BarcodeHelper.generateQRCode(
                    item.data,
                    qrCodeSize
                )
            )
        }


        binding.layoutButton.visible()
    }

    fun saveSlip(layoutBill: View, billPaymentData: BillPaymentResponse) {
        onSaveSlip.invoke(layoutBill, billPaymentData)
    }

    fun shareSlip(layoutBill: View, billPaymentData: BillPaymentResponse) {
        onShareSlip.invoke(layoutBill, billPaymentData)
    }

    fun backToHomeClick() {
        onBackToHomeClick.invoke()
    }
}