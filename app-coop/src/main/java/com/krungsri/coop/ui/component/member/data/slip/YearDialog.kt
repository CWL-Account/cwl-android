package com.krungsri.coop.ui.component.member.data.slip

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.FragmentManager
import com.krungsri.coop.R
import com.krungsri.coop.databinding.DialogMonthBinding
import com.krungsri.coop.extension.getString
import com.krungsri.coop.extension.string
import com.krungsri.coop.ui.base.BaseDialog
import timber.log.Timber
import java.util.*

class YearDialog(
    private val onSelectYear: ((year: String, display: String) -> Unit),
    private val selectedYear: String? = null
) : BaseDialog() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DialogMonthBinding.inflate(
            inflater,
            container,
            false
        )
        binding.textTitle.string(R.string.year_title)
        initView(binding)
        return binding.root
    }

    private fun initView(binding: DialogMonthBinding) {
        val canceler = Calendar.getInstance()
        var year = canceler.get(Calendar.YEAR)
        val yearDisplay = mutableListOf<String>()
        val maxDisplay = 2
        val yearValue = mutableListOf<String>()

        yearDisplay.add(binding.root.getString(R.string.year_current))
        yearValue.add("$year")
        for (ids in maxDisplay downTo 0) {
            year -= 1
            yearDisplay.add("$year")
            yearValue.add("$year")
        }

        val monthAdapter = ArrayAdapter(binding.root.context, R.layout.item_month, yearDisplay)
        monthAdapter.setDropDownViewResource(R.layout.item_month);
        binding.spinnerMonth.adapter = monthAdapter

        Timber.d("selectedYear = %s", selectedYear)
        selectedYear?.run {
            val idx = yearValue.indexOf(this)
            if (idx > 0) {
                binding.spinnerMonth.setSelection(idx)
            }
        }
        binding.buttonCancel.setOnClickListener {
            dismiss()
        }

        binding.buttonOK.setOnClickListener {
            dismiss()
            onSelectYear.invoke(
                yearValue[binding.spinnerMonth.selectedItemPosition],
                yearDisplay[binding.spinnerMonth.selectedItemPosition]
            )
        }
    }

    fun show(manager: FragmentManager) {
        super.show(manager, "YearDialog")
    }
}