package com.krungsri.coop.ui.component.member.validation

import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.navArgs
import com.google.android.material.textfield.TextInputEditText
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentMemberValidationBinding
import com.krungsri.coop.extension.clearError
import com.krungsri.coop.extension.hideKeyboard
import com.krungsri.coop.extension.showError
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.ui.base.BaseFragment

class MemberVerificationFragment :
    BaseFragment<FragmentMemberValidationBinding, MemberVerificationViewModel>() {
    override val layoutId: Int = R.layout.fragment_member_validation

    override val viewModelClass: Class<MemberVerificationViewModel> =
        MemberVerificationViewModel::class.java

    private fun TextInputEditText.onFocusAndTextChange(
        idx: Array<Int>?
    ) {
        doAfterTextChanged { text ->
            idx?.run {
                var value = text.toString().replace("-", "")
                for (_idx in idx) {
                    if (_idx < value.length) {
                        value = value.substring(0, _idx) + "-" + value.substring(_idx)
                    }
                }
                if (text?.length!! != value.length) {
                    text.clear()
                    text.insert(0, value)
                }
            }

        }
    }

    private val args by navArgs<MemberVerificationFragmentArgs>()
    override fun onFragmentStart() {
        dataBinding.fragment = this
        dataBinding.vm = viewModel
        viewModel.initData(args.memberVerificationContext)
        dataBinding.editCitizenNo.onFocusAndTextChange(arrayOf(1, 6, 12, 15))
        dataBinding.editAccountNo.onFocusAndTextChange(arrayOf(3, 5, 11))
        dataBinding.editMobileNo.onFocusAndTextChange(arrayOf(3, 7))
//        dataBinding.editMemberCode.onFocusAndTextChange(null)

        dataBinding.showNavBack =
            args.memberVerificationContext.verificationType == VerificationType.FORGOT_PIN

        dataBinding.editCitizenNo.setOnFocusChangeListener { _, isFocus ->
            if (!isFocus && !AppHelper.isThaiCitizenNo(
                    dataBinding.editCitizenNo.text.toString().replace("-", "")
                )
            ) {
                dataBinding.editCitizenNo.showError(
                    dataBinding.inputCitizenNo, getString(
                        R.string.error_input,
                        getString(R.string.label_member_validation_citizen_no)
                    )
                )
            } else {
                dataBinding.editCitizenNo.clearError(dataBinding.inputCitizenNo)
            }
        }

        if (args.memberVerificationContext.verificationType == VerificationType.REGISTER) {
            dataBinding.editAccountNo.setOnFocusChangeListener { _, isFocus ->
                if (!isFocus && dataBinding.editAccountNo.text.toString().replace("-", "").length != 10
                ) {
                    dataBinding.editAccountNo.showError(
                        dataBinding.inputAccountNo, getString(
                            R.string.error_input,
                            getString(R.string.label_member_validation_account_no)
                        )
                    )
                } else {
                    dataBinding.editAccountNo.clearError(dataBinding.inputAccountNo)
                }
            }
        }

        dataBinding.editMobileNo.setOnFocusChangeListener { _, isFocus ->
            if (!isFocus && !AppHelper.isMobileNo(dataBinding.editMobileNo.text.toString().replace("-", ""))
            ) {
                dataBinding.editMobileNo.showError(
                    dataBinding.inputMobileNo, getString(
                        R.string.error_input,
                        getString(R.string.label_member_validation_mobile_no)
                    )
                )
            } else {
                dataBinding.editMobileNo.clearError(dataBinding.inputMobileNo)
            }
        }

        dataBinding.editMemberCode.setOnFocusChangeListener { _, isFocus ->
            if (!isFocus && dataBinding.editMemberCode.length() < 5
            ) {
                dataBinding.editMemberCode.showError(
                    dataBinding.inputMemberCode, getString(
                        R.string.error_input,
                        getString(R.string.label_member_validation_member_code)
                    )
                )
            } else {
                dataBinding.editMemberCode.clearError(dataBinding.inputMemberCode)
            }
        }
        dataBinding.buttonConfirm.setOnFocusChangeListener { _, isFocus ->
            if (isFocus)
                hideKeyboard()
        }
    }


    fun onButtonConfirmClick() {

        if ((args.memberVerificationContext.verificationType == VerificationType.REGISTER
                    || args.memberVerificationContext.verificationType == VerificationType.UNLOCK)
            && dataBinding.editAccountNo.length() < 10
        ) {
            dataBinding.editAccountNo.showError(
                dataBinding.inputAccountNo, getString(
                    R.string.error_input,
                    getString(R.string.label_member_validation_account_no)
                )
            )
        } else {
            dataBinding.editAccountNo.clearError(dataBinding.inputAccountNo)
        }

        if (dataBinding.editCitizenNo.length() < 10) {
            dataBinding.editCitizenNo.showError(
                dataBinding.inputCitizenNo, getString(
                    R.string.error_input,
                    getString(R.string.label_member_validation_citizen_no)
                )
            )
        } else {
            dataBinding.editCitizenNo.clearError(dataBinding.inputCitizenNo)
        }

        if (dataBinding.editMobileNo.length() < 10) {
            dataBinding.editMobileNo.showError(
                dataBinding.inputMobileNo, getString(
                    R.string.error_input,
                    getString(R.string.label_member_validation_mobile_no)
                )
            )
        } else {
            dataBinding.editMobileNo.clearError(dataBinding.inputMobileNo)
        }

        if (dataBinding.editMemberCode.length() < 5) {
            dataBinding.editMemberCode.showError(
                dataBinding.inputMemberCode, getString(
                    R.string.error_input,
                    getString(R.string.label_member_validation_member_code)
                )
            )
        } else {
            dataBinding.editMemberCode.clearError(dataBinding.inputMemberCode)
        }


        if (!(dataBinding.inputAccountNo.error.isNullOrEmpty() && dataBinding.inputCitizenNo.error.isNullOrEmpty()
                    && dataBinding.inputMobileNo.error.isNullOrEmpty() && dataBinding.inputMemberCode.error.isNullOrEmpty())
        ) {
            return
        }


        val isValid = when (args.memberVerificationContext.verificationType) {
            VerificationType.REGISTER -> {
                dataBinding.inputAccountNo.error.isNullOrBlank()
            }
            else -> {
                true
            }
        }

        if (isValid) {
            viewModel.memberVerification()
        }
    }

//    override fun onNavBackClick() {
//        if (args.memberVerificationContext.verificationType == VerificationType.FORGOT_PIN) {
//            gotoPage(MemberVerificationFragmentDirections.gotoLogin())
//        }
//    }


}