package com.krungsri.coop.ui.component.member.pin

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentMemberPinNewBinding
import com.krungsri.coop.extension.visible
import com.krungsri.coop.ui.base.BaseDialog
import com.krungsri.coop.ui.base.PinListener


class MemberPinDialog(
    private var onEnterPin: ((pinNo: String) -> Unit),
    private val onCancelPin: (() -> Unit)
) : BaseDialog(),
    PinListener {

    private val pinNoValue = MutableLiveData<String>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireContext(), R.style.FullScreenDialog)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.setWindowAnimations(R.style.DialogShow)
        dialog.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        dialog.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                dismiss()
                true
            } else {
                false
            }
        }
        return dialog
    }

    override fun dismiss() {
        super.dismiss()
        pinNoValue.value?.run {
            onEnterPin.invoke(this)
        } ?: onCancelPin.invoke()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
    }

    companion object {
        private const val TAG = "MemberPinDialog"
        const val DIALOG_TITLE = "DIALOG_TITLE"
        const val DIALOG_DESC = "DIALOG_DESC"
    }

    fun show(manager: FragmentManager) {
        super.show(
            manager,
            TAG
        )
    }

    var dataBinding: FragmentMemberPinNewBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_member_pin_new,
            container,
            false
        )
        dataBinding?.pinListener = this
        dataBinding?.imgClose?.visible()
        dataBinding?.imgClose?.setOnClickListener {
            pinNoValue.value = null
            dismiss()
        }
        arguments?.run {
            dataBinding?.textTitle = getString(DIALOG_TITLE)
            dataBinding?.textDesc?.text = getString(DIALOG_DESC)
        }
        dataBinding?.pinDot?.onKeyCompleted = { pinNo ->
            pinNoValue.value = pinNo
            dismiss()
        }
        return dataBinding?.root
    }

    override fun onPinClick(pinNo: String) {
        dataBinding?.pinDot?.addPIN(pinNo)

    }
}