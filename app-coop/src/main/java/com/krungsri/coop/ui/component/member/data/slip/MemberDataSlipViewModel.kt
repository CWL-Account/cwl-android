package com.krungsri.coop.ui.component.member.data.slip

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.member.MemberSlipRequest
import com.krungsri.coop.model.member.MemberSlipResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.usecase.member.MemberDataSlipUseCase
import javax.inject.Inject

class MemberDataSlipViewModel @Inject constructor(
    private val memberDataSlipUseCase: MemberDataSlipUseCase
) : BaseViewModel() {

    val memberSlip = MutableLiveData<MemberSlipResponse>()
    val notFoundMemberSlip = MutableLiveData<Boolean>()
    val currentMonth = MutableLiveData<String>()
    val currentYear = MutableLiveData<String>()

    fun getMemberSlip() {
        if (currentMonth.value != null && currentYear.value != null) {
            val month = "${currentYear.value}-${currentMonth.value}"
            val request = MemberSlipRequest(month = month)
            memberDataSlipUseCase.build(request)
                .doToggleLoading()
                .subscribeWithViewModel(this, {
                    memberSlip.value = it
                    notFoundMemberSlip.value = false
                }, this::onApiError)
                .disposedBy(this)
        }
    }

    override fun onApiError(apiError: ApiError) {
//        super.onApiError(apiError)
        notFoundMemberSlip.value = true
    }
}