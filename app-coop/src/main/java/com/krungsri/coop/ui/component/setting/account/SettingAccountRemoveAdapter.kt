package com.krungsri.coop.ui.component.setting.account

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.ItemSettingAccountRemoveBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.string
import com.krungsri.coop.extension.visible
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.model.account.AccountInfo
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter

class SettingAccountRemoveAdapter(private val onRemoveAccount: ((accountInfo: AccountInfo) -> Unit)) :
    BaseRecyclerViewAdapter<AccountInfo, ItemSettingAccountRemoveBinding>() {
    override fun createBinding(parent: ViewGroup): ItemSettingAccountRemoveBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_setting_account_remove,
            parent,
            false
        )
    }

    private var checkAccountType: AccountType? = null

    override fun bind(binding: ItemSettingAccountRemoveBinding, position: Int, item: AccountInfo) {
        if (checkAccountType == null || checkAccountType != item.accountType) {
            when {
                item.productName != null -> {
                    binding.textTitle.text = item.productName
                }
                AccountType.LOAN == item.accountType -> {
                    binding.textTitle.string(R.string.label_account_type_loan)
                }
                else -> {
                    binding.textTitle.string(R.string.label_account_type_saving)
                }
            }
            binding.textTitle.visible()
            checkAccountType = item.accountType
        } else {
            binding.textTitle.gone()
        }
        binding.adapter = this
        binding.accountInfo = item
        binding.textAccountDesc.text = item.accountDesc
        binding.textAccountNo.text =
            FormatHelper.formatAccountNo(item.coopAccountNo ?: "", AppConstant.IS_MARK_ACCOUNT)
    }

    fun removeItem(item: AccountInfo) {
        checkAccountType = null
        deleteWithNotifyAll(getItems()?.indexOf(item) ?: -1)
    }

    fun removeAccount(accountInfo: AccountInfo) {
        onRemoveAccount.invoke(accountInfo)
    }

}
