package com.krungsri.coop.ui.base.menubar

interface MenuBarListener {
    fun onBackClick()
}