package com.krungsri.coop.ui.component.otp

import android.os.Parcelable
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.policy.PolicyFlag
import com.krungsri.coop.model.transaction.TransactionConfirmRequest
import kotlinx.parcelize.Parcelize

@Parcelize
data class OTPVerificationContext(
        val verificationType: VerificationType,
        val verificationToken: String? = null,
        val bayAccountNo: String? = null,
        val citizenNo: String? = null,
        val memberCode: String? = null,
        val mobileNo: String? = null,
        var forceRegister: Flag? = null,
        val policyFlag: MutableList<PolicyFlag>? = null,
        val transactionConfirm: TransactionConfirmRequest? = null
) : Parcelable