package com.krungsri.coop.ui.component.policy

import android.os.Parcelable
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.policy.PolicyFlag
import kotlinx.parcelize.Parcelize

@Parcelize
data class PolicyContext(
    val isRequestToRegister: Boolean,
    val isRequestToAddAccount: Boolean = false,
    var acceptFlagBAY: Flag? = Flag.N,
    val isNewPolicy: Flag = Flag.N,
    var policyFlag: MutableList<PolicyFlag>? = null
) : Parcelable