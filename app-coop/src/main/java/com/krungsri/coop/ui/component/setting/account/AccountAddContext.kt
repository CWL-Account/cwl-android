package com.krungsri.coop.ui.component.setting.account

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AccountAddContext(val verificationToken: String) : Parcelable