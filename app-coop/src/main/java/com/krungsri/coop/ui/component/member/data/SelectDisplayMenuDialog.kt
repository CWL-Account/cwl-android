package com.krungsri.coop.ui.component.member.data

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.FragmentManager
import com.krungsri.coop.databinding.DialogMemberInfoDisplayMenuBinding
import com.krungsri.coop.ui.base.BaseDialog


class SelectDisplayMenuDialog(
    private val onImageCaptureSelected: () -> Unit,
    private val onImagePickupSelected: () -> Unit
) : BaseDialog() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DialogMemberInfoDisplayMenuBinding
            .inflate(inflater, container, false)
        binding.buttonCapture.setOnClickListener {
            dismiss()
            onImageCaptureSelected.invoke()
        }
        binding.buttonPickup.setOnClickListener {
            dismiss()
            onImagePickupSelected.invoke()
        }
        binding.buttonClose.setOnClickListener {
            dismiss()
        }
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        val wlp = dialog.window?.attributes
        wlp?.run {
            gravity = Gravity.BOTTOM
            flags = flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
            dialog.window?.setAttributes(wlp)
        }
        return dialog
    }

    fun show(manager: FragmentManager) {
        super.show(
            manager,
            "SelectDisplayMenuDialog"
        )
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
}