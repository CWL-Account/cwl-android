package com.krungsri.coop.ui.component.transaction.confirm

import android.os.Parcelable
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.transaction.DestinationAccount
import com.krungsri.coop.model.transaction.SourceAccount
import com.krungsri.coop.model.transaction.TransactionInquiryResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class TrxConfirmContext(
    val serviceCode: ServiceCode,
    val sourceAccount: SourceAccount,
    val destinationAccount: DestinationAccount,
    val inquiryResponse: TransactionInquiryResponse,
    val note: String? = null,
    val coopMessage: String? = null
) : Parcelable