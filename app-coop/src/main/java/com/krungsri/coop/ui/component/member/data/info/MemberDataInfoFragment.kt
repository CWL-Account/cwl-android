package com.krungsri.coop.ui.component.member.data.info

import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentMemberDataInfoBinding
import com.krungsri.coop.extension.loadCircle
import com.krungsri.coop.extension.setAmount
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.ui.base.BaseFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class MemberDataInfoFragment : BaseFragment<FragmentMemberDataInfoBinding, MemberDataInfoViewModel>() {
    override val layoutId: Int = R.layout.fragment_member_data_info
    override val viewModelClass: Class<MemberDataInfoViewModel> = MemberDataInfoViewModel::class.java

    private val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
//    private val sdfTH = SimpleDateFormat("dd MMMM yyyy", Locale("th", "TH"))
    private val calendar = Calendar.getInstance();
    private lateinit var months: Array<String>

    private val args by navArgs<MemberDataInfoFragmentArgs>()

    override fun onFragmentStart() {

        val monthArray = resources.getStringArray(R.array.months_fullname)
        val memberCode = AppPrefs.getStaticString(AppConstant.MEMBER_CODE)
        val firstName = AppPrefs.getString(AppConstant.FIRST_NAME, "")
        val lastName = AppPrefs.getString(AppConstant.LAST_NAME, "")
        dataBinding.textMemberCode.text = getString(R.string.label_member_info_no, memberCode)
        dataBinding.textMemberName.text = "$firstName $lastName"
        val calendar = Calendar.getInstance()
        val date = calendar.get(Calendar.DATE).let {
            (if (it < 10) "0" else "") + it
        }
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        dataBinding.textInfoDate.text =
            getString(R.string.label_member_data_slip_date, "$date ${monthArray[month]} ${year + 543}")


        months = resources.getStringArray(R.array.months_fullname)

        val info = args.dataInfoContext.dataInfo;
        dataBinding.textCitizenNo.text = info.citizenNo?.let { FormatHelper.formatCitizenNo(it) } ?: "-"
        dataBinding.textDateOfBirth.text = info.dateOfBirth?.let { toThDate(it) } ?: "-"
        dataBinding.textAge.text = info.age?.let {
            getString(R.string.year, it.toInt())
        } ?: "-"
        dataBinding.textSpouse.text = info.spouseName ?: "-"
        dataBinding.textPosition.text = info.positionTitle ?: "-"
        dataBinding.textAffiliate.text = info.affiliate ?: "-"
        dataBinding.textSalary.setAmount(info.salary, true, "-")
        dataBinding.textMemberSince.text = info.memberSince?.let {
            val years = if (it.years ?: 0 > 0) {
                getString(R.string.year, it.years) + " "
            } else {
                ""
            }
            val months = if (it.months ?: 0 > 0) {
                getString(R.string.month, it.months) + " "
            } else {
                ""
            }
            val days = if (it.days ?: 0 > 0) {
                getString(R.string.day, it.days)
            } else {
                ""
            }
            "$years$months$days"
        } ?: "-"
        dataBinding.textMemberDate.text = info.memberDate ?: "-"
        dataBinding.textLoanInterest.setAmount(info.totalLoanInterest, true, "-")
        dataBinding.textNoOfPeriod.text = "${info.noOfPeriod}"
        dataBinding.textPaymentAmount.setAmount(info.stockPaymentAmount, true, "-")
        dataBinding.textCapitalStock.setAmount(info.capitalStock, true, "-")

        GlobalScope.launch(Dispatchers.IO) {
            AppPrefs.getString("memberDisplay")?.run {
                val memberDisplay = File(requireContext().dataDir, this)
                if (memberDisplay.exists()) {
                    launch(Dispatchers.Main) {
                        dataBinding.imgPerson.loadCircle(memberDisplay)
                    }
                }
            }
        }
    }

    private fun toThDate(dateString: String): String {
        return toDate(dateString)?.let {
            calendar.time = it
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH).let { mm ->
                months[mm]
            }
            val date = calendar.get(Calendar.DATE)

            "$date $month $year"
        } ?: "-"
    }

    private fun toDate(dateString: String): Date? {
        return try {
            sdf.parse(dateString)
        } catch (e: Exception) {
            null
        }
    }
}