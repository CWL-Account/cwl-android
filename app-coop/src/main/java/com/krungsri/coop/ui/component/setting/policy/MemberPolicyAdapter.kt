package com.krungsri.coop.ui.component.setting.policy

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentPolicyBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.visible
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter
import com.krungsri.coop.ui.component.policy.initView
import java.util.*

class MemberPolicyAdapter(val fragment: MemberPolicyFragment) : BaseRecyclerViewAdapter<PolicyResponse, FragmentPolicyBinding>() {

    override fun createBinding(parent: ViewGroup): FragmentPolicyBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.fragment_policy,
            parent,
            false
        )
    }

    override fun bind(binding: FragmentPolicyBinding, position: Int, item: PolicyResponse) {
        binding.initView(binding.root, fragment, item)
        binding.layoutButton.gone()
        binding.layoutAccepted.gone()

//        BuddhistCalendar.getInstance()

        item.acceptedDate?.run {
            binding.layoutAccepted.visible()
            binding.textAcceptedVersion.gone()
            val calendar = Calendar.getInstance()
            calendar.time = this
            val date = calendar.get(Calendar.DAY_OF_MONTH)
            val year = calendar.get(Calendar.YEAR) + 543
            val thDate =
                "$date " + binding.textAcceptedDate.context.resources.getStringArray(R.array.months_fullname)[calendar.get(Calendar.MONTH)] + " $year"
            val hr = calendar.get(Calendar.HOUR_OF_DAY);
            val mm = calendar.get(Calendar.MINUTE)
            val time = (if (hr < 10) "0" else "") + hr + ":" + (if (mm < 10) "0" else "") + mm
            binding.textAcceptedDate.text = binding.textAcceptedDate.context.getString(R.string.label_policy_accepted_date, "$thDate $time")
            item.versionNo?.run {
                binding.textAcceptedVersion.visible()
                binding.textAcceptedVersion.text =
                    binding.textAcceptedDate.context.getString(R.string.label_policy_accepted_version, item.versionNo.toString())
            }
        }

    }
}