package com.krungsri.coop.ui.base

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.krungsri.coop.R
import com.krungsri.coop.databinding.ViewPinDotBinding
import timber.log.Timber

interface PinListener {
    fun onPinClick(pinNo: String)
}

class PINDotView @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

    private val dataBinding: ViewPinDotBinding = ViewPinDotBinding.bind(
        LayoutInflater.from(context)
            .inflate(R.layout.view_pin_dot, this, false)
    )

    init {
        addView(dataBinding.root)
    }

    private var pinNoValue = "";
    var onKeyCompleted: ((pinNo: String) -> Unit)? = null
    var onKeypress: (() -> Unit)? = null

    fun clearPin() {
        pinNoValue = ""
        addPIN("")
    }

    fun addPIN(pinNo: String) {
        when {
            pinNo.equals("DEL", true) -> {
                pinNoValue = if (pinNoValue.isNotEmpty()) pinNoValue.substring(0, pinNoValue.length - 1) else pinNoValue
            }
            pinNo.equals("BIO", true) -> {
                return
            }
            else -> {
                pinNoValue += pinNo
            }
        }

        Timber.d("pinNo = %s, pinNoValue %s ", pinNo, pinNoValue)

        if (pinNoValue.length != 6) {
            onKeypress?.invoke()
        }
        when (pinNoValue.length) {
            0 -> {
                dataBinding.editPin1.text = null
                dataBinding.editPin2.text = null
                dataBinding.editPin3.text = null
                dataBinding.editPin4.text = null
                dataBinding.editPin5.text = null
                dataBinding.editPin6.text = null
            }
            1 -> {
                dataBinding.editPin1.setText(pinNo)
                dataBinding.editPin2.text = null
                dataBinding.editPin3.text = null
                dataBinding.editPin4.text = null
                dataBinding.editPin5.text = null
                dataBinding.editPin6.text = null
            }
            2 -> {
                dataBinding.editPin2.setText(pinNo)
                dataBinding.editPin3.text = null
                dataBinding.editPin4.text = null
                dataBinding.editPin5.text = null
                dataBinding.editPin6.text = null
            }
            3 -> {
                dataBinding.editPin3.setText(pinNo)
                dataBinding.editPin4.text = null
                dataBinding.editPin5.text = null
                dataBinding.editPin6.text = null
            }
            4 -> {
                dataBinding.editPin4.setText(pinNo)
                dataBinding.editPin5.text = null
                dataBinding.editPin6.text = null
            }
            5 -> {
                dataBinding.editPin5.setText(pinNo)
                dataBinding.editPin6.text = null
            }
            6 -> {
                dataBinding.editPin6.setText(pinNo)
                onKeyCompleted?.invoke(pinNoValue)
            }
        }
    }


}