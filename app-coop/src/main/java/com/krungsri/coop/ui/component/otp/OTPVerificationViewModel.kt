package com.krungsri.coop.ui.component.otp

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.member.*
import com.krungsri.coop.model.otp.OTPRequest
import com.krungsri.coop.model.otp.OTPResponse
import com.krungsri.coop.model.otp.OTPVerificationRequest
import com.krungsri.coop.model.otp.OTPVerificationResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.member.landing.LandingDestination
import com.krungsri.coop.ui.component.member.landing.MemberLandingContext
import com.krungsri.coop.ui.component.member.pin.MemberPinContext
import com.krungsri.coop.ui.component.setting.account.AccountAddContext
import com.krungsri.coop.usecase.member.MemberChangePinConfirmUseCase
import com.krungsri.coop.usecase.member.MemberForgotPinUseCase
import com.krungsri.coop.usecase.member.MemberRegistrationUseCase
import com.krungsri.coop.usecase.member.MemberUnlockUseCase
import com.krungsri.coop.usecase.otp.OTPVerificationUseCase
import com.krungsri.coop.usecase.otp.RequestNewOTPUseCase
import com.krungsri.coop.usecase.transaction.ConfirmTransactionUseCase
import timber.log.Timber
import javax.inject.Inject

class OTPVerificationViewModel @Inject
constructor(
    private val localSession: LocalSession,
    private val requestNewOTPUseCase: RequestNewOTPUseCase,
    private val otpVerificationUseCase: OTPVerificationUseCase,
    private val memberRegistrationUseCase: MemberRegistrationUseCase,
    private val memberForgotPinUseCase: MemberForgotPinUseCase,
    private val memberUnlockUseCase: MemberUnlockUseCase,
    private val memberChangePinConfirmUseCase: MemberChangePinConfirmUseCase,
    private val confirmTransactionUseCase: ConfirmTransactionUseCase
) : BaseViewModel() {

    private val otpVerificationContext = MutableLiveData<OTPVerificationContext>()

    val otpResponse = MutableLiveData<OTPResponse>()

    val delaySec = 60
    val otpResentCount = MutableLiveData<Long>()

    fun initData(_otpVerificationContext: OTPVerificationContext) {
        otpVerificationContext.value = _otpVerificationContext
        requestNewOTP()
    }

    fun requestNewOTP() {
        requestNewOTPUseCase.build(
            OTPRequest(
                verificationToken = otpResponse.value?.verificationToken
                    ?: otpVerificationContext.value?.verificationToken
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onOtpRequestSuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    private fun onOtpRequestSuccess(response: OTPResponse) {
        otpResponse.value = response
    }

    fun otpValidation(otpNo: String) {
        otpResponse.value?.run {
            otpVerificationUseCase.build(
                OTPVerificationRequest(
                    verificationToken = verificationToken,
                    otpNo = otpNo
                )
            ).doToggleLoading()
                .subscribeWithViewModel(
                    this@OTPVerificationViewModel,
                    this@OTPVerificationViewModel::onOtpValidationSuccess,
                    this@OTPVerificationViewModel::onApiError
                )
                .disposedBy(this@OTPVerificationViewModel)
        }
    }

    private fun onOtpValidationSuccess(response: OTPVerificationResponse) {
        otpVerificationContext.value?.run {
            Timber.d("verificationType = %s", verificationType)
            when (verificationType) {
                VerificationType.REGISTER -> handleVerificationRegister(this, response)
                VerificationType.UNLOCK -> handleVerificationUnlock(response)
                VerificationType.FORGOT_PIN -> handleVerificationForgotPin(response)
                VerificationType.CHANGE_PIN -> handleVerificationChangePin(response)
                VerificationType.ADD_ACCOUNT -> gotoPage(
                    OTPVerificationFragmentDirections.gotoAddCoopAccountPage(
                        AccountAddContext(verificationToken = response.verificationToken)
                    )
                )
            }
        }
    }

    private fun handleVerificationRegister(
        verificationContext:
        OTPVerificationContext, response: OTPVerificationResponse
    ) {
        Timber.d("handleVerificationRegister")
        memberRegistrationUseCase.build(
            MemberRegistrationRequest(
                verificationToken = response.verificationToken,
                forceRegister = verificationContext.forceRegister,
                memberCode = verificationContext.memberCode ?: "",
                citizenNo = verificationContext.citizenNo ?: "",
                bayAccountNo = verificationContext.bayAccountNo ?: "",
                mobileNo = verificationContext.mobileNo ?: "",
                policyFlag = verificationContext.policyFlag,
                deviceInfo = DeviceInfoRequest(
                    uuid = AppHelper.getDeviceId(appContext),
                    notificationToken = ""
                )
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onMemberRegistrationSuccess,
                this::onApiError
            )
            .disposedBy(this@OTPVerificationViewModel)
    }


    private fun handleVerificationForgotPin(
        response: OTPVerificationResponse
    ) {
        Timber.d("handleVerificationForgotPin")
        memberForgotPinUseCase.build(
            MemberForgotPinRequest(
                verificationToken = response.verificationToken
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onMemberForgotPinSuccess,
                this::onApiError
            )
            .disposedBy(this@OTPVerificationViewModel)

    }


    private fun handleVerificationUnlock(
        response: OTPVerificationResponse
    ) {
        Timber.d("handleVerificationUnlock")
        memberUnlockUseCase.build(
            MemberUnlockRequest(
                verificationToken = response.verificationToken
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onMemberUnlockSuccess,
                this::onApiError
            )
            .disposedBy(this)

    }


    private fun handleVerificationChangePin(
        response: OTPVerificationResponse
    ) {
        Timber.d("handleVerificationChangePin")
        memberChangePinConfirmUseCase.build(
            MemberChangePinConfirmRequest(
                verificationToken = response.verificationToken
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onMemberChangePinSuccess,
                this::onApiError
            )
            .disposedBy(this)

    }

    private fun onMemberRegistrationSuccess(response: MemberLoginResponse) {
        gotoPage(
            OTPVerificationFragmentDirections.gotoSetupNewPinPageForRegister(
                MemberPinContext(
//                    destination = LandingDestination.NEW_PIN_REGISTER,
                    destination = LandingDestination.HOME_FROM_AFTER_NEW_PIN,
                    memberCode = otpVerificationContext.value?.memberCode
                )
            )
        )
    }

    private fun onMemberForgotPinSuccess(response: MemberForgotPinResponse) {
        gotoPage(
            OTPVerificationFragmentDirections.gotoLandingPageWithNoLogin(
                MemberLandingContext(
                    verificationType = otpVerificationContext.value?.verificationType,
                    destination = LandingDestination.NEW_PIN_FORGOT_PIN
                )
            )
        )
    }

    private fun onMemberUnlockSuccess(response: MemberLoginResponse) {
        gotoPage(
            OTPVerificationFragmentDirections.gotoLandingPageWithNoLogin(
                MemberLandingContext(
                    verificationType = otpVerificationContext.value?.verificationType,
                    destination = LandingDestination.NEW_PIN_UNLOCK
                )
            )
        )
    }

    private fun onMemberChangePinSuccess(response: Boolean) {
        if (response) {
            gotoPage(
                OTPVerificationFragmentDirections.gotoLandingPageWithChangePin(
                    MemberLandingContext(
                        verificationType = otpVerificationContext.value?.verificationType,
                        destination = LandingDestination.CHANGE_PIN
                    )
                )
            )
        }
    }


    override fun onApiError(apiError: ApiError) {
        otpVerificationContext.value?.run {
            when (verificationType) {
                VerificationType.REGISTER -> handleApiErrorRegisterFlow(apiError)
                else -> {
                    if (AppConstant.CODE_OTP_INVALID == apiError.code || AppConstant.CODE_OTP_EXPIRED == apiError.code) {
                        super.onApiError(apiError)
                    } else {
                        showOkAlert(apiError, onDismiss = {
                            goBackPage()
                        })
                    }
                }
            }
        } ?: super.onApiError(apiError)
    }


    private fun handleApiErrorRegisterFlow(apiError: ApiError) {
        when (apiError.code) {
            AppConstant.CODE_TOKEN_VERIFY_EXP, AppConstant.CODE_DUPLICATE_DEVICE, AppConstant.CODE_DUPLICATE_MEMBER -> {
                showOkAlert(
                    apiError,
                    onDismiss = {
                        popBackStack()
                    })
            }
            else -> {
                super.onApiError(apiError)
            }
        }
    }

    private fun goBackPage() {
        popBackStack()
    }
}