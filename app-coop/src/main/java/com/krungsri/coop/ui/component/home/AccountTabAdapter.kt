package com.krungsri.coop.ui.component.home

import android.content.Context
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DimenRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.google.android.material.tabs.TabLayoutMediator
import com.krungsri.coop.R
import com.krungsri.coop.databinding.LayoutAccountDescBinding
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.home.InquiryHomeResponse
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter
import timber.log.Timber

class AccountTabAdapter(private val listener: AdapterListener) :
    BaseRecyclerViewAdapter<InquiryHomeResponse, LayoutAccountDescBinding>() {
    override fun createBinding(parent: ViewGroup): LayoutAccountDescBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.layout_account_desc,
            parent,
            false
        )
    }

    override fun bind(binding: LayoutAccountDescBinding, position: Int, item: InquiryHomeResponse) {
        initView(binding, item)
        binding.productCode = item.productCode
        TabLayoutMediator(binding.tabLayoutDots, binding.viewPager) { _, _ ->

        }.attach()
    }


    private fun initView(
        binding: LayoutAccountDescBinding,
        item: InquiryHomeResponse
    ) {
        val context = binding.root.context
        val adapter = AccountDescAdapter(item.productCode)
        adapter.replace(item.accounts)
        binding.viewPager.adapter = adapter
        binding.viewPager.offscreenPageLimit = 1
        val nextItemVisiblePx = context.resources.getDimension(R.dimen.viewpager_next_item_visible)
        val currentItemHorizontalMarginPx =
            context.resources.getDimension(R.dimen.viewpager_current_item_horizontal_margin)
        val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx
        val pageTransformer = ViewPager2.PageTransformer { page: View, position: Float ->
            page.translationX = -pageTranslationX * position
            page.scaleY = 1 - (0.25f * StrictMath.abs(position))
        }
        binding.viewPager.setPageTransformer(pageTransformer)
        val itemDecoration = HorizontalMarginItemDecoration(
            context,
            R.dimen.viewpager_current_item_horizontal_margin
        )
        binding.viewPager.addItemDecoration(itemDecoration)

        if (item.accounts.size == 1) {
            binding.accountDesc = item.accounts[0]
            initService(binding, item)
        } else if (adapter.itemCount > 1) {
            binding.viewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    binding.accountDesc = adapter.getItem(position)
                    initService(binding, item)
                }
            })
        }
    }

    private fun initService(
        binding: LayoutAccountDescBinding,
        item: InquiryHomeResponse
    ) {
        binding.buttonServiceTransfer.setDisable(true)
        binding.buttonServiceWithdraw.setDisable(true)
        binding.buttonServiceDeposit.setDisable(true)
        binding.buttonServiceHistory.setDisable(true)
//        binding.buttonServiceBarcode.setDisable(true)
        binding.buttonServiceLoanPayment.setDisable(true)
        binding.buttonServiceLoanWithdraw.setDisable(true)
        binding.buttonServiceLoanHistory.setDisable(true)
//        binding.buttonServiceBillPayment.setDisable(true)
        Timber.d("initService : %s, %s", item.productName, binding.accountDesc?.accountType)
        for (service in item.services) {
            Timber.d("initService : %s", service.serviceCode)
            if (binding.accountDesc?.accountType != AccountType.LOAN) {
                when (service.serviceCode) {
                    ServiceCode.TRANSFERRING -> {
//                        binding.buttonServiceTransfer.setTitleName(service.serviceName)
                        if (binding.accountDesc?.withdrawFlag == Flag.Y) {
                            binding.buttonServiceTransfer.setDisable(false)
                            binding.buttonServiceTransfer.setOnClickListener {
                                listener.onCoopServiceClick(
                                    service,
                                    item.productCode,
                                    item.accounts[binding.viewPager.currentItem]
                                )
                            }
                        }
                    }
                    ServiceCode.WITHDRAW -> {
//                        binding.buttonServiceWithdraw.setTitleName(service.serviceName)
                        if (binding.accountDesc?.withdrawFlag == Flag.Y) {
                            binding.buttonServiceWithdraw.setDisable(false)
                            binding.buttonServiceWithdraw.setOnClickListener {
                                listener.onCoopServiceClick(
                                    service,
                                    item.productCode,
                                    item.accounts[binding.viewPager.currentItem]
                                )
                            }
                        }
                    }
                    ServiceCode.DEPOSIT -> {
//                        binding.buttonServiceDeposit.setTitleName(service.serviceName)
                        if (binding.accountDesc?.depositFlag == Flag.Y) {
                            binding.buttonServiceDeposit.setDisable(false)
                            binding.buttonServiceDeposit.setOnClickListener {
                                listener.onCoopServiceClick(
                                    service,
                                    item.productCode,
                                    item.accounts[binding.viewPager.currentItem]
                                )
                            }
                        }
                    }
                    ServiceCode.TRANSACTION_HISTORY -> {
//                        binding.buttonServiceHistory.setTitleName(service.serviceName)
                        binding.buttonServiceHistory.setDisable(false)
                        binding.buttonServiceHistory.setOnClickListener {
                            listener.onHistoryClick(
                                item.productCode,
                                item.accounts[binding.viewPager.currentItem],
                                item.accounts
                            )
                        }
                    }
                    ServiceCode.SAVING_PAYMENT -> {
//                        binding.buttonServiceBarcode.setTitleName(service.serviceName)
//                        if (binding.accountDesc?.withdrawFlag == Flag.Y) {
//                            binding.buttonServiceBarcode.setDisable(false)
//                            binding.buttonServiceBarcode.setOnClickListener {
//                                listener.onCreateBarcode(service, item.accounts[binding.viewPager.currentItem])
//                            }
//                        }
                    }
                    else -> {
                    }
                }
            } else {
                when (service.serviceCode) {
                    ServiceCode.LOAN_WITHDRAW -> {
//                        binding.buttonServiceLoanWithdraw.setTitleName(service.serviceName)
                        if (binding.accountDesc?.withdrawFlag == Flag.Y) {
                            binding.buttonServiceLoanWithdraw.setDisable(false)
                            binding.buttonServiceLoanWithdraw.setOnClickListener {
                                listener.onCoopServiceClick(
                                    service,
                                    item.productCode,
                                    item.accounts[binding.viewPager.currentItem]
                                )
                            }
                        }
                    }
                    ServiceCode.LOAN_PAYMENT -> {
//                        binding.buttonServiceLoanPayment.setTitleName(service.serviceName)
                        if (binding.accountDesc?.depositFlag == Flag.Y) {
                            binding.buttonServiceLoanPayment.setDisable(false)
                            binding.buttonServiceLoanPayment.setOnClickListener {
                                listener.onCoopServiceClick(
                                    service,
                                    item.productCode,
                                    item.accounts[binding.viewPager.currentItem]
                                )
                            }
                        }
                    }
                    ServiceCode.BILL_PAYMENT -> {
//                        binding.buttonServiceBillPayment.setTitleName(service.serviceName)
//                        if (binding.accountDesc?.depositFlag == Flag.Y) {
//                            binding.buttonServiceBillPayment.setDisable(false)
//                            binding.buttonServiceBillPayment.setOnClickListener {
//                                listener.onCreateBarcode(service, item.accounts[binding.viewPager.currentItem])
//                            }
//                        }
                    }
                    ServiceCode.TRANSACTION_HISTORY -> {
                        binding.buttonServiceLoanHistory.setDisable(false)
//                        binding.buttonServiceLoanHistory.setTitleName(service.serviceName)
                        binding.buttonServiceLoanHistory.setOnClickListener {
                            listener.onHistoryClick(
                                item.productCode,
                                item.accounts[binding.viewPager.currentItem],
                                item.accounts
                            )
                        }
                    }
                    else -> {
                    }
                }
            }

        }
    }
}


class HorizontalMarginItemDecoration(context: Context, @DimenRes horizontalMarginInDp: Int) :
    RecyclerView.ItemDecoration() {

    private val horizontalMarginInPx: Int =
        context.resources.getDimension(horizontalMarginInDp).toInt()

    override fun getItemOffsets(
        outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State
    ) {
        outRect.right = horizontalMarginInPx
        outRect.left = horizontalMarginInPx
    }

}