package com.krungsri.coop.ui.component.setting

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.model.policy.PolicyType
import com.krungsri.coop.model.setting.BiometricSettingRequest
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.setting.policy.MemberPolicyContext
import com.krungsri.coop.usecase.account.AccountMaskingUseCase
import com.krungsri.coop.usecase.policy.GetMemberConsentUseCase
import com.krungsri.coop.usecase.policy.GetMemberTCUseCase
import com.krungsri.coop.usecase.setting.BiometricSettingUseCase
import javax.inject.Inject

class SettingViewModel @Inject
constructor(
    private val biometricSettingUseCase: BiometricSettingUseCase,
    private val getMemberTCUseCase: GetMemberTCUseCase,
    private val getMemberConsentUseCase: GetMemberConsentUseCase,
    private val accountMaskingUseCase: AccountMaskingUseCase
) : BaseViewModel() {
    val biometricSettingValue = MutableLiveData(false)

    fun initData() {
        biometricSettingValue.value = AppPrefs.hasString(AppConstant.BIOMETRIC_TOKEN)
    }

    fun onBiometricClick(pinNo: String) {
        val request = biometricSettingValue.value?.let {
            BiometricSettingRequest(
                pinNo = SecurityHelper.encryptPinNo(pinNo),
                uuid = AppHelper.getDeviceId(appContext),
                isEnable = if (pinNo.isEmpty()) Flag.N else Flag.Y
            )
        } ?: BiometricSettingRequest(
            uuid = AppHelper.getDeviceId(appContext),
            isEnable = Flag.N
        )
        biometricSettingUseCase.build(
            request
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                { onBiometricSettingSuccess(if (pinNo.isEmpty()) Flag.N else Flag.Y, it) },
                {
                    biometricSettingValue.value = false
                    onApiError(it)
                }
            )
            .disposedBy(this)
    }

    private fun onBiometricSettingSuccess(enable: Flag, response: Boolean) {
        if (enable == Flag.Y) {
            showOkAlert(message = getString(R.string.label_setting_scan_biometric_success))
        }
    }

    fun onAccountMasking(flag: Flag) {
        AppConstant.IS_MARK_ACCOUNT = flag == Flag.Y
        AppPrefs.setString(
            AppConstant.SET_MARK_ACCOUNT,
            AppConstant.IS_MARK_ACCOUNT.toString()
        )
//        accountMaskingUseCase.build(flag)
//            .doToggleLoading()
//            .subscribeWithViewModel(
//                this,
//                {
//                    AppConstant.IS_MARK_ACCOUNT = flag == Flag.Y
//                    AppPrefs.setString(
//                        AppConstant.SET_MARK_ACCOUNT,
//                        AppConstant.IS_MARK_ACCOUNT.toString()
//                    )
//                }, this::onAccountMaskingError
//            )
//            .disposedBy(this)

    }

    private fun onAccountMaskingError(apiError: ApiError) {
        AppConstant.IS_MARK_ACCOUNT = false
        AppPrefs.setString(
            AppConstant.SET_MARK_ACCOUNT,
            AppConstant.IS_MARK_ACCOUNT.toString()
        )
//        super.onApiError(apiError)
    }

    fun getMemberPolicy(policyType: PolicyType) {
        val opt = when (policyType) {
            PolicyType.BAY_TC, PolicyType.COOP_TC -> {
                getMemberTCUseCase.build()
                    .doToggleLoading()
            }
            PolicyType.BAY_CONSENT, PolicyType.COOP_CONSENT -> {
                getMemberConsentUseCase.build()
                    .doToggleLoading()
            }
            else -> return
        }

        opt.doToggleLoading()
            .subscribeWithViewModel(this, {
                onGetLatestPolicySuccess(policyType, it)
            }, this::onApiError)
            .disposedBy(this)
    }

    private fun onGetLatestPolicySuccess(
        policyType: PolicyType,
        responseList: MutableList<PolicyResponse>
    ) {
        if (responseList.isNullOrEmpty()) {
            val message = if (policyType == PolicyType.BAY_TC || policyType == PolicyType.COOP_TC) {
                appContext.getString(R.string.label_policy_tc_not_found)
            } else {
                appContext.getString(R.string.label_policy_consent_not_found)
            }
            showOkAlert(message = message)
        } else {
            gotoPage(
                SettingFragmentDirections.gotoMemberPolicyPage(
                    MemberPolicyContext(
                        policyType,
                        responseList
                    )
                )
            )
        }
    }
}