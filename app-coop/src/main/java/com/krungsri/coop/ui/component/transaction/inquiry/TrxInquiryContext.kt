package com.krungsri.coop.ui.component.transaction.inquiry

import android.os.Parcelable
import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.enumeration.ProductCode
import com.krungsri.coop.model.home.CoopService

import com.krungsri.coop.model.transaction.InitialFormResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class TrxInquiryContext(
    val coopService: CoopService,
    val productCode: ProductCode,
    val initialForm: InitialFormResponse,
    val selectedAccountDesc: AccountDesc
) : Parcelable