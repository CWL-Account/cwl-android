package com.krungsri.coop.ui.component.transaction.inquiry

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.transaction.DestinationAccount
import com.krungsri.coop.model.transaction.SourceAccount
import com.krungsri.coop.model.transaction.TransactionInquiryRequest
import com.krungsri.coop.model.transaction.TransactionInquiryResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.transaction.confirm.TrxConfirmContext
import com.krungsri.coop.usecase.transaction.InquiryTransactionUseCase
import timber.log.Timber
import java.math.BigDecimal
import javax.inject.Inject

class TrxInquiryViewModel @Inject
constructor(
    private val inquiryTransactionUseCase: InquiryTransactionUseCase
) : BaseViewModel() {


    private val inquiryContext = MutableLiveData<TrxInquiryContext>()

    val sourceAccountSelected = MutableLiveData<SourceAccount>()
    val destinationAccountSelected = MutableLiveData<DestinationAccount>()

    val destinationAccountList = MutableLiveData<MutableList<DestinationAccount>>()
    val sourceAccountList = MutableLiveData<MutableList<SourceAccount>>()
    val amount = MutableLiveData("")
    val noteText = MutableLiveData("")
    private var okButtonBg: Int = R.drawable.button_main_saving

    fun initData(trxInquiryContext: TrxInquiryContext) {
        inquiryContext.value = trxInquiryContext
        sourceAccountList.value = trxInquiryContext.initialForm.sourceAccount
        destinationAccountList.value = trxInquiryContext.initialForm.destinationAccountList

        if (trxInquiryContext.initialForm.sourceAccount?.size == 1) {
            sourceAccountSelected.value = trxInquiryContext.initialForm.sourceAccount[0]
        }
        when (trxInquiryContext.initialForm.serviceCode) {
            ServiceCode.LOAN_WITHDRAW,
            ServiceCode.LOAN_PAYMENT,
            ServiceCode.BILL_PAYMENT -> {
                okButtonBg = R.drawable.button_main_loan
            }
            else -> {
            }
        }
    }

    fun transactionInquiry() {
        if (destinationAccountSelected.value == null) {
            showOkAlert(
                message = appContext.getString(R.string.error_transaction_select_to_account),
                buttonBg = okButtonBg,
            )
            return
        }

        if (amount.value.isNullOrEmpty()) {
            showOkAlert(
                message = appContext.getString(R.string.error_transaction_input_amount),
                buttonBg = okButtonBg
            )
            return
        }

        val amountValue = amount.value?.replace(",", "")?.toBigDecimal() ?: BigDecimal.ZERO
        Timber.d("transactionInquiry %s", amountValue)
        inquiryContext.value?.let { inquiryContext ->
            inquiryTransactionUseCase.build(
                TransactionInquiryRequest(
                    verificationToken = inquiryContext.initialForm.verificationToken,
                    serviceCode = inquiryContext.initialForm.serviceCode,
                    fromCoopAccountNo = sourceAccountSelected.value?.coopAccountNo,
                    fromBayAccountNo = sourceAccountSelected.value?.bayAccountNo,
                    toCoopAccountNo = destinationAccountSelected.value?.coopAccountNo,
                    toBayAccountNo = destinationAccountSelected.value?.bayAccountNo,
                    amount = amountValue,
                    note = noteText.value
                )
            ).doToggleLoading()
                .subscribeWithViewModel(
                    this@TrxInquiryViewModel,
                    this@TrxInquiryViewModel::transactionInquirySuccess,
                    this@TrxInquiryViewModel::onApiError
                )
                .disposedBy(this@TrxInquiryViewModel)
        }
    }

    override fun onApiError(apiError: ApiError) {
        Timber.d("okButtonBg1 %s, %s", okButtonBg, R.drawable.button_main_loan)
        super.onApiError(apiError, buttonBg = okButtonBg)
    }

    private fun transactionInquirySuccess(response: ApiResponse<TransactionInquiryResponse>) {
        if (response.code == AppConstant.CODE_SUCCESS201 || response.code == AppConstant.CODE_SUCCESS200) {
            response.result?.run {
                gotoPage(
                    TrxInquiryFragmentDirections.gotoTransactionConfirmPage(
                        TrxConfirmContext(
                            serviceCode = inquiryContext.value?.initialForm?.serviceCode!!,
                            inquiryResponse = response.result,
                            sourceAccount = sourceAccountSelected.value!!,
                            destinationAccount = destinationAccountSelected.value!!,
                            note = noteText.value,
                            coopMessage = if (response.code == AppConstant.CODE_SUCCESS201) response.message else null
                        )
                    )
                )
            }
        } else {
            showOkAlert(code = response.code, message = response.message, buttonBg = okButtonBg)
        }
    }

}