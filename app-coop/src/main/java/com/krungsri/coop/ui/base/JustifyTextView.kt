package com.krungsri.coop.ui.base

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.text.StaticLayout
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import kotlin.math.ceil


class JustifyTextView(context: Context, attrs: AttributeSet?) : AppCompatTextView(context, attrs) {
    private var mLineY = 0F
    private var mViewWidth = 0

    override fun onDraw(canvas: Canvas) {
        val paint = paint
        paint.color = currentTextColor
        paint.drawableState = drawableState
        mViewWidth = measuredWidth - paddingStart - paddingEnd
        val text = text.toString()
        mLineY = 0F
        mLineY += textSize.toInt()
        val layout = layout ?: return


        val fm: Paint.FontMetrics = paint.fontMetrics
        var textHeight = ceil(fm.descent - fm.ascent).toInt()
        textHeight = (textHeight * layout.spacingMultiplier + layout.spacingAdd).toInt()
        for (i in 0 until layout.lineCount) {
            val lineStart = layout.getLineStart(i)
            val lineEnd = layout.getLineEnd(i)
            val width = StaticLayout.getDesiredWidth(text, lineStart, lineEnd, getPaint())
            val line = text.substring(lineStart, lineEnd)
            if (needScale(line) && i < layout.lineCount - 1) {
                drawScaledText(canvas, line, width)
            } else {
                canvas.drawText(line, 0F, mLineY, paint)
            }
            mLineY += textHeight
        }

    }

    private fun drawScaledText(canvas: Canvas, line: String, lineWidth: Float) {
        var line = line
        var x = 0f
        if (isFirstLineOfParagraph(line)) {
            val blanks = "  "
            canvas.drawText(blanks, x, mLineY, paint)
            val bw = StaticLayout.getDesiredWidth(blanks, paint)
            x += bw
            line = line.substring(3)
        }
        val gapCount = line.length - 1
        var i = 0
        if (line.length > 2 && line[0].toInt() == 12288 && line[1].toInt() == 12288) {
            val substring = line.substring(0, 2)
            val cw = StaticLayout.getDesiredWidth(substring, paint)
            canvas.drawText(substring, x, mLineY, paint)
            x += cw
            i += 2
        }
        val d = (mViewWidth - lineWidth) / gapCount
        while (i < line.length) {
            val c = line[i].toString()
            val cw = StaticLayout.getDesiredWidth(c, paint)
            canvas.drawText(c, x, mLineY, paint)
            x += cw + d
            i++
        }
    }

    private fun isFirstLineOfParagraph(line: String): Boolean {
        return line.length > 3 && line[0] == ' ' && line[1] == ' '
    }

    private fun needScale(line: String?): Boolean {
        return if (line == null || line.isEmpty()) {
            false
        } else {
            line[line.length - 1] != '\n'
        }
    }

    companion object {
        const val TWO_CHINESE_BLANK = "  "
    }
}