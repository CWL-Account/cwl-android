package com.krungsri.coop.ui.component.transaction.payment

import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.lib.lifecycle.MutableLiveEvent
import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.transaction.BillPaymentRequest
import com.krungsri.coop.model.transaction.BillPaymentResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.usecase.transaction.InquiryBillPaymentUseCase
import javax.inject.Inject

class BillPaymentViewModel @Inject
constructor(
    private val inquiryBillPaymentUseCase: InquiryBillPaymentUseCase
) : BaseViewModel() {

    val showBillPaymentDialog = MutableLiveEvent<BillPaymentResponse>()

    fun loadBillPayment(accountDesc: AccountDesc) {
        inquiryBillPaymentUseCase.build(
            BillPaymentRequest(
                serviceCode = ServiceCode.BILL_PAYMENT,
                coopAccountNo = accountDesc.accountNo
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::billPaymentSuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    private fun billPaymentSuccess(response: BillPaymentResponse) {
        showBillPaymentDialog.setEventValue(response)
    }
}