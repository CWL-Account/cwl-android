package com.krungsri.coop.ui.component.transaction.inquiry

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.ItemAccountDescBinding
import com.krungsri.coop.extension.loadCircle
import com.krungsri.coop.extension.setAmount
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.enumeration.ProductCode
import com.krungsri.coop.model.transaction.SourceAccount
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter

class SourceAccountAdapter(private val productCode: ProductCode) : BaseRecyclerViewAdapter<SourceAccount, ItemAccountDescBinding>() {
    override fun createBinding(parent: ViewGroup): ItemAccountDescBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_account_desc,
            parent,
            false
        )
    }

    override fun bind(binding: ItemAccountDescBinding, position: Int, item: SourceAccount) {
        binding.textAccountDesc.text = item.accountDesc
        binding.textAccountBalanceValue.setAmount(item.accountBalance)
        item.bayAccountNo?.run {
            binding.textAccountNo.text = FormatHelper.formatAccountNo(this,  AppConstant.IS_MARK_ACCOUNT)
        }
        item.coopAccountNo?.run {
            binding.textAccountNo.text = FormatHelper.formatAccountNo(this, AppConstant.IS_MARK_ACCOUNT)
        }
        if (item.accountType == AccountType.LOAN) {
            binding.layoutCard.setBackgroundResource(R.drawable.bg_account_desc_loan)
            binding.imgAccount.loadCircle(R.drawable.logo_coop_cycle_min)
            binding.labelAccountBalance.setText(R.string.label_home_acc_loan_acc_balance)
            binding.labelAvailableBalance.setText(R.string.label_home_acc_loan_ava_balance)
            binding.textAvailableBalanceValue.setTextAppearance(R.style.TextMedium1)
            binding.textAccountBalanceValue.setTextAppearance(R.style.TextMedium_Bold)

            binding.textAccountBalanceValue.setAmount(item.availableBalance)
            binding.textAvailableBalanceValue.setAmount(item.outstandingBalance)
        } else {
            binding.labelAccountBalance.setText(R.string.label_home_acc_info_acc_balance)
            binding.labelAvailableBalance.setText(R.string.label_home_acc_info_ava_balance)
            binding.textAvailableBalanceValue.setTextAppearance(R.style.TextMedium_Bold)
            binding.textAccountBalanceValue.setTextAppearance(R.style.TextMedium1)

            binding.textAccountBalanceValue.setAmount(item.accountBalance)
            binding.textAvailableBalanceValue.setAmount(item.availableBalance)

            if(productCode == ProductCode.LOAN_PRODUCT){
                binding.layoutCard.setBackgroundResource(R.drawable.bg_account_desc_loan)
            }else{
                binding.layoutCard.setBackgroundResource(R.drawable.bg_account_desc_saving)
            }
            if (item.accountType == AccountType.BAY) {
                binding.textAccountDesc.text = item.accountName
                binding.imgAccount.loadCircle(R.drawable.logo_bay_cycle_min)
            } else {
                binding.textAccountDesc.text = item.accountDesc
                binding.imgAccount.loadCircle(R.drawable.logo_coop_cycle_min)
            }
        }
    }

}