package com.krungsri.coop.ui.component.transaction.inquiry

import android.view.LayoutInflater
import android.view.ViewGroup
import com.krungsri.coop.R
import com.krungsri.coop.databinding.ItemAccountToBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.loadCircle
import com.krungsri.coop.extension.setAccountNo
import com.krungsri.coop.extension.visible
import com.krungsri.coop.model.transaction.DestinationAccount
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter

class DestinationAccountAdapter(val onSelectedAccountToItem: ((destinationAccount: DestinationAccount) -> Unit)) :
    BaseRecyclerViewAdapter<DestinationAccount, ItemAccountToBinding>() {

    override fun createBinding(parent: ViewGroup): ItemAccountToBinding {
        return ItemAccountToBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    override fun bind(binding: ItemAccountToBinding, position: Int, item: DestinationAccount) {
        binding.item = item
        binding.adapter = this
        binding.textAccountName.text = item.accountName
        binding.textAccountDesc.text = item.accountDesc
        if (position < itemCount - 1) {
            binding.divider.visible()
        } else {
            binding.divider.gone()
        }
        item.coopAccountNo?.run {
            binding.textAccountNo.setAccountNo(this, null)
            binding.imgAccount.loadCircle(R.drawable.logo_coop_cycle_min)
        }
        item.bayAccountNo?.run {
            binding.textAccountNo.setAccountNo(null, this)
            binding.imgAccount.loadCircle(R.drawable.logo_bay_cycle_min)
        }

    }
}