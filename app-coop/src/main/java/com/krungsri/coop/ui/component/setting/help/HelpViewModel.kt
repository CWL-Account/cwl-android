package com.krungsri.coop.ui.component.setting.help

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.coop.CoopInfoResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.usecase.coop.GetCoopInfoUseCase
import javax.inject.Inject

class HelpViewModel @Inject
constructor(
    private val getCoopInfoUseCase: GetCoopInfoUseCase
) : BaseViewModel() {

    val coopInfo = MutableLiveData<CoopInfoResponse>()

    fun getCoopInfo() {
        getCoopInfoUseCase.build()
            .doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::getCoopInfoSuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    private fun getCoopInfoSuccess(response: CoopInfoResponse) {
        coopInfo.value = response
    }


}