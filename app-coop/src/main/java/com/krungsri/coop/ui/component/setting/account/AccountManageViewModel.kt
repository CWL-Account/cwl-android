package com.krungsri.coop.ui.component.setting.account

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.account.AccountInfo
import com.krungsri.coop.model.account.AccountRemoveRequest
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.policy.PolicyRequest
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.model.policy.PolicyType
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.otp.OTPVerificationContext
import com.krungsri.coop.ui.component.policy.PolicyContext
import com.krungsri.coop.usecase.account.GetAccountActiveListUseCase
import com.krungsri.coop.usecase.account.RemoveAccountListUseCase
import com.krungsri.coop.usecase.policy.GetLatestPolicyActiveUseCase
import javax.inject.Inject


class AccountManageViewModel @Inject
constructor(
    private val getAccountActiveListUseCase: GetAccountActiveListUseCase,
    private val removeAccountListUseCase: RemoveAccountListUseCase,
    private val getLatestPolicyActiveUseCase: GetLatestPolicyActiveUseCase,
) : BaseViewModel() {

    val accountInfoList = MutableLiveData<MutableList<AccountInfo>>()
    val removeAccountUpdate = MutableLiveData<AccountInfo>()
    private var hasInactiveAccount: Boolean = false

    fun accountList() {
        getAccountActiveListUseCase.build()
            .doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onGetAccountListSuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    override fun onApiError(apiError: ApiError) {
        if (apiError.code == AppConstant.CODE_PRODUCT_SERVICE_NOTFOUND) {
            showOkAlert(apiError, onDismiss = {
                popBackStack()
            });
        } else {
            super.onApiError(apiError)
        }

    }

    private fun onGetAccountListSuccess(response: ApiResponse<MutableList<AccountInfo>>) {
        accountInfoList.value = response.result
        hasInactiveAccount = response.code == AppConstant.CODE_SUCCESS201
    }

    private fun onUpdateAccountSuccess(isSuccess: Boolean, accountInfo: AccountInfo) {
        hasInactiveAccount = isSuccess
        removeAccountUpdate.value = accountInfo
    }

    fun addNewAccount() {
        if (hasInactiveAccount) {
            getLatestPolicy()
        } else {
            showOkAlert(message = getString(R.string.err_setting_account_check_inactive_not_found))
        }
    }

    fun removeAccount(accountInfo: AccountInfo) {
        showSelectAlertMessage(
            title = getString(R.string.label_setting_account_remove_dialog_title),
            message = appContext.getString(
                R.string.label_setting_account_remove_dialog_desc,
                getString(R.string.app_mobile_name)
            ),
            onOkDismiss = {
                onRemoveAccount(accountInfo)
            }
        )
    }

    private fun onRemoveAccount(accountInfo: AccountInfo) {
        val accountUpdate = mutableListOf<String>()
        accountUpdate.add(accountInfo.coopAccountNo ?: "0")
        removeAccountListUseCase.build(AccountRemoveRequest(accountUpdate))
            .doToggleLoading()
            .subscribeWithViewModel(
                this,
                {
                    onUpdateAccountSuccess(it, accountInfo)
                },
                this::onApiError
            )
            .disposedBy(this)
    }

    private fun getLatestPolicy() {
        getLatestPolicyActiveUseCase.build(PolicyRequest(PolicyType.COOP_CONSENT))
            .doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onGetLatestPolicySuccess
            ) {
                requestNewOTP()
            }
            .disposedBy(this)
    }

    private fun onGetLatestPolicySuccess(response: PolicyResponse) {
        if (response.isNew == Flag.Y) {
            gotoPage(
                AccountManageFragmentDirections.gotoPolicyPage(
                    policyContext = PolicyContext(
                        isRequestToRegister = false,
                        isRequestToAddAccount = true,
                        isNewPolicy = Flag.Y
                    ),
                    policyData = response
                )
            )
        } else {
            requestNewOTP()
        }
    }

    private fun requestNewOTP() {
        gotoPage(
            AccountManageFragmentDirections.gotoOTPVerifyPage(
                OTPVerificationContext(
                    verificationType = VerificationType.ADD_ACCOUNT
                )
            )
        )
    }
}