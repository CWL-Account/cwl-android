package com.krungsri.coop.ui.component.home

import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.enumeration.ProductCode
import com.krungsri.coop.model.home.CoopService

interface AdapterListener {
    fun onCoopServiceClick(coopService: CoopService, productCode: ProductCode, accountDesc: AccountDesc)
    fun onCreateBarcode(coopService: CoopService, accountDesc: AccountDesc)
    fun onMemberDataClick()
    fun onHistoryClick(
        productCode: ProductCode,
        accountDesc: AccountDesc,
        accountList: MutableList<AccountDesc>
    )
}