package com.krungsri.coop.ui.component.login

import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentLoginBinding
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.helper.BiometricHelper
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.lib.lifecycle.observeEvent
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.base.PinListener
import com.krungsri.coop.ui.component.member.validation.MemberVerificationContext
import timber.log.Timber

class LoginFragment :
    BaseFragment<FragmentLoginBinding, LoginViewModel>(), PinListener {

    override val layoutId: Int = R.layout.fragment_login

    override val viewModelClass: Class<LoginViewModel> = LoginViewModel::class.java
    override fun onFragmentStart() {
        Timber.d("getKeyTestSize %s", SecurityHelper.getApiClientSecret())
        dataBinding.fragment = this
        dataBinding.pinListener = this
        viewModel.onClearPin.observeEvent(viewLifecycleOwner) {
            dataBinding.pinDot.clearPin()
        }
        if (BiometricHelper.canAuthenticate(requireContext())) {
            AppPrefs.getString(AppConstant.BIOMETRIC_TOKEN)?.run {
                dataBinding.showBiometricLogin = true
                BiometricHelper(requireContext(), this@LoginFragment, onAuthenticationFailed = {

                }, onAuthenticationSucceeded = {
                    viewModel.loginBiometric(this)
                }, onAuthenticationError = {

                }).biometricPrompt()
            }
        }

        dataBinding.pinDot.onKeyCompleted = { pinNo ->
            viewModel.loginPin(pinNo)
        }
    }

    override fun onPinClick(pinNo: String) {
        if (pinNo == "BIO") {
            AppPrefs.getString(AppConstant.BIOMETRIC_TOKEN)?.run {
                BiometricHelper(requireContext(), this@LoginFragment
                    , onAuthenticationFailed = {

                    }, onAuthenticationSucceeded = {
                        viewModel.loginBiometric(this)
                    }, onAuthenticationError = {

                    }).biometricPrompt()
            }
        } else {
            dataBinding.pinDot.addPIN(pinNo)
        }
    }


    fun onForgotPinClick() {
        gotoPage(
            LoginFragmentDirections.gotoForgotPinPage(
                MemberVerificationContext(verificationType = VerificationType.FORGOT_PIN)
            )
        )
    }
}