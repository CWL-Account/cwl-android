package com.krungsri.coop.ui.component.login

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.lib.lifecycle.MutableLiveEvent
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.member.MemberBiometricLoginRequest
import com.krungsri.coop.model.member.MemberLoginRequest
import com.krungsri.coop.model.member.MemberLoginResponse
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.model.setting.BiometricSettingRequest
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.member.validation.MemberVerificationContext
import com.krungsri.coop.ui.component.policy.PolicyContext
import com.krungsri.coop.usecase.member.MemberBiometricLoginCase
import com.krungsri.coop.usecase.member.MemberLoginUseCase
import com.krungsri.coop.usecase.policy.GetLatestPolicyUseCase
import com.krungsri.coop.usecase.setting.BiometricSettingUseCase
import timber.log.Timber
import javax.inject.Inject

class LoginViewModel @Inject
constructor(
    private val memberLoginCase: MemberLoginUseCase,
    private val biometricLoginCase: MemberBiometricLoginCase,
    private val getLatestPolicyUseCase: GetLatestPolicyUseCase,
    private val biometricSettingUseCase: BiometricSettingUseCase,
) : BaseViewModel() {

    val onClearPin: MutableLiveEvent<Boolean> = MutableLiveEvent()
    val showBiometricLogin = MutableLiveData(false)
    private val encryptPinNo = MutableLiveData<String>()
    fun loginPin(pinNo: String) {
        Timber.d("loginPin = %s", pinNo)
        onClearPin.setEventValue(true)
        encryptPinNo.value = SecurityHelper.encryptPinNo(pinNo)
        memberLoginCase.build(
            MemberLoginRequest(
                pinNo = encryptPinNo.value ?: "",
                uuid = AppHelper.getDeviceId(appContext)
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onMemberLoginSuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    fun loginBiometric(biometricToken: String) {
        onClearPin.setEventValue(true)
        biometricLoginCase.build(
            MemberBiometricLoginRequest(
                biometricToken = biometricToken,
                uuid = AppHelper.getDeviceId(appContext)
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onMemberLoginSuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    private fun onMemberLoginSuccess(response: MemberLoginResponse) {
        val bioExp = AppPrefs.getString(AppConstant.CODE_TOKEN_BIO_EXP)
        if (AppConstant.CODE_TOKEN_BIO_EXP == bioExp) {
            AppPrefs.remove(AppConstant.CODE_TOKEN_BIO_EXP)
            biometricSettingUseCase.build(
                BiometricSettingRequest(
                    pinNo = encryptPinNo.value ?: "",
                    isEnable = Flag.Y,
                    uuid = AppHelper.getDeviceId(appContext)
                )
            ).doToggleLoading()
                .subscribeWithViewModel(
                    this,
                    {
                        onMemberLoginSuccess(response)
                    },
                    {
                        onMemberLoginSuccess(response)
                    }
                )
                .disposedBy(this)
        } else {
            getLatestPolicy()
        }
    }


    private fun getLatestPolicy() {
        Timber.d("getLatestPolicy")
        getLatestPolicyUseCase.build()
            .doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onGetLatestPolicySuccess
            ) {
                gotoPage(LoginFragmentDirections.gotoMainHomePage())
            }
            .disposedBy(this)
    }


    private fun onGetLatestPolicySuccess(response: PolicyResponse) {
        Timber.d("response.isNew %s", response.isNew)
        if (response.isNew == Flag.Y) {
            gotoPage(
                LoginFragmentDirections.gotoPolicyPage(
                    policyContext = PolicyContext(
                        isRequestToRegister = false,
                        isNewPolicy = Flag.Y
                    ),
                    policyData = response
                )
            )
        } else {
            gotoPage(LoginFragmentDirections.gotoMainHomePage())
        }
    }

    override fun onApiError(apiError: ApiError) {
        onClearPin.setEventValue(true)
        when (apiError.code) {
            AppConstant.CODE_DEVICE_NOTFOUND, AppConstant.CODE_MEMBER_NOTFOUND -> {
                showOkAlert(apiError, onDismiss = {
                    AppPrefs.clearAll()
                    gotoPage(LoginFragmentDirections.gotoSplashScreenPage())
                })
            }
            AppConstant.CODE_PIN_LOCK -> {
                showOkAlert(apiError, onDismiss = {
                    gotoPage(
                        LoginFragmentDirections.gotoUnlockPage(
                            MemberVerificationContext(verificationType = VerificationType.UNLOCK)
                        )
                    )
                })
            }
            AppConstant.CODE_TOKEN_BIO_EXP -> {
                AppPrefs.setString(AppConstant.CODE_TOKEN_BIO_EXP, AppConstant.CODE_TOKEN_BIO_EXP)
                super.onApiError(apiError)
            }
            else -> {
                super.onApiError(apiError)
            }
        }
    }
}