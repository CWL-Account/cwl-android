package com.krungsri.coop.ui.component.transaction.history

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.krungsri.coop.R
import com.krungsri.coop.databinding.ItemHistoryDetailBinding
import com.krungsri.coop.extension.*
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.enumeration.TransactionChannel
import com.krungsri.coop.model.enumeration.TransactionStatus
import com.krungsri.coop.model.transaction.TransactionResponse
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter
import java.util.*

class TrxHistoryDetailAdapter(private val context: Context) :
    BaseRecyclerViewAdapter<TransactionResponse, ItemHistoryDetailBinding>() {

    private val months: Array<String> = context.resources.getStringArray(R.array.months)
    private val normalColor = ContextCompat.getColor(context, android.R.color.black)
    private val redColor = ContextCompat.getColor(context, android.R.color.holo_red_light)
    private val greenColor = ContextCompat.getColor(context, android.R.color.holo_green_dark)
    private val calendar = Calendar.getInstance()
    private var selectedAccount: AccountDesc? = null

    override fun createBinding(parent: ViewGroup): ItemHistoryDetailBinding {
        return ItemHistoryDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    fun setSelectedAccount(selectedAccount: AccountDesc) {
        this.selectedAccount = selectedAccount
    }

    override fun bind(binding: ItemHistoryDetailBinding, position: Int, item: TransactionResponse) {

        calendar.time = item.transactionDate

        val date = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR) + 543
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        val dateString = if (date < 10) "0$date" else "$date"
        val hourString = if (hour < 10) "0$hour" else "$hour"
        val minuteString = if (minute < 10) "0$minute" else "$minute"
        val monthString = months[month]
        val yearString = "$year".substring(2)
        val transDate = "$dateString $monthString $yearString"
        if (position > 0) {
            getItem(position - 1)?.run {
                calendar.time = transactionDate
                val aboveDate = calendar.get(Calendar.DAY_OF_MONTH)
                binding.textTransDate.visibility = if (aboveDate == date) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
            }
        } else {
            binding.textTransDate.visible()
        }

        binding.textTransFee.setAmount(item.transactionFee, true)
        binding.layoutAccount.visible()
        when (item.transactionType) {
            ServiceCode.TRANSFERRING -> {
                setupDetail(binding, item)
                binding.textTransType.string(R.string.text_transfer)
            }
            ServiceCode.WITHDRAW -> {
                setupDetail(binding, item)
                binding.textTransType.string(R.string.text_withdraw)
            }
            ServiceCode.LOAN_WITHDRAW -> {
                setupDetail(binding, item)
                binding.textTransType.string(R.string.text_loan_withdraw)
            }
            ServiceCode.DEPOSIT -> {
                setupDetail(binding, item)
                binding.textTransType.string(R.string.text_deposit)

            }
            ServiceCode.LOAN_PAYMENT -> {
                setupDetail(binding, item)
                binding.textTransType.string(R.string.text_loan_payment)

            }
            ServiceCode.BILL_PAYMENT -> {
                binding.amountColor = greenColor
                binding.textTransType.string(R.string.text_loan_payment)
            }
            else -> {
                binding.layoutAccount.gone()
                binding.amountColor = normalColor
                binding.textTransType.context.getString(R.string.text_trans)
            }
        }


        binding.textTransStatus.text = when (item.transactionStatus) {
            TransactionStatus.FINALIZED -> context.getString(R.string.label_transaction_status_finalized)
            TransactionStatus.PROCESSING -> context.getString(R.string.label_transaction_status_processing)
            TransactionStatus.BAY_REJECTED, TransactionStatus.COOP_REJECTED -> {
                binding.amountColor = redColor
                context.getString(R.string.label_transaction_status_rejected)
            }
            TransactionStatus.ACCEPTED -> context.getString(R.string.label_transaction_status_accepted)
            else -> context.getString(R.string.label_transaction_status_other)
        }

        binding.layoutHistoryDetailExt.gone()
        binding.imgDetailEx.setImageResource(R.drawable.ic_down)
        binding.layoutHistoryDetail.setBackgroundResource(0)
        binding.imgDetailEx.rotation = 0F
        binding.adapter = this
        binding.textTransDate.text = transDate
        binding.textTransTime.text = "$hourString:$minuteString"

        val prefix = if (binding.amountColor == redColor) {
            "- "
        } else {
            ""
        }

        binding.textTransAmount.text = prefix + FormatHelper.formatAmount(item.transactionAmount)
        binding.textTransRef.text = item.referenceNo
        binding.textTransNote.text = item.note
        binding.hasNote = item.note?.isNotEmpty()

        binding.rootLayout = binding.layoutHistoryDetail
        binding.layoutViewDetail = binding.layoutHistoryDetailExt
        binding.imgViewEx = binding.imgDetailEx

        binding.textTransChannel.text = when (item.transactionChannel) {
            TransactionChannel.MOBILE -> context.getString(R.string.label_transaction_channel_mobile)
            TransactionChannel.BRANCH -> context.getString(R.string.label_transaction_channel_branch)
            else -> context.getString(R.string.label_transaction_channel_other)
        }
    }

    private fun setupDetail(binding: ItemHistoryDetailBinding, item: TransactionResponse) {
        if (selectedAccount?.accountNo == item.fromBayAccountNo || selectedAccount?.accountNo == item.fromCoopAccountNo) {
            binding.amountColor = redColor
            binding.labelAccountNo.string(R.string.label_transaction_to_account)
            binding.textAccountName.text = item.toAccountName
            item.toCoopAccountNo?.run {
                binding.imgAccountIcon.loadCircle(R.drawable.logo_coop_cycle_min)
                binding.textAccountNo.setAccountNo(
                    coopAccountNo = this,
                    bayAccountNo = null
                )
            } ?: item.toBayAccountNo?.run {
                binding.imgAccountIcon.loadCircle(R.drawable.logo_bay_cycle_min)
                binding.textAccountNo.setAccountNo(
                    coopAccountNo = null,
                    bayAccountNo = this
                )
            }
        } else {
            binding.amountColor = greenColor
            binding.labelAccountNo.string(R.string.label_transaction_from_account)
            binding.textAccountName.text = item.fromAccountName
            item.fromCoopAccountNo?.run {
                binding.imgAccountIcon.loadCircle(R.drawable.logo_coop_cycle_min)
                binding.textAccountNo.setAccountNo(
                    coopAccountNo = this,
                    bayAccountNo = null
                )
            } ?: item.fromBayAccountNo?.run {
                binding.imgAccountIcon.loadCircle(R.drawable.logo_bay_cycle_min)
                binding.textAccountNo.setAccountNo(
                    coopAccountNo = null,
                    bayAccountNo = this
                )
            }
        }
    }

    fun showDetail(root: View, imgViewEx: AppCompatImageView, layoutViewDetail: RelativeLayout) {
        if (layoutViewDetail.visibility == View.VISIBLE) {
            imgViewEx.animate().rotation(0F).start()
            layoutViewDetail.animationCollapse()
            root.setBackgroundResource(0)
        } else {
            imgViewEx.animate().rotation(-180F).start()
            layoutViewDetail.animationExpand()
            if (selectedAccount?.accountType == AccountType.LOAN) {
                root.setBackgroundResource(R.color.item_loan_history_selected)
            } else {
                root.setBackgroundResource(R.color.item_saving_history_selected)
            }
        }
    }

}