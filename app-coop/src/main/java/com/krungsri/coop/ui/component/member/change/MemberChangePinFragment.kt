package com.krungsri.coop.ui.component.member.change

import android.os.Build
import android.text.Html
import com.google.android.material.textfield.TextInputEditText
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentMemberChangePinBinding
import com.krungsri.coop.extension.clearError
import com.krungsri.coop.extension.showError
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.ui.base.BaseFragment
import timber.log.Timber

class MemberChangePinFragment :
    BaseFragment<FragmentMemberChangePinBinding, MemberChangePinViewModel>() {
    override val layoutId: Int = R.layout.fragment_member_change_pin

    override val viewModelClass: Class<MemberChangePinViewModel> =
        MemberChangePinViewModel::class.java


    override fun onFragmentStart() {
        dataBinding.fragment = this
        dataBinding.editCurrentPin.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus && (v as TextInputEditText).text?.length != 6) {
                dataBinding.editCurrentPin.showError(
                    dataBinding.inputCurrentPin, getString(
                        R.string.error_input,
                        getString(R.string.label_member_change_pin_current_pin)
                    )
                )
            } else {
                dataBinding.editCurrentPin.clearError(dataBinding.inputCurrentPin)
            }
        }

        dataBinding.editNewPin.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus && (v as TextInputEditText).text?.length != 6) {
                dataBinding.editNewPin.showError(
                    dataBinding.inputNewPin,
                    getString(
                        R.string.error_input,
                        getString(R.string.label_member_change_pin_new_pin)
                    )
                )
            } else {
                dataBinding.editNewPin.clearError(dataBinding.inputNewPin)
            }
        }
        dataBinding.editConfirmPin.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus && (v as TextInputEditText).text?.length != 6) {
                dataBinding.editConfirmPin.showError(
                    dataBinding.inputConfirmPin,
                    getString(
                        R.string.error_input,
                        getString(R.string.label_member_change_pin_confirm_pin)
                    )
                )
            } else {
                dataBinding.editConfirmPin.clearError(dataBinding.inputConfirmPin)
            }
        }

        dataBinding.textPinCondition.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(
                getString(R.string.label_member_change_pin_condition_desc),
                Html.FROM_HTML_MODE_COMPACT
            )
        } else {
            Html.fromHtml(getString(R.string.label_member_change_pin_condition_desc))
        }
    }

    private fun validatePin(): Boolean {

        val currentPin = dataBinding.editCurrentPin.text.toString()
        val newPin = dataBinding.editNewPin.text.toString()
        val confirmPin = dataBinding.editConfirmPin.text.toString()
        val memberCode = AppPrefs.getStaticString(AppConstant.MEMBER_CODE)
        Timber.d("validatePin %s, %s, %s", currentPin, newPin, confirmPin)

        val pinLength = 6
        var isValid = true
        if (currentPin.isEmpty() || currentPin.length != pinLength) {
            dataBinding.editCurrentPin.showError(
                dataBinding.inputCurrentPin, getString(
                    R.string.error_input,
                    getString(R.string.label_member_change_pin_current_pin)
                )
            )
            isValid = false
        } else {
            dataBinding.editCurrentPin.clearError(dataBinding.inputCurrentPin)
        }

        if (newPin.isEmpty() || newPin.length != pinLength) {
            dataBinding.editNewPin.showError(
                dataBinding.inputNewPin,
                getString(
                    R.string.error_input,
                    getString(R.string.label_member_change_pin_new_pin)
                )
            )
            isValid = false
        } else {
            dataBinding.editNewPin.clearError(dataBinding.inputNewPin)
        }

        if (newPin.isEmpty() || newPin.length != pinLength) {
            dataBinding.editConfirmPin.showError(
                dataBinding.inputConfirmPin,
                getString(
                    R.string.error_input,
                    getString(R.string.label_member_change_pin_confirm_pin)
                )
            )
            isValid = false
        } else {
            dataBinding.editConfirmPin.clearError(dataBinding.inputConfirmPin)
        }

        if (!isValid) {
            return false
        }

        var errorMessage = when {
            currentPin.isEmpty() -> {
//                dataBinding.editCurrentPin.showError(
//                    dataBinding.inputCurrentPin, getString(
//                        R.string.error_input,
//                        getString(R.string.label_member_change_pin_current_pin)
//                    )
//                )
                getString(
                    R.string.error_input,
                    getString(R.string.label_member_change_pin_current_pin)
                )
            }
            newPin.isEmpty() -> {
//                dataBinding.editNewPin.showError(
//                    dataBinding.inputNewPin,
//                    getString(
//                        R.string.error_input,
//                        getString(R.string.label_member_change_pin_new_pin)
//                    )
//                )
                getString(
                    R.string.error_input,
                    getString(R.string.label_member_change_pin_new_pin)
                )
            }
            confirmPin.isEmpty() -> {
//                dataBinding.editConfirmPin.showError(
//                    dataBinding.inputConfirmPin,
//                    getString(
//                        R.string.error_input,
//                        getString(R.string.label_member_change_pin_confirm_pin)
//                    )
//                )
                getString(
                    R.string.error_input,
                    getString(R.string.label_member_change_pin_confirm_pin)
                )
            }
            newPin != confirmPin -> {
//                dataBinding.editConfirmPin.showError(
//                    dataBinding.inputConfirmPin,
//                    getString(R.string.err_confirm_pin_warning)
//                )
                getString(R.string.err_confirm_pin_warning)
            }
            memberCode != null && memberCode == newPin -> {
                getString(R.string.error_member_change_pin_member_code)
            }
            else -> null
        }

        errorMessage?.run {
            showOkAlert(message = this)
        }

        return errorMessage == null
    }

    fun onChangePinCLick() {
        if (!validatePin()) {
            return
        }


        val currentPin = dataBinding.editCurrentPin.text.toString()
        val newPin = dataBinding.editNewPin.text.toString()
        if (dataBinding.inputCurrentPin.error.isNullOrEmpty()
            && dataBinding.inputNewPin.error.isNullOrEmpty()
            && dataBinding.inputConfirmPin.error.isNullOrEmpty()
        ) {
//            dataBinding.editCurrentPin.text = null
//            dataBinding.editNewPin.text = null
//            dataBinding.editConfirmPin.text = null
            viewModel.onChangePin(currentPin, newPin)
        }
    }
}