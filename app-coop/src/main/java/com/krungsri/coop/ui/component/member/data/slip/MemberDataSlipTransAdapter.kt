package com.krungsri.coop.ui.component.member.data.slip

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import androidx.appcompat.widget.AppCompatImageView
import com.krungsri.coop.R
import com.krungsri.coop.databinding.ItemMemberSlipDetailBinding
import com.krungsri.coop.extension.animationCollapse
import com.krungsri.coop.extension.animationExpand
import com.krungsri.coop.extension.setAmount
import com.krungsri.coop.model.member.MemberSlipTrans
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter

class MemberDataSlipTransAdapter : BaseRecyclerViewAdapter<MemberSlipTrans, ItemMemberSlipDetailBinding>() {
    override fun createBinding(parent: ViewGroup): ItemMemberSlipDetailBinding {
        return ItemMemberSlipDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    }

    override fun bind(binding: ItemMemberSlipDetailBinding, position: Int, item: MemberSlipTrans) {
        binding.adapter = this
        binding.rootLayout = binding.root
        binding.imgViewEx = binding.imgDetailEx
        binding.layoutViewDetail = binding.layoutDetail
        binding.textTransactionCode.text = item.transactionCode
        binding.textTransactionDesc.text = item.transactionType
        binding.textMonthlyPayment.setAmount(item.paymentAmount)
        binding.textTransNo.text = item.no.toString()
        binding.textTransNoOfPeriod.text = item.noOfPeriod.toString()
        binding.textTransPrinciplePaid.setAmount(item.principlePaid)
        binding.textTransInterestPaid.setAmount(item.interestPaid)

    }

    fun showDetail(root: View, imgViewEx: AppCompatImageView, layoutViewDetail: GridLayout) {
        if (layoutViewDetail.visibility == View.VISIBLE) {
            imgViewEx.animate().rotation(0F).start()
            layoutViewDetail.animationCollapse()
            root.setBackgroundResource(0)
        } else {
            imgViewEx.animate().rotation(-180F).start()
            layoutViewDetail.animationExpand()
            root.setBackgroundResource(R.color.item_saving_history_selected)
        }
    }
}