package com.krungsri.coop.ui.component.transaction.pin

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.transaction.TransactionConfirmRequest
import com.krungsri.coop.model.transaction.TransactionResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.transaction.slip.TrxSlipContext
import com.krungsri.coop.usecase.transaction.ConfirmTransactionUseCase
import javax.inject.Inject

class TrxPinViewModel @Inject constructor(
    private val localSession: LocalSession,
    private val confirmTransactionUseCase: ConfirmTransactionUseCase
) : BaseViewModel() {

    private val confirmContext = MutableLiveData<TrxPinContext>()
    val invalidPin = MutableLiveData(true)
    private var okButtonBg: Int = R.drawable.button_main_saving

    fun initData(trxPinContext: TrxPinContext) {
        confirmContext.value = trxPinContext
        when (trxPinContext.serviceCode) {
            ServiceCode.LOAN_WITHDRAW,
            ServiceCode.LOAN_PAYMENT,
            ServiceCode.BILL_PAYMENT -> {
                okButtonBg = R.drawable.button_main_loan
            }
            else -> {
            }
        }
    }


    fun transactionConfirm(pinNo: String) {
        confirmContext.value?.inquiryResponse?.run {

            val transactionConfirm = TransactionConfirmRequest(
                serviceCode = confirmContext.value?.serviceCode!!,
                verificationToken = verificationToken,
                amount = transactionAmount,
                fromCoopAccountNo = fromCoopAccountNo,
                fromBayAccountNo = fromBayAccountNo,
                toCoopAccountNo = toCoopAccountNo,
                toBayAccountNo = toBayAccountNo,
                pinNo = SecurityHelper.encryptPinNo(pinNo),
                note = note
            )

            confirmTransactionUseCase.build(
                transactionConfirm
            ).doToggleLoading()
                .subscribeWithViewModel(
                    this@TrxPinViewModel,
                    this@TrxPinViewModel::transactionConfirmSuccess,
                    this@TrxPinViewModel::onApiError
                )
                .disposedBy(this@TrxPinViewModel)
        }

    }

    private fun transactionConfirmSuccess(response: TransactionResponse) {
        localSession.inquiryHome.clear()
        gotoPage(
            TrxPinFragmentDirections.gotoTransactionSlipPage(
                TrxSlipContext(
                    sourceAccount = confirmContext.value?.sourceAccount,
                    destinationAccount = confirmContext.value?.destinationAccount,
                    transaction = response
                )
            )
        )
    }

    override fun onApiError(apiError: ApiError) {
        super.onApiError(apiError, buttonBg = okButtonBg)
        invalidPin.value = true
    }
}