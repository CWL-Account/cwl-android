package com.krungsri.coop.ui.component.otp

import android.os.Build
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentOtpVerificationBinding
import com.krungsri.coop.extension.visible
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.base.PinListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

class OTPVerificationFragment :
    BaseFragment<FragmentOtpVerificationBinding, OTPVerificationViewModel>(), PinListener {

    override val layoutId: Int = R.layout.fragment_otp_verification

    override val viewModelClass: Class<OTPVerificationViewModel> =
        OTPVerificationViewModel::class.java

    override fun onCreateView(rootView: View) {
        Build.FINGERPRINT
    }

    private val args by navArgs<OTPVerificationFragmentArgs>()


    override fun onFragmentStart() {
        dataBinding.fragment = this
        dataBinding.pinListener = this
        dataBinding.vm = viewModel
        viewModel.initData(args.otpVerificationContext)

        dataBinding.pinDot.onKeyCompleted = { otpNo ->
            viewModel.otpValidation(otpNo)
            dataBinding.pinDot.clearPin()
        }

        viewModel.otpResponse.observe(this, Observer { response ->
            dataBinding.textDesc2.visible()
            dataBinding.editOtpRef.visible()
            val minutes = response.expireIn / 60
            dataBinding.textDesc1.text = getString(
                R.string.label_otp_desc1,
                FormatHelper.formatMobileNNo(response.mobileNo, false)
            )
            dataBinding.textDesc2.text = getString(R.string.label_otp_desc2, minutes)
            dataBinding.editOtpRef.text = getString(R.string.label_otp_ref, response.otpRef)
            dataBinding.buttonResent.setBackgroundResource(R.drawable.button_empty_gray)
            dataBinding.buttonResent.text = getString(R.string.label_otp_delay, viewModel.delaySec)
            dataBinding.buttonResent.isEnabled = false
            dataBinding.buttonResent.visible()
            dataBinding.pinDot.addPIN("")
            viewModel.otpResentCount.value = System.currentTimeMillis()
        })

        viewModel.otpResentCount.observe(this, {
            val delaySec = viewModel.delaySec - (System.currentTimeMillis() - it) / 1000
            Timber.d("delaySec %s", delaySec)
            if (delaySec > 0) {
                dataBinding.buttonResent.text = getString(R.string.label_otp_delay, delaySec)
                otpResentCount()
            } else {
                dataBinding.buttonResent.setBackgroundResource(R.drawable.button_empty_saving)
                dataBinding.buttonResent.text = getString(R.string.label_otp_resent)
                dataBinding.buttonResent.isEnabled = true
            }
        })
    }


    private fun otpResentCount() {
        GlobalScope.launch(Dispatchers.IO) {
            delay(1000)
            viewModel.otpResentCount.postValue(viewModel.otpResentCount.value)
        }
    }

    override fun onPinClick(pinNo: String) {
        dataBinding.pinDot.addPIN(pinNo)
    }
}