package com.krungsri.coop.ui.base


import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.krungsri.coop.MainActivity
import com.krungsri.coop.R
import timber.log.Timber

/**
 * Created by Santi T.
 */


class ProgressDialog(private val context: Context) : LifecycleObserver {

    private var dialog: Dialog = Dialog(context)
    private var handler: Handler?

    companion object {
        private const val SHOW = 111
        private const val HIDE = 222
    }


    init {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_progress)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        handler = object : Handler(Looper.getMainLooper()) {
            override fun handleMessage(msg: Message) {
//                Timber.d("ToggleLoading %s", msg.what)
                when (msg.what) {
                    SHOW -> showDialog1()
                    HIDE -> hideDialog1()
                }
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        handler?.removeMessages(SHOW)
        handler?.removeMessages(HIDE)
    }

    fun isShowing(): Boolean {
        return dialog.isShowing
    }

    fun showDialog() {
        handler?.removeMessages(HIDE)
        handler?.sendEmptyMessage(SHOW)
    }

    fun hideDialog() {
        handler?.sendEmptyMessageDelayed(HIDE, 1000)
    }

    fun hideDialogNow() {
        handler?.sendEmptyMessage(HIDE)
    }

    private fun showDialog1() {
        try {
            if(context is MainActivity && context.isFinishing){
                return
            }
            dialog.show()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun hideDialog1() {
        try {
            if (context is MainActivity && context.isFinishing) {
                return
            }
            if (dialog.isShowing)
                dialog.dismiss()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }


}