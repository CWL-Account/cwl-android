package com.krungsri.coop.ui.component.member.landing

import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.policy.PolicyContext
import com.krungsri.coop.usecase.policy.GetLatestPolicyUseCase
import javax.inject.Inject

class MemberLandingViewModel @Inject
constructor(
    private val getLatestPolicyUseCase: GetLatestPolicyUseCase
) : BaseViewModel() {
    fun getLatestPolicy() {
        getLatestPolicyUseCase.build()
            .doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onGetLatestPolicySuccess
            ) {
                gotoPage(MemberLandingFragmentDirections.gotoMainHomePage())
            }
            .disposedBy(this)
    }

    private fun onGetLatestPolicySuccess(response: PolicyResponse) {
        if (response.isNew == Flag.Y) {
            gotoPage(
                MemberLandingFragmentDirections.gotoPolicyPage(
                    policyContext = PolicyContext(isRequestToRegister = false, isNewPolicy = Flag.Y),
                    policyData = response
                )
            )
        } else {
            gotoPage(MemberLandingFragmentDirections.gotoMainHomePage())
        }
    }
}