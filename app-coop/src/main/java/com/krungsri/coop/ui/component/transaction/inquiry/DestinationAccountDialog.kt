package com.krungsri.coop.ui.component.transaction.inquiry

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.krungsri.coop.databinding.DialogAccountToBinding
import com.krungsri.coop.model.transaction.DestinationAccount
import com.krungsri.coop.ui.base.BaseDialog
import timber.log.Timber


class DestinationAccountDialog(private var onSelectedAccountTo: ((destinationAccount: DestinationAccount) -> Unit)) :
    BaseDialog() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = DialogAccountToBinding.inflate(inflater, container, false)
        arguments?.run {
            initView(binding, this)
        }
        return binding.root
    }

    private fun initView(binding: DialogAccountToBinding, args: Bundle) {
        binding.recyclerView.layoutManager =
            LinearLayoutManager(binding.recyclerView.context,
                LinearLayoutManager.VERTICAL, false)

        val accountList: Array<DestinationAccount>? =
            args.getParcelableArray(ACCOUNT_LIST) as Array<DestinationAccount>

        Timber.d("accountList = %s", accountList)
        accountList?.run {
            val adapter = DestinationAccountAdapter {
                dismiss()
                onSelectedAccountTo.invoke(it)
            }
            binding.recyclerView.adapter = adapter
            adapter.replace(this.toMutableList())
        }
    }

    fun show(manager: FragmentManager) {
        super.show(
            manager,
            TAG
        )
    }

    companion object {
        private const val TAG = "DestinationAccountDialog"
        private const val ACCOUNT_LIST = "ACCOUNT_LIST"

        fun createDialog(
            accountList: MutableList<DestinationAccount>,
            destinationSelected: ((destinationAccount: DestinationAccount) -> Unit)
        ): DestinationAccountDialog {

            val args = Bundle()
            args.putParcelableArray(ACCOUNT_LIST, accountList.toTypedArray())

            return DestinationAccountDialog(destinationSelected).apply {
                arguments = args
            }
        }
    }
}