package com.krungsri.coop.ui.component.member.data

import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.member.MemberDataInfoResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.usecase.member.MemberDataInfoUseCase
import javax.inject.Inject

class MemberDataViewModel @Inject constructor(
    private val memberDataInfoUseCase: MemberDataInfoUseCase
) : BaseViewModel() {

    fun getMemberInfo(showInfo: (dataInfo: MemberDataInfoResponse) -> Unit) {
        memberDataInfoUseCase.build()
            .doToggleLoading()
            .subscribeWithViewModel(
                this, {
                    showInfo.invoke(it)
                }, this::onApiError
            ).disposedBy(this)
    }
}