package com.krungsri.coop.ui.component.transaction.history

import android.view.View
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentTrxHistoryBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.loadRes
import com.krungsri.coop.extension.visible
import com.krungsri.coop.lib.lifecycle.observeEvent
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.component.home.HorizontalMarginItemDecoration
import java.util.*

class TrxHistoryFragment :
    BaseFragment<FragmentTrxHistoryBinding, TrxHistoryViewModel>() {

    override val layoutId: Int = R.layout.fragment_trx_history

    override val viewModelClass = TrxHistoryViewModel::class.java


    private val args by navArgs<TrxHistoryFragmentArgs>()

    override fun onFragmentStart() {
        dataBinding.header.toolbar.setNavigationOnClickListener {
            popToRoot()
        }
        dataBinding.fragment = this
        dataBinding.vm = viewModel
        dataBinding.recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        dataBinding.recyclerView.adapter = TrxHistoryDetailAdapter(requireContext())
        dataBinding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        viewModel.transactionHis.observeEvent(viewLifecycleOwner, { accountList ->
            val adapter = dataBinding.recyclerView.adapter as TrxHistoryDetailAdapter
            adapter.add(accountList)
            if (adapter.itemCount == 0) {
                dataBinding.layoutNotFound.visible()
            } else {
                dataBinding.layoutNotFound.gone()
            }
        })

        val scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                    viewModel.loadNextPage()
                }
            }
        }
        dataBinding.recyclerView.addOnScrollListener(scrollListener)

        if (args.historyContext.accountDesc.accountType == AccountType.LOAN) {
            dataBinding.buttonDropDownMonth.loadRes(R.drawable.ic_dropdown_secondary)
        }


        initSourceAdapter()
    }

    private fun initSourceAdapter() {
        val canceler = Calendar.getInstance()
        val year = canceler.get(Calendar.YEAR)
        val month = canceler.get(Calendar.MONTH)
        var monthString = if ((month + 1) < 10) "0${month + 1}" else "${month + 1}"
        val currentMonth = "$year-$monthString"

        val accountTabAdapter = TrxHistoryPagerAdapter(args.historyContext.productCode)
        accountTabAdapter.replace(args.historyContext.accountList)
        dataBinding.viewPager.adapter = accountTabAdapter

        val initSelectAccountNo = args.historyContext.accountDesc.accountNo
        var idx = args.historyContext.accountList.indexOfFirst {
            it.accountNo == initSelectAccountNo
        }

        dataBinding.viewPager.post {
            dataBinding.viewPager.setCurrentItem(idx, true)
        }

        dataBinding.viewPager.offscreenPageLimit = 1
        val nextItemVisiblePx = resources.getDimension(R.dimen.viewpager_next_item_visible)
        val currentItemHorizontalMarginPx =
            resources.getDimension(R.dimen.viewpager_current_item_horizontal_margin)
        val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx
        val pageTransformer = ViewPager2.PageTransformer { page: View, position: Float ->
            page.translationX = -pageTranslationX * position
        }
        dataBinding.viewPager.setPageTransformer(pageTransformer)
        val itemDecoration = HorizontalMarginItemDecoration(
            requireContext(),
            R.dimen.viewpager_current_item_horizontal_margin
        )
        dataBinding.viewPager.addItemDecoration(itemDecoration)

        dataBinding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (idx > 0 && idx != position) {
                    return
                }
                accountTabAdapter.getItem(position)?.run {
                    idx = 0
                    val hisAdapter = dataBinding.recyclerView.adapter as TrxHistoryDetailAdapter
                    hisAdapter.clear()
                    hisAdapter.setSelectedAccount(this)
                    viewModel.loadHistory(this, currentMonth)
                }
            }
        })

        dataBinding.tabLayoutDots.visibility = if (accountTabAdapter.itemCount > 1) View.VISIBLE else View.INVISIBLE
        TabLayoutMediator(dataBinding.tabLayoutDots, dataBinding.viewPager) { _, _ -> }.attach()

    }

    fun showMonthDialog() {
        MonthDialog(
            onSelectMonth = { month ->
                (dataBinding.recyclerView.adapter as TrxHistoryDetailAdapter).clear()
                viewModel.loadHistory(month, 0)
            },
            accountType = args.historyContext.accountDesc.accountType,
            selectedMonth = viewModel.currentMonth.value
        ).show(childFragmentManager)
    }
}