package com.krungsri.coop.ui.component.policy

import android.graphics.Bitmap
import android.os.Build
import android.view.View
import android.webkit.*
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentPolicyBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.loadRes
import com.krungsri.coop.extension.setColor
import com.krungsri.coop.extension.string
import com.krungsri.coop.helper.OpenURLHelper
import com.krungsri.coop.lib.eventbut.EventBus
import com.krungsri.coop.lib.eventbut.ToggleLoading
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.model.policy.PolicyType
import com.krungsri.coop.ui.base.BaseFragment
import timber.log.Timber


class PolicyFragment : BaseFragment<FragmentPolicyBinding, PolicyViewModel>() {

    override val layoutId: Int = R.layout.fragment_policy

    override val viewModelClass: Class<PolicyViewModel>
        get() = PolicyViewModel::class.java

    private val args by navArgs<PolicyFragmentArgs>()

    override fun onFragmentStart() {
        viewModel.initData(args.policyContext)
        dataBinding.vm = viewModel
        dataBinding.initView(dataBinding.root, this, args.policyData)
        viewModel.policyResponseData.observe(viewLifecycleOwner, Observer {
            dataBinding.initView(dataBinding.root, this@PolicyFragment, it)
        })
        viewModel.checkboxAccept.observe(viewLifecycleOwner, Observer {
            dataBinding.buttonConfirmTC.isEnabled = it
            dataBinding.buttonConfirmTC.setColor(if (it) android.R.color.white else R.color.gray)
        })
    }
}

fun FragmentPolicyBinding.initView(root: View, lifecycleOwner: LifecycleOwner, policyData: PolicyResponse) {
    policyResponse = policyData
    scrollView.smoothScrollTo(0, 0)
    checkboxAccept.isChecked = false

    val head = "<head>\n" +
            "    <style type=\"text/css\">\n" +
            "@font-face {\n" +
            "    font-family: krungsri_condensed;\n" +
            "    src: url('fonts/krungsri_condensed.ttf');\n" +
            "}\n" +
            "body {\n" +
            "    font-family: krungsri_condensed;\n" +
            "    font-size: medium;\n" +
            "    text-align: justify;\n" +
            "}\n" +
            "\n" +
            "    </style>\n" +
            "</head>"
    val htmlData = "<html>$head<body><div id='contentDataDiv'>${policyData.content}</div></body></html>"

    val fontSize = webView.resources.getDimension(R.dimen.font4).toInt()
    webView.settings.javaScriptEnabled = false
    webView.settings.useWideViewPort = false
    webView.settings.setSupportZoom(false)
    webView.settings.builtInZoomControls = false
    webView.settings.defaultTextEncodingName = "utf-8"
    webView.settings.minimumFontSize = fontSize
    webView.webChromeClient = WebChromeClient()
    webView.addJavascriptInterface(webView, "CustomJS");

    webView.webViewClient = object : WebViewClient() {

        override fun onPageFinished(view: WebView?, url: String?) {
            Timber.d(
                "CustomWebView.onPageFinished %s : %s",
                webView.getContentVertical(),
                webView.layoutParams.height
            )
            webView.settings.javaScriptEnabled = true
            webView.loadUrl("javascript:CustomJS.resize(document.getElementById('contentDataDiv').clientHeight)");
            webView.settings.javaScriptEnabled = false
            super.onPageFinished(view, url)
            EventBus.publish(ToggleLoading.HideProgress)
        }

        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
            super.onReceivedError(view, request, error)
            EventBus.publish(ToggleLoading.HideProgress)
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            EventBus.publish(ToggleLoading.ShowProgress)
        }

        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            Timber.d("shouldOverrideUrlLoading %s", request?.url)
            request?.url?.run {
                OpenURLHelper.open(webView.context, this)
            }

            return true
        }

        override fun onLoadResource(view: WebView?, url: String?) {
            Timber.d("onLoadResource %s", url)
            super.onLoadResource(view, url)
        }

        override fun onSafeBrowsingHit(
            view: WebView?,
            request: WebResourceRequest?,
            threatType: Int,
            callback: SafeBrowsingResponse?
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                callback?.backToSafety(false)
            }
        }
    }

    webView.webViewHeight.observe(lifecycleOwner) {
        webView.settings.javaScriptEnabled = false
        Timber.d("observeEvent.resize %s", it)
        val param = LinearLayoutCompat.LayoutParams(
            webView.context.resources.displayMetrics.widthPixels,
            it
        )
        param.setMargins(0, 0, 0, layoutRootButton.height)
        webView.layoutParams = param
    }
    webView.clearHistory()
    webView.clearCache(true)
    textTitle.setColor(R.color.font_subtitle_color)
    when (policyData.policyType) {
        PolicyType.BAY_TC, PolicyType.COOP_TC -> {
            textTitle.string(R.string.label_policy_coop_tc_title)
            if (PolicyType.BAY_TC == policyData.policyType) {
                imgLogo.loadRes(R.drawable.logo_bay_cycle)
                textTitle.setColor(R.color.brown)
            } else {
                imgLogo.loadRes(R.drawable.logo_coop_tc)
            }
        }
        PolicyType.BAY_CONSENT -> {
            textTitle.string(R.string.label_policy_coop_consent_title)
            imgLogo.loadRes(R.drawable.logo_bay_cycle)
            textTitle.setColor(R.color.brown)
        }
        PolicyType.COOP_CONSENT -> {
            textTitle.string(R.string.label_policy_coop_consent_title)
            imgLogo.loadRes(R.drawable.logo_coop_consent)
        }
        PolicyType.BAY_OTHER -> {
            textTitle.string(R.string.label_policy_other)
            imgLogo.loadRes(R.drawable.logo_bay_cycle)
            textTitle.setColor(R.color.brown)
        }
        PolicyType.COOP_OTHER -> {
            textTitle.string(R.string.label_policy_other)
            imgLogo.loadRes(R.drawable.logo_coop_other)
        }
        PolicyType.OTHER -> {
            textTitle.string(R.string.label_policy_other)
            imgLogo.gone()
        }
    }

    val layoutHeader = root.findViewById<LinearLayoutCompat>(R.id.layoutHeader)
    if(layoutHeader != null) {
        layoutHeader.post {
            webView.headerSize = layoutHeader.measuredHeight
            webView.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "utf-8", null);
        }
    }else{
        webView.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "utf-8", null);
    }
}