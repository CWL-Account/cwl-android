package com.krungsri.coop.ui.component.setting

import android.os.Bundle
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentSettingsBinding
import com.krungsri.coop.helper.BiometricHelper
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.component.member.pin.MemberPinDialog


class SettingFragment :
    BaseFragment<FragmentSettingsBinding, SettingViewModel>() {

    override val layoutId: Int = R.layout.fragment_settings

    override val viewModelClass: Class<SettingViewModel> = SettingViewModel::class.java

    override fun onFragmentStart() {
        dataBinding.vm = viewModel
        dataBinding.fragment = this
        viewModel.initData()

        if (!BiometricHelper.canAuthenticate(requireContext())) {
//            dataBinding.layoutBiometric.gone()
        }

        dataBinding.buttonAccountNoSetting.isChecked = AppConstant.IS_MARK_ACCOUNT
//        dataBinding.buttonBiometric.isChecked = AppPrefs.hasString(AppConstant.BIOMETRIC_TOKEN)
    }

    fun onChangePinClick() {
        gotoPage(
            SettingFragmentDirections.gotoChangePinPage()
        )

    }

    fun onBiometricSettingClick() {
        if (viewModel.biometricSettingValue.value == true) {
            val args = Bundle().apply {
                putString(
                    MemberPinDialog.DIALOG_TITLE,
                    getString(R.string.label_setting_scan_biometric_bar)
                )
                putString(
                    MemberPinDialog.DIALOG_DESC,
                    getString(R.string.label_setting_scan_biometric_title)
                )
            }

            MemberPinDialog(
                onEnterPin = { pinNo ->
                    if (pinNo.length == 6) {
                        viewModel.onBiometricClick(pinNo)
                    } else {
                        viewModel.biometricSettingValue.value = false
                        viewModel.onBiometricClick("")
                    }
                },
                onCancelPin = {
                    viewModel.biometricSettingValue.value = false
                }
            ).apply {
                arguments = args
            }.show(childFragmentManager)
        } else {
            viewModel.onBiometricClick("")
        }
    }

    fun onAccountManagementClick() {
        gotoPage(SettingFragmentDirections.gotoAccountManagePage())
    }

    fun onAccountMaskingClick() {
        viewModel.onAccountMasking(
            if (dataBinding.buttonAccountNoSetting.isChecked) {
                Flag.Y
            } else {
                Flag.N
            }
        )
    }

    fun onHelpClick() {
        gotoPage(SettingFragmentDirections.gotoHelpPage())
    }


}