package com.krungsri.coop.ui.base


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.google.android.material.appbar.MaterialToolbar
import com.krungsri.coop.MainActivity
import com.krungsri.coop.R
import com.krungsri.coop.component.di.Injectable
import com.krungsri.coop.component.util.autoCleared
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.visible
import com.krungsri.coop.lib.eventbut.AppEvent
import com.krungsri.coop.lib.eventbut.EventBus
import com.krungsri.coop.lib.eventbut.ToggleLoading
import com.krungsri.coop.lib.lifecycle.observeEvent
import com.krungsri.coop.ui.base.binding.FragmentDataBindingComponent
import timber.log.Timber
import javax.inject.Inject


abstract class BaseFragment<FragmentDataBinding : ViewDataBinding,
        FragmentViewModel : BaseViewModel>
    : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var viewModel: FragmentViewModel
    protected lateinit var dataBindingComponent: DataBindingComponent

    protected var dataBinding by autoCleared<FragmentDataBinding>()
    abstract val layoutId: Int

    abstract val viewModelClass: Class<FragmentViewModel>

    protected var toolbar: MaterialToolbar? = null
    open fun onCreateView(rootView: View) {}

    abstract fun onFragmentStart()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.onAlertEvent.observeEvent(viewLifecycleOwner, this::onAlert)
        viewModel.onViewLoadingEvent.observeEvent(viewLifecycleOwner, this::onViewLoading)
        viewModel.onExitAppEvent.observeEvent(viewLifecycleOwner) {
            Timber.d("[EXIT]onExitAppEvent = %s", it)
            activity?.run {
                finish()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }

        EventBus.listen(ToggleLoading::class.java)
            .subscribe {
                Timber.d("ToggleLoading :: %s", it)
                when (it) {
                    ToggleLoading.ShowViewLoading -> onViewLoading(true)
                    ToggleLoading.HideViewLoading -> onViewLoading(false)
                    else -> {
                    }
                }
            }.also {
                viewModel.addDisposableInternal(it)
            }

        onFragmentStart()
    }


    open fun showOkAlert(
        code: String? = "",
        title: String? = null,
        message: String,
        onDismiss: (() -> Unit)? = null
    ) {
        viewModel.onAlertEvent.setEventValue(
            Alert.OkDialog(
                code = code,
                title = title,
                message = message,
                onDismiss = onDismiss
            )
        )
    }

    protected fun popBackStack() {
        Timber.d("popBackStack")
        findNavController().popBackStack()
    }

    protected fun popToRoot() {
        Timber.d("popToRoot")
        findNavController().navigateUp()
    }

    open fun onNavBackClick() {
        Timber.d("onNavBackClick")
        popBackStack()
    }

    protected fun onViewLoading(show: Boolean) {
//        Timber.d("onViewLoading %s", show)
        if (show) {
            dataBinding.root.findViewById<View>(R.id.loadingView)?.visible()
        } else {
            dataBinding.root.findViewById<View>(R.id.loadingView)?.gone()
        }
    }

    protected fun onAlert(alert: Alert) {
        EventBus.publish(ToggleLoading.HideProgress)
        if (alert is Alert.Toast) {
            context?.run {
                AlertMessageDialog.showToast(this, alert)
            }
        } else {
            AlertMessageDialog.create(alert).show(childFragmentManager)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBindingComponent = FragmentDataBindingComponent(this)
        DataBindingUtil.setDefaultComponent(dataBindingComponent)
        dataBinding = DataBindingUtil.inflate(
            inflater,
            layoutId,
            container,
            false, dataBindingComponent
        )

        dataBinding.lifecycleOwner = this
        onCreateView(dataBinding.root)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(viewModelClass)
        dataBinding.lifecycleOwner = viewLifecycleOwner
        (viewModel as BaseViewModel).apply {
            gotoPage = {
                gotoPage(it)
            }
            popBackStack = {
                popBackStack()
            }
            popToRoot = {
                popToRoot()
            }
        }
        toolbar = dataBinding.root.findViewById(R.id.toolbar)
        toolbar?.setOnClickListener {
            onNavBackClick()
        }
    }


    private fun navController() = findNavController()
    protected fun gotoPage(page: NavDirections) {
        Timber.d("currentDestination = %s, page=%s", navController().currentDestination?.id, page.actionId)
        try {
            if (navController().currentDestination?.id == null) {
                onAlert(
                    Alert.OkDialog(
                        message = getString(R.string.err_refresh_app), onDismiss = {
                            EventBus.publish(AppEvent.ReloadApp)
                        })
                )
            } else {
                navController().navigate(page)
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

}



