package com.krungsri.coop.ui.component.member.pin

import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentMemberPinNewBinding
import com.krungsri.coop.extension.visible
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.base.PinListener

class MemberPinNewFragment :
    BaseFragment<FragmentMemberPinNewBinding, MemberPinViewModel>(), PinListener {
    override val layoutId: Int = R.layout.fragment_member_pin_new
    override val viewModelClass: Class<MemberPinViewModel> =
        MemberPinViewModel::class.java

    override fun onFragmentStart() {
        dataBinding.pinListener = this

        dataBinding.pinDot.onKeyCompleted = { pinNo ->
            if (validatePin(pinNo)) {
                args.memberPinContext.newPin = pinNo
                dataBinding.pinDot.clearPin()
                gotoPage(
                    MemberPinNewFragmentDirections.gotoConfirmNewPinPage(
                        args.memberPinContext
                    )
                )
            } else {
                dataBinding.pinDot.clearPin()
            }
        }
        dataBinding.textTitle = getString(R.string.label_member_new_pin_step1_title)
        dataBinding.imgInfo.visible()
        dataBinding.imgInfo.setOnClickListener {
            PinInfoDialog().show(childFragmentManager)
        }
        if (args.memberPinContext.memberCode.isNullOrEmpty()) {
            args.memberPinContext.memberCode = AppPrefs.getStaticString(AppConstant.MEMBER_CODE)
        }

    }

    private fun validatePin(pinNo: String): Boolean {
        if (pinNo.isNotEmpty() && AppConstant.PIN_SEQ.indexOf(pinNo) != -1) {
            showOkAlert(message = getString(R.string.error_member_change_pin_seq))
        } else if (SecurityHelper.checkPinDuplicateNumber(pinNo, 4)) {
            showOkAlert(message = getString(R.string.error_member_change_pin_pin_duplicate))
        } else if (args.memberPinContext.memberCode != null && args.memberPinContext.memberCode == pinNo) {
            showOkAlert(message = getString(R.string.error_member_change_pin_member_code))
        } else {
            return true
        }
        return false
    }

    private val args by navArgs<MemberPinNewFragmentArgs>()
    override fun onPinClick(pinNo: String) {
        dataBinding.pinDot.addPIN(pinNo)
    }
}