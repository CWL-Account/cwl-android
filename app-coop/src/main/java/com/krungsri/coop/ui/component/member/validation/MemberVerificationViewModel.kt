package com.krungsri.coop.ui.component.member.validation

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.member.MemberConfirmationRequest
import com.krungsri.coop.model.member.MemberVerificationRequest
import com.krungsri.coop.model.member.MemberVerificationResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.member.confirm.MemberConfirmContext
import com.krungsri.coop.usecase.member.MemberVerificationUseCase
import timber.log.Timber
import javax.inject.Inject

class MemberVerificationViewModel @Inject
constructor(
    private val memberVerificationUseCase: MemberVerificationUseCase
) : BaseViewModel() {

    val inputCitizenNo = MutableLiveData("")
    val inputMobileNo = MutableLiveData("")
    val inputAccountNo = MutableLiveData("")
    val inputMemberCode = MutableLiveData("")

    val memberContext = MutableLiveData<MemberVerificationContext>()

    val displayRegisterFlow = MutableLiveData(View.GONE)
    val displayForgotPinFlow = MutableLiveData(View.GONE)

    val displayTitlePage = MutableLiveData("")
    val displayDescPage = MutableLiveData("")

    fun initData(_memberVerificationContext: MemberVerificationContext) {
        memberContext.value = _memberVerificationContext
        _memberVerificationContext.verificationData?.run {
            inputCitizenNo.value = FormatHelper.formatCitizenNo(citizenNo)
            inputAccountNo.value = FormatHelper.formatAccountNo(bayAccountNo)
            inputMobileNo.value = FormatHelper.formatMobileNNo(mobileNo)
            inputMemberCode.value = memberCode
        }

        Timber.d("verificationType = %s", _memberVerificationContext.verificationType)
        when (_memberVerificationContext.verificationType) {
            VerificationType.REGISTER -> {
                displayRegisterFlow.value = View.VISIBLE
                displayTitlePage.value = appContext.getString(R.string.label_member_validation_register_title)
                displayDescPage.value = appContext.getString(R.string.label_member_validation_register_desc)
            }
            VerificationType.UNLOCK -> {
                displayRegisterFlow.value = View.VISIBLE
                displayTitlePage.value = appContext.getString(R.string.label_member_validation_unlock_title)
                displayDescPage.value = appContext.getString(R.string.label_member_validation_unlock_desc)
            }
            VerificationType.FORGOT_PIN -> {
                displayForgotPinFlow.value = View.VISIBLE
                displayTitlePage.value = appContext.getString(R.string.label_member_validation_forgot_pin_title)
                displayDescPage.value = appContext.getString(R.string.label_member_validation_forgot_pin_desc)
            }
            else -> {
            }
        }
    }

    fun memberVerification() {
        memberContext.value?.apply {


            verificationData = MemberVerificationRequest(
                verificationType = verificationType,
                uuid = AppHelper.getDeviceId(appContext),
                bayAccountNo = inputAccountNo.value?.replace("-", "") ?: "",
                mobileNo = inputMobileNo.value?.replace("-", "") ?: "",
                citizenNo = inputCitizenNo.value?.replace("-", "") ?: "",
                memberCode = inputMemberCode.value?.replace("-", "") ?: "",
                forceRegister = Flag.N,
                policyFlag = policyFlag
            )
        }?.also {
            memberVerification(Flag.N)
        } ?: run {
            showOkAlert(message = appContext.getString(R.string.error_input))
        }
    }

    private fun memberVerification(_forceRegister: Flag) {
        memberContext.value?.verificationData?.apply {
            forceRegister = _forceRegister
        }?.also {
            memberVerificationUseCase.build(it)
                .doToggleLoading()
                .subscribeWithViewModel(
                    this@MemberVerificationViewModel,
                    this@MemberVerificationViewModel::onMemberVerificationSuccess,
                    this@MemberVerificationViewModel::onApiError
                )
                .disposedBy(this@MemberVerificationViewModel)
        } ?: run {
            showOkAlert(message = appContext.getString(R.string.error_system))
        }
    }


    private fun onMemberVerificationSuccess(response: MemberVerificationResponse) {
        memberContext.value?.verificationData?.run {
            gotoPage(
                MemberVerificationFragmentDirections.gotoMemberConfirmPage(
                    memberConfirmContext = MemberConfirmContext(
                        verificationData = MemberConfirmationRequest(
                            verificationType = verificationType,
                            verificationToken = response.verificationToken,
                            forceRegister = forceRegister,
                            memberCode = memberCode,
                            citizenNo = citizenNo,
                            bayAccountNo = bayAccountNo,
                            mobileNo = mobileNo,
                            uuid = AppHelper.getDeviceId(appContext),
                            policyFlag = policyFlag
                        ),
                        firstName = response.firstName,
                        lastName = response.lastName
                    )
                )
            )
        }
    }

    override fun onApiError(apiError: ApiError) {
        when (apiError.code) {
            AppConstant.CODE_DUPLICATE_DEVICE -> {
                showSelectAlert(apiError,
                    onOkDismiss = {
                        memberVerification(Flag.Y)
                    },
                    onCancelDismiss = {
                    })
            }
            else -> {
                super.onApiError(apiError)
            }
        }
    }

}