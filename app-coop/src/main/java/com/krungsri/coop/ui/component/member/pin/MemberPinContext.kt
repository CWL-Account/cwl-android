package com.krungsri.coop.ui.component.member.pin

import android.os.Parcelable
import com.krungsri.coop.ui.component.member.landing.LandingDestination
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberPinContext(
    val destination: LandingDestination,
    var memberCode: String? = null,
    var newPin: String? = null
) : Parcelable