package com.krungsri.coop.ui.component.member.confirm

import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentMemberConfirmBinding
import com.krungsri.coop.ui.base.BaseFragment

class MemberConfirmFragment :
    BaseFragment<FragmentMemberConfirmBinding, MemberConfirmViewModel>() {

    override val layoutId: Int = R.layout.fragment_member_confirm

    override val viewModelClass: Class<MemberConfirmViewModel> = MemberConfirmViewModel::class.java

    private val args by navArgs<MemberConfirmFragmentArgs>()
    override fun onFragmentStart() {
        dataBinding.vm = viewModel
        dataBinding.fragment = this
        viewModel.initData(args.memberConfirmContext)
    }




}