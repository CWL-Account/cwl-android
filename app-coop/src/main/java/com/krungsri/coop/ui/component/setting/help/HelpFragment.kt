package com.krungsri.coop.ui.component.setting.help

import android.Manifest
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.Observer
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentHelpBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.model.coop.CoopInfoResponse
import com.krungsri.coop.ui.base.Alert
import com.krungsri.coop.ui.base.BaseFragment
import timber.log.Timber


class HelpFragment : BaseFragment<FragmentHelpBinding, HelpViewModel>() {
    override val layoutId: Int = R.layout.fragment_help

    override val viewModelClass: Class<HelpViewModel> = HelpViewModel::class.java

    override fun onFragmentStart() {
        viewModel.getCoopInfo()
        viewModel.coopInfo.observe(viewLifecycleOwner, Observer {
            initData(it)
        })
        initPhoneCall()
    }

    private fun initData(coopInfo: CoopInfoResponse) {
        if (coopInfo.telephoneNo.isNullOrEmpty()) {
            dataBinding.layoutCoopNo.gone()
        } else {
            dataBinding.textCoopNo.text = FormatHelper.formatPhoneNo(coopInfo.telephoneNo)
            dataBinding.imgCoopCall.setOnClickListener {
                showCall(getString(R.string.coop_name), coopInfo.telephoneNo)
            }
        }

        if (coopInfo.telephoneTime.isNullOrEmpty() && coopInfo.telephoneDay.isNullOrEmpty()) {
            dataBinding.layoutCoopOpenDay.gone()
        } else {
            coopInfo.telephoneTime?.run {
                dataBinding.textCoopOpenTime.text = FormatHelper.formatMobileNNo(this)
            }
            coopInfo.telephoneDay?.run {
                dataBinding.textCoopOpenDay.text = this
            }
        }

        coopInfo.bayCallCenterNo?.run {
            dataBinding.textBayCoopNo.text = this
        }
        coopInfo.bayCallCenterDay?.run {
            dataBinding.textBayCoopDay.text = this
        }
        coopInfo.bayCallCenterTime?.run {
            dataBinding.textBayCoopTime.text = this
        }


    }

    private fun initPhoneCall() {
        val bayCoopCallCenterNo = dataBinding.textBayCoopNo.text.toString().replace("-", "")
//        val textCoopNo = dataBinding.textCoopNo.text.toString().replace("-", "")

        dataBinding.imgBayCoopCall.setOnClickListener {
            showCall(getString(R.string.label_help_bay_department), bayCoopCallCenterNo)
        }

        dataBinding.imgBayCCCall.setOnClickListener {
            showCall(
                getString(R.string.label_help_bay_cc_title),
                getString(R.string.label_help_bay_cc_tel)
            )
        }


    }

    private fun showCall(callTo: String, phoneNo: String) {
        Timber.d("showCall %s :: %s",callTo, phoneNo)
        viewModel.onAlertEvent.setEventValue(
            Alert.SelectDialog(
                code = "200",
                title = getString(R.string.label_call_phone_title),
                okButtonLabel = getString(R.string.button_call),
                message = getString(R.string.label_call_phone_msg, callTo, phoneNo),
                onOkDismiss = {
                    call(phoneNo)
                }
            )
        )
    }

    private fun call(phoneNo: String) {
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.CALL_PHONE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        val intent = Intent(Intent.ACTION_CALL)
                        intent.data = Uri.parse("tel:$phoneNo")
                        this@HelpFragment.startActivity(intent)
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()
    }


}