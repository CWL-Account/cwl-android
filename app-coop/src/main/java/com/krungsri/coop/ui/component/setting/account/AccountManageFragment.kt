package com.krungsri.coop.ui.component.setting.account

import androidx.recyclerview.widget.LinearLayoutManager
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentSettingAccountManageBinding
import com.krungsri.coop.model.account.AccountInfo
import com.krungsri.coop.ui.base.BaseFragment

class AccountManageFragment :
    BaseFragment<FragmentSettingAccountManageBinding, AccountManageViewModel>() {

    override val layoutId: Int = R.layout.fragment_setting_account_manage

    override val viewModelClass: Class<AccountManageViewModel> = AccountManageViewModel::class.java

    override fun onFragmentStart() {
        dataBinding.fragment = this
        dataBinding.vm = viewModel
        viewModel.accountList()

        viewModel.accountInfoList.observe(viewLifecycleOwner, { accountInfoList ->
            if (accountInfoList.isNotEmpty()){
                settingAccountListView(accountInfoList)
            }
        })

        viewModel.removeAccountUpdate.observe(viewLifecycleOwner, {
            val adapter = dataBinding.recyclerView.adapter as SettingAccountRemoveAdapter
            adapter.removeItem(it)
        })
    }

    private fun settingAccountListView(accountInfoList: MutableList<AccountInfo>) {
        val adapter = SettingAccountRemoveAdapter(onRemoveAccount = {
            viewModel.removeAccount(it)
        })
        val sortedList = accountInfoList.sortedWith((compareBy(AccountInfo::accountType, AccountInfo::coopAccountNo)))
        adapter.replace(sortedList.toMutableList())

        dataBinding.recyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        dataBinding.recyclerView.adapter = adapter
    }
}