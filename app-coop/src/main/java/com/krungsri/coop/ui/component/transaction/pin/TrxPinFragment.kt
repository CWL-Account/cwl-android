package com.krungsri.coop.ui.component.transaction.pin

import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentTrxPinBinding
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.base.PinListener


class TrxPinFragment : BaseFragment<FragmentTrxPinBinding, TrxPinViewModel>(),
    PinListener {
    override val layoutId: Int = R.layout.fragment_trx_pin
    override val viewModelClass: Class<TrxPinViewModel> =
        TrxPinViewModel::class.java

    private val args by navArgs<TrxPinFragmentArgs>()

    override fun onFragmentStart() {
        viewModel.initData(args.trxPinContext)
        dataBinding.pinListener = this
        dataBinding.pinDot.onKeyCompleted = { pinNo ->
            if (pinNo.length == 6) {
                viewModel.transactionConfirm(pinNo)
            }
        }
        viewModel.invalidPin.observe(this, {
            dataBinding.pinDot.clearPin()
        })
    }


    override fun onPinClick(pinNo: String) {
        dataBinding.pinDot.addPIN(pinNo)
    }
}