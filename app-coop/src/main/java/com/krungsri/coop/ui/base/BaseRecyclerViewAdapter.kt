package com.krungsri.coop.ui.base

import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView


/**
 * Created by Santi T.
 */
class BaseRecyclerViewHolder<T : ViewDataBinding> internal constructor(val binding: T) :
    RecyclerView.ViewHolder(binding.root)

abstract class BaseRecyclerViewAdapter<MODEL, ItemBinding : ViewDataBinding> :
    RecyclerView.Adapter<BaseRecyclerViewHolder<ItemBinding>>() {
    private var items: MutableList<MODEL> = mutableListOf()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseRecyclerViewHolder<ItemBinding> {
        val binding = createBinding(parent)

        return BaseRecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: BaseRecyclerViewHolder<ItemBinding>,
        position: Int
    ) {
        bind(holder.binding, position, items[position])
        holder.binding.executePendingBindings()
    }

    fun getItems(): MutableList<MODEL>? {
        return items
    }

    @MainThread
    open fun replace(update: MutableList<MODEL>) {
        items.clear()
        items.addAll(update)
        notifyDataSetChanged()
    }

    @MainThread
    open fun add(item: MODEL) {
        items.add(item)
        notifyItemInserted(itemCount)
    }

    @MainThread
    open fun add(item: MODEL, position: Int) {
        items.add(position, item)
        notifyItemInserted(position)
    }

    @MainThread
    open fun add(update: MutableList<MODEL>) {
        val size = items.size
        items.addAll(update)
        notifyItemInserted(size)

    }

    open fun delete(position: Int) {
        if (itemCount > 0 && itemCount > position) {
            val notifyEnd = itemCount - 1
            items.removeAt(position)
            if (position == 0) {
                notifyDataSetChanged()
            } else {
                if (position < notifyEnd) notifyItemRangeRemoved(
                    position,
                    notifyEnd
                ) else notifyItemRemoved(position)
            }
        }
    }

    open fun deleteWithNotifyAll(position: Int) {
        if (itemCount > 0 && items.size > position) {
            items.removeAt(position)
            notifyDataSetChanged()
        }
    }

    open override fun getItemCount(): Int {
        return items.size
    }

    fun getItem(position: Int): MODEL? {
        return items.let {
            if (itemCount > 0 && position < itemCount) {
                it[position]
            } else null
        }

    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    protected abstract fun createBinding(parent: ViewGroup): ItemBinding
    protected abstract fun bind(binding: ItemBinding, position: Int, item: MODEL)
    fun onViewPaused() {}
    fun onViewResumed() {}
    fun onViewDestroyed() {}
}
