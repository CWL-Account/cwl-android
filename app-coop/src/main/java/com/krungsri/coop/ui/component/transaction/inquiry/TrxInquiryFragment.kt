package com.krungsri.coop.ui.component.transaction.inquiry

import android.text.InputFilter
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentTrxInquiryBinding
import com.krungsri.coop.extension.*
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.transaction.SourceAccount
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.component.home.HorizontalMarginItemDecoration
import timber.log.Timber


class TrxInquiryFragment :
    BaseFragment<FragmentTrxInquiryBinding, TrxInquiryViewModel>() {

    override val layoutId: Int = R.layout.fragment_trx_inquiry

    override val viewModelClass: Class<TrxInquiryViewModel>
        get() = TrxInquiryViewModel::class.java


    private val args by navArgs<TrxInquiryFragmentArgs>()
    override fun onFragmentStart() {
        dataBinding.vm = viewModel
        viewModel.initData(args.inquiryContext)

        dataBinding.navTitle = if (args.inquiryContext.coopService.serviceName.isEmpty()) {
            when (args.inquiryContext.initialForm.serviceCode) {
                ServiceCode.TRANSFERRING -> getString(R.string.label_transaction_title_inquiry_transfer)
                ServiceCode.WITHDRAW -> getString(R.string.label_transaction_title_inquiry_withdraw)
                ServiceCode.DEPOSIT -> getString(R.string.label_transaction_title_inquiry_deposit)
                ServiceCode.LOAN_WITHDRAW -> getString(R.string.label_transaction_title_inquiry_loan_withdraw)
                ServiceCode.LOAN_PAYMENT -> getString(R.string.label_transaction_title_inquiry_loan_payment)
                ServiceCode.BILL_PAYMENT -> getString(R.string.label_transaction_title_inquiry_bill_payment)
                else -> getString(R.string.label_transaction_title_inquiry)
            }
        } else {
            args.inquiryContext.coopService.serviceName
        }
        when (args.inquiryContext.initialForm.serviceCode) {
            ServiceCode.LOAN_WITHDRAW,
            ServiceCode.LOAN_PAYMENT,
            ServiceCode.BILL_PAYMENT -> {
                dataBinding.buttonConfirm.setBackgroundResource(R.drawable.button_main_loan)
                dataBinding.editAmount.setBackgroundResource(R.drawable.bg_edittext_secondary)
                dataBinding.editNote.setBackgroundResource(R.drawable.bg_edittext_secondary)
            }
            else -> {
            }
        }

        val maxNoteLength = resources.getInteger(R.integer.input_transfer_note_max)
        dataBinding.fragment = this
        dataBinding.editAmount.moneyFormat()
        dataBinding.txtNoteCount.text =
            "0/$maxNoteLength"
        val emojiFilter = InputFilter { source, start, end, _, _, _ ->
            if (source.length > 35) return@InputFilter ""
            for (i in start until end) {
                val type = Character.getType(source[i])
                if (type == Character.SURROGATE.toInt() || type == Character.OTHER_SYMBOL.toInt()) {
                    return@InputFilter ""
                }
            }
            null
        }
        val lengthFilter = InputFilter.LengthFilter(maxNoteLength)
        dataBinding.editNote.filters = arrayOf(lengthFilter, emojiFilter)


        dataBinding.editNote.doAfterTextChanged {
            dataBinding.txtNoteCount.text =
                "${it?.length}/$maxNoteLength"
        }

        dataBinding.viewPager.offscreenPageLimit = 1
        context?.run {
            val nextItemVisiblePx = resources.getDimension(R.dimen.viewpager_next_item_visible)
            val currentItemHorizontalMarginPx =
                resources.getDimension(R.dimen.viewpager_current_item_horizontal_margin)
            val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx
            val pageTransformer = ViewPager2.PageTransformer { page: View, position: Float ->
                page.translationX = -pageTranslationX * position
            }
            dataBinding.viewPager.setPageTransformer(pageTransformer)
            val itemDecoration = HorizontalMarginItemDecoration(
                this,
                R.dimen.viewpager_current_item_horizontal_margin
            )
            dataBinding.viewPager.addItemDecoration(itemDecoration)
        }

        viewModel.sourceAccountList.observe(viewLifecycleOwner, {
            initSourceAdapter(it)
        })

        viewModel.destinationAccountSelected.observe(viewLifecycleOwner, {
            dataBinding.txtSelect.invisible()
            dataBinding.txtToAccountName.visible()
            dataBinding.txtToAccountDesc.visible()
            dataBinding.txtToAccountNo.visible()
            dataBinding.txtToAccountName.text = it?.accountName
            dataBinding.txtToAccountDesc.text = it?.accountDesc
            it?.coopAccountNo?.run {
                dataBinding.txtToAccountNo.text =
                    FormatHelper.formatAccountNo(this, AppConstant.IS_MARK_ACCOUNT)
                dataBinding.imgDown.loadCircle(R.drawable.logo_coop_cycle_min)
            } ?: it?.bayAccountNo?.run {
                dataBinding.txtToAccountNo.text =
                    FormatHelper.formatAccountNo(this, AppConstant.IS_MARK_ACCOUNT)
                dataBinding.imgDown.loadCircle(R.drawable.logo_bay_cycle_min)
            } ?: run {
                dataBinding.txtSelect.visible()
                dataBinding.txtToAccountName.invisible()
                dataBinding.txtToAccountDesc.invisible()
                dataBinding.txtToAccountNo.invisible()
                dataBinding.imgDown.loadRes(R.drawable.ic_down)
            }
        })
//        scrollView.setOnClickListener {
//            hideKeyboard()
//        }
    }

    private fun initSourceAdapter(sourceAccountList: MutableList<SourceAccount>) {
        val adapter = SourceAccountAdapter(args.inquiryContext.productCode).apply {
            replace(sourceAccountList)
        }
        dataBinding.viewPager.adapter = adapter
        if (adapter.itemCount == 1) {
            viewModel.sourceAccountSelected.value = adapter.getItem(0)
        }

        if (args.inquiryContext.initialForm.destinationAccountList?.size ?: 0 == 1) {
            viewModel.destinationAccountSelected.value = args.inquiryContext.initialForm.destinationAccountList?.first()
        }

        if (adapter.itemCount > 1) {

            val initSelectAccountNo = args.inquiryContext.selectedAccountDesc.accountNo

            val idx = sourceAccountList.indexOfFirst {
                it.coopAccountNo?.equals(initSelectAccountNo) ?: it.bayAccountNo?.equals(initSelectAccountNo) ?: false
            }

            Timber.d(
                "setCurrentItem %s",
                args.inquiryContext.selectedAccountDesc.accountNo
            )
            Timber.d("setCurrentItem %d", idx)
            if (idx >= 0) {
                dataBinding.viewPager.post {
                    viewModel.sourceAccountSelected.value = sourceAccountList[idx]
                    dataBinding.viewPager.setCurrentItem(idx, true)
                }
            }
            dataBinding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

                override fun onPageSelected(position: Int) {

                    val sourceAccount = adapter.getItem(position)
                    viewModel.sourceAccountSelected.value = sourceAccount

                    viewModel.destinationAccountSelected.value?.run {
                        val isEquals = coopAccountNo?.equals(sourceAccount?.coopAccountNo)
                            ?: bayAccountNo?.equals(
                                sourceAccount?.bayAccountNo
                            ) ?: false
                        if (isEquals) {
                            viewModel.destinationAccountSelected.value = null
                        }
                    }

                    args.inquiryContext.initialForm.destinationAccountList?.run {
                        val selectAccountNo = viewModel.sourceAccountSelected.value?.bayAccountNo
                            ?: viewModel.sourceAccountSelected.value?.coopAccountNo

                        indexOfFirst {
                            it.coopAccountNo?.equals(selectAccountNo) ?: it.bayAccountNo?.equals(
                                selectAccountNo
                            ) ?: false
                        }.let { searchDescIdx ->
                            if (searchDescIdx > -1) {
                                this.toMutableList().also {
                                    it.removeAt(searchDescIdx)
                                }
                            } else
                                this
                        }.also { descList ->
                            if (descList.size == 1) {
                                viewModel.destinationAccountSelected.value = descList.first()
                            }
                        }
                    }
                }
            })
            dataBinding.tabLayoutDots.visible()
            TabLayoutMediator(dataBinding.tabLayoutDots, dataBinding.viewPager) { _, _ ->

            }.attach()
        }

    }

    fun onSelectedAccountToClick() {
        viewModel.destinationAccountList.value?.run {

            val selectAccountNo = viewModel.sourceAccountSelected.value?.bayAccountNo
                ?: viewModel.sourceAccountSelected.value?.coopAccountNo

            val idx = this.indexOfFirst {
                it.coopAccountNo?.equals(selectAccountNo) ?: it.bayAccountNo?.equals(
                    selectAccountNo
                ) ?: false
            }

            val descList = if (idx > -1) {
                this.toMutableList().also {
                    it.removeAt(idx)
                }
            } else
                this


            val dialog = DestinationAccountDialog.createDialog(descList, destinationSelected = {
                viewModel.destinationAccountSelected.value = it
            })
            dialog.show(childFragmentManager)
        }

    }
}
