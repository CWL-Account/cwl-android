package com.krungsri.coop.ui.component.splash

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.policy.PolicyRequest
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.model.policy.PolicyType
import com.krungsri.coop.model.token.TokenValidationRequest
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.member.landing.LandingDestination
import com.krungsri.coop.ui.component.member.landing.MemberLandingContext
import com.krungsri.coop.ui.component.member.pin.MemberPinContext
import com.krungsri.coop.ui.component.policy.PolicyContext
import com.krungsri.coop.usecase.policy.GetLatestPolicyActiveUseCase
import com.krungsri.coop.usecase.token.TokenVerificationUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class SplashScreenViewModel @Inject
constructor(
    private val tokenVerificationUseCase: TokenVerificationUseCase,
    private val getLatestPolicyActiveUseCase: GetLatestPolicyActiveUseCase,
) : BaseViewModel() {

    private var startTime: Long? = null
    val tokenVerification = MutableLiveData<ApiResponse<Any>>()

    @SuppressLint("HardwareIds")
    fun tokenVerification() {
        val deviceId = AppHelper.getDeviceId(appContext)
        startTime = System.currentTimeMillis()
        tokenVerificationUseCase.build(TokenValidationRequest(uuid = deviceId))
            .subscribeWithViewModel(
                this,
                {
                    tokenVerification.value = it
                },
                this::onApiError
            )
            .disposedBy(this)
    }


    fun onTokenVerificationSuccess(response: ApiResponse<Any>) {
        Timber.d("onTokenVerificationSuccess :: %s %s", response.code, response.message)
        when (response.code) {
            AppConstant.CODE_SUCCESS200 -> {
                navToPage(
                    SplashScreenFragmentDirections.gotoLoginPage()
                )
            }
            AppConstant.CODE_SUCCESS201 -> {
                getLatestPolicyActive(PolicyType.COOP_TC)
            }
            else -> {
                showOkAlert(message = response.message)
            }
        }

    }

    override fun onApiError(apiError: ApiError) {
        Timber.d("onApiError %s", apiError.code)
        when (apiError.code) {
            AppConstant.CODE_INTERNET_CONNECT -> {
                showOkAlert(apiError, onDismiss = {
                    onExitAppEvent.postEventValue("INTERNET_CONNECT");
                })
            }
            AppConstant.CODE_REQUEST_NEW_PIN -> {
                showOkAlert(apiError, onDismiss = {
                    navToPage(
                        SplashScreenFragmentDirections.gotoSetupNewPinPage(
                            MemberPinContext(
                                destination = LandingDestination.NEW_PIN_UNLOCK
                            )
                        )
                    )
                })
            }
            else -> {
                showOkAlert(apiError, onDismiss = {
                    if (AppConstant.CODE_TOKEN_EXR.contains(apiError.code)
                        || apiError.code == AppConstant.CODE_DEVICE_INVALID
                    ) {
                        AppPrefs.clearAll()
                        getLatestPolicyActive(PolicyType.COOP_TC)
                    } else {
                        onExitAppEvent.setEventValue(
                            "ERROR_LOAD_COOP_TC " + AppConstant.CODE_TOKEN_EXR.contains(
                                apiError.code
                            )
                        )
                    }
                })
            }
        }

    }

    private fun getLatestPolicyActive(policyType: PolicyType) {
        getLatestPolicyActiveUseCase.build(PolicyRequest(policyType))
            .subscribeWithViewModel(this, this::onGetLatestPolicySuccess, this::onApiError)
            .disposedBy(this)
    }

    private fun onGetLatestPolicySuccess(response: PolicyResponse) {
        when {
            response.isNew == Flag.Y -> {
                navToPage(
                    SplashScreenFragmentDirections.gotoPolicyPage(
                        policyContext = PolicyContext(true),
                        policyData = response
                    )
                )
            }
            response.policyType == PolicyType.COOP_TC -> {
                getLatestPolicyActive(PolicyType.COOP_CONSENT)
            }
            response.policyType == PolicyType.COOP_CONSENT -> {
                getLatestPolicyActive(PolicyType.COOP_OTHER)
            }
            else -> {
                navToPage(
                    SplashScreenFragmentDirections.gotoRegisterPage(
                        MemberLandingContext(
                            destination = LandingDestination.GOTO_REGISTER,
                            verificationType = VerificationType.REGISTER
                        )
                    )
                )
            }
        }
    }

    private fun getDelay(): Long {
        return startTime?.let {
            2500 - (System.currentTimeMillis() - it)
        } ?: 2500
    }

    private fun navToPage(page: NavDirections, gotoNow: Boolean = false) {
        Timber.d("getDelay = %s", getDelay())
        if (gotoNow) {
            gotoPage(page)
        } else {
            GlobalScope.launch(Dispatchers.Main) {
                Timber.d("navToPage 1")
                delay(getDelay())
                Timber.d("navToPage 2")
                navToPage(page, true)
            }
//            Handler(Looper.getMainLooper()).postDelayed({
//                navToPage(page, true)
//            }, getDelay())
        }
    }
}
