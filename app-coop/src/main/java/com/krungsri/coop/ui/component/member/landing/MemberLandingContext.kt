package com.krungsri.coop.ui.component.member.landing

import android.os.Parcelable
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.policy.PolicyFlag
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberLandingContext(
    val destination: LandingDestination,
    var verificationType: VerificationType? = null,
    var verificationToken: String? = null,
    var policyFlag: MutableList<PolicyFlag>? = null
) : Parcelable

