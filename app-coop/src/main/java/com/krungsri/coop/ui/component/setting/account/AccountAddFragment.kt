package com.krungsri.coop.ui.component.setting.account

import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.krungsri.coop.databinding.FragmentSettingAccountAddBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.invisible
import com.krungsri.coop.extension.visible
import com.krungsri.coop.model.account.AccountInfo
import com.krungsri.coop.ui.base.BaseFragment
import timber.log.Timber

class AccountAddFragment : BaseFragment<FragmentSettingAccountAddBinding, AccountAddViewModel>() {
    override val layoutId: Int = com.krungsri.coop.R.layout.fragment_setting_account_add

    override val viewModelClass: Class<AccountAddViewModel> = AccountAddViewModel::class.java

    private val args by navArgs<AccountAddFragmentArgs>()
    override fun onFragmentStart() {
        dataBinding.fragment = this
        viewModel.accountList(args.accountAddContext.verificationToken)
        viewModel.accountInActive.observe(viewLifecycleOwner, Observer {
            if (it.coopAccountList.isEmpty()) {
                dataBinding.layoutNotFound.visible()
                dataBinding.layoutData.invisible()
            } else {
                settingAccountListView(it.coopAccountList)
            }
        })

    }

    private fun settingAccountListView(accountInfoList: MutableList<AccountInfo>) {
        val adapter = SettingAccountAddAdapter()
        val sortedList = accountInfoList.sortedWith((compareBy(AccountInfo::accountType, AccountInfo::coopAccountNo)))
        adapter.replace(sortedList.toMutableList())

        dataBinding.recyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        dataBinding.recyclerView.adapter = adapter
        dataBinding.layoutNotFound.gone()
        dataBinding.layoutData.visible()
    }

    fun addNewAccount() {
        val adapter = dataBinding.recyclerView.adapter as SettingAccountAddAdapter
        Timber.d("addNewAccount : %s", adapter.getSelectedAccountList())
        viewModel.updateAccount(adapter.getSelectedAccountList())
    }
}