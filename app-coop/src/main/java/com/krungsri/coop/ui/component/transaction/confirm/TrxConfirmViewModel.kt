package com.krungsri.coop.ui.component.transaction.confirm

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.ui.base.BaseViewModel
import javax.inject.Inject

class TrxConfirmViewModel @Inject
constructor() : BaseViewModel() {


    val noteDisplay = MutableLiveData(false)
    val noteText = MutableLiveData("")
    val invalidPin = MutableLiveData(true)
    private val confirmContext = MutableLiveData<TrxConfirmContext>()


    fun initData(trxInquiryContext: TrxConfirmContext) {
        confirmContext.value = trxInquiryContext
        noteDisplay.value = trxInquiryContext.inquiryResponse.note?.isEmpty() == false
        noteText.value = trxInquiryContext.inquiryResponse.note ?: ""
    }





}