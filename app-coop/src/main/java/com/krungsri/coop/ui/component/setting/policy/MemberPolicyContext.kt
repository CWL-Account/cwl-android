package com.krungsri.coop.ui.component.setting.policy

import android.os.Parcelable
import com.krungsri.coop.model.policy.PolicyResponse
import com.krungsri.coop.model.policy.PolicyType
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberPolicyContext(
    val policyType: PolicyType,
    val policyList: MutableList<PolicyResponse>
) : Parcelable