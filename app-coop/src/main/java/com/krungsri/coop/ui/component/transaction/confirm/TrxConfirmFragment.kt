package com.krungsri.coop.ui.component.transaction.confirm

import android.view.Gravity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentTrxConfirmBinding
import com.krungsri.coop.extension.loadCircle
import com.krungsri.coop.extension.setAmount
import com.krungsri.coop.extension.setColor
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.lib.eventbut.EventBus
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.ui.base.Alert
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.component.transaction.pin.TrxPinContext

class TrxConfirmFragment :
    BaseFragment<FragmentTrxConfirmBinding, TrxConfirmViewModel>() {

    override val layoutId: Int = R.layout.fragment_trx_confirm

    override val viewModelClass = TrxConfirmViewModel::class.java


    private val args by navArgs<TrxConfirmFragmentArgs>()

    override fun onFragmentStart() {
        dataBinding.fragment = this
        dataBinding.vm = viewModel

        viewModel.initData(args.confirmContext)
        var isLoan = false
        dataBinding.navTitle = when (args.confirmContext.serviceCode) {
            ServiceCode.TRANSFERRING -> getString(R.string.label_transaction_title_confirm_transfer)
            ServiceCode.WITHDRAW -> getString(R.string.label_transaction_title_confirm_withdraw)
            ServiceCode.DEPOSIT -> getString(R.string.label_transaction_title_confirm_deposit)
            ServiceCode.LOAN_WITHDRAW -> {
                isLoan = true
                getString(R.string.label_transaction_title_confirm_loan_withdraw)
            }
            ServiceCode.LOAN_PAYMENT -> {
                isLoan = true
                getString(R.string.label_transaction_title_confirm_loan_payment)
            }
            ServiceCode.BILL_PAYMENT -> {
                isLoan = true
                getString(R.string.label_transaction_title_confirm_bill_payment)
            }
            else -> getString(R.string.label_transaction_title_confirm)
        }


        if (isLoan) {
            dataBinding.textFromAccountTitle.setColor(R.color.font_confirm_color)
            dataBinding.textToAccountTitle.setColor(R.color.font_confirm_color)
            dataBinding.imgL1.setBackgroundResource(R.color.font_confirm_color)
            dataBinding.imgL2.setBackgroundResource(R.drawable.circle_normal_loan)
            dataBinding.imgL3.setBackgroundResource(R.color.font_confirm_color)
            dataBinding.buttonConfirm.setBackgroundResource(R.drawable.button_main_loan)
            dataBinding.buttonCancel.setBackgroundResource(R.drawable.button_empty_loan)
        }

        val sourceAccount = args.confirmContext.sourceAccount
        val destinationAccount = args.confirmContext.destinationAccount
        val inquiryTrans = args.confirmContext.inquiryResponse
        dataBinding.textFromAccountName.text = sourceAccount.accountName
        dataBinding.textFromAccountDesc.text = sourceAccount.accountDesc
        dataBinding.textFromAccountNo.text = FormatHelper.formatAccountNo(
            sourceAccount.coopAccountNo ?: sourceAccount.bayAccountNo ?: "",
            AppConstant.IS_MARK_ACCOUNT
        )

        activity?.window?.statusBarColor = ContextCompat.getColor(
            requireContext(),
            R.color.confirm_tran_top
        )

        dataBinding.textToAmountValue.text =
            FormatHelper.formatAmount(inquiryTrans.transactionAmount)
        dataBinding.textToAccountName.text = destinationAccount.accountName
        dataBinding.textToAccountDesc.text = destinationAccount.accountDesc
        dataBinding.textToAccountNo.text = FormatHelper.formatAccountNo(
            destinationAccount.coopAccountNo ?: destinationAccount.bayAccountNo ?: "",
            AppConstant.IS_MARK_ACCOUNT
        )

        inquiryTrans.fromCoopAccountNo?.run {
            dataBinding.imgFromIcon.loadCircle(R.drawable.logo_coop_cycle_min)
        } ?: inquiryTrans.fromBayAccountNo?.run {
            dataBinding.imgFromIcon.loadCircle(R.drawable.logo_bay_cycle_min)
        }

        inquiryTrans.toCoopAccountNo?.run {
            dataBinding.imgToIcon.loadCircle(R.drawable.logo_coop_cycle_min)
        } ?: inquiryTrans.toBayAccountNo?.run {
            dataBinding.imgToIcon.loadCircle(R.drawable.logo_bay_cycle_min)
        }

        var message = ""
        dataBinding.textToFeeValue.setAmount(inquiryTrans.transactionFee)
        if (inquiryTrans.transactionFee?.toDouble() ?: 0.toDouble() > 0.toDouble()) {
            message = getString(
                R.string.alert_has_fee_s, when (args.confirmContext.serviceCode) {
                    ServiceCode.DEPOSIT -> getString(R.string.text_deposit)
                    ServiceCode.TRANSFERRING -> getString(R.string.text_transfer)
                    ServiceCode.WITHDRAW -> getString(R.string.text_withdraw)
                    ServiceCode.TRANSACTION_HISTORY -> getString(R.string.text_view_history)
                    ServiceCode.LOAN_WITHDRAW -> getString(R.string.text_loan_withdraw)
                    ServiceCode.LOAN_PAYMENT -> getString(R.string.text_loan_payment)
                    else -> getString(R.string.text_trans)
                }
            , dataBinding.textToFeeValue.text.toString())

            dataBinding.textToFeeValue.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    android.R.color.holo_red_light
                )
            )
        }

        dataBinding.textNoteTitle.clearFocus()
        viewModel.invalidPin.observe(viewLifecycleOwner, Observer {
            dataBinding.buttonCancel.isEnabled = true
            dataBinding.buttonConfirm.isEnabled = true
        })

        var messageGravity = Gravity.CENTER
        if (args.confirmContext.coopMessage?.isNotEmpty() == true) {
            if (message.isNotEmpty()) {
                message = "• $message\n• "
                messageGravity = Gravity.START
            }
            message += args.confirmContext.coopMessage
        }

        if (message.isNotEmpty() && arguments?.getBoolean("showMessage", false) == false) {
            arguments?.putBoolean("showMessage", true)
            EventBus.publish(
                Alert.OkDialog(
                    message = message,
                    gravity = messageGravity,
                    buttonBg = if(isLoan) R.drawable.button_main_loan else R.drawable.button_main_saving
                )
            )
        }
    }

    override fun onDestroyView() {
        activity?.window?.statusBarColor = ContextCompat.getColor(
            requireContext(),
            R.color.colorStatusBar
        )
        super.onDestroyView()
    }

    fun onCancelClick() {
        popToRoot()
    }

    fun onConfirmClick() {

        gotoPage(
            TrxConfirmFragmentDirections.gotoTransactionPinPage(
                TrxPinContext(
                    serviceCode = args.confirmContext.serviceCode,
                    inquiryResponse = args.confirmContext.inquiryResponse,
                    sourceAccount = args.confirmContext.sourceAccount,
                    destinationAccount = args.confirmContext.destinationAccount,
                    note = args.confirmContext.note,
                )
            )
        )
    }

}

