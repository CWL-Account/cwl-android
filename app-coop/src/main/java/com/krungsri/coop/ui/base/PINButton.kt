package com.krungsri.coop.ui.base

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.FrameLayout
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import com.krungsri.coop.R

class PINButton @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    FrameLayout(context, attrs, defStyleAttr) {

    private val size = resources.getDimension(R.dimen.button_pin_size).toInt()
    private val fingerprintSize = resources.getDimension(R.dimen.fingerprint_icon_size).toInt()
    private val margin12 = resources.getDimension(R.dimen.margin12).toInt()
    private val margin16 = resources.getDimension(R.dimen.margin16).toInt()
    private val margin24 = resources.getDimension(R.dimen.margin24).toInt()

    init {
        setPadding(margin16, margin12, margin16, margin12)
        if (isInEditMode) {
            setPinText("0")
        } else {
            val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.PINButton, 0, 0
            )

            val pinText = typedArray.getString(R.styleable.PINButton_pinText)
            val pinIc = typedArray.getResourceId(R.styleable.PINButton_pinIc, 0)
            if (pinText?.isEmpty() == false) {
                setPinText(pinText)
            } else if (pinIc != 0) {
                setPinIc(pinIc)
            }
            typedArray.recycle()
        }
    }

    private fun setPinText(pin: String) {
        val textPin = AppCompatTextView(
            ContextThemeWrapper(
                context,
                R.style.AppTheme
            )
        )
        textPin.text = pin
        textPin.gravity = Gravity.CENTER
        TextViewCompat.setTextAppearance(textPin, R.style.Text20_Bold)
        textPin.setBackgroundResource(R.drawable.button_circle)
        addView(textPin, LayoutParams(size, size, Gravity.CENTER))
    }

    private fun setPinIc(ic: Int) {
        val imgIc = AppCompatImageView(context)
        imgIc.setImageResource(ic)
        if (ic == R.drawable.ic_fingerprint) {
            addView(imgIc, LayoutParams(fingerprintSize, fingerprintSize, Gravity.CENTER))
        } else {
            addView(imgIc, LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER))
        }

    }
}