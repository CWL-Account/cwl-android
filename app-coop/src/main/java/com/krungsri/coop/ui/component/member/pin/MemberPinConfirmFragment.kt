package com.krungsri.coop.ui.component.member.pin

import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentMemberPinConfirmBinding
import com.krungsri.coop.extension.visible
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.base.PinListener

class MemberPinConfirmFragment :
    BaseFragment<FragmentMemberPinConfirmBinding, MemberPinViewModel>(), PinListener {
    override val layoutId: Int = R.layout.fragment_member_pin_confirm
    override val viewModelClass: Class<MemberPinViewModel> =
        MemberPinViewModel::class.java

    override fun onFragmentStart() {
        dataBinding.fragment = this
        dataBinding.pinListener = this
        dataBinding.header.toolbar.setNavigationOnClickListener {
            onNavBackClick()
        }
        viewModel.initData(args.memberPinContext)
        dataBinding.pinDot.onKeyCompleted = { pinNo ->
            if (pinNo != args.memberPinContext.newPin) {
                showOkAlert(message = getString(R.string.err_confirm_pin_warning))
            } else {
                viewModel.setupNewPin()
            }
            dataBinding.pinDot.clearPin()
        }

        dataBinding.imgInfo.visible()
        dataBinding.imgInfo.setOnClickListener {
            PinInfoDialog().show(childFragmentManager)
        }
    }

    private val args by navArgs<MemberPinConfirmFragmentArgs>()

//    override fun onNavBackClick() {
//        findNavController().navigate(
//            MemberPinConfirmFragmentDirections.backToMemberPinNewPage(
//                args.memberPinContext
//            )
//        )
//    }

    override fun onPinClick(pinNo: String) {
        dataBinding.pinDot.addPIN(pinNo)
    }
}