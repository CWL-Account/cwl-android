package com.krungsri.coop.ui.component.member.data.slip

import android.content.Intent
import android.net.Uri
import androidx.recyclerview.widget.DividerItemDecoration
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentMemberDataSlipBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.setAmount
import com.krungsri.coop.extension.visible
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.ui.base.BaseFragment
import java.util.*


class MemberDataSlipFragment : BaseFragment<FragmentMemberDataSlipBinding, MemberDataSlipViewModel>() {
    override val layoutId: Int = R.layout.fragment_member_data_slip
    override val viewModelClass: Class<MemberDataSlipViewModel> = MemberDataSlipViewModel::class.java

    override fun onFragmentStart() {
        dataBinding.fragment = this
        dataBinding.vm = viewModel
        val monthArray = resources.getStringArray(R.array.months_fullname)
        val memberCode = AppPrefs.getStaticString(AppConstant.MEMBER_CODE)
        val firstName = AppPrefs.getString(AppConstant.FIRST_NAME, "")
        val lastName = AppPrefs.getString(AppConstant.LAST_NAME, "")
        dataBinding.textMemberCode.text = getString(R.string.label_member_info_no, memberCode)
        dataBinding.textMemberName.text = "$firstName $lastName"
        dataBinding.recyclerView.adapter = MemberDataSlipTransAdapter()
        dataBinding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )

        val calendar = Calendar.getInstance()
        val date = calendar.get(Calendar.DATE).let {
            (if (it < 10) "0" else "") + it
        }
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        dataBinding.textInfoDate.text =
            getString(R.string.label_member_data_slip_date, "$date ${monthArray[month]} ${year + 543}")

        viewModel.currentMonth.value = if (month + 1 < 10) "0${month + 1}" else "${month + 1}"
        viewModel.currentYear.value = "$year"
        viewModel.getMemberSlip()

        viewModel.notFoundMemberSlip.observe(this, {
            if(it){
                dataBinding.layoutSlipInfo.gone()
                dataBinding.buttonSlipView.gone()
                dataBinding.layoutNotFound.visible()
            }
        })
        viewModel.memberSlip.observe(this, {
            if (it == null) {
                dataBinding.layoutSlipInfo.gone()
                dataBinding.buttonSlipView.gone()
                dataBinding.layoutNotFound.visible()
            } else {
                dataBinding.layoutSlipInfo.visible()

                dataBinding.layoutNotFound.gone()
                calendar.time = it.slipDate
                val slipDate = calendar.get(Calendar.DAY_OF_MONTH)
                val slipMonth = calendar.get(Calendar.MONTH)
                val slipYear = calendar.get(Calendar.YEAR) + 543
                val slipDateInfo = "$slipDate ${monthArray[slipMonth]} $slipYear"
                dataBinding.textSlipNo.text = it.noOfSlip
                dataBinding.textSlipDate.text = slipDateInfo
                dataBinding.textCapitalStock.setAmount(it.memberCapitalStock, true)
                dataBinding.textLoanInterest.setAmount(it.totalLoanInterest, true)
                (dataBinding.recyclerView.adapter as MemberDataSlipTransAdapter).replace(it.transaction)
                it.printUrl?.run {
                    dataBinding.buttonSlipView.visible()
                    dataBinding.buttonSlipView.setOnClickListener {
                        val i = Intent(Intent.ACTION_VIEW)
                        i.data = Uri.parse(this@run)
                        startActivity(i)
                    }
                }

            }
        })
    }

    fun showMonthDialog() {
        MonthDialog(onSelectMonth = { month, display ->
            (dataBinding.recyclerView.adapter as MemberDataSlipTransAdapter).clear()
            dataBinding.textMonth.text = display
            viewModel.currentMonth.value = month
            viewModel.getMemberSlip()
        }, selectedMonth = viewModel.currentMonth.value).show(childFragmentManager)
    }

    fun showYearDialog() {
        YearDialog(onSelectYear = { year, display ->
            dataBinding.textYear.text = display
            (dataBinding.recyclerView.adapter as MemberDataSlipTransAdapter).clear()
            viewModel.currentYear.value = year
            viewModel.getMemberSlip()
        }, selectedYear = viewModel.currentYear.value).show(childFragmentManager)
    }
}