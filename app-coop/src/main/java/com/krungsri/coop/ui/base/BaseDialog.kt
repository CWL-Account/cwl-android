package com.krungsri.coop.ui.base

import android.app.Dialog
import android.os.Bundle
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.krungsri.coop.R
import com.krungsri.coop.lib.eventbut.AppEvent
import com.krungsri.coop.lib.eventbut.EventBus
import io.reactivex.disposables.CompositeDisposable

open class BaseDialog: DialogFragment() {
    private val disposeBag: CompositeDisposable = CompositeDisposable()
    init {
        EventBus.listen(AppEvent::class.java)
            .subscribe {
                if (it == AppEvent.AutoLogout) {
                    dismissAllowingStateLoss()
                }
            }.also {
                disposeBag.add(it)
            }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireContext(), R.style.BaseDialog)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        return dialog
    }

    override fun onDestroyView() {
        disposeBag.clear()
        super.onDestroyView()
    }

}