package com.krungsri.coop.ui.base

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import com.krungsri.coop.R
import com.krungsri.coop.extension.string

class ServiceButton : LinearLayoutCompat {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        loadAttribute(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        loadAttribute(attrs)
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.view_service_button, this, true)
        orientation = VERTICAL
    }

    private lateinit var iconMenu: AppCompatImageView
    private lateinit var textMenu: AppCompatTextView

    private fun loadAttribute(attrs: AttributeSet) {
        iconMenu = findViewById(R.id.imgIconMenu)
        textMenu = findViewById(R.id.textMenu)

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ServiceButton)
        val icon = typedArray.getResourceId(R.styleable.ServiceButton_src, 0)
        if (icon != 0) {
            iconMenu.setImageResource(icon)
        }
        val text = typedArray.getResourceId(R.styleable.ServiceButton_text, 0)
        if (text != 0) {
            textMenu.string(text)
        }
        typedArray.recycle()
    }

    fun setTitleName(string: String) {
        textMenu.text = string
    }

    fun setDisable(isDisable: Boolean) {
        isClickable = !isDisable
        isFocusable = !isDisable
        if (isDisable) {
            iconMenu.setColorFilter(
                ContextCompat.getColor(context, R.color.service_disable_icon)
            )
            textMenu.setTextColor(ContextCompat.getColor(context, R.color.service_disable_text))
        } else {
            iconMenu.colorFilter = null
            textMenu.setTextColor(ContextCompat.getColor(context, R.color.font_normal_color))
        }
    }

}