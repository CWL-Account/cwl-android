package com.krungsri.coop.ui.component.splash

import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentSplashScreenBinding
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.ui.base.BaseFragment

class SplashScreenFragment : BaseFragment<FragmentSplashScreenBinding, SplashScreenViewModel>() {

    override val layoutId: Int = R.layout.fragment_splash_screen

    override val viewModelClass: Class<SplashScreenViewModel>
        get() = SplashScreenViewModel::class.java

    override fun onFragmentStart() {
        viewModel.tokenVerification()
        AppConstant.IS_MARK_ACCOUNT = AppPrefs.getString(AppConstant.SET_MARK_ACCOUNT)?.toBoolean() ?: true

        viewModel.tokenVerification.observe(this, {
            viewModel.onTokenVerificationSuccess(it)
        })
    }


}