package com.krungsri.coop.ui.component.member.data

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.provider.MediaStore
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.FragmentMemberDataBinding
import com.krungsri.coop.extension.loadCircle
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.component.member.data.info.MemberDataInfoContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*


class MemberDataFragment : BaseFragment<FragmentMemberDataBinding, MemberDataViewModel>() {
    override val layoutId: Int = R.layout.fragment_member_data
    override val viewModelClass: Class<MemberDataViewModel> = MemberDataViewModel::class.java

    override fun onFragmentStart() {
        dataBinding.fragment = this
        val memberCode = AppPrefs.getStaticString(AppConstant.MEMBER_CODE)
        val firstName = AppPrefs.getString(AppConstant.FIRST_NAME, "")
        val lastName = AppPrefs.getString(AppConstant.LAST_NAME, "")

        dataBinding.textMemberCode.text = getString(R.string.label_member_info_no, memberCode)
        dataBinding.textMemberName.text = "$firstName $lastName"

        val cal = Calendar.getInstance()
        val date = cal.get(Calendar.DATE).let {
            (if (it < 10) "0" else "") + it
        }
        val month = cal.get(Calendar.MONTH).let {
            resources.getStringArray(R.array.months_fullname)[it]
        }
        val year = cal.get(Calendar.YEAR) + 543
        dataBinding.textDate.text = getString(R.string.label_member_info_date, "$date $month $year")



        GlobalScope.launch(Dispatchers.IO) {
            AppPrefs.getString("memberDisplay")?.run {
                val memberDisplay = File(requireContext().dataDir, this)
                if (memberDisplay.exists()) {
                    launch(Dispatchers.Main) {
                        dataBinding.imgPerson.loadCircle(memberDisplay)
                    }
                }
            }
        }

    }

    fun setupMemberDisplay() {
        SelectDisplayMenuDialog(
            onImageCaptureSelected = this::imageCapture,
            onImagePickupSelected = this::imagePickup
        ).show(childFragmentManager)
    }

    private fun imageCapture() {
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.CAMERA
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        openImageCapture()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()


    }

    private fun imagePickup() {
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        openImagePickup()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun openImagePickup() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, AppConstant.IMAGE_PICK_CODE)
    }

    private fun openImageCapture() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, AppConstant.IMAGE_CAPTURE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            val bitmap = when (requestCode) {
                AppConstant.IMAGE_CAPTURE_CODE -> data.extras?.get("data") as Bitmap
                AppConstant.IMAGE_PICK_CODE -> {
                    val returnUri = data.data
                    if (returnUri != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            ImageDecoder.decodeBitmap(
                                ImageDecoder.createSource(
                                    requireContext().contentResolver,
                                    returnUri
                                )
                            )
                        } else {
                            MediaStore.Images.Media.getBitmap(requireContext().contentResolver, returnUri)
                        }
                    } else {
                        null
                    }
                }
                else -> null
            }

            Timber.d("requestCode = %s", requestCode)
            Timber.d("bitmap = %s", bitmap)
            bitmap?.run {
                GlobalScope.launch(Dispatchers.IO) {
                    val originalBitmap = this@run
                    val newBitmap =
                        try {
                            val maxLength = 300;
                            if (height >= width) {
                                if (height <= maxLength) {
                                    originalBitmap
                                } else {
                                    val aspectRatio = width.toDouble() / height.toDouble()
                                    val targetWidth = (maxLength * aspectRatio).toInt()
                                    Bitmap.createScaledBitmap(originalBitmap, targetWidth, maxLength, false)
                                }
                            } else {
                                if (width <= maxLength) {
                                    originalBitmap
                                } else {
                                    val aspectRatio = height.toDouble() / width.toDouble()
                                    val targetHeight = (maxLength * aspectRatio).toInt()
                                    Bitmap.createScaledBitmap(originalBitmap, maxLength, targetHeight, false)
                                }
                            }
                        } catch (e: Exception) {
                            originalBitmap
                        }

                    val fileName = "memberDisplay${System.currentTimeMillis()}.png"
                    AppPrefs.setString("memberDisplay", fileName)
                    val memberDisplay = File(requireContext().dataDir, fileName)
                    val out: OutputStream = FileOutputStream(memberDisplay)
                    newBitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                    newBitmap.recycle()
                    if(!originalBitmap.isRecycled){
                        originalBitmap.recycle()
                    }
                    delay(500)
                    launch(Dispatchers.Main) {
                        dataBinding.imgPerson.loadCircle(memberDisplay)
                    }

                }
            }
        }
    }

    fun onMemberDataInfoClick() {
        viewModel.getMemberInfo {
            gotoPage(
                MemberDataFragmentDirections.gotoMemberDataInfoPage(
                    MemberDataInfoContext(it)
                )
            )
        }
    }

    fun onMemberDataSlipClick() {
        gotoPage(
            MemberDataFragmentDirections.gotoMemberDataSlipPage()
        )
    }
}