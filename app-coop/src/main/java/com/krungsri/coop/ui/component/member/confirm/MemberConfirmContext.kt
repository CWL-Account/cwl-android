package com.krungsri.coop.ui.component.member.confirm

import android.os.Parcelable
import com.krungsri.coop.model.member.MemberConfirmationRequest
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberConfirmContext(
    var verificationData: MemberConfirmationRequest,
    val firstName: String? = null,
    val lastName: String? = null
) : Parcelable