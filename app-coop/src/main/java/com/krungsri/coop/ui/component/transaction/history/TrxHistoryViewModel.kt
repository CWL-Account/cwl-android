package com.krungsri.coop.ui.component.transaction.history

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.lib.lifecycle.MutableLiveEvent
import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.transaction.TransactionHistoryRequest
import com.krungsri.coop.model.transaction.TransactionResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.usecase.transaction.GetTransactionHistoryUseCase
import javax.inject.Inject

class TrxHistoryViewModel @Inject
constructor(
    private val getTransactionHistoryUseCase: GetTransactionHistoryUseCase
) : BaseViewModel() {

    private var pageNo = 0
    private var accountNo: String = ""
    private var isLast = false
    val transactionHis = MutableLiveEvent<MutableList<TransactionResponse>>()
    val currentMonth = MutableLiveData<String>()

    fun loadHistory(accountDesc: AccountDesc, month: String) {
        if (accountNo != accountDesc.accountNo) {
            pageNo = 0
            isLast = false
            accountNo = accountDesc.accountNo
        }
        loadHistory(month, pageNo)
    }

    fun loadNextPage() {
        if (isLast) return
        currentMonth.value?.run {
            loadHistory(this, ++pageNo)
        }
    }

    fun loadHistory(month: String, pageNo: Int) {
        currentMonth.value = month
        getTransactionHistoryUseCase.build(
            TransactionHistoryRequest(
                coopAccountNo = accountNo,
                month = month,
                pageNo = pageNo
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::getTransactionHistorySuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    private fun getTransactionHistorySuccess(accountPage: ApiPageResponse<MutableList<TransactionResponse>>) {
        isLast = accountPage.result?.size == 0
        accountPage.result?.run {
            transactionHis.setEventValue(this)
        }
    }
}