package com.krungsri.coop.ui.component.web

import android.graphics.Bitmap
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.appbar.MaterialToolbar
import com.krungsri.coop.R
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.visible
import timber.log.Timber

class PublicWebActivity : AppCompatActivity() {

    private lateinit var toolbar: MaterialToolbar
    private lateinit var webView: WebView
    private lateinit var loadingProgress: LottieAnimationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_public_web)

        toolbar = findViewById(R.id.toolbar)
        webView = findViewById(R.id.webView)
        loadingProgress = findViewById(R.id.loadingProgress)

        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener {
            Timber.d("[EXIT]Exit Web view")
            finish()
        }
        Timber.d("openURL1 %s", intent.getStringExtra("url"))
        intent.getStringExtra("url")?.run {
            openURL(this)
        }
    }


    private fun openURL(url: String) {
        Timber.d("openURL2 %s", url)
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                loadingProgress.visible()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                loadingProgress.gone()
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                url?.run {
                    view?.loadUrl(url)
                }
                return true
            }
        }
        webView.settings.javaScriptEnabled = false
        webView.loadUrl(url)
    }
}