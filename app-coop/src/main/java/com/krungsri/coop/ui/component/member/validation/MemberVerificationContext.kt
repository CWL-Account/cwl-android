package com.krungsri.coop.ui.component.member.validation

import android.os.Parcelable
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.member.MemberVerificationRequest
import com.krungsri.coop.model.policy.PolicyFlag
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberVerificationContext(
    var verificationType: VerificationType,
    var policyFlag: MutableList<PolicyFlag>? = null,
    var verificationData: MemberVerificationRequest? = null
) : Parcelable