package com.krungsri.coop.ui.component.news

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleViewLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.news.NewsRequest
import com.krungsri.coop.model.news.NewsResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.usecase.home.NewsUseCase
import javax.inject.Inject

class NewsViewModel @Inject
constructor(
    private val newsUseCase: NewsUseCase
) : BaseViewModel() {

    val newsListData: MutableLiveData<ApiPageResponse<MutableList<NewsResponse>>> = MutableLiveData()
    fun listNews() {
        if (newsListData.value == null || newsListData.value?.last == false) {
            val pageNo = newsListData.value?.let {
                it.pageNo + 1
            } ?: 0
            newsUseCase.build(NewsRequest(pageNo = pageNo, size = 20))
                .doToggleViewLoading(this)
                .subscribeWithViewModel(
                    this,
                    this::listNewsSuccess,
                    this::onApiError
                ).disposedBy(this)
        }
    }

    private fun listNewsSuccess(response: ApiPageResponse<MutableList<NewsResponse>>) {
        newsListData.value = response
    }
}