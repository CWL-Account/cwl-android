package com.krungsri.coop.ui.component.member.change

import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.member.MemberChangePinVerifyRequest
import com.krungsri.coop.model.member.MemberChangePinVerifyResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.otp.OTPVerificationContext
import com.krungsri.coop.usecase.member.MemberChangePinVerificationUseCase
import javax.inject.Inject

class MemberChangePinViewModel @Inject
constructor(
    private val changePinVerificationUseCase: MemberChangePinVerificationUseCase
) : BaseViewModel() {


    fun onChangePin(currentPin: String, newPin: String) {
        changePinVerificationUseCase.build(
            MemberChangePinVerifyRequest(
                currentPinNo = SecurityHelper.encryptPinNo(currentPin),
                newPinNo = SecurityHelper.encryptPinNo(newPin),
                uuid = AppHelper.getDeviceId(appContext)
            )
        ).doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onChangePinVerificationSuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    private fun onChangePinVerificationSuccess(response: MemberChangePinVerifyResponse) {
        gotoPage(
            MemberChangePinFragmentDirections.gotoOTPVerifyPage(
                OTPVerificationContext(
                    verificationType = VerificationType.CHANGE_PIN,
                    verificationToken = response.verificationToken
                )
            )
        )
    }
}