package com.krungsri.coop.ui.component.setting.account

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.account.AccountAddRequest
import com.krungsri.coop.model.account.AccountInActiveRequest
import com.krungsri.coop.model.account.AccountInActiveResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.usecase.account.AddAccountListUseCase
import com.krungsri.coop.usecase.account.GetAccountInActiveListUseCase
import javax.inject.Inject

class AccountAddViewModel @Inject
constructor(
    private val getAccountInActiveListUseCase: GetAccountInActiveListUseCase,
    private val addAccountListUseCase: AddAccountListUseCase
) : BaseViewModel() {

    val accountInActive = MutableLiveData<AccountInActiveResponse>()
    fun accountList(verificationToken: String) {
        getAccountInActiveListUseCase.build(AccountInActiveRequest(verificationToken = verificationToken))
            .doToggleLoading()
            .subscribeWithViewModel(
                this,
                this::onGetAccountListSuccess,
                this::onApiError
            )
            .disposedBy(this)
    }

    private fun onGetAccountListSuccess(response: AccountInActiveResponse) {
        accountInActive.value = response
    }

    fun updateAccount(accountUpdate: MutableList<String>) {
        if (accountUpdate.isNullOrEmpty()) {
            showOkAlert(message = getString(R.string.label_setting_account_manage_add_desc))
        } else {
            addAccountListUseCase.build(
                AccountAddRequest(
                    verificationToken = accountInActive.value?.verificationToken ?: "",
                    coopAccountList = accountUpdate
                )
            )
                .doToggleLoading()
                .subscribeWithViewModel(
                    this,
                    this::onUpdateAccountListSuccess,
                    this::onApiError
                )
                .disposedBy(this)
        }
    }

    private fun onUpdateAccountListSuccess(accountList: Boolean) {
//        gotoPage(
//            AccountAddFragmentDirections.gotoMemberLandingPage(
//                MemberLandingContext(destination = LandingDestination.ACCOUNT_MANAGE)
//            )
//        )
        popBackStack()
    }
}