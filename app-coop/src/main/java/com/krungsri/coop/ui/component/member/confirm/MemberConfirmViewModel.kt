package com.krungsri.coop.ui.component.member.confirm

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.member.MemberConfirmationResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.otp.OTPVerificationContext
import com.krungsri.coop.usecase.member.MemberConfirmationUseCase
import javax.inject.Inject

class MemberConfirmViewModel @Inject
constructor(
    private val memberConfirmationUseCase: MemberConfirmationUseCase
) : BaseViewModel() {

    val inputCitizenNo = MutableLiveData("")
    val inputMobileNo = MutableLiveData("")
    val inputAccountNo = MutableLiveData("")
    val inputMemberCode = MutableLiveData("")
    val inputCustomerName = MutableLiveData("")
    private val memberContext = MutableLiveData<MemberConfirmContext>()

    val displayRegisterFlow = MutableLiveData(View.GONE)
    val displayForgotPinFlow = MutableLiveData(View.GONE)

    val displayTitlePage = MutableLiveData("")
    val displayDescPage = MutableLiveData("")

    fun initData(_memberConfirmContext: MemberConfirmContext) {
        memberContext.value = _memberConfirmContext
        _memberConfirmContext.verificationData.run {
            inputCitizenNo.value = FormatHelper.formatCitizenNo(citizenNo)
            inputAccountNo.value = FormatHelper.formatAccountNo(bayAccountNo)
            inputMobileNo.value = FormatHelper.formatMobileNNo(mobileNo)
            inputMemberCode.value = memberCode
            when (verificationType) {
                VerificationType.REGISTER -> {
                    displayRegisterFlow.value = View.VISIBLE
                    displayTitlePage.value = appContext.getString(R.string.label_member_confirmation_register_title)
                    displayDescPage.value = appContext.getString(R.string.label_member_confirmation_register_desc)
                }
                VerificationType.UNLOCK -> {
                    displayRegisterFlow.value = View.VISIBLE
                    displayTitlePage.value = appContext.getString(R.string.label_member_confirmation_unlock_title)
                    displayDescPage.value = appContext.getString(R.string.label_member_confirmation_unlock_desc)
                }
                VerificationType.FORGOT_PIN -> {
                    displayTitlePage.value = appContext.getString(R.string.label_member_confirmation_forgot_title)
                    displayDescPage.value = appContext.getString(R.string.label_member_confirmation_forgot_desc)
                    displayForgotPinFlow.value = View.VISIBLE
                    inputCustomerName.value = "${_memberConfirmContext.firstName} ${_memberConfirmContext.lastName}"
                }
            }
        }
    }

    fun memberConfirmation() {
        memberContext.value?.run {
            memberConfirmationUseCase.build(verificationData)
                .doToggleLoading()
                .subscribeWithViewModel(
                    this@MemberConfirmViewModel,
                    this@MemberConfirmViewModel::onMemberConfirmationSuccess,
                    this@MemberConfirmViewModel::onApiError
                )
                .disposedBy(this@MemberConfirmViewModel)
        }

    }

    fun onCancelMemberConfirmClick() {
//        memberContext.value?.run {
//            gotoPage(
//                MemberConfirmFragmentDirections.gotoMemberValidationFragment(
//                    MemberVerificationContext(
//                        verificationType = verificationData.verificationType,
//                        policyFlag = verificationData.policyFlag,
//                        MemberVerificationRequest(
//                            bayAccountNo = inputAccountNo.value?.replace("-", "") ?: "",
//                            mobileNo = inputMobileNo.value?.replace("-", "") ?: "",
//                            citizenNo = inputCitizenNo.value?.replace("-", "") ?: "",
//                            memberCode = inputMemberCode.value?.replace("-", "") ?: "",
//                            uuid = verificationData.uuid ?: "",
//                            forceRegister = verificationData.forceRegister ?: Flag.N,
//                            verificationType = verificationData.verificationType,
//                            policyFlag = mutableListOf()
//                        )
//                    )
//                )
//            )
//        }
        popBackStack()

    }


    private fun onMemberConfirmationSuccess(response: MemberConfirmationResponse) {
        memberContext.value?.run {
            gotoPage(
                MemberConfirmFragmentDirections.gotoOTPVerifyPage(
                    OTPVerificationContext(
                        verificationType = verificationData.verificationType,
                        verificationToken = response.verificationToken,
                        mobileNo = response.mobileNo,
                        bayAccountNo = response.bayAccountNo,
                        citizenNo = response.citizenNo,
                        memberCode = response.memberCode,
                        forceRegister = verificationData.forceRegister ?: Flag.N,
                        policyFlag = verificationData.policyFlag
                    )
                )
            )
        }
    }

    override fun onApiError(apiError: ApiError) {
        when (apiError.code) {
            AppConstant.CODE_TOKEN_VERIFY_EXP, AppConstant.CODE_DUPLICATE_DEVICE, AppConstant.CODE_DUPLICATE_MEMBER -> {
                showOkAlert(
                    apiError,
                    onDismiss = {
//                        gotoPage(
//                            MemberConfirmFragmentDirections.gotoMemberLandingPage(
//                                MemberLandingContext(
//                                    destination = LandingDestination.GOTO_REGISTER,
//                                    policyFlag = memberContext.value?.verificationData?.policyFlag
//                                )
//                            )
//                        )
                        popToRoot()
                    })
            }
            else -> {
                super.onApiError(apiError)
            }
        }
    }

}