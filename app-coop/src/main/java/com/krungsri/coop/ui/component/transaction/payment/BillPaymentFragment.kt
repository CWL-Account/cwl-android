package com.krungsri.coop.ui.component.transaction.payment

import android.content.Intent
import android.view.View
import androidx.navigation.fragment.navArgs
import com.google.android.material.tabs.TabLayoutMediator
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentBillPaymentBinding
import com.krungsri.coop.extension.invisible
import com.krungsri.coop.extension.visible
import com.krungsri.coop.helper.BarcodeHelper
import com.krungsri.coop.lib.lifecycle.observeEvent
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.transaction.BillPaymentData
import com.krungsri.coop.model.transaction.BillPaymentResponse
import com.krungsri.coop.model.transaction.BillerDataType
import com.krungsri.coop.ui.base.BaseFragment

class BillPaymentFragment :
    BaseFragment<FragmentBillPaymentBinding, BillPaymentViewModel>() {

    override val layoutId: Int = R.layout.fragment_bill_payment

    override val viewModelClass: Class<BillPaymentViewModel> = BillPaymentViewModel::class.java

    private val args by navArgs<BillPaymentFragmentArgs>()
    override fun onFragmentStart() {
        viewModel.loadBillPayment(args.billPaymentContext.accountDesc)
        viewModel.showBillPaymentDialog.observeEvent(viewLifecycleOwner, this::createBillPayment)

        dataBinding.navTitle =
            if (args.billPaymentContext.accountDesc.accountType == AccountType.LOAN) {
                dataBinding.tabLayout.invisible()
                dataBinding.tabLayout2.visible()
                getString(R.string.label_bill_loan_title)
            } else {
                dataBinding.tabLayout.visible()
                dataBinding.tabLayout2.invisible()
                getString(R.string.label_bill_saving_title)
            }
    }

//    override fun onNavBackClick() {
//        popToRoot()
//    }

    private fun createBillPayment(billPayment: BillPaymentResponse) {
        val billPaymentList = mutableListOf<BillPaymentData>()
        billPayment.qrCodeData?.run {
            billPaymentList.add(
                BillPaymentData(
                    dataType = BillerDataType.QR,
                    response = billPayment,
                    data = this
                )
            )
        }

        billPayment.barCodeData?.run {
            billPaymentList.add(
                BillPaymentData(
                    dataType = BillerDataType.BARCODE,
                    response = billPayment,
                    data = this
                )
            )
        }

        val adapter = BillPaymentAdapter(
            onSaveSlip = { layoutBill, billPaymentData ->
                saveSlip(layoutBill, billPaymentData)
            },
            onShareSlip = { layoutBill, billPaymentData ->
                shareSlip(layoutBill, billPaymentData)
            },
            onBackToHomeClick = {
                onNavBackClick()
            },
            accountType = args.billPaymentContext.accountDesc.accountType
        )
        adapter.replace(billPaymentList)
        dataBinding.viewPager.adapter = adapter
        TabLayoutMediator(
            dataBinding.tabLayout,
            dataBinding.viewPager
        ) { tab, position ->
            val type = (dataBinding.viewPager.adapter as BillPaymentAdapter).getItem(position)?.dataType
            tab.text = if (type == BillerDataType.QR) {
                getString(R.string.label_bill_qr)
            } else {
                getString(R.string.label_bill_barcode)
            }
            dataBinding.viewPager.setCurrentItem(tab.position, true)
        }.attach()
        TabLayoutMediator(
            dataBinding.tabLayout2,
            dataBinding.viewPager
        ) { tab, position ->
            val type = (dataBinding.viewPager.adapter as BillPaymentAdapter).getItem(position)?.dataType
            tab.text = if (type == BillerDataType.QR) {
                getString(R.string.label_bill_qr)
            } else {
                getString(R.string.label_bill_barcode)
            }
            dataBinding.viewPager.setCurrentItem(tab.position, true)
        }.attach()
    }

    private fun saveSlip(layoutBill: View, billPaymentData: BillPaymentResponse) {
//        layoutBill.setBackgroundResource(R.drawable.bg_slip)
        BarcodeHelper.saveBillPayment(
            context = requireContext(), billPayment = billPaymentData, view = layoutBill,
            onSaveCompleted = {
                layoutBill.setBackgroundResource(0)
            }
        )
    }

    private fun shareSlip(layoutBill: View, billPaymentData: BillPaymentResponse) {
        BarcodeHelper.saveBillPayment(
            context = requireContext(), billPayment = billPaymentData,
            view = layoutBill,
            onSaveCompleted = { path ->
                layoutBill.setBackgroundResource(0)
                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_STREAM, path)
                    type = "image/png"
                }
                startActivity(
                    Intent.createChooser(
                        shareIntent,
                        getString(R.string.dialog_create_bill_payment_share)
                    )
                )
            })
    }


}