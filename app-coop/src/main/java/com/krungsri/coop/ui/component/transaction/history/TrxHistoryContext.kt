package com.krungsri.coop.ui.component.transaction.history

import android.os.Parcelable
import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.enumeration.ProductCode
import kotlinx.parcelize.Parcelize

@Parcelize
data class TrxHistoryContext(
    val productCode: ProductCode,
    val accountDesc: AccountDesc,
    val accountList: MutableList<AccountDesc>
) : Parcelable