package com.krungsri.coop.ui.component.policy

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.policy.*
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.member.landing.LandingDestination
import com.krungsri.coop.ui.component.member.landing.MemberLandingContext
import com.krungsri.coop.ui.component.otp.OTPVerificationContext
import com.krungsri.coop.usecase.policy.GetLatestPolicyActiveUseCase
import com.krungsri.coop.usecase.policy.GetLatestPolicyUseCase
import com.krungsri.coop.usecase.policy.UpdatePolicyUseCase
import timber.log.Timber
import javax.inject.Inject

class PolicyViewModel @Inject
constructor(
    private val getLatestPolicyUseCase: GetLatestPolicyUseCase,
    private val getLatestPolicyActiveUseCase: GetLatestPolicyActiveUseCase,
    private val updatePolicyUseCase: UpdatePolicyUseCase,
) : BaseViewModel() {

    val checkboxAccept = MutableLiveData(false)
    val policyResponseData = MutableLiveData<PolicyResponse>()

    private val policyContextData = MutableLiveData<PolicyContext>()

    fun initData(policyContext: PolicyContext) {
        policyContextData.value = policyContext
    }

    private fun isRegisterFlow(): Boolean {
        return policyContextData.value?.isRequestToRegister == true
    }

    private fun isAddAccountFlow(): Boolean {
        return policyContextData.value?.isRequestToAddAccount == true
    }

    private fun getLatestPolicy() {
        Timber.d("getLatestPolicy")
        getLatestPolicyUseCase.build()
            .doToggleLoading()
            .subscribeWithViewModel(this, this::onGetLatestPolicySuccess, this::onGetLatestPolicyError)
            .disposedBy(this)

    }

    private fun onGetLatestPolicySuccess(response: PolicyResponse) {
        Timber.d("onGetLatestPolicySuccess %s", response.policyType)
        if (response.isNew == Flag.Y) {
            policyResponseData.value = response
        } else {
            gotoNextPage()
        }
    }

    private fun onGetLatestPolicyError(apiError: ApiError) {
        Timber.d("onGetLatestPolicyError %s", apiError.message)
        gotoNextPage()
    }

    private fun getLatestPolicyActive(policyType: PolicyType) {
        Timber.d("getLatestPolicyActive %s", policyType)
        getLatestPolicyActiveUseCase.build(PolicyRequest(policyType))
            .doToggleLoading()
            .subscribeWithViewModel(this, this::onGetLatestPolicyActiveSuccess, this::onGetLatestPolicyError)
            .disposedBy(this)

    }

    private fun onGetLatestPolicyActiveSuccess(response: PolicyResponse) {
        Timber.d("onGetLatestPolicyActiveSuccess %s, %s", response.policyType, response.isNew)
        if (isRegisterFlow() && response.isNew != Flag.Y) {
            when {
                PolicyType.COOP_TC == response.policyType -> {
                    getLatestPolicyActive(PolicyType.COOP_CONSENT)
                }
                PolicyType.COOP_CONSENT == response.policyType -> {
                    getLatestPolicyActive(PolicyType.COOP_OTHER)
                }
                else -> {
                    gotoRegister()
                }
            }
        } else if (response.isNew == Flag.Y) {
            checkboxAccept.value = false
            policyResponseData.value = response
        } else {
            gotoNextPage()
        }
    }

    fun updatePolicy(policyResponse: PolicyResponse, flag: Flag) {
        if (isRegisterFlow()) {
            when (policyResponse.policyType) {
                PolicyType.COOP_TC -> {
                    if (checkboxAccept.value != true) {
                        showOkAlert(message = appContext.getString(R.string.error_coop_tc_checkbox))
                    } else {
                        policyContextData.value?.run {
                            if (policyFlag == null) {
                                policyFlag = mutableListOf()
                            }
                            policyFlag?.add(
                                PolicyFlag(
                                    policyType = policyResponse.policyType,
                                    verificationToken = policyResponse.verificationToken,
                                    flag = flag,
                                )
                            )
                        }.also {
                            getLatestPolicyActive(PolicyType.COOP_CONSENT)
                        }
                    }
                }
                PolicyType.COOP_CONSENT -> {
                    policyContextData.value?.run {
                        if (policyFlag == null) {
                            policyFlag = mutableListOf()
                        }
                        policyFlag?.add(
                            PolicyFlag(
                                policyType = policyResponse.policyType,
                                verificationToken = policyResponse.verificationToken,
                                flag = flag,
                            )
                        )
                    }.also {
                        getLatestPolicyActive(PolicyType.COOP_OTHER)
                    }
                }
                PolicyType.COOP_OTHER -> {
                    policyContextData.value?.run {
                        if (policyFlag == null) {
                            policyFlag = mutableListOf()
                        }
                        policyFlag?.add(
                            PolicyFlag(
                                policyType = policyResponse.policyType,
                                verificationToken = policyResponse.verificationToken,
                                flag = flag,
                            )
                        )
                    }.also {
                        gotoRegister()
                    }
                }
                else -> {
                }
            }
        } else {
            if (policyResponse.policyType == PolicyType.COOP_TC || policyResponse.policyType == PolicyType.BAY_TC) {
                if (checkboxAccept.value != true) {
                    showOkAlert(message = appContext.getString(R.string.error_coop_tc_checkbox))
                    return
                }
            }
            updatePolicyUseCase.build(
                PolicyUpdateRequest(
                    policyType = policyResponse.policyType,
                    verificationToken = policyResponse.verificationToken,
                    acceptFlag = flag
                )
            ).doToggleLoading()
                .subscribeWithViewModel(this, this::onUpdatePolicySuccess, this::onApiError)
                .disposedBy(this)
        }

    }

    private fun onUpdatePolicySuccess(response: ApiResponse<Any>) {
        if (policyContextData.value?.isNewPolicy == Flag.Y) {
            getLatestPolicy()
        } else {
            gotoNextPage()
        }
    }

    private fun gotoNextPage() {
        Timber.d("gotoHome isAddAccountFlow = %s", isAddAccountFlow())
        if (isAddAccountFlow()) {
            requestNewOTP()
        } else if (!isRegisterFlow()) {
            gotoPage(PolicyFragmentDirections.gotoMainHomePage())
        } else {
            gotoPage(
                PolicyFragmentDirections.gotoWelcomePage(
                    MemberLandingContext(
                        destination = LandingDestination.HOME
                    )
                )
            )
        }
    }

    private fun gotoRegister() {
        if (isAddAccountFlow()) {
            requestNewOTP()
        } else {
            gotoPage(
                PolicyFragmentDirections.gotoRegisterPage(
                    MemberLandingContext(
                        destination = LandingDestination.GOTO_REGISTER,
                        verificationType = VerificationType.REGISTER,
                        policyFlag = policyContextData.value?.policyFlag
                    )
                )
            )
        }
    }

    private fun requestNewOTP() {
        gotoPage(
            PolicyFragmentDirections.gotoOTPVerifyPageForAddAccount(
                OTPVerificationContext(
                    verificationType = VerificationType.ADD_ACCOUNT
                )
            )
        )
    }

}