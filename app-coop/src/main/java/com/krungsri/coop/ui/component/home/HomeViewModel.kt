package com.krungsri.coop.ui.component.home

import androidx.lifecycle.MutableLiveData
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.data.session.LocalSession
import com.krungsri.coop.extension.disposedBy
import com.krungsri.coop.extension.doToggleLoading
import com.krungsri.coop.extension.doToggleViewLoading
import com.krungsri.coop.extension.subscribeWithViewModel
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.enumeration.ProductCode
import com.krungsri.coop.model.home.CoopService
import com.krungsri.coop.model.home.InquiryHomeResponse
import com.krungsri.coop.model.news.NewsRequest
import com.krungsri.coop.model.news.NewsResponse
import com.krungsri.coop.model.transaction.InitialFormRequest
import com.krungsri.coop.model.transaction.InitialFormResponse
import com.krungsri.coop.ui.base.BaseViewModel
import com.krungsri.coop.ui.component.transaction.history.TrxHistoryContext
import com.krungsri.coop.ui.component.transaction.inquiry.TrxInquiryContext
import com.krungsri.coop.ui.component.transaction.payment.BillPaymentContext
import com.krungsri.coop.usecase.home.InquiryHomeUseCase
import com.krungsri.coop.usecase.home.NewsUseCase
import com.krungsri.coop.usecase.member.MemberLogoutUseCase
import com.krungsri.coop.usecase.transaction.InitialFormUseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class HomeViewModel @Inject
constructor(
    private val localSession: LocalSession,
    private val inquiryHomeUseCase: InquiryHomeUseCase,
    private val initialFormUseCase: InitialFormUseCase,
    private val newsUseCase: NewsUseCase,
    private val memberLogoutUseCase: MemberLogoutUseCase
) : BaseViewModel(), AdapterListener {

    val inquiryHomeData: MutableLiveData<MutableList<InquiryHomeResponse>> = MutableLiveData()
    val newsListData: MutableLiveData<MutableList<NewsResponse>> = MutableLiveData()

    val showAddNewAccount = MutableLiveData(false)
    val showMoreNewsButton = MutableLiveData(false)
    val customerName = MutableLiveData<String>()

    val accountTab = MutableLiveData(0)

    fun init() {
        if (customerName.value == null) {
            GlobalScope.launch {
                val firstName = AppPrefs.getString(AppConstant.FIRST_NAME, "")
                val lastName = AppPrefs.getString(AppConstant.LAST_NAME, "")
                customerName.postValue("$firstName $lastName")
            }
        }
    }


    fun initHome() {
        val obs = inquiryHomeUseCase.build()
        if (!localSession.inquiryHome.exists()) {
            obs.doToggleLoading()
        }

        obs.doOnNext {
            listNews()
        }.subscribeWithViewModel(
            this,
            {
                if (it.isNullOrEmpty()) {
                    showAddNewAccount.value = true
                } else {
                    inquiryHomeData.value = it
                }
            },
            this::inquiryHomeError
        ).disposedBy(this)
    }


    private fun inquiryHomeError(apiError: ApiError) {
        Timber.d("inquiryHomeError %s %s", apiError.code, apiError.message)
        when (apiError.code) {
            AppConstant.CODE_TOKEN_ACCESS_EXP -> {
                showOkAlert(
                    code = apiError.code,
                    message = apiError.message, onDismiss = {
                        popToRoot()
                    })
            }
            else -> {
                showOkAlert(
                    code = apiError.code,
                    message = apiError.message, onDismiss = {
                        onExitAppEvent.setEventValue("HOME_ERROR_1")
                    })
            }
        }
        if (apiError.code != AppConstant.CODE_SUCCESS200) {
            showOkAlert(
                code = apiError.code,
                message = apiError.message, onDismiss = {
                    onExitAppEvent.setEventValue("HOME_ERROR_2")
                })
        } else {
            super.onApiError(apiError)
        }
    }

    private fun listNews() {
        newsUseCase.build(NewsRequest(pageNo = 0, size = 1))
            .doToggleViewLoading(this)
            .subscribeWithViewModel(
                this,
                this::listNewsSuccess,
                {}
            ).disposedBy(this)
    }

    private fun listNewsSuccess(response: ApiPageResponse<MutableList<NewsResponse>>) {
        showMoreNewsButton.value = !response.last
        newsListData.value = response.result
    }

    override fun onCoopServiceClick(coopService: CoopService, productCode: ProductCode, accountDesc: AccountDesc) {
        if (coopService.serviceCode != null) {
            initialFormUseCase.build(
                InitialFormRequest(
                    productCode = productCode,
                    serviceCode = coopService.serviceCode
                )
            ).doToggleLoading()
                .subscribeWithViewModel(this, {
                    onInitialFormSuccess(coopService, productCode, it, accountDesc)
                }, this::onApiError)
                .disposedBy(this)
        }
    }

    override fun onCreateBarcode(coopService: CoopService, accountDesc: AccountDesc) {
        gotoPage(
            HomeFragmentDirections.gotoBillPaymentPage(
                billPaymentContext = BillPaymentContext(
                    coopService = coopService,
                    accountDesc = accountDesc
                )
            )
        )
    }

    override fun onMemberDataClick() {
        gotoPage(
            HomeFragmentDirections.gotoMemberDatePage()
        )
    }

    private fun onInitialFormSuccess(
        coopService: CoopService,
        productCode: ProductCode,
        response: InitialFormResponse,
        accountDesc: AccountDesc
    ) {
        gotoPage(
            HomeFragmentDirections.gotoTransactionInquiryPage(
                TrxInquiryContext(
                    coopService = coopService,
                    productCode = productCode,
                    initialForm = response,
                    selectedAccountDesc = accountDesc
                )
            )
        )
    }

    override fun onHistoryClick(
        productCode: ProductCode, accountDesc: AccountDesc,
        accountList: MutableList<AccountDesc>
    ) {
        gotoPage(
            HomeFragmentDirections.gotoTransactionHistoryPage(
                historyContext = TrxHistoryContext(
                    productCode = productCode,
                    accountDesc = accountDesc,
                    accountList = accountList
                )
            )
        )
    }

    fun logout() {
        showSelectAlertMessage(
            message = appContext.getString(R.string.label_logout_message),
            onOkDismiss = {
                memberLogoutUseCase.build()
                    .doToggleLoading()
                    .subscribeWithViewModel(
                        this@HomeViewModel,
                        {
                            popToRoot()
                        },
                        this::onApiError
                    )
                    .disposedBy(this)
            })
    }


}