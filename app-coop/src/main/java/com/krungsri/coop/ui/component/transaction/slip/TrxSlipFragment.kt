package com.krungsri.coop.ui.component.transaction.slip

import android.Manifest
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.media.MediaScannerConnection
import androidx.navigation.fragment.navArgs
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentTrxSlipViewBinding
import com.krungsri.coop.extension.*
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.lib.eventbut.EventBus
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.ui.base.Alert
import com.krungsri.coop.ui.base.BaseFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

class TrxSlipFragment :
    BaseFragment<FragmentTrxSlipViewBinding, TrxSlipViewModel>() {

    override val layoutId: Int = R.layout.fragment_trx_slip_view

    override val viewModelClass: Class<TrxSlipViewModel>
        get() = TrxSlipViewModel::class.java

    private val args by navArgs<TrxSlipFragmentArgs>()

    override fun onFragmentStart() {
        val tran = args.slipContext.transaction
        val fromAccount = args.slipContext.sourceAccount
        val toAccount = args.slipContext.destinationAccount

        var isLoan = false
        dataBinding.fragment = this
        dataBinding.textTitle.text = when (tran.transactionType) {
            ServiceCode.TRANSFERRING -> getString(R.string.label_transaction_transfer_success)
            ServiceCode.WITHDRAW -> getString(R.string.label_transaction_withdraw_success)
            ServiceCode.DEPOSIT -> getString(R.string.label_transaction_deposit_success)
            ServiceCode.LOAN_WITHDRAW -> {
                isLoan = true
                getString(R.string.label_transaction_withdraw_loan_success)
            }
            ServiceCode.LOAN_PAYMENT -> {
                isLoan = true
                getString(R.string.label_transaction_payment_loan_success)
            }
            ServiceCode.BILL_PAYMENT -> {
                isLoan = true
                getString(R.string.label_transaction_payment_loan_success)
            }
            else -> getString(R.string.label_transaction_success)
        }

        if (isLoan) {
            dataBinding.imgL1.setBackgroundResource(R.color.colorSecondary)
            dataBinding.imgL2.setBackgroundResource(R.drawable.circle_normal_loan2)
            dataBinding.imgL3.setBackgroundResource(R.color.colorSecondary)
            dataBinding.buttonBackToHome.setBackgroundResource(R.drawable.button_main_loan)
        }



        dataBinding.textRefNo.text = tran.referenceNo
        dataBinding.textAmount.setAmount(tran.transactionAmount)
        dataBinding.textFee.setAmount(tran.transactionFee)

        dataBinding.textToAccountName.text = toAccount?.accountName
        dataBinding.textToAccountDesc.text = toAccount?.accountDesc
        dataBinding.textToAccountNo.setAccountNo(tran.toCoopAccountNo, tran.toBayAccountNo)

        dataBinding.textFromAccountName.text = fromAccount?.accountName
        dataBinding.textFromAccountDesc.text = fromAccount?.accountDesc
        dataBinding.textFromAccountNo.setAccountNo(tran.fromCoopAccountNo, tran.fromBayAccountNo)

        dataBinding.imgFromIcon.setAccountIcon(tran.fromCoopAccountNo, tran.fromBayAccountNo)
        dataBinding.imgToIcon.setAccountIcon(tran.toCoopAccountNo, tran.toBayAccountNo)

//        10 มิ.ย. 62 15:00 น.
        val calendar = Calendar.getInstance()
        calendar.time = tran.transactionDate

        val date = calendar.get(Calendar.DATE)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR) + 543
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        val dateString = if (date < 10) "0$date" else "$date"
        val hourString = if (hour < 10) "0$hour" else "$hour"
        val minuteString = if (minute < 10) "0$minute" else "$minute"

        val monthString = resources.getStringArray(R.array.months)[month]

        dataBinding.textTransDate.text = "$dateString $monthString $year  $hourString:$minuteString น."

        dataBinding.displayNote = tran.note?.let {
            if (it.isNotEmpty()) {
                dataBinding.textNote.text = it
                dataBinding.layoutSave.textSaveNote.text = it
                true
            } else {
                false
            }
        } ?: false


        dataBinding.layoutSave.textSaveTitle.text = dataBinding.textTitle.text
        dataBinding.layoutSave.textSaveTransDate.text = dataBinding.textTransDate.text
        dataBinding.layoutSave.textSaveRefNo.text = tran.referenceNo
        dataBinding.layoutSave.textSaveAmount.setAmount(tran.transactionAmount)
        dataBinding.layoutSave.textSaveFee.setAmount(tran.transactionFee)

        dataBinding.layoutSave.textSaveToAccountTitle.text = toAccount?.accountName
        dataBinding.layoutSave.textSaveToAccountDesc.text = toAccount?.accountDesc
        dataBinding.layoutSave.textSaveToAccountNo.setAccountNo(tran.toCoopAccountNo, tran.toBayAccountNo, true)

        dataBinding.layoutSave.textSaveFromAccountTitle.text = fromAccount?.accountName
        dataBinding.layoutSave.textSaveFromAccountDesc.text = fromAccount?.accountDesc
        dataBinding.layoutSave.textSaveFromAccountNo.setAccountNo(tran.fromCoopAccountNo, tran.fromBayAccountNo, true)

        dataBinding.layoutSave.imgSaveFromIcon.setAccountIcon(tran.fromCoopAccountNo, tran.fromBayAccountNo)
        dataBinding.layoutSave.imgSaveToIcon.setAccountIcon(tran.toCoopAccountNo, tran.toBayAccountNo)

        GlobalScope.launch(Dispatchers.Main) {
            delay(1000)
            checkPermission()
        }
    }

    private fun checkPermission() {
        if (isDetached) {
            return
        }
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        saveSlip()
                    } else {
                        dataBinding.buttonBackToHome.visible()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun saveSlip() {
        try {
            val bitmap = Bitmap.createBitmap(
                dataBinding.layoutSave.layoutSaveSlip.width,
                dataBinding.layoutSave.layoutSaveSlip.height,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            canvas.drawColor(Color.WHITE)
            dataBinding.layoutSave.layoutSaveSlip.setBackgroundResource(R.drawable.bg_slip)
            dataBinding.layoutSave.layoutSaveSlip.draw(canvas)
            dataBinding.layoutSave.layoutSaveSlip.setBackgroundResource(0)

            val filename = "${args.slipContext.transaction.referenceNo}.png"

            AppHelper.getPicturesPath(requireContext(), filename)?.let { filePath ->
                context?.contentResolver?.openOutputStream(filePath).use { output ->

                    bitmap.compress(
                        Bitmap.CompressFormat.PNG,
                        100,
                        output
                    )

                    MediaScannerConnection.scanFile(
                        context, arrayOf(filePath.toString()), null
                    ) { path, uri ->
                        Timber.i("MediaScannerConnection1.Scanned $path:")
                        Timber.i("MediaScannerConnection1.uri=$uri")
                    }

                    EventBus.publish(
                        Alert.Toast(
                            message = getString(R.string.alert_transaction_save_slip_success)
                        )
                    )
                }

            }
        } catch (e: Exception) {
            Timber.e(e)
        } finally {
            dataBinding.layoutSave.layoutSaveSlip.gone()
            dataBinding.buttonBackToHome.visible()
        }
    }


    fun backToHome() {
        gotoPage(TrxSlipFragmentDirections.gotoMainHomePage())
    }

    override fun onNavBackClick() {
        backToHome()
    }

}