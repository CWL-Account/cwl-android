package com.krungsri.coop.ui.component.member.data.info

import android.os.Parcelable
import com.krungsri.coop.model.member.MemberDataInfoResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberDataInfoContext(
 val dataInfo: MemberDataInfoResponse
) : Parcelable