package com.krungsri.coop.ui.component.news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.krungsri.coop.R
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.databinding.ItemNewsBinding
import com.krungsri.coop.extension.loadUrl
import com.krungsri.coop.model.news.NewsResponse
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter

class NewsAdapter(
    val tokenService: TokenService,
    private val onOpenNewsUrl: ((url: String) -> Unit)
) :
    BaseRecyclerViewAdapter<NewsResponse, ItemNewsBinding>() {
    override fun createBinding(parent: ViewGroup): ItemNewsBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_news,
            parent,
            false
        )
    }

    override fun bind(binding: ItemNewsBinding, position: Int, item: NewsResponse) {
        binding.adapter = this
        binding.news = item
        val header = tokenService.getTokenMap()
        binding.imageView.loadUrl(
            GlideUrl(item.pictureUrl) { header },
            placeholderRes = R.drawable.bg_news,
            onErrorRes = R.drawable.bg_news,
            diskCacheStrategy = DiskCacheStrategy.AUTOMATIC
        )
    }

    fun onItemCLick(news: NewsResponse) {
        news.newsUrl?.run {
            onOpenNewsUrl.invoke(this)
        }
    }

}