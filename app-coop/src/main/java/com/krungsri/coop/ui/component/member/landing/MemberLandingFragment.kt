package com.krungsri.coop.ui.component.member.landing

import androidx.core.content.ContextCompat
import androidx.navigation.fragment.navArgs
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentMemberLandingBinding
import com.krungsri.coop.extension.visible
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.ui.base.BaseFragment
import com.krungsri.coop.ui.component.member.pin.MemberPinContext
import com.krungsri.coop.ui.component.member.validation.MemberVerificationContext
import timber.log.Timber

class MemberLandingFragment :
    BaseFragment<FragmentMemberLandingBinding, MemberLandingViewModel>() {

    override val layoutId: Int = R.layout.fragment_member_landing

    override val viewModelClass: Class<MemberLandingViewModel> = MemberLandingViewModel::class.java
    private val args by navArgs<MemberLandingFragmentArgs>()
    override fun onFragmentStart() {
        dataBinding.fragment = this
        Timber.d("destination = ${args.memberLandingContext.destination}")
        Timber.d("verificationToken = ${args.memberLandingContext.verificationToken}")
        when (args.memberLandingContext.destination) {
            LandingDestination.GOTO_REGISTER -> {
                dataBinding.imgCoopLogo.visible()
                dataBinding.textLandingTitle.text = getString(R.string.coop_name_2line)
                dataBinding.textLandingTitle.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.font_subtitle_color
                    )
                )
                dataBinding.textLandingDesc.text = null
                dataBinding.buttonAction.text = getString(R.string.button_register)
            }
            LandingDestination.NEW_PIN_UNLOCK,
            LandingDestination.NEW_PIN_FORGOT_PIN -> {
                dataBinding.imgByBayLogo.visible()
                dataBinding.imgCompletedLogo.visible()
                dataBinding.textLandingTitle.text = getString(R.string.label_member_landing_completed_title)
                dataBinding.textLandingDesc.text = getString(R.string.label_member_landing_completed_desc)
                dataBinding.buttonAction.text = getString(R.string.button_goto_next)
            }
            LandingDestination.HOME,
//            LandingDestination.NEW_PIN_REGISTER,
            LandingDestination.UNLOCK_PIN,
            LandingDestination.HOME_FROM_AFTER_NEW_PIN -> {
                dataBinding.imgByBayLogo.visible()
                dataBinding.imgCompletedLogo.visible()
                dataBinding.textLandingTitle.text = getString(R.string.label_member_landing_register_title)
                dataBinding.textLandingDesc.text = getString(R.string.label_member_landing_register_desc)
                dataBinding.buttonAction.text = getString(R.string.button_goto_start)
            }
            LandingDestination.FORGOT_PIN -> {
                dataBinding.imgByBayLogo.visible()
                dataBinding.imgCompletedLogo.visible()
                dataBinding.textLandingTitle.text = getString(R.string.label_member_landing_change_pin_title)
                dataBinding.textLandingDesc.text = getString(R.string.label_member_landing_register_desc)
                dataBinding.buttonAction.text = getString(R.string.button_goto_start)
            }
            LandingDestination.CHANGE_PIN -> {
                dataBinding.imgByBayLogo.visible()
                dataBinding.imgCompletedLogo.visible()
                dataBinding.textLandingTitle.text = getString(R.string.label_member_landing_change_pin_title)
//                dataBinding.textLandingDesc.text = getString(R.string.label_member_landing_change_pin_desc)
                dataBinding.textLandingDesc.text = null
                dataBinding.buttonAction.text = getString(R.string.button_goto_finish)
            }
            LandingDestination.ACCOUNT_MANAGE -> {
                dataBinding.imgByBayLogo.visible()
                dataBinding.imgCompletedLogo.visible()
                dataBinding.textLandingTitle.text = getString(R.string.label_member_landing_add_account_title)
                dataBinding.textLandingDesc.text = getString(R.string.label_member_landing_add_account_desc)
                dataBinding.buttonAction.text = getString(R.string.button_goto_finish)
            }
        }
    }

    fun gotoDestination() {
        val landingContext = args.memberLandingContext
        Timber.d("%s", landingContext.destination)
        if (landingContext.destination == LandingDestination.HOME_FROM_AFTER_NEW_PIN) {
            viewModel.getLatestPolicy()
        } else {
            val navDirections = when (landingContext.destination) {
                LandingDestination.GOTO_REGISTER -> {
                    MemberLandingFragmentDirections.gotoRegisterPage(
                        MemberVerificationContext(
                            verificationType = landingContext.verificationType
                                ?: VerificationType.REGISTER,
                            policyFlag = landingContext.policyFlag
                        )
                    )
                }
                LandingDestination.NEW_PIN_UNLOCK -> {
                    MemberLandingFragmentDirections.gotoSetupNewPinPage(
                        MemberPinContext(
                            destination = LandingDestination.UNLOCK_PIN
                        )
                    )
                }
                LandingDestination.NEW_PIN_FORGOT_PIN -> {
                    MemberLandingFragmentDirections.gotoSetupNewPinPage(
                        MemberPinContext(
                            destination = LandingDestination.FORGOT_PIN
                        )
                    )
                }
//                LandingDestination.NEW_PIN_REGISTER -> MemberLandingFragmentDirections.gotoMemberPinNewPage(
//                    MemberPinContext(
//                        destination = LandingDestination.HOME_FROM_AFTER_NEW_PIN
//                    )
//                )
                LandingDestination.FORGOT_PIN,
                LandingDestination.UNLOCK_PIN,
                LandingDestination.HOME -> MemberLandingFragmentDirections.gotoMainHomePage()
                LandingDestination.CHANGE_PIN -> MemberLandingFragmentDirections.gotoNewLoginPage()
                LandingDestination.ACCOUNT_MANAGE -> MemberLandingFragmentDirections.gotoAccountManagePage()
                else -> null
            }
            navDirections?.run {
                gotoPage(navDirections)
            } ?: popToRoot()
        }
    }

}