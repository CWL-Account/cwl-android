package com.krungsri.coop.ui.component.setting.policy

import androidx.navigation.fragment.navArgs
import com.google.android.material.tabs.TabLayoutMediator
import com.krungsri.coop.R
import com.krungsri.coop.databinding.FragmentSettingMemberPolicyBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.visible
import com.krungsri.coop.model.policy.PolicyType
import com.krungsri.coop.ui.base.BaseFragment

class MemberPolicyFragment : BaseFragment<FragmentSettingMemberPolicyBinding, MemberPolicyViewModel>() {

    override val layoutId: Int = R.layout.fragment_setting_member_policy

    override val viewModelClass: Class<MemberPolicyViewModel> = MemberPolicyViewModel::class.java

    private val args by navArgs<MemberPolicyFragmentArgs>()
    override fun onFragmentStart() {
        dataBinding.navTitle = when (args.memberPolicyContext.policyType) {
            PolicyType.BAY_TC, PolicyType.COOP_TC -> {
                getString(R.string.label_setting_tc)
            }
            PolicyType.BAY_CONSENT, PolicyType.COOP_CONSENT -> {
                getString(R.string.label_setting_consent)
            }
            else -> return
        }

        val adapter = MemberPolicyAdapter(this)
        dataBinding.viewPager.adapter = adapter

        if (args.memberPolicyContext.policyList.isNullOrEmpty()) {
            dataBinding.layoutNotFound.visible()
            dataBinding.viewPager.gone()
        } else {
            adapter.replace(args.memberPolicyContext.policyList)
            dataBinding.layoutNotFound.gone()
            dataBinding.viewPager.visible()
            if (args.memberPolicyContext.policyList.size > 1) {
                dataBinding.tabLayout.visible()
                TabLayoutMediator(
                    dataBinding.tabLayout,
                    dataBinding.viewPager
                ) { tab, position ->

                    tab.text = when (adapter.getItem(position)?.policyType) {
                        PolicyType.BAY_TC, PolicyType.BAY_CONSENT -> {
                            getString(R.string.label_bay_abbr)
                        }
                        PolicyType.COOP_TC, PolicyType.COOP_CONSENT -> {
                            getString(R.string.label_coop_abbr)
                        }
                        else -> adapter.getItem(position)?.policyType?.name ?: getString(R.string.label_policy_abbr)
                    }
                    dataBinding.viewPager.setCurrentItem(tab.position, true)
                }.attach()
            }
        }
    }

}