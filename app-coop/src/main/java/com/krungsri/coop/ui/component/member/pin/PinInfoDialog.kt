package com.krungsri.coop.ui.component.member.pin


import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.FragmentManager
import com.krungsri.coop.R
import com.krungsri.coop.ui.base.BaseDialog

class PinInfoDialog : BaseDialog() {


    fun show(manager: FragmentManager) {
        super.show(
            manager,
            "PinInfoDialog"
        )
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.dialog_pin_condition, container, false)

        root.findViewById<AppCompatTextView>(R.id.textMessage)?.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(getString(R.string.label_member_pin_condition_desc), Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(getString(R.string.label_member_pin_condition_desc))
        }
        root.findViewById<AppCompatButton>(R.id.buttonClose).setOnClickListener {
            dismiss()
        }
        return root
    }

}