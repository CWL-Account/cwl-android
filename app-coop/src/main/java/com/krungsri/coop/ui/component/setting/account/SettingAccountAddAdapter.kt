package com.krungsri.coop.ui.component.setting.account

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.databinding.DataBindingUtil
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.databinding.ItemSettingAccountAddBinding
import com.krungsri.coop.extension.gone
import com.krungsri.coop.extension.string
import com.krungsri.coop.extension.visible
import com.krungsri.coop.helper.FormatHelper
import com.krungsri.coop.model.account.AccountInfo
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.ui.base.BaseRecyclerViewAdapter
import timber.log.Timber

class SettingAccountAddAdapter :
    BaseRecyclerViewAdapter<AccountInfo, ItemSettingAccountAddBinding>() {
    override fun createBinding(parent: ViewGroup): ItemSettingAccountAddBinding {
        return DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_setting_account_add,
            parent,
            false
        )
    }

    private val accountUpdate = HashMap<String, Flag>()
    private val accountTypeSet = HashSet<AccountType>()

    override fun bind(binding: ItemSettingAccountAddBinding, position: Int, item: AccountInfo) {
        if (!accountTypeSet.contains(item.accountType)) {
            when {
                item.productName != null -> {
                    binding.textTitle.text = item.productName
                }
                AccountType.LOAN == item.accountType -> {
                    binding.textTitle.string(R.string.label_account_type_loan)
                }
                else -> {
                    binding.textTitle.string(R.string.label_account_type_saving)
                }
            }
            accountTypeSet.add(item.accountType)
            binding.textTitle.visible()
        } else {
            binding.textTitle.gone()
        }
        binding.adapter = this
        binding.accountInfo = item
        binding.textAccountDesc.text = item.accountDesc
        binding.textAccountNo.text =
            FormatHelper.formatAccountNo(item.coopAccountNo ?: "", AppConstant.IS_MARK_ACCOUNT)
        binding.checkboxAddAccountInfo = binding.checkboxAddAccount
    }


    fun settingAccount(checkboxAddAccount: AppCompatCheckBox, accountInfo: AccountInfo) {
        if (accountUpdate.isEmpty()) {
            getItems()?.run {
                for (info in this) {
                    accountUpdate[info.coopAccountNo ?: "-"] = info.mobileFlag
                }
            }
        }

        accountUpdate[accountInfo.coopAccountNo ?: "-"] =
            if (checkboxAddAccount.isChecked) Flag.Y else Flag.N

    }

    fun getSelectedAccountList(): MutableList<String> {
        Timber.d("getSelectedAccountList : %s", accountUpdate)
        val selectedList = mutableListOf<String>()
        for (key in accountUpdate.keys) {
            if (accountUpdate[key] == Flag.Y) {
                selectedList.add(key)
            }
        }
        return selectedList
    }

}
