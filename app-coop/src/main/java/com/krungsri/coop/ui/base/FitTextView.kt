package com.krungsri.coop.ui.base

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatTextView
import com.krungsri.coop.R


class FitTextView(context: Context, attrs: AttributeSet?) : AppCompatTextView(context, attrs) {
    private var maxTextSize = this.textSize
    private var minTextSize = maxTextSize

    init {
        val typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.FitTextView, 0, 0
        )

        val minSizeRds = typedArray.getResourceId(R.styleable.FitTextView_minTextSize, 0)
        if (minSizeRds != 0) {
            minTextSize = resources.getDimension(minSizeRds)
        }
        typedArray.recycle()
    }

    private fun refitText(text: String, textWidth: Int) {
        if (textWidth > 0 && minTextSize < maxTextSize) {
            val availableWidth = (textWidth - this.paddingLeft - this.paddingRight)
            var trySize = maxTextSize
            this.setTextSize(TypedValue.COMPLEX_UNIT_PX, trySize)
            while (trySize > minTextSize
                && this.paint.measureText(text) > availableWidth
            ) {
                trySize -= 1f
                if (trySize <= minTextSize) {
                    trySize = minTextSize
                    break
                }
                this.setTextSize(TypedValue.COMPLEX_UNIT_PX, trySize)
            }
            this.setTextSize(TypedValue.COMPLEX_UNIT_PX, trySize)
        }
    }

    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        refitText(text.toString(), this.width);
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        if (w != oldw) {
            refitText(this.text.toString(), w);
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val parentWidth = MeasureSpec.getSize(widthMeasureSpec)
        refitText(this.text.toString(), parentWidth)
    }
}