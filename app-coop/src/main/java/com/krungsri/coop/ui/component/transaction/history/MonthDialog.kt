package com.krungsri.coop.ui.component.transaction.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.FragmentManager
import com.krungsri.coop.R
import com.krungsri.coop.databinding.DialogMonthBinding
import com.krungsri.coop.extension.getString
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.ui.base.BaseDialog
import timber.log.Timber
import java.util.*

class MonthDialog(
    private val onSelectMonth: ((month: String) -> Unit),
    private val accountType: AccountType = AccountType.SAVING,
    private val selectedMonth: String? = null
) : BaseDialog() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DialogMonthBinding.inflate(
            inflater,
            container,
            false
        )
        if (accountType == AccountType.LOAN) {
            binding.buttonCancel.setBackgroundResource(R.drawable.button_empty_loan)
            binding.buttonOK.setBackgroundResource(R.drawable.button_main_loan)
        }
        initView(binding)
        return binding.root
    }

    private fun initView(binding: DialogMonthBinding) {
        val canceler = Calendar.getInstance()
        var year = canceler.get(Calendar.YEAR)
        var month = canceler.get(Calendar.MONTH)
        val monthArray = binding.root.context.resources.getStringArray(R.array.months_fullname)
        val monthDisplay = mutableListOf<String>()
        val maxDisplay = 10
        val monthValue = mutableListOf<String>()
        var monthString = if ((month + 1) < 10) "0${month + 1}" else "${month + 1}"
        monthDisplay.add(binding.root.getString(R.string.month_current))
        monthValue.add("$year-$monthString")
        for (ids in maxDisplay downTo 0) {
            month -= 1;
            if (month < 0) {
                month = 11
                year -= 1
            }
            monthString = if ((month + 1) < 10) "0${month + 1}" else "${month + 1}"
            monthDisplay.add(monthArray[month] + " ${year + 543}")
            monthValue.add("$year-$monthString")
        }

        val monthAdapter = ArrayAdapter(binding.root.context, R.layout.item_month, monthDisplay)
        monthAdapter.setDropDownViewResource(R.layout.item_month);
        binding.spinnerMonth.adapter = monthAdapter

        Timber.d("selectedMonth = %s", selectedMonth)
        selectedMonth?.run {
            val idx = monthValue.indexOf(this)
            if (idx > 0) {
                binding.spinnerMonth.setSelection(idx)
            }
        }
        binding.buttonCancel.setOnClickListener {
            dismiss()
        }

        binding.buttonOK.setOnClickListener {
            dismiss()
            onSelectMonth.invoke(monthValue[binding.spinnerMonth.selectedItemPosition])
        }
    }

    fun show(manager: FragmentManager) {
        super.show(manager, "MonthDialog")
    }
}