package com.krungsri.coop.ui.component.transaction.payment

import android.os.Parcelable
import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.home.CoopService
import kotlinx.parcelize.Parcelize

@Parcelize
data class BillPaymentContext(val coopService: CoopService, val accountDesc: AccountDesc) : Parcelable