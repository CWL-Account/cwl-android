package com.krungsri.coop.ui.component.member.landing

enum class LandingDestination {
    GOTO_REGISTER,
//    NEW_PIN_REGISTER,
    NEW_PIN_FORGOT_PIN,
    NEW_PIN_UNLOCK,
    FORGOT_PIN,
    CHANGE_PIN,
    UNLOCK_PIN,
    HOME, HOME_FROM_AFTER_NEW_PIN, ACCOUNT_MANAGE
}