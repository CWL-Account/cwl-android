package com.krungsri.coop.ui.component.home

import android.graphics.Color
import android.view.LayoutInflater
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.google.android.material.tabs.TabLayoutMediator
import com.krungsri.coop.R
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.databinding.FragmentHomeBinding
import com.krungsri.coop.databinding.TabAccountBinding
import com.krungsri.coop.extension.*
import com.krungsri.coop.helper.AppPrefs
import com.krungsri.coop.helper.OpenURLHelper
import com.krungsri.coop.model.home.InquiryHomeResponse
import com.krungsri.coop.ui.base.BaseFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class HomeFragment :
    BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    override val layoutId: Int = R.layout.fragment_home

    @Inject
    lateinit var tokenService: TokenService

    override val viewModelClass: Class<HomeViewModel> = HomeViewModel::class.java
    override fun onFragmentStart() {
        viewModel.init()
        dataBinding.fragment = this
        dataBinding.vm = viewModel
        dataBinding.toolbar.setNavigationIcon(R.drawable.ic_settings)
        dataBinding.toolbar.setNavigationOnClickListener {
            this.onSettingClick()
        }

        viewModel.showAddNewAccount.observe(viewLifecycleOwner, Observer {
            if (it) {
                dataBinding.divider.invisible()
                dataBinding.viewPager.gone()
                dataBinding.tabLayout.gone()
                dataBinding.imgAccountBg.visible()
                dataBinding.layoutNoAccountList.visible()
            }
        })


        viewModel.newsListData.observe(viewLifecycleOwner, Observer { newsList ->
            dataBinding.labelNews.visible()
            if (newsList.isNotEmpty()) {
                dataBinding.imgNews.loadUrl(
                    GlideUrl(newsList[0].pictureUrl) { tokenService.getTokenMap() },
                    placeholderRes = R.drawable.bg_news,
                    onErrorRes = R.drawable.bg_news,
                    diskCacheStrategy = DiskCacheStrategy.AUTOMATIC
                )
                newsList[0].newsUrl?.run {
                    dataBinding.imgNews.setOnClickListener {
                        OpenURLHelper.open(requireContext(), this)
                    }
                }
                newsList[0].content?.run {
                    dataBinding.textNews.visible()
                    dataBinding.textNews.text = this
                }
            } else {
                dataBinding.imgNews.loadRes(R.drawable.bg_news)
            }
        })

        viewModel.inquiryHomeData.observe(viewLifecycleOwner, {
            intiView(it)
        })

        dataBinding.viewPager.post {
            viewModel.initHome()
        }

        GlobalScope.launch(Dispatchers.IO) {
            AppPrefs.getString("memberDisplay")?.run {
                val memberDisplay = File(requireContext().dataDir, this)
                if (memberDisplay.exists()) {
                    launch(Dispatchers.Main) {
                        dataBinding.imgPerson.loadCircle(memberDisplay)
                    }
                }
            }
        }

    }

    private fun intiView(inquiryHomeData: MutableList<InquiryHomeResponse>) {
        Timber.d("intiView1 %s, %s", dataBinding.viewPager.adapter, inquiryHomeData.isNullOrEmpty())
        if (inquiryHomeData.isNullOrEmpty() || inquiryHomeData.size == 0) {
            return
        }

        Timber.d("intiView1 %s, %s", dataBinding.viewPager.adapter, inquiryHomeData.size)

        dataBinding.divider.invisible()
        dataBinding.viewPager.visible()
        dataBinding.imgAccountBg.visible()
        dataBinding.layoutNoAccountList.gone()
        dataBinding.loadingAccount.gone()
        dataBinding.tabLayout.gone()

        dataBinding.viewPager.adapter = AccountTabAdapter(listener = viewModel).apply {
            replace(inquiryHomeData)
        }

        if (inquiryHomeData.size > 1) {
            dataBinding.divider.visible()
            dataBinding.tabLayout.visible()
            val colorPrimary = ContextCompat.getColor(requireContext(), R.color.colorPrimary)
            val colorSecondary = ContextCompat.getColor(requireContext(), R.color.colorSecondary)
            if (dataBinding.viewPager.tag == null) {

                val tabLayoutMediator = TabLayoutMediator(
                    dataBinding.tabLayout,
                    dataBinding.viewPager
                ) { tab, position ->
                    val productName =
                        (dataBinding.viewPager.adapter as AccountTabAdapter).getItem(position)?.productName
                    val tabAccount =
                        TabAccountBinding.inflate(LayoutInflater.from(requireContext()), dataBinding.tabLayout, false)
                    tabAccount.textTitle.text = productName
                    tab.customView = tabAccount.root
                    dataBinding.viewPager.setCurrentItem(tab.position, true)
                }

                tabLayoutMediator.attach()
                dataBinding.viewPager.tag = tabLayoutMediator
                dataBinding.viewPager.registerOnPageChangeCallback(object :
                    ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        if (position == 1) {
                            dataBinding.tabLayout.setSelectedTabIndicatorColor(colorSecondary)
                            dataBinding.divider.setBackgroundColor(colorSecondary)
                            changeTabColor(position, colorSecondary)
                        } else {
                            dataBinding.tabLayout.setSelectedTabIndicatorColor(colorPrimary)
                            dataBinding.divider.setBackgroundColor(colorPrimary)
                            changeTabColor(position, colorPrimary)
                        }
                    }
                })
            }
            Timber.d("accountTab = %s", viewModel.accountTab.value)
            viewModel.accountTab.value?.run {
                val position = this
                if (position != 0) {
                    dataBinding.viewPager.post {
                        dataBinding.viewPager.currentItem = position

                    }

                    dataBinding.tabLayout.setScrollPosition(position, 0f, true)
                    changeTabColor(position, if (position == 1) colorSecondary else colorPrimary)
                }
            }
        } else {
            dataBinding.tabLayout.gone()
        }
    }

    private fun changeTabColor(position: Int, bgColor: Int) {
        for (i in 0..dataBinding.tabLayout.tabCount) {
            val tab = dataBinding.tabLayout.getTabAt(i)?.customView
            val textTitle = tab?.findViewById<AppCompatTextView>(R.id.textTitle)
            if (i == position) {
                textTitle?.setTextColor(Color.WHITE)
                textTitle?.setBackgroundColor(bgColor)
            } else {
                textTitle?.setTextColor(Color.BLACK)
                textTitle?.setBackgroundColor(Color.WHITE)
            }
        }

    }

    override fun onPause() {
        viewModel.accountTab.value = dataBinding.viewPager.currentItem
        super.onPause()
    }

    private fun onSettingClick() {
        gotoPage(
            HomeFragmentDirections.gotoSettingPage()
        )
    }

    fun gotoAddAccount() {
        gotoPage(
            HomeFragmentDirections.gotoAccountManagePage()
        )
    }

    fun gotoNews() {
        gotoPage(
            HomeFragmentDirections.gotoNewsPage()
        )
    }


}