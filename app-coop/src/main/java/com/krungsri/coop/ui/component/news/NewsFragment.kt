package com.krungsri.coop.ui.component.news

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.krungsri.coop.R
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.databinding.FragmentNewsBinding
import com.krungsri.coop.helper.OpenURLHelper
import com.krungsri.coop.ui.base.BaseFragment
import javax.inject.Inject

class NewsFragment : BaseFragment<FragmentNewsBinding, NewsViewModel>() {
    override val layoutId: Int = R.layout.fragment_news

    @Inject
    lateinit var tokenService: TokenService

    override val viewModelClass: Class<NewsViewModel> = NewsViewModel::class.java
    override fun onFragmentStart() {
        dataBinding.recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        dataBinding.recyclerView.adapter = NewsAdapter(tokenService) {
            OpenURLHelper.open(requireContext(), it)
        }

        viewModel.listNews()

        viewModel.newsListData.observe(viewLifecycleOwner, Observer { newsList ->
            if (newsList.result?.isNotEmpty() == true) {
                (dataBinding.recyclerView.adapter as NewsAdapter).add(newsList.result)
            }
        })

        val scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                    viewModel.listNews()
                }
            }
        }
        dataBinding.recyclerView.addOnScrollListener(scrollListener)
    }
}