package com.krungsri.coop


import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.google.firebase.FirebaseApp
import com.krungsri.coop.data.session.TokenService
import com.krungsri.coop.extension.visible
import com.krungsri.coop.helper.SecurityHelper
import com.krungsri.coop.lib.eventbut.AppEvent
import com.krungsri.coop.lib.eventbut.EventBus
import com.krungsri.coop.lib.eventbut.ToggleLoading
import com.krungsri.coop.ui.base.Alert
import com.krungsri.coop.ui.base.AlertMessageDialog
import com.krungsri.coop.ui.base.ProgressDialog
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


class MainActivity : AppCompatActivity(), HasAndroidInjector, LifecycleObserver {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var tokenService: TokenService

    private val disposeBag: CompositeDisposable = CompositeDisposable()

    private lateinit var progressDialog: ProgressDialog

    private lateinit var layoutTop: View
    private var isActiveApp = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        if (SecurityHelper.isRooted()) {
            setContentView(R.layout.fragment_splash_screen)
            AlertMessageDialog.create(Alert.OkDialog(
                message = getString(R.string.error_rooted),
                onDismiss = {
                    finish()
                }
            )).show(supportFragmentManager)
            return
        }

        setContentView(R.layout.activity_main)
        layoutTop = findViewById(R.id.layoutTop)
        progressDialog = ProgressDialog(this)
        lifecycle.addObserver(this)
        lifecycle.addObserver(progressDialog)


        EventBus.listen(AppEvent::class.java)
            .subscribe {
                if (it == AppEvent.AutoLogout) {
                    showLogoutDialog()
                } else if (it == AppEvent.ReloadApp) {
                    Timber.d("[EXIT]]Reload App")
                    restartApp()
                }
            }.also {
                disposeBag.add(it)
            }
        EventBus.listen(Alert.OkDialog::class.java).subscribe {
            AlertMessageDialog.create(it).show(supportFragmentManager)
        }.also {
            disposeBag.add(it)
        }
        EventBus.listen(Alert.SelectDialog::class.java).subscribe {
            AlertMessageDialog.create(it).show(supportFragmentManager)
        }.also {
            disposeBag.add(it)
        }
        EventBus.listen(Alert.Toast::class.java).subscribe {
            AlertMessageDialog.showToast(this, it)
        }.also {
            disposeBag.add(it)
        }

        EventBus.listen(ToggleLoading::class.java)
            .subscribe {
                Timber.d("ToggleLoading :: %s", it)
                when (it) {
                    ToggleLoading.ShowProgress -> onLoading(true)
                    ToggleLoading.HideProgress -> onLoading(false)
                    else -> {
                    }
                }
            }.also {
                disposeBag.add(it)
            }

        val dm = resources.displayMetrics
        Timber.d("displayMetrics size = %s x %s", dm.widthPixels, dm.heightPixels)
        Timber.d(
            "displayMetrics density = %s, densityDpi = %s, scaledDensity = %s, xdpi=%s, ydpi=%s",
            dm.density,
            dm.densityDpi,
            dm.scaledDensity,
            dm.xdpi,
            dm.ydpi
        )

//        if (BuildConfig.DEBUG) {
//            findViewById<View>(R.id.layoutDebugScreen).visible()
//            val densityDpi = (dm.density * 160f).toInt()
//            val testScreen = findViewById<AppCompatTextView>(R.id.testScreen)
//            testScreen.text = "${dm.densityDpi}/$densityDpi"
//        }
    }


    private fun showLogoutDialog() {
        if (isActiveApp) {
            AlertMessageDialog.create(Alert.OkDialog(
                message = getString(R.string.alert_auto_logout),
                onDismiss = {
                    findNavController(R.id.container).navigate(R.id.splashFragment)
//                    GlobalScope.launch(Dispatchers.Main) {
//                        delay(300)
//                    }
                }
            )).show(supportFragmentManager)
            layoutTop.visible()
        } else {
            Timber.d("[EXIT]]Auto Logout")
            restartApp()
        }
    }

    private fun restartApp() {
        finish()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun onLoading(show: Boolean) {
        Timber.d("onLoading %s", show)
        if (show) {
            progressDialog.showDialog()
        } else {
            progressDialog.hideDialog()
        }
    }

    override fun onDestroy() {
        tokenService.logout()
        disposeBag.clear()
        lifecycle.removeObserver(this)
        super.onDestroy()
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector


    private var doubleBackToExitPressedOnce = false


    override fun onBackPressed() {
        Timber.d("onBackPressed 1")
        val nav = Navigation.findNavController(this, R.id.container)
        if (nav.currentDestination?.id == R.id.transactionSlipFragment) {
            return
        }
        Timber.d("onBackPressed 2, %s", nav.previousBackStackEntry)
        if (nav.previousBackStackEntry == null) {
            if (!doubleBackToExitPressedOnce) {
                doubleBackToExitPressedOnce = true
                Timber.d("onBackPressed 3.1")
                GlobalScope.launch(Dispatchers.IO) {
                    delay(2000)
                    doubleBackToExitPressedOnce = false
                }
            } else {
                Timber.d("onBackPressed 3.2")
                super.onBackPressed()
            }
            return
        }

        val isPopBackStack = nav.popBackStack()
        Timber.d(
            "onBackPressed 4, isPopBackStack = %s, name = %s",
            isPopBackStack,
            nav.currentDestination
        )
        if (!isPopBackStack && doubleBackToExitPressedOnce) {
            super.onBackPressed()
        } else {
            doubleBackToExitPressedOnce = true
            GlobalScope.launch(Dispatchers.IO) {
                delay(2000)
                doubleBackToExitPressedOnce = false
            }
        }
    }

    override fun onUserInteraction() {
        EventBus.publish(AppEvent.UserActive)
        super.onUserInteraction()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        Timber.e("================================>>>> lifecycle owner ON_START")
        isActiveApp = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun stop() {
        Timber.e("================================>>>> lifecycle owner ON_PAUSE")
        isActiveApp = false
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)
        val newOverride = Configuration(newBase?.resources?.configuration)
        newOverride.fontScale = 1.0f
        applyOverrideConfiguration(newOverride)
    }

}
