package com.krungsri.coop.extension

import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.widget.doAfterTextChanged
import com.krungsri.coop.helper.FormatHelper
import timber.log.Timber
import java.math.RoundingMode
import java.text.DecimalFormat

fun AppCompatEditText.setMoneyFormat() {
    if (text?.length ?: 0 > 0) {
        val value = text.toString().replace(",", "")
        val money = FormatHelper.formatAmount(value.toBigDecimal().setScale(2, RoundingMode.FLOOR))
        setText(money)
    }
}

fun AppCompatEditText.moneyFormat() {
    val df = DecimalFormat("#,###.##")

    setOnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) {
            setMoneyFormat()
        }
    }

    setOnEditorActionListener { _, actionId, event ->
        if (actionId == EditorInfo.IME_ACTION_DONE
            || event.action == KeyEvent.ACTION_DOWN
            && event.keyCode == KeyEvent.KEYCODE_ENTER
        ) {
            setMoneyFormat()
            true;
        }
        false
    }

    doAfterTextChanged { edit ->
        var value = edit.toString().replace(",", "")
        if (value.isNotEmpty() && value.toDoubleOrNull() != null) {
            if (value.contains(".")) {
                val digit = value.substring(value.indexOf(".") + 1).let {
                    if (it.length > 2) {
                        it.substring(0, 2)
                    } else {
                        it
                    }
                }
                value = value.substring(0, value.indexOf(".") + 1) + digit
            }
            if (text?.toString() != value) {
                val idxStart = selectionStart
                setText(value)
                val count = value.count { ",".contains(it) }
                val idx = idxStart + count
                val selection = if (idx > value.length) value.length else idx
                Timber.d("moneyFormat = %s, selection = %s", value, selection)
                if (selection > 0 && text?.length ?: 0 > 0) {
                    setSelection(selection)
                }
            }
        }
    }
}
