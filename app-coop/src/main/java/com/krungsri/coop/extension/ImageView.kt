package com.krungsri.coop.extension

import android.content.res.ColorStateList
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.DiskCacheStrategy.NONE
import com.bumptech.glide.load.model.GlideUrl
import com.krungsri.coop.R


fun AppCompatImageView.loadRes(@DrawableRes drawableRes: Int) {
    Glide.with(this)
        .load(drawableRes)
        .diskCacheStrategy(NONE)
        .into(this)
}

fun AppCompatImageView.loadCircle(any: Any) {
    Glide.with(this).load(any)
        .circleCrop()
        .diskCacheStrategy(NONE)
        .into(this)
}

fun AppCompatImageView.loadUrl(
    url: GlideUrl,
    @DrawableRes placeholderRes: Int? = R.drawable.bg_news,
    @DrawableRes onErrorRes: Int? = R.drawable.bg_news,
    diskCacheStrategy: DiskCacheStrategy = DiskCacheStrategy.NONE
) {
    val builder = Glide
        .with(this)
        .load(url)
        .diskCacheStrategy(diskCacheStrategy)

    placeholderRes?.run {
        builder.placeholder(this)
    }
    onErrorRes?.run {
        builder.error(this)
    }

    builder.into(this)
}

fun AppCompatImageView.setAccountIcon(coopAccountNo: String?, bayAccountNo: String?) {
    coopAccountNo?.run {
        loadCircle(R.drawable.logo_coop_cycle_min)
    } ?: bayAccountNo.run {
        loadCircle(R.drawable.logo_bay_cycle_min)
    }
}

fun AppCompatImageView.setColorTint(@ColorRes colorRes: Int) {
    ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(ContextCompat.getColor(context, colorRes)));
}