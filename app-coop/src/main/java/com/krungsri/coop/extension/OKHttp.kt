package com.krungsri.coop.extension


import android.content.Context
import com.krungsri.coop.BuildConfig
import com.krungsri.coop.helper.SecurityHelper
import okhttp3.*
import okhttp3.tls.HandshakeCertificates
import okhttp3.tls.decodeCertificatePem
import java.net.URI
import java.util.*
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLSession


fun OkHttpClient.Builder.initConnectionSpec(): OkHttpClient.Builder {
    if (BuildConfig.FLAVOR_server.equals("dev", true)) {
        return this
    }
    val spec: ConnectionSpec = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
        .tlsVersions(TlsVersion.TLS_1_2)
        .cipherSuites(
            CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
            CipherSuite.TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,
            CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
            CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256
        )
        .build()
    this.connectionSpecs(Collections.singletonList(spec))
    return this
}

fun OkHttpClient.Builder.addSslPinner(
    url: String,
    pinRoot: String?, pinIntermediate: String?, pinLeaf: String?
): OkHttpClient.Builder {
    if (BuildConfig.FLAVOR_server.equals("dev", true)
        || pinRoot.isNullOrEmpty() || pinIntermediate.isNullOrEmpty()
        || pinLeaf.isNullOrEmpty()) {
        return this
    }
    val uri = URI.create(url)
    val host = uri.host
    val certificatePinner = CertificatePinner.Builder()
        .add(host, pinRoot)
        .add(host, pinIntermediate)
        .add(host, pinLeaf)
        .build()
    this.certificatePinner(certificatePinner)
    return this
}

fun OkHttpClient.Builder.addSslSocketFactory(context: Context): OkHttpClient.Builder {
    if (BuildConfig.FLAVOR_server.equals("dev", true)) {
        return this
    }
    val certificates =
        HandshakeCertificates.Builder()
            .addTrustedCertificate(SecurityHelper.getSSHCert().trimIndent().decodeCertificatePem())
            .build()

    sslSocketFactory(certificates.sslSocketFactory(), certificates.trustManager)
    return this
}
