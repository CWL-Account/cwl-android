package com.krungsri.coop.extension

import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.krungsri.coop.R

fun TextInputEditText.clearError(inputLayout: TextInputLayout) {
    inputLayout.error = null
    error = null
    setBackgroundResource(R.drawable.input_boarder)
    setTextColor(ContextCompat.getColor(context, R.color.font_normal_color))
}

fun TextInputEditText.showError(inputLayout: TextInputLayout, errorMsg: String) {
    inputLayout.error = errorMsg
    error = null
    setBackgroundResource(R.drawable.input_boarder_error)
    setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_light))
}