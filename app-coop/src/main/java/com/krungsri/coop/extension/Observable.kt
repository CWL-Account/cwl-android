package com.krungsri.coop.extension

import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.data.api.http.ApiError
import com.krungsri.coop.data.api.http.ApiPageResponse
import com.krungsri.coop.data.api.http.ApiResponse
import com.krungsri.coop.data.session.CacheData
import com.krungsri.coop.exception.NoConnectivityException
import com.krungsri.coop.exception.TokenExpiredException
import com.krungsri.coop.helper.AppHelper
import com.krungsri.coop.lib.eventbut.EventBus
import com.krungsri.coop.lib.eventbut.ToggleLoading
import com.krungsri.coop.ui.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import org.json.JSONObject
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeoutException
import javax.net.ssl.SSLHandshakeException

private var errorNoInternetCount = 0
fun <T> Observable<T>.retryWhenConnectionError(
    vm: BaseViewModel,
    onError: (ApiError) -> Unit
): Observable<T> {
    return this.retryWhen { errorStream ->
        errorStream.flatMap<Any> { t ->
//            Timber.e(t, "retryWhenConnectionError, %s", t.javaClass)
            if (errorNoInternetCount > 2 ||
                t is SocketTimeoutException ||
                t is NoConnectivityException ||
                t is TimeoutException ||
                t is UnknownHostException ||
                t is ConnectException ||
                t is SocketException ||
                t is SSLHandshakeException
            ) {
                when {
                    t is SSLHandshakeException || AppHelper.isInternetAvailable(vm.appContext) -> {
                        return@flatMap Observable.create { emitter ->
                            onError(
                                ApiError(
                                    code = AppConstant.CODE_INTERNET_CONNECT,
                                    message = vm.appContext.getString(R.string.label_error_internet_connect),
                                    httpCode = 500,
                                    throwable = t
                                )
                            )
                        }
                    }
                    errorNoInternetCount > 2 -> {
                        return@flatMap Observable.create { emitter ->
                            vm.showOkAlert(
                                messageRds = R.string.label_no_internet_connect_and_exit,
                                buttonLabelRes = R.string.button_close,
//                                onDismiss = {
//                                    vm.onExitAppEvent.setEventValue("CONNECTION_TIMEOUT_2")
//                                })
                                onDismiss = { emitter.onNext("") })
                        }
                    }
                    else -> {
                        errorNoInternetCount += 1
                        return@flatMap Observable.create { emitter ->
                            vm.showOkAlert(
                                messageRds = R.string.label_no_internet_connect,
                                buttonLabelRes = R.string.button_retry,
                                onDismiss = { emitter.onNext("") })
                        }
                    }
                }
            } else {
                return@flatMap Observable.error(t)
            }
        }

    }
}

fun <T> Observable<T>.doToggleLoading(): Observable<T> = this
    .doOnSubscribe {
        Timber.d("Observable.doOnSubscribe")
        EventBus.publish(ToggleLoading.ShowProgress)
    }
    .doAfterTerminate {
        Timber.d("Observable.doAfterTerminate")
        EventBus.publish(ToggleLoading.HideProgress)
    }

fun <T> Observable<T>.doToggleViewLoading(
    vm: BaseViewModel
): Observable<T> = this

    .doOnSubscribe { vm.showViewLoading() }
    .doOnTerminate { vm.dismissViewLoading() }


fun <T> Observable<T>.subscribeWithViewModel(
    vm: BaseViewModel,
    onNext: (T) -> Unit,
    onError: (ApiError) -> Unit
): Disposable = this
//    .doConnectionError(vm)
    .retryWhenConnectionError(vm, onError)
    .subscribe(onNext.also {
        errorNoInternetCount = 0
    }, {
        Timber.e(it, "RequestError")
        when (it) {
            is TokenExpiredException -> {
                vm.callRefreshToken(it)
            }
            is retrofit2.HttpException -> {
                val apiError = it.response()?.let { response ->
                    response.errorBody()?.byteString()?.string(StandardCharsets.UTF_8)
                }?.let { errorJson ->
                    if (errorJson.isNullOrEmpty()) {
                        ApiError(
                            AppConstant.CODE_MS599ERROR,
                            vm.appContext.getString(R.string.error_system),
                            it.code(), it
                        )
                    } else {
                        Timber.d("errorJson = %s", errorJson)
                        try {
                            val jsonObject = JSONObject(errorJson)
                            var code = AppConstant.CODE_MS599ERROR
                            var message = "System Error";
                            if (jsonObject.has("code")) {
                                code = jsonObject.getString("code")
                            }
                            if (jsonObject.has("message")) {
                                message = jsonObject.getString("message")
                            }

                            ApiError(
                                code,
                                message,
                                it.code(), it
                            )
                        } catch (e: Exception) {
                            Timber.e(e)
                            ApiError(AppConstant.CODE_MS599ERROR, "", it.code(), it)
                        }
                    }
                } ?: ApiError(AppConstant.CODE_MS599ERROR, "", it.code(), it)

                onError(apiError)
            }
            else -> onError(ApiError(AppConstant.CODE_MS599ERROR, "", 500, it))
        }
    })

fun Disposable.disposedBy(vm: BaseViewModel) {
    vm.addDisposableInternal(this)
}

fun <T> Observable<T>.withCache(
    cache: CacheData<T>
): Observable<T> {
    if (cache.get() != null) {
        return Observable.just(cache.get())
    }
    return this.doOnNext {
        if (it is ApiResponse<*> || it is ApiPageResponse<*> && it.code == AppConstant.CODE_SUCCESS200) {
            cache.set(it)
        }
    }
}