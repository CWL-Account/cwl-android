package com.krungsri.coop.extension

import android.os.Build
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Patterns
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.krungsri.coop.R
import com.krungsri.coop.constants.AppConstant
import com.krungsri.coop.helper.FormatHelper
import java.math.BigDecimal


fun TextView.string(@StringRes stringRds: Int) {
    text = context.getString(stringRds)
}

fun TextView.stringHTML(@StringRes stringRds: Int) {
    text = Html.fromHtml(context.getString(stringRds), Html.FROM_HTML_MODE_LEGACY)
}

fun TextView.textSize(@DimenRes sizeRds: Int) {
    textSize = context.resources.getDimension(sizeRds)
}

fun TextView.setColor(@ColorRes colorRes: Int) {
    setTextColor(ContextCompat.getColor(context, colorRes))
}

fun TextView.setFontTypeface(typeFace: Int) {
    setTypeface(typeface, typeFace)
}

fun TextView.setAmount(amount: BigDecimal?, showCurrency: Boolean = false, default: String = "") {
    text = amount?.let {
        FormatHelper.formatAmount(it) + " " + (if (showCurrency) getString(R.string.label_transaction_currency) else "")
    } ?: default

}

fun TextView.setAccountNo(
    coopAccountNo: String?,
    bayAccountNo: String?,
    masked: Boolean = AppConstant.IS_MARK_ACCOUNT
) {
    text = FormatHelper.formatAccountNo(
        accNumber = coopAccountNo ?: bayAccountNo ?: "",
        masked = masked
    )
}