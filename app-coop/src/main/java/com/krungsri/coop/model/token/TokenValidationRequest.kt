package com.krungsri.coop.model.token

import com.google.gson.annotations.SerializedName


data class TokenValidationRequest(
    @SerializedName("uuid")
    val uuid: String
)

data class RefreshTokenRequest(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("uuid")
    val uuid: String
)