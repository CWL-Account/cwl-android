package com.krungsri.coop.model.otp

import com.google.gson.annotations.SerializedName

data class OTPResponse(
    @SerializedName("verification_token")
    val verificationToken: String,
    @SerializedName("otp_ref")
    val otpRef: String,
    @SerializedName("expire_in")
    val expireIn: Long,
    @SerializedName("mobile_no")
    val mobileNo: String
)