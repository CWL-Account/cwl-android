package com.krungsri.coop.model.enumeration

import com.google.gson.annotations.SerializedName

enum class VerificationType {
    @SerializedName("REGISTER")
    REGISTER,

    @SerializedName("UNLOCK")
    UNLOCK,

    @SerializedName("FORGOT-PIN")
    FORGOT_PIN,

    @SerializedName("CHANGE-PIN")
    CHANGE_PIN,

    @SerializedName("ADD-ACCOUNT")
    ADD_ACCOUNT,

    @SerializedName("TRANSACTION-CONFIRM")
    TRANSACTION_CONFIRM
}


enum class Flag {
    @SerializedName("Y")
    Y,

    @SerializedName("N")
    N
}

enum class ProductCode {
    @SerializedName("SAVING_PRODUCT")
    SAVING_PRODUCT,

    @SerializedName("LOAN_PRODUCT")
    LOAN_PRODUCT
}

enum class ServiceCode {
    @SerializedName("TRANSFERRING")
    TRANSFERRING,

    @SerializedName("WITHDRAW")
    WITHDRAW,

    @SerializedName("DEPOSIT")
    DEPOSIT,

    @SerializedName("TRANSACTION_HISTORY")
    TRANSACTION_HISTORY,

    @SerializedName("LOAN_WITHDRAW")
    LOAN_WITHDRAW,

    @SerializedName("LOAN_PAYMENT")
    LOAN_PAYMENT,

    @SerializedName("BILL_PAYMENT")
    BILL_PAYMENT,

    @SerializedName("SAVING_PAYMENT")
    SAVING_PAYMENT,

    @SerializedName("MEMBER_INFO")
    MEMBER_INFO,
}

enum class AccountType {
    @SerializedName("SAVING")
    SAVING,

    @SerializedName("FIXED")
    FIXED,

    @SerializedName("LOAN")
    LOAN,

    @SerializedName("BAY")
    BAY,

    @SerializedName("OTHER")
    OTHER

}

enum class TransactionStatus {
    @SerializedName("FNLD")
    FINALIZED,

    @SerializedName("PPRC")
    PROCESSING,

    @SerializedName("RJDT")
    BAY_REJECTED,

    @SerializedName("RJDT-C")
    COOP_REJECTED,

    @SerializedName("ACPT")
    ACCEPTED,

    @SerializedName("OTHER")
    OTHER
}

enum class TransactionChannel {
    @SerializedName("MOBILE")
    MOBILE,

    @SerializedName("BRANCH")
    BRANCH,

    @SerializedName("OTHER")
    OTHER
}