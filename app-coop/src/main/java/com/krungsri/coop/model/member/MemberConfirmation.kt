package com.krungsri.coop.model.member

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.VerificationType
import com.krungsri.coop.model.policy.PolicyFlag
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberConfirmationRequest(
    @SerializedName("verification_type")
    val verificationType: VerificationType,
    @SerializedName("verification_token")
    val verificationToken: String,
    @SerializedName("bay_account_no")
    val bayAccountNo: String,
    @SerializedName("citizen_no")
    val citizenNo: String,
    @SerializedName("member_code")
    val memberCode: String,
    @SerializedName("mobile_no")
    val mobileNo: String,
    @SerializedName("uuid")
    val uuid: String,
    @SerializedName("force_register")
    var forceRegister: Flag?,
    @SerializedName("policy_flag")
    val policyFlag: MutableList<PolicyFlag>?

) : Parcelable

@Parcelize
data class MemberConfirmationResponse(
    @SerializedName("bay_account_no")
    val bayAccountNo: String,
    @SerializedName("citizen_no")
    val citizenNo: String,
    @SerializedName("first_name")
    val firstName: String?,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("member_code")
    val memberCode: String,
    @SerializedName("mobile_no")
    val mobileNo: String,
    @SerializedName("verification_token")
    val verificationToken: String
) : Parcelable