package com.krungsri.coop.model.member

import android.os.Build
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.policy.PolicyFlag
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberRegistrationRequest(
    @SerializedName("verification_token")
    val verificationToken: String,
    @SerializedName("bay_account_no")
    val bayAccountNo: String,
    @SerializedName("citizen_no")
    val citizenNo: String,
    @SerializedName("member_code")
    val memberCode: String,
    @SerializedName("mobile_no")
    val mobileNo: String,
    @SerializedName("force_register")
    var forceRegister: Flag?,
    @SerializedName("policy_flag")
    val policyFlag: MutableList<PolicyFlag>?,
    @SerializedName("device_info")
    val deviceInfo: DeviceInfoRequest
) : Parcelable

@Parcelize
data class DeviceInfoRequest(
    @SerializedName("manufacture")
    val manufacture: String = Build.MANUFACTURER,
    @SerializedName("model")
    val model: String = Build.MODEL,
    @SerializedName("os_name")
    val osName: String = "Android",
    @SerializedName("os_version")
    val osVersion: String = Build.VERSION.RELEASE.toString(),
    @SerializedName("notification_token")
    val notificationToken: String,
    @SerializedName("uuid")
    val uuid: String
) : Parcelable

//@Parcelize
//data class MemberRegistrationResponse(
//    @SerializedName("access_token")
//    val accessToken: String,
//    @SerializedName("token_type")
//    val tokenType: String,
//    @SerializedName("first_name")
//    val firstName: String?,
//    @SerializedName("last_name")
//    val lastName: String?,
//    @SerializedName("member_code")
//    val memberCode: String,
//    @SerializedName("mobile_no")
//    val mobileNo: String
//) : Parcelable