package com.krungsri.coop.model.member

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberNewPinRequest(
    @SerializedName("pin_no")
    val pinNo: String
) : Parcelable

@Parcelize
data class MemberNewPinResponse(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("refresh_token")
    val refreshToken: String,
    @SerializedName("token_type")
    val tokenType: String
) : Parcelable