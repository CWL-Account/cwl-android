package com.krungsri.coop.model.member

import com.google.gson.annotations.SerializedName


data class MemberChangePinVerifyRequest(
    @SerializedName("current_pin_no")
    val currentPinNo: String,
    @SerializedName("new_pin_no")
    val newPinNo: String,
    @SerializedName("uuid")
    val uuid: String
)

data class MemberChangePinVerifyResponse(
    @SerializedName("verification_token")
    val verificationToken: String
)


data class MemberChangePinConfirmRequest(
    @SerializedName("verification_token")
    val verificationToken: String
)
