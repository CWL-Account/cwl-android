package com.krungsri.coop.model.member

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal

@Parcelize
data class MemberDataInfoResponse(
    @SerializedName("affilate")
    val affiliate: String?,
    @SerializedName("age")
    val age: String?,
    @SerializedName("capital_stock")
    val capitalStock: BigDecimal?,
    @SerializedName("citizen_no")
    val citizenNo: String?,
    @SerializedName("date_of_birth")
    val dateOfBirth: String?,
    @SerializedName("first_name")
    val firstName: String?,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("member_code")
    val memberCode: String?,
    @SerializedName("member_date")
    val memberDate: String?,
    @SerializedName("member_since")
    val memberSince: MemberSince?,
    @SerializedName("member_status")
    val memberStatus: String?,
    @SerializedName("no_of_period")
    val noOfPeriod: Int?,
    @SerializedName("position_title")
    val positionTitle: String?,
    @SerializedName("salary")
    val salary: BigDecimal?,
    @SerializedName("spouse_name")
    val spouseName: String?,
    @SerializedName("stock_payment_amount")
    val stockPaymentAmount: BigDecimal?,
    @SerializedName("total_loan_interest")
    val totalLoanInterest: BigDecimal?,
    @SerializedName("update_date")
    val updateDate: String
) : Parcelable {
    @Parcelize
    data class MemberSince(
        @SerializedName("years")
        val years: Int?,
        @SerializedName("months")
        val months: Int?,
        @SerializedName("days")
        val days: Int?
    ) : Parcelable
}

