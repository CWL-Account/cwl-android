package com.krungsri.coop.model.account

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.enumeration.Flag
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal

@Parcelize
data class AccountInfo(
    @SerializedName("account_type")
    val accountType: AccountType,
    @SerializedName("account_status")
    val accountStatus: Flag,
    @SerializedName("account_desc")
    val accountDesc: String,
    @SerializedName("account_name")
    val accountName: String,
    @SerializedName("coop_account_no")
    val coopAccountNo: String?,
    @SerializedName("bay_account_no")
    val bayAccountNo: String?,
    @SerializedName("mobile_flag")
    val mobileFlag: Flag,
    @SerializedName("product_name")
    val productName: String?

) : Parcelable

@Parcelize
data class AccountDesc(
    @SerializedName("account_type")
    val accountType: AccountType,
    @SerializedName("account_status")
    val accountStatus: Flag,
    @SerializedName("account_description")
    val accountDesc: String,
    @SerializedName("account_name")
    val accountName: String,
    @SerializedName("account_no")
    val accountNo: String,
    @SerializedName("available_balance")
    val availableBalance: BigDecimal?,
    @SerializedName("account_balance")
    val accountBalance: BigDecimal?,
    @SerializedName("outstanding_balance")
    val outstandingBalance: BigDecimal?,
    @SerializedName("deposit_flag")
    val depositFlag: Flag,
    @SerializedName("withdraw_flag")
    val withdrawFlag: Flag,
    @SerializedName("max_limit")
    val maxLimit: BigDecimal?,
    @SerializedName("min_limit")
    val minLimit: BigDecimal?
) : Parcelable

@Parcelize
data class AccountInActiveRequest(
    @SerializedName("verification_token")
    val verificationToken: String
) : Parcelable

@Parcelize
data class AccountInActiveResponse(
    @SerializedName("verification_token")
    val verificationToken: String,
    @SerializedName("coop_account_list")
    val coopAccountList: MutableList<AccountInfo>
) : Parcelable


@Parcelize
data class AccountRemoveRequest(
    @SerializedName("coop_account_list")
    val coopAccountList: MutableList<String>
) : Parcelable

@Parcelize
data class AccountAddRequest(
    @SerializedName("verification_token")
    val verificationToken: String,
    @SerializedName("coop_account_list")
    val coopAccountList: MutableList<String>
) : Parcelable

data class AccountMaskingRequest(
    @SerializedName("flag")
    val flag: Flag
)