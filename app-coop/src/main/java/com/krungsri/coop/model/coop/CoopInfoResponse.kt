package com.krungsri.coop.model.coop

import com.google.gson.annotations.SerializedName


data class CoopInfoResponse(
    @SerializedName("address")
    val address: String?,
    @SerializedName("contact_person")
    val contactPerson: String?,
    @SerializedName("coop_abbreviate_name")
    val coopAbbreviateName: String?,
    @SerializedName("coop_en_name")
    val coopEnName: String?,
    @SerializedName("coop_local_name")
    val coopLocalName: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("telephone_no")
    val telephoneNo: String?,
    @SerializedName("telephone_time")
    val telephoneTime: String?,
    @SerializedName("telephone_day")
    val telephoneDay: String?,
    @SerializedName("bay_call_center_day")
    val bayCallCenterDay: String?,
    @SerializedName("bay_call_center_no")
    val bayCallCenterNo: String?,
    @SerializedName("bay_call_center_time")
    val bayCallCenterTime: String?

)