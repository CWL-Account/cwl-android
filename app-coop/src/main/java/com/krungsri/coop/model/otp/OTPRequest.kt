package com.krungsri.coop.model.otp

import com.google.gson.annotations.SerializedName

data class OTPRequest(
    @SerializedName("verification_token")
    val verificationToken: String? = null
)