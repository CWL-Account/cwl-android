package com.krungsri.coop.model.policy

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.Flag
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal
import java.util.*

@Parcelize
data class PolicyResponse(
    @SerializedName("content")
    val content: String,
    @SerializedName("is_new")
    val isNew: Flag,
    @SerializedName("version_no")
    val versionNo: BigDecimal?,
    @SerializedName("policy_type")
    val policyType: PolicyType,
    @SerializedName("accepted_date")
    val acceptedDate: Date?,
    @SerializedName("accepted_flag")
    val acceptedFlag: Flag?,
    @SerializedName("verification_token")
    val verificationToken:String
) : Parcelable
