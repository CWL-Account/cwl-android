package com.krungsri.coop.model.member

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.Flag
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberLoginRequest(
    @SerializedName("pin_no")
    val pinNo: String,
    @SerializedName("uuid")
    val uuid: String
) : Parcelable

@Parcelize
data class MemberBiometricLoginRequest(
    @SerializedName("biometric_token")
    val biometricToken: String,
    @SerializedName("uuid")
    val uuid: String
) : Parcelable


@Parcelize
data class MemberLoginResponse(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("refresh_token")
    val refreshToken: String?,
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("first_name")
    val firstName: String?,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("is_new_policy")
    val isNewPolicy: Flag?
) : Parcelable