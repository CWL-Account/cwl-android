package com.krungsri.coop.model.transaction

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.enumeration.Flag
import com.krungsri.coop.model.enumeration.ServiceCode
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal

@Parcelize
data class InitialFormResponse(
    @SerializedName("service_code")
    val serviceCode: ServiceCode,
    @SerializedName("verification_token")
    val verificationToken: String,
    @SerializedName("source_account")
    val sourceAccount: MutableList<SourceAccount>?,
    @SerializedName("destination_account")
    val destinationAccountList: MutableList<DestinationAccount>?
) : Parcelable

@Parcelize
data class SourceAccount(
    @SerializedName("bay_account_no")
    val bayAccountNo: String?,
    @SerializedName("coop_account_no")
    val coopAccountNo: String?,
    @SerializedName("account_desc")
    val accountDesc: String,
    @SerializedName("account_name")
    val accountName: String,
    @SerializedName("account_status")
    val accountStatus: Flag,
    @SerializedName("account_type")
    val accountType: AccountType,
    @SerializedName("account_balance")
    val accountBalance: BigDecimal,
    @SerializedName("available_balance")
    val availableBalance: BigDecimal,
    @SerializedName("outstanding_balance")
    val outstandingBalance: BigDecimal?,
    @SerializedName("max_limit")
    val maxLimit: BigDecimal?,
    @SerializedName("min_limit")
    val minLimit: BigDecimal?
) : Parcelable

@Parcelize
data class DestinationAccount(
    @SerializedName("coop_account_no")
    val coopAccountNo: String?,
    @SerializedName("bay_account_no")
    val bayAccountNo: String?,
    @SerializedName("account_type")
    val accountType: String,
    @SerializedName("account_desc")
    val accountDesc: String,
    @SerializedName("account_name")
    val accountName: String
) : Parcelable