package com.krungsri.coop.model.analytic

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal

@Parcelize
data class AnalyticRequest(
    @SerializedName("screen_name")
    val screenName: String,
    @SerializedName("action_name")
    val actionName: String,
    @SerializedName("action_status")
    val actionStatus: String,
    @SerializedName("app_version")
    val appVersion: String,
    @SerializedName("backend_data")
    val backendData: String,
    @SerializedName("source_account")
    val sourceAccount: AnalyticAccountInfo,
    @SerializedName("destination_account")
    val destinationAccount: AnalyticAccountInfo
): Parcelable

@Parcelize
data class AnalyticAccountInfo(
    @SerializedName("coop_account_no")
    val coopAccountNo: String?,
    @SerializedName("bay_account_no")
    val bayAccountNo: String?,
    @SerializedName("amount")
    val amount: BigDecimal,
    @SerializedName("fee")
    val fee: BigDecimal,
    @SerializedName("charge_fee")
    val chargeFee: String): Parcelable