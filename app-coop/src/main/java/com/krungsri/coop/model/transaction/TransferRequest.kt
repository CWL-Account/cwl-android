package com.krungsri.coop.model.transaction

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.ServiceCode
import com.krungsri.coop.model.enumeration.TransactionChannel
import com.krungsri.coop.model.enumeration.TransactionStatus
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal
import java.util.*

data class AccountInquiryRequest(
        @SerializedName("account_no")
        val accountNo: String
)

data class TransactionInquiryRequest(
        @SerializedName("verification_token")
        val verificationToken: String,
        @SerializedName("service_code")
        val serviceCode: ServiceCode?,
        @SerializedName("from_coop_account_no")
        val fromCoopAccountNo: String? = null,
        @SerializedName("from_bay_account_no")
        val fromBayAccountNo: String? = null,
        @SerializedName("to_coop_account_no")
        val toCoopAccountNo: String? = null,
        @SerializedName("to_bay_account_no")
        val toBayAccountNo: String? = null,
        @SerializedName("amount")
        val amount: BigDecimal,
        @SerializedName("note")
        val note: String? = null
)

@Parcelize
data class TransactionInquiryResponse(
        @SerializedName("from_account_name")
        val fromAccountName: String,
        @SerializedName("from_coop_account_no")
        val fromCoopAccountNo: String?,
        @SerializedName("from_bay_account_no")
        val fromBayAccountNo: String?,
        @SerializedName("to_account_name")
        val toAccountName: String?,
        @SerializedName("to_coop_account_no")
        val toCoopAccountNo: String?,
        @SerializedName("to_bay_account_no")
        val toBayAccountNo: String?,
        @SerializedName("transaction_amount")
        val transactionAmount: BigDecimal,
        @SerializedName("transaction_fee")
        val transactionFee: BigDecimal?,
        @SerializedName("verification_token")
        val verificationToken: String,
        @SerializedName("note")
        val note: String? = null,
        @SerializedName("otp_ref")
        val otpRef: String? = null,
        @SerializedName("otp_mobile_no")
        val otpMobileNo: String? = null,
) : Parcelable

@Parcelize
data class TransactionConfirmRequest(
        @SerializedName("service_code")
        val serviceCode: ServiceCode,
        @SerializedName("verification_token")
        val verificationToken: String,
        @SerializedName("pin_no")
        val pinNo: String,
        @SerializedName("otp_no")
        val otpNo: String? = null,
        @SerializedName("from_coop_account_no")
        val fromCoopAccountNo: String? = null,
        @SerializedName("from_bay_account_no")
        val fromBayAccountNo: String? = null,
        @SerializedName("to_coop_account_no")
        val toCoopAccountNo: String? = null,
        @SerializedName("to_bay_account_no")
        val toBayAccountNo: String? = null,
        @SerializedName("amount")
        val amount: BigDecimal,
        @SerializedName("note")
        val note: String?
): Parcelable

@Parcelize
data class TransactionResponse(
        @SerializedName("reference_no")
        val referenceNo: String,
        @SerializedName("transaction_type")
        val transactionType: ServiceCode,
        @SerializedName("transaction_status")
        val transactionStatus: TransactionStatus,
        @SerializedName("transaction_channel")
        val transactionChannel: TransactionChannel,
        @SerializedName("from_account_name")
        val fromAccountName: String,
        @SerializedName("from_coop_account_no")
        val fromCoopAccountNo: String?,
        @SerializedName("from_bay_account_no")
        val fromBayAccountNo: String?,
        @SerializedName("to_account_name")
        val toAccountName: String?,
        @SerializedName("to_coop_account_no")
        val toCoopAccountNo: String?,
        @SerializedName("to_bay_account_no")
        val toBayAccountNo: String?,
        @SerializedName("transaction_date")
        val transactionDate: Date,
        @SerializedName("transaction_amount")
        val transactionAmount: BigDecimal,
        @SerializedName("transaction_fee")
        val transactionFee: BigDecimal?,
        @SerializedName("note")
        val note: String?

) : Parcelable


data class TransactionHistoryRequest(
        @SerializedName("coop_account_no")
        val coopAccountNo: String,
        @SerializedName("month")
        val month: String,
        @SerializedName("page_no")
        val pageNo: Int
)