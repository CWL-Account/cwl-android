package com.krungsri.coop.model.member

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal
import java.util.*

@Parcelize
data class MemberSlipRequest(
    @SerializedName("month")
    val month: String
) : Parcelable

@Parcelize
data class MemberSlipResponse(
    @SerializedName("affiliate")
    val affiliate: String,
    @SerializedName("member_capital_stock")
    val memberCapitalStock: BigDecimal,
    @SerializedName("no_of_slip")
    val noOfSlip: String,
    @SerializedName("slip_date")
    val slipDate: Date,
    @SerializedName("payment_amount")
    val paymentAmount: BigDecimal,
    @SerializedName("total_loan_interest")
    val totalLoanInterest: BigDecimal,
    @SerializedName("total_payment_amount")
    val totalPaymentAmount: BigDecimal,
    @SerializedName("transaction")
    val transaction: MutableList<MemberSlipTrans>,
    @SerializedName("printurl")
    val printUrl: String?
) : Parcelable


@Parcelize
data class MemberSlipTrans(
    @SerializedName("no")
    val no: Int,
    @SerializedName("transaction_code")
    val transactionCode: String,
    @SerializedName("transaction_type")
    val transactionType: String,
    @SerializedName("transaction_desc")
    val transactionDesc: String,
    @SerializedName("no_of_period")
    val noOfPeriod: Int,
    @SerializedName("principle_paid")
    val principlePaid: BigDecimal,
    @SerializedName("interest_paid")
    val interestPaid: BigDecimal,
    @SerializedName("payment_amount")
    val paymentAmount: BigDecimal,
) : Parcelable