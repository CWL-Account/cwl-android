package com.krungsri.coop.model.policy

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.Flag
import kotlinx.parcelize.Parcelize


data class PolicyRequest(
    @SerializedName("policy_type")
    val policyType: PolicyType
)

@Parcelize
data class PolicyFlag(
    @SerializedName("policy_type")
    val policyType: PolicyType,
    @SerializedName("flag")
    val flag: Flag,
    @SerializedName("verification_token")
    val verificationToken: String
) : Parcelable

enum class PolicyType {
    @SerializedName("BAY_TC")
    BAY_TC,

    @SerializedName("BAY_CONSENT")
    BAY_CONSENT,

    @SerializedName("BAY_OTHER")
    BAY_OTHER,

    @SerializedName("COOP_TC")
    COOP_TC,

    @SerializedName("COOP_CONSENT")
    COOP_CONSENT,

    @SerializedName("COOP_OTHER")
    COOP_OTHER,

    @SerializedName("OTHER")
    OTHER,
}

