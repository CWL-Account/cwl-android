package com.krungsri.coop.model.otp

import com.google.gson.annotations.SerializedName

data class OTPVerificationRequest(
    @SerializedName("verification_token")
    val verificationToken: String,
    @SerializedName("otp_no")
    val otpNo: String
)