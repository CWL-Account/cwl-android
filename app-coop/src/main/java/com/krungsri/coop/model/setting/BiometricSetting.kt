package com.krungsri.coop.model.setting

import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.Flag


data class BiometricSettingRequest(
    @SerializedName("is_enable")
    val isEnable: Flag,
    @SerializedName("pin_no")
    val pinNo: String? = null,
    @SerializedName("uuid")
    val uuid: String
)

data class BiometricSettingResponse(
    @SerializedName("biometric_token")
    var biometricToken: String
)