package com.krungsri.coop.model.member

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MemberUnlockRequest(
    @SerializedName("verification_token")
    val verificationToken: String
) : Parcelable

//@Parcelize
//data class MemberUnlockResponse(
//    @SerializedName("access_token")
//    val accessToken: String,
//    @SerializedName("token_type")
//    val tokenType: String,
//    @SerializedName("first_name")
//    val firstName: String?,
//    @SerializedName("last_name")
//    val lastName: String?,
//    @SerializedName("member_code")
//    val memberCode: String,
//    @SerializedName("mobile_no")
//    val mobileNo: String
//) : Parcelable