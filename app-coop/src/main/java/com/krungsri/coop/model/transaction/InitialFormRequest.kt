package com.krungsri.coop.model.transaction

import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.ProductCode
import com.krungsri.coop.model.enumeration.ServiceCode

data class InitialFormRequest(
    @SerializedName("product_code")
    val productCode: ProductCode,
    @SerializedName("service_code")
    val serviceCode: ServiceCode
)