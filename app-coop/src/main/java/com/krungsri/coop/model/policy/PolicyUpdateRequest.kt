package com.krungsri.coop.model.policy

import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.Flag


data class PolicyUpdateRequest(
    @SerializedName("accept_flag")
    val acceptFlag: Flag,
    @SerializedName("policy_type")
    val policyType: PolicyType,
    @SerializedName("verification_token")
    val verificationToken: String
)