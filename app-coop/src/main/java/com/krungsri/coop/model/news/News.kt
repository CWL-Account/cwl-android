package com.krungsri.coop.model.news

import com.google.gson.annotations.SerializedName

data class NewsRequest(val pageNo: Int, val size: Int)
data class NewsResponse(
    @SerializedName("picture_url")
    val pictureUrl: String,
    @SerializedName("news_url")
    val newsUrl: String?,
    @SerializedName("content")
    val content: String?
)