package com.krungsri.coop.model.transaction

import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.enumeration.AccountType
import com.krungsri.coop.model.enumeration.ServiceCode

data class BillPaymentRequest(
    @SerializedName("service_code")
    val serviceCode: ServiceCode,
    @SerializedName("from_coop_account_no")
    val coopAccountNo: String?
) {

}

data class BillPaymentResponse(
    @SerializedName("account_type")
    val accountType: AccountType,
    @SerializedName("account_desc")
    val accountDesc: String,
    @SerializedName("coop_account_no")
    val coopAccountNo: String,
    @SerializedName("account_name")
    val accountName: String,
    @SerializedName("tax_id")
    val taxId: String,
    @SerializedName("ref_no1")
    val refNo1: String,
    @SerializedName("ref_no2")
    val refNo2: String?,
    @SerializedName("qr_code_data")
    val qrCodeData: String?,
    @SerializedName("bar_code_data")
    val barCodeData: String?
)

data class BillPaymentData(
    val data: String,
    val dataType: BillerDataType,
    val response: BillPaymentResponse
)

enum class BillerDataType {
    QR, BARCODE
}
