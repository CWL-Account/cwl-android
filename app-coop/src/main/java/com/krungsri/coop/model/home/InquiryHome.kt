package com.krungsri.coop.model.home

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.krungsri.coop.model.account.AccountDesc
import com.krungsri.coop.model.enumeration.ProductCode
import com.krungsri.coop.model.enumeration.ServiceCode
import kotlinx.parcelize.Parcelize

@Parcelize
data class InquiryHomeResponse(
    @SerializedName("product_code")
    val productCode: ProductCode,
    @SerializedName("product_name")
    val productName: String,
    @SerializedName("services")
    val services: MutableList<CoopService>,
    @SerializedName("accounts")
    val accounts: MutableList<AccountDesc>
) : Parcelable


@Parcelize
data class CoopService(
    @SerializedName("fee_amount")
    val feeAmount: Int,
    @SerializedName("icon_url")
    val iconUrl: String,
    @SerializedName("limit")
    val limit: Int,
    @SerializedName("service_code")
    val serviceCode: ServiceCode?,
    @SerializedName("service_name")
    val serviceName: String
) : Parcelable